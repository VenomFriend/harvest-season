# README #

This is gonna be an attempt to make a Harvest Moon-like game with multiplayer. The goal for the first "playable" version will be:

* Crude drop-in/drop-out multiplayer, all synced obviously
* At least one crop fully working
* Hability to dig/plant/water
* Rudimentar Inventory

And a list of goals, probably gonna add more things later:

* Character and map save(server side, probably)
* Better inventory system, upgradeable
* Being able to sell stuff
* Seasons/days
* Several crops
* Hability to mine
* Hability to chop trees
* Hability to break rocks
* A store to buy stuff
* Upgradeable tools
* Rain
* House
* Option to have one farm per player or just one farm
* Option to have global money or individual money for each player
* Animals
* (Maybe) NPCs
