{
    "id": "aa18fe2c-fd46-4342-9e79-b995d60b0111",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3488f07e-2aee-4144-be33-335d4098ed0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 134
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "12c8cc62-20e8-49d7-a1a9-06dade8a1720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 136,
                "y": 167
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fd7b9494-a747-430b-a955-b6f671c3507f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 154,
                "y": 167
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "eed27e0f-d5b4-4bfb-879f-853050307975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 140,
                "y": 68
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8a2cd512-f501-457d-8a10-f01ae6cea47d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 107,
                "y": 68
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7da6ee26-b816-4ac2-bc1e-8b3ac84216d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "37baf3de-e135-4568-97b4-abc67bafc4d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 121,
                "y": 35
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "28760242-90a4-47ac-8b52-913f74ba2caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 177,
                "y": 167
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bdf72af2-9d43-42e9-afb5-fb8f02efffaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 203,
                "y": 134
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "910bb81a-c634-4295-aa4b-d3811a982035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 134
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "08a0e6f1-1f7d-4d20-987c-9cfbd2ac21a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 94,
                "y": 167
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d8ff1942-5ca0-4242-9acb-412df85f1701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 77,
                "y": 134
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "935df8f9-11a9-460c-9407-8396a9dc4a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 112,
                "y": 167
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "10e7c2d7-e7c4-4731-a1e8-482438cd34a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 167
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4b21cfeb-0610-4da7-b62b-ca8135ebddd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 148,
                "y": 167
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d9ad80cd-46ab-4d19-9388-aea0fe0857d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 167
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "452bacde-d60f-44a5-bc47-78b5eba8ad24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "791565b7-72a3-4d7c-aad2-ed3af068cfdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "245f8c3d-1716-4c09-8fac-eac5998f3e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2bdae209-ae15-4db7-b32c-e640a2b916de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 134
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "93033591-0fa7-4014-820d-5baa07f0aa1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4bab9cfa-610d-49a1-8775-f54ba5d1ec4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 134
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f7fb4af2-120f-4ca6-bb5e-66c4f3ec344e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d59e1ac8-6429-4f88-885d-0c88fbf6a988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 142,
                "y": 101
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ca4394f4-e9a5-4dcf-9ff5-e39a755ba178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a02e9da7-b6e7-4441-befa-245601b648ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0c8c9d99-b1a1-4546-8988-dd1ac20c0499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 142,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c6216350-75f5-46bb-a9e0-24c66d066cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 106,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fd704d65-5703-4bae-adc5-c293d54610de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 93,
                "y": 134
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1c0f11e3-0064-4173-84b5-889b790e3c85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 139,
                "y": 134
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dba74fe7-7e2a-45e6-932f-5ca168c43315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 109,
                "y": 134
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1f9f7c69-89c5-46a2-b100-fc5c902596f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 172,
                "y": 101
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "acf85960-6a5a-47c4-ba5c-a7ac68f5b13a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "500cc72c-bef7-41cc-91ef-3c347c502672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6d649397-492c-419f-8163-bea94c537b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "71f84d77-c7e4-466b-8c3c-32299636d974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d2bfe1c2-70c2-48b0-b288-f98e874efd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 140,
                "y": 35
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "27f666f2-e925-4691-8ae4-8d90eb2aa6a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 174,
                "y": 68
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0c880067-8fe1-4755-bf17-9e04f6b16939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 66,
                "y": 101
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dabdb7bf-54a2-4550-a3b8-051d1c1a3104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "06aff8ee-43bb-43cd-983f-04d35917ae0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 225,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c764ea69-6a0b-46ef-bad0-8b166f065085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 124,
                "y": 167
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f199df5a-10ab-40ea-9611-567645f33141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 125,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "43586864-967d-44ff-893e-aa157b79c26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 102,
                "y": 35
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cb1392bb-a587-4872-a7fc-46f7776d50b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 101
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9099368f-435b-40cd-a71a-d3fcfcbbbb5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7b2cd9e3-1c80-455c-9763-f1fce47c2afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 56,
                "y": 68
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7b29374c-ba6b-400f-b6be-4202840188b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "568a4339-0401-4399-ad3c-acd64a921d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 123,
                "y": 68
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7c228617-d243-4260-a506-1ad610acbe56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "737f8dce-fa4b-4877-b0e2-be90394b4f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "113ae145-ab33-4223-9708-51629d19a341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 68
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cf356d66-e791-479b-99d0-e95349779138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c8743a40-ee93-48b7-93db-8e2487b58322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 207,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7c28c226-dc74-43d4-8da8-e89271808d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 82,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "38a71e0e-b782-4a3f-a21e-cf5b7e5b1c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e82cb7f3-546c-45b6-bbf6-52abbadb5549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 62,
                "y": 35
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "17d098fb-aa3c-4dce-a3cd-98be81247cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 42,
                "y": 35
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "de21d076-4cf8-474e-a6c3-72eb3e0f0b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 74,
                "y": 68
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "40469dd3-dc6e-4cc9-80d8-5d3fd97e8a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 13,
                "y": 167
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1460f187-3f3e-47a5-8a35-65c064460f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 167
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0154998d-5ca7-40b8-b30c-0da2b08e5b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 75,
                "y": 167
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "94a3abb1-64af-48cc-a246-dcd86fb713da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 61,
                "y": 167
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f1d5b15c-4006-4de3-a2e7-d7c7bcf9744f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2325c14d-55bd-4e39-bbcc-1c132392de0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 169,
                "y": 167
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "42dfb49f-9ecf-4049-9dea-21cbc7e5960c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 191,
                "y": 68
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dc3287ec-aa82-4b1a-947a-ebd6564aba60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 127,
                "y": 101
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c14f006f-e550-425a-8455-deaf93ffa8f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 112,
                "y": 101
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "26d7f384-3a8d-4642-8163-290f1c33cf22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 101
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "90288327-4495-471b-9905-b947c1961057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 207,
                "y": 68
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8c65e808-6df9-410e-a474-0738489a4c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 223,
                "y": 134
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c75385eb-9aeb-4d0b-ac4d-3a41ada204d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 175,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bc6daf0e-0023-42ab-bbf8-376cb7cb38d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 82,
                "y": 101
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5d632bcd-b4b6-4914-b118-cafb1e2a8681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 130,
                "y": 167
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cef41458-6881-49be-855c-b0c0950d9863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 22,
                "y": 167
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f8608873-3685-4615-8cac-9780130cbe69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 157,
                "y": 101
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "909a7eec-6053-4fc5-ae8d-b4c5853a6887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 118,
                "y": 167
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1ffe4624-b57c-4482-9c49-3d0f23a8d340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3477fa2d-09da-42b6-aa43-cdb881c3d66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 232,
                "y": 101
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "138b0c1a-2a4f-4ec3-bdd1-2d561fb2d9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 157,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "eea972b7-3430-4f75-8ad1-6a5641657b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2d459a33-81c5-4407-8eb1-b83d0edde5f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 159,
                "y": 35
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2ab2a72b-6575-44d5-96db-45a1719bd5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 234,
                "y": 134
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2f0aa62b-6636-40d2-b858-7941c0af0e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 202,
                "y": 101
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9eb66db7-c6c7-40a6-8b85-cfd8cc12b8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 41,
                "y": 167
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8f23e79b-d859-4843-8ca2-8669a0730bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 187,
                "y": 101
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2ef83545-156c-4ead-877c-1d2610dfe869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 239,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "13bc40e1-5d0d-4a18-b4c5-aaf0b72496ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f40052a5-f8de-4064-8ba2-77b110379b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 223,
                "y": 68
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "46f3dcff-f8e1-4867-821e-744bd5c037b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 191,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "83a356e5-66bc-42ae-a29f-89928983d838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 97,
                "y": 101
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b7aab3af-5702-4d64-82d8-65d0f83ad4cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 155,
                "y": 134
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9f4dba0b-def7-44ef-b84b-c7d21dd376e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 164,
                "y": 167
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cd77d9ab-4f74-46af-b8a1-5cc4f2034820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 166,
                "y": 134
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "366ee48b-18d0-492f-b0a7-9ebb80311073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 177,
                "y": 134
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "9c55f0b7-06df-4666-9354-fb99991afed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "cac62811-98c0-400b-9e06-5a1d39a36115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "45ea120e-d658-499f-81a2-75311b3dc747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "9c073d3a-e80d-4d0b-95a2-b43a11c65b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "a4e5a068-f672-4e4f-ab35-e25a3394afff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "d70b103c-98ff-4e39-af52-6aeef7b40ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "1595036c-770d-4647-ad29-0e7bc0f2707e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "5b3f066d-6a5d-4c76-8741-291e15d13167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "b395db42-140c-452c-a184-94ea4363eeb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "a5916b45-5d73-4d99-a10c-c4aff74441f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "4c6740b3-3383-4aea-a943-623fec7f45d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "f206292b-f4eb-4aa0-b567-63d6e4287dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "8e30a939-58c4-472d-a247-14bc006fee83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "7c3da573-0cae-4d2d-9b8d-8c1466b72177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "f8a200c4-13a3-4512-ad3a-641ec4bf26b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "eae51d9c-a45b-4df2-88df-2f2237477022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "a80c0b62-42d7-4368-b9fc-01d007fb1521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "ace14e88-e54a-4d3e-bb0d-12df47be5546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "5563b4b5-7fda-4cef-8a74-b8cd30467aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "0d1cc00c-ad14-4b0e-908a-b71a916768fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "0dcfc0f6-4639-4d84-8076-570b2fab1a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "d0776801-dc16-4b10-9ee8-b0995702b9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "f94bc381-525f-4b73-9bac-e0755b0809a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "e765e6d9-666a-4d9b-9c4f-64a930619cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "0afde1a4-09de-4f0f-bc3c-ceaae5bcf9eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "90ae7888-b814-4ff0-9830-d086bcb8c8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "609510a5-91be-46f1-900b-7368f6612380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "c38f14f8-97bd-4a7f-a95b-0ea848310b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "d7ea1ce2-d1d2-43c3-864c-9ef31a4be48c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "e9b7bb7b-c6e3-4330-bab7-c1a8860e1cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "a36d3f1a-4f39-4947-92fd-07117d037e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "4b8a6b7d-8b67-418d-ab17-7bb5b54d1876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "bb9ed8c2-3d2f-40af-86db-85de7f8d7a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "df541a02-f31c-4e48-a30b-c9184a5a19b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "d09e7a54-f2a5-4ce6-8975-1f5dcc221448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "1f190691-a970-408b-865d-dfc1854ea1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "d1f2a78f-3f36-4ad8-9afa-50cd11ac8a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "8f5a1772-e421-4737-ac0d-5314faab87f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "6cf42fb7-9f2e-44c2-9f4a-b794e10e4d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "33ef7283-bc90-4945-a6ab-cdea30bc342f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "c9fd1745-2736-43b7-990f-024f33a41580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "33cb1f25-d36d-47f0-b516-d6cde44a746c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "22b4d784-9057-4a13-b098-140c3e085288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "57d8e714-cd90-40d7-8934-7ffe0846679d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "40fe696c-d599-44c4-81b7-f1d2d1821d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "ee9cc703-3ebe-485c-9bce-573f422e791f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "960b80a7-1ca8-495e-869f-8a634b01fd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "68bf08d0-29b2-41a5-95c8-dbaf1b73c11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "7708554a-cdb7-45eb-bf45-323dab441b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "7fff8baa-aab6-4dcc-b076-b7b1528ac743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "fc97dc17-cdfb-4b0e-a111-d442874c2e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "3e26c057-b9da-4c4a-9b53-6ca34403ddc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "72676a79-65a7-4ab6-9b23-87898b32ee87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "98e267ac-de5b-47ac-bd87-b4bd3fccae68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "727e7624-5736-48ae-9a79-5e7c1e371493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "576b500f-0921-484c-a00b-1502c290698b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "c4db1b2b-390f-422b-81a4-7565863e2b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "ed5ff0fa-dab6-4388-aec1-ab9fa6380cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "32e088bf-f2d2-4e58-8d34-37695784cfa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "d08f6c09-8b53-4b47-9dab-2c8173153dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "bc32a94a-00d3-439f-b35e-16b80f527f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "13b14ffb-889c-4cd6-94d2-9cf7f96606e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "c08e6e58-9143-42c8-974b-f011551b80e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "72dc330d-0616-4aca-97d3-14d1aefd2016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "a8dd0169-4678-41c3-a134-48d1454136f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "dc78bfd0-0b53-4c95-9a88-7f492fe9af07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "a452b25c-13b9-40dc-a8bd-6087fa138871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "71fb4e0c-f947-42aa-ac52-ca452078a97f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "e6db3d9f-b302-4165-8584-29d8baf3cc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "54c3dd5f-54c9-4939-bb39-c2e657acda2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "0efb3625-c226-4e37-bb6a-54c94404c8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "b35b1c81-bd74-4e2e-9cba-2fd59239e974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "71851c60-df05-4e4d-905e-93d145ecef93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "44d9ad67-6e6a-477d-a132-1b50f6cf9486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "15db788e-25f3-46cf-8605-31492aa332aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "8e37e404-ccd2-4b27-83ae-265236d02ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "7d9fd9de-29f6-40a2-9e09-9eb8d7e87ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "41fd5720-5f0d-4a76-8563-e24381662861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "e2dffe3f-5dd9-4eef-8788-999898a19909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "cd78e468-0bf8-49ca-b7e4-dcad4ea0b5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "e22f8ef2-7305-44c7-891f-92dde80d6d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "7628091b-afb3-467b-8adc-a62c60ff0ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "e282232c-f21f-4612-9b83-e280a602ba43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "9d7c56a3-93e1-4ff0-a648-11408f5f02e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "c237d447-fe46-4722-ae12-87eead6afbb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "36832bbe-25cb-4ed5-871b-5779a51cbf94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "36e30ce8-fcdf-488c-9542-77048e87f0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "adb9ce12-b03b-453b-9eb1-66f52574f1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}