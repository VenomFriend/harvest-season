{
    "id": "7a6dc788-a870-45ba-8080-1a28c7577477",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontMoney",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bookman Old Style",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4f89a75e-f968-4075-972a-a0c096cc1c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 13,
                "y": 110
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6441da32-30b3-4da0-be05-1f4b9e189c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 194,
                "y": 110
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0989e1e5-4695-4a61-8673-f44cf1189356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 178,
                "y": 110
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "031d8921-2850-42de-9694-c67ff13dacb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 182,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "462fcd35-dc4b-477a-95e5-b56aa1ba287f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 99,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a5bd98e0-4cb1-46fc-bddd-4c0bda8b033e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "74b45624-61c0-4515-b8b8-aa62021c37be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f07890b7-f270-45a1-8356-ef7aafbd895f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 208,
                "y": 110
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ed5c2a84-2bdf-4b28-97cf-2a8625c6efab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 143,
                "y": 110
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "bb542825-5d78-4da1-9ad6-2e714c27b8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 149,
                "y": 110
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "667f1506-2cf3-40e0-b89e-0d42fbf59d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 110
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "17d6d457-de84-4a7f-9a1b-317d15c93bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 235,
                "y": 83
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "aea3a20f-782f-4989-90d0-155aa3b1aff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 173,
                "y": 110
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4258b11a-16c9-4d43-abcc-cfb8391b3d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 161,
                "y": 110
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "aed75615-bad6-4c0e-bfc6-ea30f4dd0862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 198,
                "y": 110
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8df1b32e-4736-4120-8000-f1c54cdbfeda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 75,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5d80fa3f-bebf-4bc0-8740-4c62cdfa5adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 42,
                "y": 83
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "878b7c3c-a573-47e2-9221-8ceca1222843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 32,
                "y": 110
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "68223d3b-2dc4-4343-b6df-620c947bfd84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 120,
                "y": 83
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1f1587c3-c5d9-44ac-8d44-8be2c2ec71f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 107,
                "y": 83
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6ddeea0d-9615-4c17-adf7-4bda3c202185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 168,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6cc0e92e-8387-4ba6-9f7d-5fadc6e90bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 16,
                "y": 83
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "06bc16b9-9761-41c8-94b7-80130bb6b5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 29,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0ba6676e-8f39-4cb9-ae37-8c4cf09816c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 94,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2e4d0e45-3a3d-448b-9b23-1e708ba2ea0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 55,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "eb5ab272-b340-4b42-8a21-c86e719fd376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 68,
                "y": 83
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bd4c58c5-8bfc-4484-8905-7746bef5b2fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 190,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d76c7898-4d4c-449c-aaa3-2840093bc089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 168,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "478cfb75-e25a-437e-9855-942b3a40bcc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 223,
                "y": 83
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8e43bf20-820d-47a5-8e07-bb1a0b95585a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 63,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "052e9c86-3e23-479c-98b6-5b6c41f3c195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 211,
                "y": 83
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f5e14ef5-5129-4496-ae51-269df950210d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 200,
                "y": 83
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b3bcecb7-b0e4-4b3c-9f63-380563a380ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "980ed425-1612-491f-bd18-752f4464f2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 87,
                "y": 29
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "afa3083b-169f-4074-8207-5a08ccaa21ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 196,
                "y": 29
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "79cb6d93-e1be-41d8-8568-a7b102522089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 29
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4d8c67fa-2cd4-4431-8b96-fa2d342ff2af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 29
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7f871f51-8d8b-41ae-9532-1f264a1c919b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 47,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a75ddd3f-9ea4-4aa5-ba93-3b436149f95d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 140,
                "y": 56
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "230a5d66-3109-48e5-9bbd-b7c53576bf65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 29
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5cfc94ec-8d00-420f-9c80-6b343336fd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 29
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "25fc36c8-a5c0-4e81-b178-e82da5a048c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 103,
                "y": 110
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4d1014ac-96c2-4d8c-b786-eb058ab22726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 196,
                "y": 56
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4f1fb90a-d337-4f2c-87a1-2c4c22c2defb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4eb8ad3b-990d-4caf-9cee-140aa3f2d54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7d2f0636-f88c-42c0-9fca-7854907e51ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "318f219d-c97f-403d-b609-b9c1fe7e13a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "04033dbb-efaf-4214-b5a7-2c9b519a6f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 29
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "53726404-27e6-4194-8a3f-a132f0ddcda3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2924b257-aae5-4fb4-9c7e-bba136b01068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "71012bbe-3774-4551-8ac9-16051ed60c78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 164,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4b056497-3900-409d-b79d-7e0658f0d7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 238,
                "y": 56
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8c5cc34e-0abe-4778-a67d-83d893827921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 148,
                "y": 29
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "94864aa9-48ba-43fb-84fd-f7cbb9889ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "caa1a9c4-c12d-421e-b33d-f17518537d58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "df56574e-9e77-4fb0-b2c7-b565a1cddcda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a169d23a-e5c5-419b-ac65-c8e90c4bdc52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a7789a7c-975f-4fee-9b33-6c5b05345f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 132,
                "y": 29
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "510c4a85-65b0-46b5-8d22-a6c1a259ee91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "741f5e28-d917-48da-96c9-d1b0a223af97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 3,
                "shift": 6,
                "w": 4,
                "x": 155,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7b60fb4c-90fe-4c77-b6b7-7a0757a93320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 87,
                "y": 56
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8c9b6b4c-a1e3-498a-a4d5-3dbc1ec79503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 137,
                "y": 110
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "755292a7-1d48-46c1-9921-a290b9ac0bf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 42,
                "y": 110
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8e932e48-be2e-4cf6-a404-290ecc87c6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 62,
                "y": 56
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "12eda056-5d7a-4cb4-8c49-0d3c81b2f836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 202,
                "y": 110
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "85d4f1ba-20bc-4c3c-aa89-6b30507054a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 81,
                "y": 83
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9b0b33ea-8762-4a84-ade2-18e9f108aded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": -1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b86e41aa-4a2c-4bab-8ae4-8d370d417a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "62cce6bf-e084-4d16-adc8-81f6ee3170ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 126,
                "y": 56
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b420bdbf-3166-48cb-9332-692d46c997a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 167,
                "y": 83
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "376aa151-f51d-4398-a92a-bdd4e9dad541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 9,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7740d5aa-b3a1-44e0-a38d-f28b48ab9318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 212,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "40b890ba-b7fc-44f0-8173-c31d40e5b43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 240,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0288afe1-01dd-46ec-a94b-9ca4fe9857a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 110
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "28a08207-1d2d-4378-9050-22eac2fc4dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -2,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 110
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e6299341-56de-4fab-a01b-342ec43f4516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d08e8d24-1f6b-4423-8966-a230660d9873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 111,
                "y": 110
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e1dfbcb3-c2ef-4441-be13-1ddd091faa0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d7d4d17f-e06f-409a-bea0-e9af2f04f5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 32,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "607be74b-37d2-422f-84d3-edb6f25ae6eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 132,
                "y": 83
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "52b1f771-2506-4f0e-9a64-39911388d57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 118,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b00b054c-17f1-4e43-a10d-8abce3587a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 104,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "431a9330-03af-4932-99c8-9854a16c5ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 83
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e17ed4d1-54b5-46ec-bb7f-7af07c4631da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 83
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ee931bd5-a6df-44d5-bc1d-a3ccdc9520c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 110
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9f205330-9fd7-4909-add5-411161884c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "de644ddd-8ece-4549-9500-111255e7d782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": -2,
                "shift": 11,
                "w": 13,
                "x": 225,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c52f16b5-349e-49cd-b588-aa60e9c3abb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a8ac9dba-0e6f-4749-8342-3c66e0acab55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 224,
                "y": 56
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a1609843-1bb7-415f-9df6-53739afe2530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7690ea27-ddf3-4534-b088-da14f28f6195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 144,
                "y": 83
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bc9a3d21-ed45-47fa-b6fd-689c4dfbae99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 55,
                "y": 110
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2a9fa4a9-e7e2-4ad3-b2e1-cdbc075c45be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 5,
                "shift": 13,
                "w": 2,
                "x": 186,
                "y": 110
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "46749e8a-7f45-428a-891f-f01185024e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 110
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "20018a2c-e0e6-4c94-b463-f7ff67ba9747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 75,
                "y": 110
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}