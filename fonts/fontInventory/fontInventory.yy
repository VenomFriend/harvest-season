{
    "id": "1c86f7e4-8f4d-4d30-b81e-d4c417dce6d8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontInventory",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3df70eb4-be8e-44cd-bb75-c9d3fb0c2ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3192ec7c-d149-41bb-979e-90d3352423d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 11,
                "y": 77
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4c64f358-c5d7-4606-be2a-8950376c6a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 5,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "673e82f1-437d-4ca2-8852-2bb11073528f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 32
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "94ff6db8-cbe9-4f0a-9944-ace017a43ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 25,
                "y": 32
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a232f321-bbed-4769-b145-4beb8146f0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6dcbd0eb-1a4c-4031-a169-b6c24cb6d11a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 17
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4abe49ab-afbd-4201-97c7-4cb399b78557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 5,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 33,
                "y": 77
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5408fe2f-2c19-4326-b6a1-8f63c0047e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b428cc0a-a420-43ac-920f-0d0bd756cc53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f4553bac-86d0-4ab7-84c4-7499393d4f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 6,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ee568078-a7d9-4a4b-a4aa-7b447fb81bff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1767c284-f12f-4c1d-a754-888944a15c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 8,
                "y": 77
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2d253cf2-b946-4ab1-945f-8945323c48e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 103,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "836acc80-1265-4448-b1ec-0390a104cabc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 26,
                "y": 77
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e2ce875c-de33-4668-a5e0-c2851598eb4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2866e496-d558-4cee-bfd6-dcda77968b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2b12d73c-83b0-4fc1-8151-1727b9fd3de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 3,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "59c27d9e-d987-4870-aea3-32aeae672111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a024a9e4-584d-4a05-be73-5b9b567c4ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3ce1ab45-689a-45d5-8808-6aab99655cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "78cfd5f5-2148-4d9a-b27b-11e52969acbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "24bf6118-e524-49de-bd58-e4c92387f69f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "49829dea-8ab6-4ad2-8d49-38971b7511cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a0b6ad3c-dfb7-4c3d-b41e-a182dbf8f334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9e11a3d2-053c-4196-a38b-f83104b33191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "10e68a51-da65-4e95-91a8-dd7464738d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 14,
                "y": 77
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bc6d8dc7-01a4-40d5-93fd-aa2a7c7ba345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bb6d682c-63fa-4b5b-8e68-13e8cbda35aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 47
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c422bb7e-1b8d-4932-9fff-0568477e1c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "333fef89-3334-4047-83ed-be4a60b9ce86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c74d632c-1a2b-4abc-a008-7787c6dd06cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 47
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "14464ff6-aa3f-4946-b75a-f19bab0cc106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3b6855b7-1de8-4207-ae6b-8b9cdcc28c01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a4c7b138-10ab-4cd3-b957-85a31b3dc42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 93,
                "y": 17
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8e4e7033-07e4-4fa3-a01a-f1d92d9ac9dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 17
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e1033e6e-c588-49f2-8206-2e17ae249f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 17
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b52c4e75-36d5-475d-b52b-b0b140bccae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 47
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "67e01085-5c8e-40cd-801f-8d415242cc80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 47
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "620924a5-52d1-41f8-8f91-6874bf00de7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 17
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "09210438-bb71-4142-9d70-d6c7f823ee16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 38,
                "y": 17
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fb0fbdba-35b9-4c8e-bf4e-021bcf712e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 20,
                "y": 77
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2f9f74cb-1343-4431-bb0b-6c55d25a39ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 102,
                "y": 32
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fb6f0c66-594b-4a4b-93a4-41e2c39e165a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7cf80db8-a410-4e23-bdec-c9f98a01b497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 32
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "97149c05-52e4-4d70-ba91-880ca92120d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ed2040e5-d517-45c7-81eb-b3eeffa72365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "37679d36-d333-42fa-8cd8-8a92c4a72363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 17
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5f982fa4-84f6-4971-b6c7-a6af329a9323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b0b5fab0-2e1a-4f16-91a7-258d9f2b9210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e9191b53-68ad-464e-89d8-8851efed17e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3756ac84-cd56-458b-8ff9-71f32d0ecfd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3300e175-eabd-453e-ab2e-c453179f7b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 46,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "435d3970-d20c-4c7d-a15d-80948805a4cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e1121b73-798c-4893-ae29-9d2b64ef967d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "84e8adf8-8e84-479c-a0aa-5d436f64f387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0ca53458-7db1-428c-92f6-6fa5548b789d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 32
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "28b54e43-03be-485d-bb68-06ba12cc5487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d5b6527a-b944-425d-86f6-be7e433c0ccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 17
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a45d0d38-6801-4e76-87b8-d520d3fb60e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 112,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "27861b40-a3f6-4d1f-8e8d-10dca577c062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 10,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 94,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4d84fb46-3370-473e-bdb1-cd6cbe70a809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 108,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d0fe344e-e801-49fa-b9a6-f5666f3db70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 6,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a7c343e1-cef5-4df1-8578-5bc8d18e5b34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4b5a2c1b-5978-4344-9cc1-18a9fcb218ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 4,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 29,
                "y": 77
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "993185a2-a6a7-4ece-9e53-0def0dc394d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 47
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "99cb8cb8-ae89-4cd4-b364-ec9872b31a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2db967af-b18c-4032-a351-74a4978e6a48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 32
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f7423e8e-8adf-4f3b-baa9-b48433da163b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 74,
                "y": 32
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "972229eb-b81e-476e-a2b6-73bf324fc9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 67,
                "y": 32
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5347ac06-7623-400a-93c5-4a568832b833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3b03a82f-2dbb-4747-90f8-3915204a06fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "516fd12d-44c1-4892-85c8-15c8955644d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 47
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dab3a500-a0d8-440a-ad9a-6d601ca53c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 23,
                "y": 77
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "755211c5-27d6-41b8-97a9-877f39d4e7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 2,
                "w": 2,
                "x": 99,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3944e94b-85c9-4d2b-a4ea-2c3ec776f0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "70cde65d-9bf8-4a4d-964d-88dff36114b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 17,
                "y": 77
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b7c426f4-bd02-44af-bed7-50b726807cbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 17
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dcd37d75-2212-4d2d-b941-c7dbea12f6c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c8f97b69-2e1a-4d7d-bafb-327fa47afff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1aed249a-a5f5-420f-a473-48358d7ced35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 17
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "515ba10c-8ffd-430c-84ef-e722eb0c83c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 18,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "908a704b-1726-46fe-9076-71ba4fe8bb7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "76d4d9ff-98da-4270-9eee-dc44e8ca60b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "94663b2b-cf2a-4776-b0ff-07408b609e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 67,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "df470e18-c737-4677-a763-63dc6ec676e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 47
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e007b589-227f-408f-9cf1-426d0b3d872f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 62,
                "y": 17
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a8da28d0-974c-4044-9e26-dc63924c9a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "912705b5-a551-42d8-b57a-3392d590e841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 32,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fc0fdfed-f1aa-4981-92df-953eb17d338f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d3779e19-846f-4b36-b357-ab3b00608b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 60,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5b0b71e1-4a8c-4755-90d1-cfeef13c34d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2fd6f7f0-7beb-4719-a5ea-0194184f4514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 5,
                "y": 77
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1682789a-4644-412b-81f5-2b3b30d359e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2ee21a31-363d-459b-adec-897c9977199b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 28,
                "y": 62
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2cac4ce9-8259-41d7-8d2c-3726c36fff28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "e1829b6a-47fd-498f-911b-3fc5c7383f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "18bddb5e-bae7-49bc-97cb-40e043f44f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "bbc8b0e4-eec9-430c-9af9-5ae0601b84e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "16c496f6-dd4a-4919-b3e4-49f304607716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "586d337a-a0c6-48cc-a78d-50e82d9ae9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "06d6e9f9-e5ac-4dd3-bcaf-74219c5a317f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "d1c986d4-4a62-473f-b9a9-85904b51c331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "cbfcecdb-2af6-41ef-b88a-336cd11527d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f31d3b47-7446-4d2f-864f-a67cc2d5c230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "b6e85140-a16c-4e18-95c2-2f23fa70a1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "04e590cb-2ae8-4166-b503-227bc8812aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "1154af12-8641-4f4a-b901-160aaf7d3cb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "9b4f7a28-ac1b-4326-9e68-664cc4f9b0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "be972ac9-fed6-476d-b879-42d3faa2ff1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "23ce2b7a-a49a-45ab-a8ae-fd515c125cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}