{
    "id": "630c4284-b96f-4887-b498-37f92ea8b7fc",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontChat",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8bb6a62c-6ff5-4e6a-893c-2dad66699586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 80
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "627e2aee-5e63-4961-af25-39f6c62f8791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 52,
                "y": 106
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "dd2d3e61-ce8c-4171-9498-6fafb7bfc3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 32,
                "y": 106
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "335d3aa6-c1e6-4a7d-919e-940a877d346e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 86,
                "y": 28
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f8d90a70-0489-4552-8757-dae3e592e688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 219,
                "y": 54
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bd9eb7d4-b702-4e4a-8333-1f572399148b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8cf3ee9e-326d-41fd-a38f-d063effb01c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 30,
                "y": 28
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "221d3770-1e13-4bfb-b9e6-276ee6ec3f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 81,
                "y": 106
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fb2c1e51-15e2-4bac-9eea-b927ac3b3363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 201,
                "y": 80
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4549c6ca-769b-44a2-925f-09971a044649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 208,
                "y": 80
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5b4b5746-9423-4252-94f1-bc8b2f31c4d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 106
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "db35140f-be5c-4de1-bb27-74e957bc4d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 114,
                "y": 80
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d866756d-ed42-4832-bb45-da3511bac43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 48,
                "y": 106
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c9864976-97d2-4424-94f7-a8abff8925dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 14,
                "y": 106
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "68f71404-8138-453e-8291-abc5b2ac4d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 72,
                "y": 106
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d800d5aa-fdf8-40c2-9c67-e8effb6b9a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 192,
                "y": 80
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3dab0094-5153-41b4-8205-8d4b4aa7387c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 99,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fb927fef-203a-43bb-b0e4-3f6355338187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 223,
                "y": 80
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "49b004f7-74d2-4582-ba37-cc572c1695c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9c3b3aad-af0d-40ab-8a08-637b2d63d4ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 111,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8e4a55a1-f0fa-411e-a100-a3292aa65a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 123,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4444b099-02a0-4e11-9efc-a0f1f0784429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 135,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8d1ca760-c956-4854-a00b-9032002ad805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 171,
                "y": 54
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "732b7c6e-5fb9-48d4-aa06-5ec442172aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 147,
                "y": 54
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4e6bef52-f04a-4952-9650-ed75849daf0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 159,
                "y": 54
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "36d7b2f9-6ce3-4e53-9885-fc997d9383e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 207,
                "y": 54
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d715d56e-d2df-46cf-bab4-9a1f297ed483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 56,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "557224b7-cd3e-4709-9b8c-ddb9adcdbf9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 44,
                "y": 106
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a976c07d-8e38-4609-a571-040d053a599e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0e570fea-5192-44df-addc-3a20471037a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 126,
                "y": 80
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "552c1659-ad8a-45f5-b106-1edc34d287d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 90,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "45175a9f-6c23-4900-a39f-fcfcd158e932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 183,
                "y": 54
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5465ef06-3a39-4026-9900-7ff5fdff824d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e41b3e5c-9be3-47f5-937a-d79fd0cb7ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c5ded51d-2061-4b1a-b06a-94e14de8dada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4adf64bd-9ca2-45b6-ae85-3c8ff814cd01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 45,
                "y": 28
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cdda0042-c635-4e05-9b14-19984d809754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 212,
                "y": 28
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d3163ed4-36da-4592-b07f-c3711e22656f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 239,
                "y": 28
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "56601074-82de-4405-ab26-65a9c28e3052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 195,
                "y": 54
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a47cf843-c86d-47ba-b9db-a77de64d98f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2f64a896-476e-4d87-a6b6-be3ef255ef94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 198,
                "y": 28
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f48392d5-ee9d-4f5e-bf1e-f2c018afa0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 60,
                "y": 106
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f0e99d2e-ac23-439a-9323-ed2492579fed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c129b830-e4bf-40d6-b7bf-fffdb3c29e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 28
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dbb12ae8-3c83-49d5-ba8e-0ba334fe266c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 79,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a96d7efb-3e05-44a2-b4f2-81518147af4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5ac3b416-6ac8-4370-898b-11d2eaf9666b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "299c3e38-cc3f-40e1-8c9d-6c5f10e32700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dc9e9a22-3c7e-47ce-9568-670d368173cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 226,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9d098c18-7474-42a5-bea1-009465f5c963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1f7a44c8-192a-4c68-9842-b9d721f6ce6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 142,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "98505d3b-c352-407d-b7a9-f8cc9998769d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5fd9298d-2ad6-43d7-8424-2c88d4e03fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 114,
                "y": 28
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6cc91f72-ddc5-4dce-9576-7488d447bff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 28
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6fc49c2c-6513-4888-903c-90e63ac4d791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1c63943c-29b0-480d-bd64-1d4a5ee1ee7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3c2ca8c8-14d9-4dcb-a5e8-e1bca1f0a74d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "10d08b66-6774-4096-b990-fcd05b06f283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9b91b2c0-67d8-4fa1-abdf-62c2133967a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 28
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "02078229-5f84-4795-9db4-3012b3054962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 106
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e3eca8d9-88e1-43f0-928e-5c9ea1f39b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 174,
                "y": 80
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2f519279-1924-48fe-b7db-26a3f98d9905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fe808f6d-eb12-4eab-8bc6-8485a0de9ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 231,
                "y": 80
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8c2e1703-aa01-4da5-8c70-47a86ac223a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ff5b84dc-a121-423c-b2cf-f609c8b9ce45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 76,
                "y": 106
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ffab61ed-8752-4a4a-aaa7-1011c6f3c8f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 54
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9999dce7-0343-4075-b873-59e977ba6b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 241,
                "y": 54
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1e97fd44-53cc-480b-9f45-e9924a908202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "16a0cf46-5d0c-4b44-86cd-2954d4fbb231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 80
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "65b86605-b62b-43d8-9bd0-fe4e3c7d4034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 63,
                "y": 54
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5b2d9956-cf32-4d25-9a04-48a4041b6912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 183,
                "y": 80
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2df20f29-06ea-4f07-b66c-72394f373410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 60,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e12a130b-dc9c-41f1-9565-7b0db1463c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "48cf4418-f2d0-4ec0-8b91-58c84f5208a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 68,
                "y": 106
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "aa15d2fc-2e3d-4f75-9789-895e7394ac13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 248,
                "y": 80
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "48de5fa2-9612-454d-99e6-550991db6c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 80
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "67b8285f-0b5c-434b-8822-dbba4870806e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 64,
                "y": 106
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c00d3906-b7ff-43d5-b2d1-dfca1dc1e5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "73298e74-e573-451e-9ca1-38c762e422e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ff6a0039-3e2f-435c-9464-243ba85c5256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 75,
                "y": 54
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a7b0d38f-39a3-4cc0-95d3-e1e67562a6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 41,
                "y": 54
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8d31a4d5-5193-40e7-a546-afba57157c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 52,
                "y": 54
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "47f37677-25fc-461c-a376-83bbd1feccf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 215,
                "y": 80
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d70e54ae-1984-45bf-b682-8f10221cdaf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 230,
                "y": 54
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6d3c45eb-306c-46be-8b29-ea15bdb6f296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 241,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4583ca09-715b-4aa9-aafb-82a93baba8f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "af60037c-ea95-42ca-b37d-4c8da9a4d488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 15,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "66386fcc-523a-4d75-ab23-f65ffb358394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f5286aae-b5bf-4beb-a4a2-d096b5107088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 170,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cab4a838-cf5f-4f10-a7d8-6980aac8bb9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b2ecb51e-b5a4-4a52-970d-bea1c81efe58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 87,
                "y": 54
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8d64c5da-bace-4d89-9e2c-26df5ddf6efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 158,
                "y": 80
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "60dd3eb7-4315-453b-a7ca-c8ae5e77d07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 40,
                "y": 106
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8c917f37-0cd1-4aea-a1fc-d2ae637c7663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 166,
                "y": 80
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1d6ac05b-83a9-4429-9af5-10c71cf8cda1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 80
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "81515fb7-6a27-43bc-9640-2635c5de0adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "e11cf2be-0ad3-4588-bd03-0408afd2a35d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "e5095061-f7a1-4ba3-8c1e-86e3805f4132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "473ec0d7-dd0f-45e9-8d9b-bd1c3a43b026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "7e909c1f-e33e-4e12-8f20-45a3bb05d54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "430d6c2b-4dab-4b32-b1e3-18a71248ab09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "6eec6118-7f7b-470d-a082-6ba98f2a1abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "d7274ee8-fd2e-417b-88e1-b227a90dd549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "06fc71b2-a2d2-4cb0-8d43-50b08f056190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "a194287a-f3f2-438e-87b4-413662993ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ff7c6e3a-b47d-4482-a3eb-3e33c7ec2aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "b8333ab3-f3fe-4ecd-a138-8dbd8d6cdffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "baa82451-fc63-486b-9141-c4bd4f9e6ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "f04ba918-cb26-46bd-8d61-c5e2b8e9584d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "b7dcca98-5ffc-4b23-9634-1da4d00d957e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "f042bfe7-cdfe-4910-ac6e-7e8f7f3b99d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "0c58aae4-43b7-4969-b058-53efb27b18cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "1d328707-68e2-44aa-b91d-9a44e8e6a2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "594155fb-f016-4527-ab1e-bcaa50898d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "34afa1ab-f116-4f62-b75f-c32dd15532ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "39e8feee-3f1c-4c95-95eb-f63b05cc9b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "430d57aa-60c7-49ee-b1a3-bb12592d9d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "c33617b3-cee7-4b21-9bdb-052a75abd28d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "24cd0294-b51d-4890-bec1-8cbc57a521de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "214d78ce-7dc0-4d40-a76e-49dbec9d110a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "13046f69-b24c-4125-bf41-9efd5a117122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "b3e011da-ff19-4921-8a00-453c10f84113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "55ec40f4-980a-4242-9db2-8d89f70d036f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "8d54a00f-2da9-495a-ae25-0da5d078058d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "ff97f0d4-c826-4cba-8272-6d8e915e6a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "6e44ac9b-43b6-408a-8f60-af9d1e6d7d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "76941a18-9d8f-4630-a151-f370a9d1884b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "424df433-3794-4981-b728-05a37a234319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "55bb4822-224e-43ba-8408-87e4ad4e65a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "9899e0ab-7ed1-4bb3-befb-7117ac8ca9b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "85b33ef6-0837-470e-97b0-6934a06251a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "131e707e-6456-40dd-b9a5-36283c354697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "ea3c4905-bece-4e13-bb35-86b99beb8ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "b271b7a2-59cb-4338-a7c2-5c88f3c20b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "26534cbc-dcfe-4cd5-aa1b-415fd0c30627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "b23ebeae-0398-4a78-83e6-54b800a86fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "8fa9c921-4b3b-4f9d-be95-e33ee33bbea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "183b9d39-7a7f-4ebf-aa83-b9e3166ad41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "33838331-2cdb-44e1-ae6b-c10d682d46fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "69d1ca2d-a3ae-456c-a354-3671daeb036a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "c9844c7d-af51-4606-afa7-35400b156033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "68b1ff1b-df47-499a-a59a-e9811771a707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "27a96c79-4b62-497c-bba8-5444d8a5c109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "24508df7-c5fb-454c-bf6c-c8ecb1e69d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "59cb4e3b-21dd-4770-beda-7f7498cec753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "2ff72ef7-99e6-426c-be5f-348e89d8125d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "bf29fb39-3db0-4b7b-ab95-ec6feacea005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "6ff8e348-7e65-47c2-af99-67a1ae5b8804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "e0e3679a-22a7-4e9f-b436-e5f796711eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "052c6cb9-8d6f-4c5a-b4ae-518837172ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "1020443b-6da4-4443-bf7c-02f30abc990f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "a149ceb3-a5c0-4825-baf5-01ae404495bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "3f1f73f9-896a-4cac-a5ec-8b493d0b08e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "f8077fbc-af54-4c4a-9871-15ca12ccc638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "8d999cbb-b478-41c2-8f26-ea0609a1a56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "eb6a010b-625c-4532-a1da-11942418e18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "e8893ffb-16fa-4c5d-ae23-877f70797914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "c6b3052a-1a5a-4033-ad3d-731d9ea0d66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "63dca43f-248b-4804-bb83-780f2203b076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "8f9c3b26-7e9b-4e7a-913c-f4baf4040d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "e051cba0-b781-45d8-a0fd-4e3a85b3144b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "f583b3e4-a4fe-4aee-a3a2-8c7717fd2736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "bade3c81-18e3-4f27-bb7b-5a6c75f78f84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "55930d4a-1973-4d89-9eaf-0afd5a3f1511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "003d3eec-e68c-4237-bffa-6bab13fb1b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "f835f5df-22cd-4ee5-8a62-b7b75e62bf54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "e54c4439-f14b-4250-9809-733d5687d885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "49bace35-52ae-408b-97b7-09f5e4e23e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "62ad2a66-6778-4e25-bef6-4bd240515e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "c53f655e-ff9a-42aa-b54c-2703226c6460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "ca7efee6-3140-434b-a77e-0b2a9e86d23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "d5ef70b0-9b23-4ec3-a9ed-4e3b47d48dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "f89bdd56-5385-4091-be17-9c4295c0bc9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "b96224c0-b39a-447d-8002-0cd166520e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "dccdd119-8170-4342-998d-914ccbc50428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "721473cb-ae26-427f-9c72-e8b8760483ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "a83b524a-5cf6-4ff3-a5a0-12cc28149fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "fab67b60-cdbb-4378-a437-583a53798019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "f586b6a6-cadd-4ff8-b781-fa03bf26e59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "0af3ce60-7a11-4247-9fd8-f5eed1be4041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "d6576364-e002-4441-98d3-cbba3bb9162f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "ae44b7da-4bfb-4da2-8066-178eb2edfede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "0e356094-a40e-4e3a-88a9-32f50ff08ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}