{
    "id": "f074094e-a4f8-4fb5-820d-b59176e1b773",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objWood",
    "eventList": [
        {
            "id": "95b1c7ab-4d85-4bfd-afd9-839fe98225bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f074094e-a4f8-4fb5-820d-b59176e1b773"
        },
        {
            "id": "5c9adebf-c868-4020-8118-5a0edb302fd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f074094e-a4f8-4fb5-820d-b59176e1b773"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
    "visible": true
}