{
    "id": "d3ec6c06-f4cd-4760-a0b6-976e1d19b1ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objError",
    "eventList": [
        {
            "id": "2eb7a4d8-6c09-42da-8fee-9833c795390c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d3ec6c06-f4cd-4760-a0b6-976e1d19b1ec"
        },
        {
            "id": "7675fac4-f593-471c-80aa-7d902767ab1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3ec6c06-f4cd-4760-a0b6-976e1d19b1ec"
        },
        {
            "id": "5f600026-07f5-400c-9b51-cd3ff20c9cb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d3ec6c06-f4cd-4760-a0b6-976e1d19b1ec"
        },
        {
            "id": "9b7afae9-1634-4197-809c-7198543eba4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3ec6c06-f4cd-4760-a0b6-976e1d19b1ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0e480cf8-590a-4eba-bcf0-a20025eca91f",
    "visible": true
}