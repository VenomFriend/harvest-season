{
    "id": "d321a432-9b02-4385-a6ab-cd6116aa0fd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClient",
    "eventList": [
        {
            "id": "1749856f-6cfa-45dd-bfae-3d1c537aa9f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d321a432-9b02-4385-a6ab-cd6116aa0fd5"
        },
        {
            "id": "16d71b25-b04c-42d1-8eef-e87614cef3e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "d321a432-9b02-4385-a6ab-cd6116aa0fd5"
        },
        {
            "id": "45bced50-2125-4841-a4e6-2a141ed3bad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "d321a432-9b02-4385-a6ab-cd6116aa0fd5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}