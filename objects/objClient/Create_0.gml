/// @description Insert description here
// You can write your code in this editor

var type, ipAddress, port;
type = network_socket_tcp;
//ipAddress = "shbr.duckdns.org";
//global.ipAddress = "127.0.0.1";
//global.port = 8000;

global.socket = network_create_socket(type);
isConnected = network_connect(global.socket, global.ipAddress, global.port);

var size, typeBuffer, alignment;
size = 1024;
typeBuffer = buffer_fixed;
alignment = 1;
global.bufferClientWrite = buffer_create(size, typeBuffer, alignment);

// List of other players
global.players = ds_map_create();

// List of other stuff:
global.crops = ds_map_create();
global.trees = ds_map_create();
global.woods = ds_map_create();
global.rocks = ds_map_create();
global.brokenRocks = ds_map_create();