/// @description Insert description here
// You can write your code in this editor
var height = camera_get_view_height(view_camera[0]) - 40;
if(show) {
	for(var i = 2; i > -1; i--) {
		if(messages[i, 0] != "") {
			draw_set_font(fontChat);
			var text = messages[i, 0] + ": " + messages[i, 1];
			scriptTextOutline(text, 0, height - i* 20);
		}
	}
}