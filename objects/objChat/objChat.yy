{
    "id": "3d40f15b-4b52-40ca-a2e9-c7a2bf838b0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objChat",
    "eventList": [
        {
            "id": "35f5528b-f817-4a73-aff2-b0ccd964065b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d40f15b-4b52-40ca-a2e9-c7a2bf838b0b"
        },
        {
            "id": "4a7e6c64-c487-4a7d-bf1a-df27f86a7ff7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3d40f15b-4b52-40ca-a2e9-c7a2bf838b0b"
        },
        {
            "id": "d4c06d10-c7eb-42f0-989b-1178ed7c1982",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d40f15b-4b52-40ca-a2e9-c7a2bf838b0b"
        },
        {
            "id": "1c1d989e-428a-4466-af8e-6e269fa04562",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3d40f15b-4b52-40ca-a2e9-c7a2bf838b0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}