var keyUp = keyboard_check_pressed(vk_up);
var keyDown = keyboard_check_pressed(vk_down);
var keyGo = keyboard_check_pressed(vk_enter) or keyboard_check_pressed(ord("A"));

switch(menu){
	case 0:
		if(keyUp) {
			if(optionSelected > 0) {
				optionSelected--;
			} else {
				optionSelected = 2;
			}
		} else if(keyDown) {
			if(optionSelected < 2) {	
				optionSelected ++;
			} else {
				optionSelected = 0;
			}
		} else if(keyGo) {
			switch(optionSelected) {
				case 0: // HOST
					show_debug_message("1");
					room_goto(roomFarm);
					instance_create_depth(0, 0, 0, objServer);
					show_debug_message("2");
					instance_destroy(id);
					break;
				case 1: // JOIN
					menu = 1; // join menu
					optionSelected = 0;
					io_clear();
					exit;
				case 2: // EXIT
					game_end();
					break;
			}
		}
		break;
	case 1:
		if(typing == false) {
			if(keyUp) {
				if(optionSelected > 0) {
					optionSelected--;
				} else {
					optionSelected = 4;
				}
			} else if(keyDown) {
				if(optionSelected < 4) {	
					optionSelected ++;
				} else {
					optionSelected = 0;
				}
			} else if (keyGo) {
				switch(optionSelected){
					case 0: //IP
						typing = true;
						io_clear(); // clear the keyboard and stuff
						break;
					case 1: //PORT
						typing = true;
						io_clear(); // clear the keyboard and stuff
						break;
					case 2: // NAME
						typing = true;
						io_clear(); // clear the keyboard and stuff
						break;
					case 3: // CONNECT
						room_goto(roomFarm);
						instance_create_depth(0, 0, 0, objClient);
						instance_destroy(id);
						break;
					case 4: // BACK
						menu = 0;
						optionSelected = 0;
						exit;
						break;
				}
			}
		} else {
			if(keyboard_check_pressed(vk_enter)) {
				typing = false;
				exit;
			}
			switch(optionSelected){
				case 0:
					global.ipAddress = keyboard_string;
					break;
				case 1:
					global.port = real(keyboard_string);
					break;
				case 2:
					global.name = keyboard_string;
					break;
			}
		}
		break;
}
/*
if keyboard_check_pressed(ord("H")) {
	instance_create_depth(0, 0, 0, objServer);
	instance_destroy();
}
if keyboard_check_pressed(ord("C")) {
	instance_create_depth(0, 0, 0, objClient);
	instance_destroy();
}
*/