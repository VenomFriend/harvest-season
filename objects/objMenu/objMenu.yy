{
    "id": "8f278fa1-dde6-4768-bf2f-e60084b8c48a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenu",
    "eventList": [
        {
            "id": "89f972d7-753b-47ce-a0f6-150b47c6e51c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8f278fa1-dde6-4768-bf2f-e60084b8c48a"
        },
        {
            "id": "89305cb8-148f-4be8-81b4-ecf5c225f20e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f278fa1-dde6-4768-bf2f-e60084b8c48a"
        },
        {
            "id": "da30147c-76b7-410e-a5c8-705a77000391",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f278fa1-dde6-4768-bf2f-e60084b8c48a"
        },
        {
            "id": "631741a2-6e95-4342-a673-417f6c7beea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f278fa1-dde6-4768-bf2f-e60084b8c48a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}