var xDraw = room_width/2;


switch(menu) {
	case 0:
		draw_set_alpha(1);
		if(optionSelected == 0) {
			draw_sprite_ext(sprMenu, 0, xDraw, 300, 1, 1, 0, c_green, 0.5);
		} else {
			draw_sprite(sprMenu, 0, xDraw, 300);
		}
		if(optionSelected == 1) {
			draw_sprite_ext(sprMenu, 1, xDraw, 350, 1, 1, 0, c_green, 0.5);
		} else {
			draw_sprite(sprMenu, 1, xDraw, 350);
		}
		if(optionSelected == 2) {
			draw_sprite_ext(sprMenu, 3, xDraw, 400, 1, 1, 0, c_green, 0.5);
		} else {
			draw_sprite(sprMenu, 3, xDraw, 400);
		}
		break;
	case 1:
		draw_set_alpha(1);
		draw_set_font(fontMenu);
		if(optionSelected == 0) {
			draw_sprite_ext(sprMenu, 4, xDraw, 200, 1, 1, 0, c_green, 0.5);
			if(showBar and typing) {
				scriptTextOutline(global.ipAddress + "|", 300, 205);
			} else {
				scriptTextOutline(global.ipAddress, 300, 205);
			}
		} else {
			draw_sprite(sprMenu, 4, xDraw, 200);
			scriptTextOutline(global.ipAddress, 300, 205);
		}
		if(optionSelected == 1) {
			draw_sprite_ext(sprMenu, 5, xDraw, 250, 1, 1, 0, c_green, 0.5);
			if(showBar and typing) {
				scriptTextOutline(string(global.port) + "|", 300, 255);
			} else {
				scriptTextOutline(string(global.port), 300, 255);
			}
		} else {
			draw_sprite(sprMenu, 5, xDraw, 250);
			scriptTextOutline(string(global.port), 300, 255);
		}
		if(optionSelected == 2) {
			draw_sprite_ext(sprMenu, 6, xDraw, 300, 1, 1, 0, c_green, 0.5);
			if(showBar and typing) {
				scriptTextOutline(global.name + "|", 300, 305);
			} else {
				scriptTextOutline(global.name, 300, 305);
			}
		} else {
			draw_sprite(sprMenu, 6, xDraw, 300);
			scriptTextOutline(global.name, 300, 305);
		}
		if(optionSelected == 3) {
			draw_sprite_ext(sprMenu, 7, xDraw, 350, 1, 1, 0, c_green, 0.5);
		} else {
			draw_sprite(sprMenu, 7, xDraw, 350);
		}
		if(optionSelected == 4) {
			draw_sprite_ext(sprMenu, 8, xDraw, 400, 1, 1, 0, c_green, 0.5);
		} else {
			draw_sprite(sprMenu, 8, xDraw, 400);
		}
		
		break;
}