{
    "id": "2a6b0f0b-3fa9-4bc6-850c-1028de662d2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDoor",
    "eventList": [
        {
            "id": "7ccd469c-9baa-48c4-8a20-f92f6b2a9600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a6b0f0b-3fa9-4bc6-850c-1028de662d2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1e533a0b-0069-492d-a205-747ce67edf62",
    "visible": true
}