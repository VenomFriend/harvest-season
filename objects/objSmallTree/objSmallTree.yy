{
    "id": "90766770-6c46-4c16-9323-7fb859b2dfcf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSmallTree",
    "eventList": [
        {
            "id": "6c51b11b-932e-4041-8057-14d20a6dfc7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90766770-6c46-4c16-9323-7fb859b2dfcf"
        },
        {
            "id": "51551b56-1d54-476b-a289-6f0e4bf1e136",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "90766770-6c46-4c16-9323-7fb859b2dfcf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "7f30503c-1189-4fd2-b924-9b42eb2c4428",
    "visible": true
}