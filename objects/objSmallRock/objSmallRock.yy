{
    "id": "ea5fbfa1-766f-4756-8e78-fee502e8af3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSmallRock",
    "eventList": [
        {
            "id": "74e866ea-63b0-4cf9-9972-3ee89bf8aebd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea5fbfa1-766f-4756-8e78-fee502e8af3b"
        },
        {
            "id": "5b8ddf2c-7388-4b60-8567-607c012957f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ea5fbfa1-766f-4756-8e78-fee502e8af3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "64eb5b2d-3552-482e-8af3-1f68bf372e29",
    "visible": true
}