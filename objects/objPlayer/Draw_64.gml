/// @description Insert description here
// You can write your code in this editor
display_set_gui_size(640, 480);
if(myPlayer){
	draw_set_font(fontMoney);
	var textMoney = "G$: " + string(money);
	var textXMoney = camera_get_view_width(view_camera[0])/2;
	var textYMoney = 0;
	scriptTextOutline(textMoney, textXMoney, textYMoney);
	draw_set_font(fontInventory);
	draw_sprite(sprHand, 0, 0, 28);
	if(handItem != NO_ITEM) {
		if(global.itemEquipSprites[? handItem] != undefined) {
			draw_sprite(sprToolsIcons, global.itemEquipSprites[? handItem], camera_get_view_width(view_camera[0]) - (sprite_get_width(sprToolsIcons) * 2),0);
		}
		if(global.itemSprites[? handItem] != undefined) {
			draw_sprite(sprToolsSmall, global.itemSprites[? handItem], 2, 30);
			scriptTextOutline(string(handQuantity), 2, 30 + 22);
			draw_text_color(2, 30 + 22, string(handQuantity), c_white, c_white, c_white, c_white, 1);
		}
	}
	if(itemEquipped != NO_ITEM) {
		draw_sprite(sprToolsIcons, global.itemEquipSprites[? itemEquipped], camera_get_view_width(view_camera[0]) - sprite_get_width(sprToolsIcons),0);
	}
	// Watering Can Level
	if(itemEquipped == TOOL_WATERING_CAN) {
		scriptTextOutline("Water: " + string(waterLevel), camera_get_view_width(view_camera[0]) - sprite_get_width(sprToolsIcons),100);
	}
	draw_sprite(sprQuickslot, 0, 0, 0);
	for(var i = 0; i < quickSlotSize; i++) {
		var xx = 2 + (i * 26);
		var yy = 2;
		if(i == itemSelected) {
			draw_rectangle_color(xx, yy, xx+23, yy+23,c_orange, c_orange, c_orange, c_orange, 0);
		}
		if(global.itemSprites[? inventory[# 0, i]] != undefined) {
			draw_sprite(sprToolsSmall, global.itemSprites[? inventory[# 0, i]], xx, yy);
			scriptTextOutline(string(inventory[# 1, i]), xx, yy + 22);
			draw_text_color(xx, yy + 22, string(inventory[# 1, i]), c_white, c_white, c_white, c_white, 1);
		}
		scriptTextOutline(string(i + 1), xx, yy);
		draw_text_color(xx, yy, string(i + 1), c_white, c_white, c_white, c_white, 1);
	}
	
	// Draw the stamina hearts
	for(var i = 1; i <= 5; i++) {
		var yy = 400;
		var xx = 50*(i - 1);
		if( (i*20) - 9 <= stamina) {
			draw_sprite(sprHeart, 0, xx, yy);
		} else if( (i*20) - 19 <= stamina){
			draw_sprite(sprHeart, 1, xx, yy);
		} else {
			draw_sprite(sprHeart, 2, xx, yy);
		}
	}
	
	if(state == states.inventory) {
		//draw bag
		var xInit = camera_get_view_width(view_camera[0])/4;
		var yInit = camera_get_view_height(view_camera[0])/4;
		draw_sprite(sprBag, 0, xInit - 2, yInit - 2);
		for(var i = 0; i < 2; i++) {
			for(var ii = 0; ii < quickSlotSize; ii++){
				var num = (i*8) + ii;
				show_debug_message(string(num));
				var xx = xInit + (ii * 26);
				var yy = yInit + (i * 26);
				if(num == bagSlotSelected) {
					draw_rectangle_color(xx, yy, xx+23, yy+23,c_blue, c_blue, c_blue, c_blue, 0);
				}
				if(num == bagItemClicked) {
					draw_rectangle_color(xx, yy, xx+23, yy+23,c_green, c_green, c_green, c_green, 0);
				}
				draw_sprite(sprToolsSmall, global.itemSprites[? inventory[# 0, num]], xx, yy);
				if(inventory[# 0, num] != NO_ITEM) {
					scriptTextOutline(string(inventory[# 1, num]), xx, yy + 16);
					draw_text_color(xx, yy + 16, string(inventory[# 1, num]), c_white, c_white, c_white, c_white, 1);
				}
			}
		}
	}
	
	if(state == states.chat) {
		var yy = camera_get_view_height(view_camera[0]) - 20;
		draw_rectangle(0, yy, 600, yy+20, false)
		draw_set_font(fontChat);
		if(showBar) {
			scriptTextOutline(chatText + "|", 10, yy);
		} else {
			scriptTextOutline(chatText, 10, yy);
		}
	}

	
}