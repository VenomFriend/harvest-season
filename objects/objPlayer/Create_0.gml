// Player information
name = global.name;
playerSocket = 0;
facing = "down";
isMoving = false;
isRunning = false;
playerSpeed = 2.5;
playerRunSpeed = 5;
isHost = false;
myPlayer = false;
money = 999999;
myRoom = ROOM_FARM;
stamina = 100;


// Keys Pressed
moveUp = 0;
moveDown = 0;
moveLeft = 0;
moveRight = 0;
useItemPressed = 0;
useItemReleased = 0;
useItem = 0;
changeItemPressed = 0;
actionPressed = 0;
openBagPressed = 0;
inventoryKey = -1;
runPressed = 0;
runReleased = 0;
chatPressed = 0;
storeItemPressed = 0;



chatText = "";


// Player state
state = states.moving;


// Money to show animated
showMoney = false;
showMoneyValue = 0;
showMoneyY = 16;

usingItem = false;

// Inventory
inventorySize = 16;
quickSlotSize = 8;
bagWidth = 8;
bagHeight = 2;

handItem = NO_ITEM;
handQuantity = 0;

inventory = ds_grid_create(2, inventorySize);
for(var i = 0; i < inventorySize; i++) {
	ds_grid_add(inventory, 0, i, NO_ITEM);
}
for(var i = 0; i < inventorySize; i++) {
	ds_grid_add(inventory, 1, i , 0);
}
ds_grid_add(inventory, 0, 0, TOOL_HOE);
ds_grid_add(inventory, 1, 0, 1);
ds_grid_add(inventory, 0, 1, TOOL_WATERING_CAN);
ds_grid_add(inventory, 1, 1, 1);
ds_grid_add(inventory, 0, 2, TOOL_SICKLE);
ds_grid_add(inventory, 1, 2, 1);
ds_grid_add(inventory, 0, 3, TOOL_AXE);
ds_grid_add(inventory, 1, 3, 1);
ds_grid_add(inventory, 0, 4, TOOL_HAMMER);
ds_grid_add(inventory, 1, 4, 1);
ds_grid_add(inventory, 0, 5, TOOL_FISHING_ROD);
ds_grid_add(inventory, 1, 5, 1);
ds_grid_add(inventory, 0, 6, SEED_CARROT);
ds_grid_add(inventory, 1, 6, 99);

// Item equipped
itemSelected = 0; // the number of the tool in the quickslot
itemEquipped = ds_grid_get(inventory, 0, itemSelected); // which tool is equipped in the moment

bagSlotSelected = 0;
bagItemClicked = -1;


// The plot nearest the player
plotToHighlight = noone;

//alarm[0] = room_speed * 5;


//sprite info

sprite_bbox_left = sprite_get_bbox_left(sprite_index) - sprite_get_xoffset(sprite_index);
sprite_bbox_right = sprite_get_bbox_right(sprite_index) - sprite_get_xoffset(sprite_index);
sprite_bbox_bottom = sprite_get_bbox_bottom(sprite_index) - sprite_get_yoffset(sprite_index);
sprite_bbox_top = sprite_get_bbox_top(sprite_index) - sprite_get_yoffset(sprite_index);

// Misc

fishFrame = 0; // the frame of circle thing when you are fishing
gearsFrame = 0; // the frame of the gears when your invenory is open
pointsFrame = 0; // the frame of the "..." animation when you are i nchat
exclamationFrame = 0; // the frame of the "!" when you are fishing and catch a fish
showBar = false; // show the | when you are writing on the chat
timeToFish = -1; // how long for the fish to bit
fishOnRod = false; // if the fish is on the fishing rod
emitterID = audio_emitter_create(); // every player emits sound
waterLevel = 0; // How much water there is left on the watering can(max = 20)
outOfStamina = false; // To show the out of stamina animation
staminaFrame = 0; // The frame of the heart animation when you run out of stamina