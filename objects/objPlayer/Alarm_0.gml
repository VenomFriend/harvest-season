/// @description Send player coordinates

if(isHost) {
	ServerSendPlayerUpdate(id);
} else {
	ClientSendMyPlayer(id);
}

alarm[0] = room_speed * 5;