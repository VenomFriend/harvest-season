/// @description Insert description here
// You can write your code in this editor

scrCheckKeys();
if(myPlayer) {
	audio_listener_position(x, y, 0);
}
audio_emitter_position(emitterID,x,y,0);
switch(state){
	case states.moving:
		scrMovingState();
		break;
	case states.inventory:
		scrInventoryState();
		break;
	case states.action:
		scrActionState();
		break;
	case states.animation:
		scrAnimationState();
		break;
	case states.store:
		scrStoreState();
		break;
	case states.chat:
		scrChatState();
		break;
	case states.fishing:
		scrFishingState();
		break;
}