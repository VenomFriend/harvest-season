if(myPlayer || global.roomID == myRoom) {
	draw_self();
	if(global.showName){
		draw_set_font(fontMoney);
		draw_text_color(x - 10, y - 50, name, c_black, c_black, c_black, c_black, 1);
	}
	if(showMoney) {
		draw_set_font(fontMoney);
		draw_text(x - 16, y - showMoneyY, "+G$ " + string(showMoneyValue));
		showMoneyY++;
		if(showMoneyY > 50) {
			showMoneyY = 16;
			showMoney = false;
		}
	}

	if(state == states.moving){
		if(global.itemTypes[? handItem] == "Crop" or global.itemTypes[? handItem] == "Misc") {
			sprite_index = sprPlayerMaleHolding;
			draw_sprite(sprToolsSmall, global.itemSprites[? handItem], x-3, y-35);
		} else {	
			sprite_index = sprPlayerMaleWalking;
		}
		
		#region Moving Animation
		if(isMoving) {
			image_speed = 0.5;
			switch(facing){
				case "up":
					if (image_index < 12 or image_index >= 15.5) {
						image_index = 12;
					}
					break;
				case "down":
					if (image_index >= 3.5) {
						image_index = 0;
					}
					break;
				case "left":
					if (image_index < 4 or image_index >= 7.5) {
						image_index = 4;
					}
					break;
				case "right":
					if (image_index < 8 or image_index >= 11.5) {
						image_index = 8;
					}
					break;
			}
		} else {
			image_speed = 0;
			switch(facing){
				case "up":
					image_index = 13;
					break;
				case "down":
					image_index = 1;
					break;
				case "left":
					image_index = 5;
					break;
				case "right":
					image_index = 9;
					break;
			}
		}
		#endregion
		
		// Show the heart animation if you run out of stamina and tries to do an action that requires stamina
		if(outOfStamina) {
			draw_sprite(sprStamina, staminaFrame, x + 8, y - 36);
			staminaFrame += 0.1;
			if(staminaFrame > 7.5) {
				staminaFrame = 0;
				outOfStamina = false;
			}
		}
	}

	if(state == states.inventory) {
		draw_sprite(sprGears, gearsFrame, x + 8, y - 36);
		gearsFrame += 0.2;
		if(gearsFrame > 7.5) {
			gearsFrame = 0;
		}
		image_speed = 0;
		switch(facing){
			case "up":
				image_index = 13;
				break;
			case "down":
				image_index = 1;
				break;
			case "left":
				image_index = 5;
				break;
			case "right":
				image_index = 9;
				break;
		}
	}
	
	if(state == states.chat) {
		draw_sprite(sprPoints, pointsFrame, x + 8, y - 36);
		pointsFrame += 0.2;
		if(pointsFrame > 7.5) {
			pointsFrame = 0;
		}
		image_speed = 0;
		switch(facing){
			case "up":
				image_index = 13;
				break;
			case "down":
				image_index = 1;
				break;
			case "left":
				image_index = 5;
				break;
			case "right":
				image_index = 9;
				break;
		}
	}
	
	if(state == states.fishing) {
		var xToDraw = x;
		var yToDraw = y;
		switch(facing){
			case "up":
				yToDraw -= 40;
				break;
			case "down":
				xToDraw -= 10;
				yToDraw += 20;
				break;
			case "left":
				xToDraw -= 40;
				break;
			case "right":
				xToDraw += 40;
				break;
		}
		draw_sprite(sprFishingBait, fishFrame, xToDraw, yToDraw);
		fishFrame+= 0.1;
		if(fishFrame > 2.5) {
			fishFrame = 0;
		}
		if(fishOnRod) {
			draw_sprite(sprExclamation, exclamationFrame, x + 8, y - 36);
			exclamationFrame += 0.2;
			if(exclamationFrame > 7.5) {
				exclamationFrame = 0;
			}
		}
	}
}