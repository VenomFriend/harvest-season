{
    "id": "75d872ae-0e85-462f-a281-cc300bd12ac4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlayer",
    "eventList": [
        {
            "id": "522f942b-c934-4a11-b409-d13b202fdb9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "5dcea902-3f70-452b-b4ec-acf4b76ea226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "d5519d9d-8488-4adf-80da-c3b1d972b4c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "fe2be519-dc43-4f5e-9a1e-ec5696fea696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "12df7687-a687-4e2e-8f72-d84787f666fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "8baec3e0-17c7-4a3e-b21f-685f33dce1c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "a5df5ac3-b24d-4144-9736-b49eaf3b4ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        },
        {
            "id": "17e10ebe-ec77-4780-990e-2da8a656956b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "75d872ae-0e85-462f-a281-cc300bd12ac4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
    "visible": true
}