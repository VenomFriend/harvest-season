/// @description Change the collision layer

if(myPlayer) {
	layer_set_target_room(room);
	var playerLayer = layer_get_id("collision_layer");
	global.tilemap = layer_tilemap_get_id(playerLayer);
}