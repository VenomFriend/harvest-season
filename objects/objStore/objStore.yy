{
    "id": "45535953-f09d-45ea-a112-121112c9aee3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objStore",
    "eventList": [
        {
            "id": "e81a46c3-2242-464a-af37-767ba7ef4dad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "45535953-f09d-45ea-a112-121112c9aee3"
        },
        {
            "id": "3e0b624d-1495-4257-ba1b-db15b883e05b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45535953-f09d-45ea-a112-121112c9aee3"
        },
        {
            "id": "c756ffd6-c7e9-402f-b28c-1bdbfb486cbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45535953-f09d-45ea-a112-121112c9aee3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "2fa7b0ca-1de4-4282-ad2f-181778527bd2",
    "visible": true
}