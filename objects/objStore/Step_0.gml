/// @description Insert description here
// You can write your code in this editor
if(open) {
	moveUpPressed = keyboard_check_pressed(vk_up);
	moveDownPressed = keyboard_check_pressed(vk_down);
	moveLeftPressed = keyboard_check_pressed(vk_left);
	moveRightPressed = keyboard_check_pressed(vk_right);
	buyItemPressed = keyboard_check_pressed(ord("A"));
	cancelBuyPressed = keyboard_check_pressed(ord("S"));
	var player = instance_find(objPlayer, 0);
	
	var storeSize = 10;
	var storeItems = storeItemsPageOne;
	
	switch(pageSelected) {
		case 0:
			storeSize = 10;
			storeItems = storeItemsPageOne;
			break;
		case 1:
			storeSize = 10;
			storeItems = storeItemsPageTwo;
			break;
		case 2:
			storeSize = 5;
			storeItems = storeItemsPageThree;
			break;
	}
	var maxQuantity = player.money div global.itemPricesBuy[? storeItems[itemSelected]];
	
	if(buyingQuantity) {
		if(moveUpPressed) {
			if(quantityToBuy < 99) {
				quantityToBuy++;
			}
		} else if(moveDownPressed) {
			if(quantityToBuy > 1) {
				quantityToBuy--;
			}
		} else if(moveLeftPressed) {
			quantityToBuy -= 10;
			if(quantityToBuy < 1) {
				quantityToBuy = 1;
			}
		} else if(moveRightPressed) {
			quantityToBuy += 10;
			if(quantityToBuy > 99) {
				quantityToBuy = 99;
			}			
		} else if(cancelBuyPressed) {
			buyingQuantity = false;
			quantityToBuy = 1;
		}
		if(quantityToBuy > maxQuantity) {
			quantityToBuy = maxQuantity;
		}
		if(buyItemPressed) {
			// This var will be true if the player has space in the inventory to put all the items
			// he bought
			var checkIfAdd = scrAddItem(0, player, storeItems[itemSelected], quantityToBuy);
			var price = quantityToBuy * global.itemPricesBuy[? storeItems[itemSelected]];
			if(checkIfAdd) {
				if(player.isHost) {
					player.money -= price;
				} else {
					ClientSendBuyItem(price);
					ClientSendInventory(player.inventory);
				}
			} else {
				// Not enough space
			}
			buyingQuantity = false;
			buyItemPressed = false;
			quantityToBuy = 1;
		}
	} else {
		switch(pageSelected) {
			case 0:
				storeSize = 10;
				storeItems = storeItemsPageOne;
				break;
			case 1:
				storeSize = 10;
				storeItems = storeItemsPageTwo;
				break;
			case 2:
				storeSize = 5;
				storeItems = storeItemsPageThree;
				break;
		}
		if(moveUpPressed) {
			if(itemSelected > 0) {
				itemSelected--;
			} else {
				itemSelected = storeSize - 1;
			}
		} else if(moveDownPressed) {
			if(itemSelected < storeSize - 1) {
				itemSelected++;
			} else {
				itemSelected = 0;
			}	
		} else if(moveLeftPressed) {
			itemSelected = 0;
			if(pageSelected == 0) {
				pageSelected = 2;
			} else {
				pageSelected--;
			}
		
		} else if(moveRightPressed) {
			itemSelected = 0;
			if(pageSelected == 2) {
				pageSelected = 0;
			} else {
				pageSelected++;
			}
		}
	}
	
	switch(pageSelected) {
		case 0:
			storeSize = 10;
			storeItems = storeItemsPageOne;
			break;
		case 1:
			storeSize = 10;
			storeItems = storeItemsPageTwo;
			break;
		case 2:
			storeSize = 5;
			storeItems = storeItemsPageThree;
			break;
	}
	
	if(buyItemPressed and !buyingQuantity) {
		if(player.money >= global.itemPricesBuy[? storeItems[itemSelected]]) {
			buyingQuantity = true;
		}
	}
}