/// @description Insert description here
// You can write your code in this editor

if(open) {
	var xInit = camera_get_view_width(view_camera[0])/2 - 150;
	var yInit = camera_get_view_height(view_camera[0])/4;
	switch(pageSelected) {
		case 0:
			var storeSize = 10;
			var storeItems = storeItemsPageOne;
			break;
		case 1:
			var storeSize = 10;
			var storeItems = storeItemsPageTwo;
			break;
		case 2:
			var storeSize = 5;
			var storeItems = storeItemsPageThree;
			break;
	}
	draw_set_font(fontInventory);
	for(var i = 0; i < storeSize; i++) {
		draw_sprite(sprStore, 0, xInit, yInit + i*26);
		draw_sprite(sprToolsSmall, global.itemSprites[? storeItems[i]], xInit + 2, yInit + 2 + i*26);
		draw_text(xInit + 30, yInit + i*26, global.itemNames[? storeItems[i]]);
		draw_text(xInit + 210, yInit + i*26, "G$: " + string(global.itemPricesBuy[? storeItems[i]]));
	}

	var xSize = sprite_get_width(sprStore);
	var ySize = sprite_get_height(sprStore);
	
	draw_set_alpha(0.5);
	draw_rectangle_color(xInit, yInit + itemSelected * 26, xInit + xSize, yInit + (ySize - 2) + itemSelected * 26, c_red, c_red, c_red, c_red, 0);
	draw_set_alpha(1);
	if(buyingQuantity) {
		draw_sprite(sprStoreQuantity, 0, xInit + xSize, yInit + itemSelected * 26);
		draw_text(xInit + xSize + 4, yInit + (itemSelected * 26) + 4, string(quantityToBuy));
	}
}
