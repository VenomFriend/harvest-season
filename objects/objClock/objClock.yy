{
    "id": "27df91a4-abcb-45b5-9e99-7001a4c325e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClock",
    "eventList": [
        {
            "id": "b8142d77-7f2a-45a6-b077-4851cdcdefa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27df91a4-abcb-45b5-9e99-7001a4c325e3"
        },
        {
            "id": "a56663e8-2068-4fa7-9055-5a7c704be07b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27df91a4-abcb-45b5-9e99-7001a4c325e3"
        },
        {
            "id": "d0ca0be0-f2c8-40a4-8798-f540e54e6b51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "27df91a4-abcb-45b5-9e99-7001a4c325e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}