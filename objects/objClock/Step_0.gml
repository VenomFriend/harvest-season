timePassed += delta_time/1000000;

if (timePassed >= 5) {
	timePassed = 0;
	if(minute < 50) {
		minute += 10;
	} else {
		minute = 0;
		if(hour < 23) {
			hour++;
		} else {
			hour = 0;
		}
	}
}