draw_set_font(fontChat);
var textHour = string(hour);
if(hour < 10) {
	textHour = "0" + textHour;
}
var textMinute = string(minute);
if(minute < 10) {
	textMinute = "0" + textMinute;
}
var text = textHour + ":" + textMinute;
scriptTextOutline(text, 0, 60);