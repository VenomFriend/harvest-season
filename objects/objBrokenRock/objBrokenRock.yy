{
    "id": "55a80813-8aaf-483a-bf2e-08fd557642d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBrokenRock",
    "eventList": [
        {
            "id": "d147fecd-070d-4e30-8de4-00428d68cf97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55a80813-8aaf-483a-bf2e-08fd557642d7"
        },
        {
            "id": "5330d270-0f8b-4840-86a3-91012c7fabe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "55a80813-8aaf-483a-bf2e-08fd557642d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
    "visible": true
}