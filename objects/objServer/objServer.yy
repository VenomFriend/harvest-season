{
    "id": "1a607b4e-b958-4980-892f-3a5c43b63fec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objServer",
    "eventList": [
        {
            "id": "3534975c-ba7f-4771-bda4-d674aa5a61d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a607b4e-b958-4980-892f-3a5c43b63fec"
        },
        {
            "id": "d2417489-9d40-49f5-9fd3-3eeb53bd843a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "1a607b4e-b958-4980-892f-3a5c43b63fec"
        },
        {
            "id": "bc38af03-1d73-4d8f-a38d-9f618598d0fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "1a607b4e-b958-4980-892f-3a5c43b63fec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}