var typeEvent = ds_map_find_value(async_load, "type");

switch(typeEvent) {
	case network_type_connect:
		var socket = ds_map_find_value(async_load, "socket");
		show_debug_message("SOCKET RECEBIDO: ");
		show_debug_message(string(socket));
		// Gonna use this socket to give the player an ID!
		
		ServerSendPlayerSocket(socket);
		var otherPlayer = instance_create_depth(320, 320, -1000, objPlayer); // Create the other player on the server
		otherPlayer.playerSocket = socket;
		// list of connected players
		ds_list_add(global.socketList, socket);
		
		//Send him to all the players who are already online!
        ServerSendPlayerToAll(socket, otherPlayer);
		//And send to him the other players
		ServerSendEveryoneToPlayer(socket);
		//And send all the objects in the game to him
		ServerSendAllCrops(socket);
		ServerSendAllGeneric(socket, objSmallTree);
		ServerSendAllGeneric(socket, objWood);
		ServerSendAllGeneric(socket, objSmallRock);
		ServerSendAllGeneric(socket, objBrokenRock);
		//And send him the global money, if it is global
		if(globalMoney) {
			ServerSendMoney(socket, serverPlayer.money);
		} else {
			ServerSendMoney(socket, 1000);
		}
		//Update his clock as well
		var clock = instance_find(objClock, 0);
		ServerSendClock(clock.hour, clock.minute, clock.timePassed);
		//and also add him to the client list, so I can know which player is each socket
        ds_map_add(global.clients, socket, otherPlayer);

		break;
	case network_type_disconnect:
		var socket = ds_map_find_value(async_load, "socket");
		var otherPlayer = ds_map_find_value(global.clients, socket);
		
		//Save it
		with(otherPlayer) {
			SavePlayer();
		}		
        //Delete it from the connected clients map
		
        ds_map_delete(global.clients, socket);
		
        // Tell everybody to delete the player, and delete their instance on the server
        ServerSendDeletePlayer(socket);
        with (otherPlayer) {
            instance_destroy()
        }

		// and delete the player from the socket list
		var findSocket = ds_list_find_index(global.socketList, socket);
		if(findSocket >= 0){
			ds_list_delete(global.socketList, findSocket);
		}
		break;
	case network_type_data:
		var buffer = ds_map_find_value(async_load, "buffer");
		var socket = ds_map_find_value(async_load, "id");
		buffer_seek(buffer, buffer_seek_start, 0);
		ServerReceivedPacket(buffer, socket);
		break;
}