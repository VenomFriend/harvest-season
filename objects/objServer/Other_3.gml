network_destroy(global.server);
buffer_delete(global.bufferServerWrite);
ds_list_destroy(global.socketList);

var saveMap = ds_map_create();
var cropList = ds_list_create();
var playerList = ds_list_create();

with(objCrop){
	SaveCrops(cropList);
}
ds_map_add_list(saveMap, "crops", cropList);

// save all the players that are still online
with(objPlayer){
	SavePlayer();
}
ds_map_add_list(saveMap, "players", global.playersList);

var treeList = ds_list_create();
var woodList = ds_list_create();
var rockList = ds_list_create();
var brokenRockList = ds_list_create();

with(objSmallTree) {
	SaveGeneric(treeList);
} 
ds_map_add_list(saveMap, "trees", treeList);

with(objWood) {
	SaveGeneric(woodList);
}
ds_map_add_list(saveMap, "woods", woodList);


with(objSmallRock) {
	SaveGeneric(rockList);
}
ds_map_add_list(saveMap, "rocks", rockList);


with(objBrokenRock) {
	SaveGeneric(brokenRockList);
}
ds_map_add_list(saveMap, "broken rocks", brokenRockList);



var myObjects = json_encode(saveMap);
ds_map_destroy(saveMap);
var file;
file = file_text_open_write("server.json");
file_text_write_string(file, myObjects);
file_text_close(file);