// All the players info stored to be saved/loaded
global.playersList = ds_list_create();

show_debug_message("Server Created");
serverPlayer = instance_create_depth(320, 320, -1000, objPlayer);
instance_create_depth(0, 0, -2000, objClock);
with(serverPlayer) {
	audio_listener_orientation(0,1,0,0,0,1);
}
show_debug_message("Player should have been created!");
serverPlayer.myPlayer = true;
serverPlayer.alarm[0] = room_speed * 5;
serverPlayer.isHost = true;
serverPlayer.playerSocket = 0;
serverPlayer.name = "Host";


// Check if there's a server file saved
var file = file_text_open_read("server.json");
if(file) {
	var jsonData = "";
	while (!file_text_eof(file))
	{
	    jsonData += file_text_read_string(file);
	    file_text_readln(file);
	}
	file_text_close(file);
	
	var loadObjects = json_decode(jsonData);
	var cropsList = ds_map_find_value(loadObjects, "crops");
	show_debug_message("CROP LIST SIZE");
	show_debug_message(string(ds_list_size(cropsList)));
	for(var i = 0; i < ds_list_size(cropsList); i++) {
		var thisCrop = ds_list_find_value(cropsList, i);
		var xx = ds_map_find_value(thisCrop, "x");
		var yy = ds_map_find_value(thisCrop, "y");
		var isDig = ds_map_find_value(thisCrop, "isDig");
		var isGrass = ds_map_find_value(thisCrop, "isGrass");
		var water = ds_map_find_value(thisCrop, "water");
		var stage = ds_map_find_value(thisCrop, "stage");
		var cropType = ds_map_find_value(thisCrop, "cropType");
		var timer = ds_map_find_value(thisCrop, "timer");
		var crop = instance_create_depth(xx, yy, -10, objCrop);
		crop.isDig = isDig;
		crop.water = water;
		crop.stage = stage;
		crop.cropType = cropType;
		crop.timer = timer;
		crop.isHost = true;
		crop.isGrass = isGrass;
	}
	
	var treeList = ds_map_find_value(loadObjects, "trees");
	for(var i = 0; i < ds_list_size(treeList); i++) {
		var thisTree = ds_list_find_value(treeList, i);
		var xx = ds_map_find_value(thisTree, "x");
		var yy = ds_map_find_value(thisTree, "y");
		var imageIndex = ds_map_find_value(thisTree, "image_index");
		var imageSpeed = ds_map_find_value(thisTree, "image_speed");
		var tree = instance_create_depth(xx, yy, -50, objSmallTree);
		tree.image_index = imageIndex;
		tree.image_speed = imageSpeed;
	}
	
	var woodList = ds_map_find_value(loadObjects, "woods");
	for(var i = 0; i < ds_list_size(woodList); i++) {
		var thisWood = ds_list_find_value(woodList, i);
		var xx = ds_map_find_value(thisWood, "x");
		var yy = ds_map_find_value(thisWood, "y");
		var imageIndex = ds_map_find_value(thisWood, "image_index");
		var imageSpeed = ds_map_find_value(thisWood, "image_speed");
		var wood = instance_create_depth(xx, yy, -50, objWood);
		wood.image_index = imageIndex;
		wood.image_speed = imageSpeed;
	}
	
	var rockList = ds_map_find_value(loadObjects, "rocks");
	for(var i = 0; i < ds_list_size(rockList); i++) {
		var thisRock = ds_list_find_value(rockList, i);
		var xx = ds_map_find_value(thisRock, "x");
		var yy = ds_map_find_value(thisRock, "y");
		var imageIndex = ds_map_find_value(thisRock, "image_index");
		var imageSpeed = ds_map_find_value(thisRock, "image_speed");
		var rock = instance_create_depth(xx, yy, -50, objSmallRock);
		rock.image_index = imageIndex;
		rock.image_speed = imageSpeed;
	}
	
	var brokenRockList = ds_map_find_value(loadObjects, "broken rocks");
	for(var i = 0; i < ds_list_size(brokenRockList); i++) {
		var thisBrokenRock = ds_list_find_value(brokenRockList, i);
		var xx = ds_map_find_value(thisBrokenRock, "x");
		var yy = ds_map_find_value(thisBrokenRock, "y");
		var imageIndex = ds_map_find_value(thisBrokenRock, "image_index");
		var imageSpeed = ds_map_find_value(thisBrokenRock, "image_speed");
		var brokenRock = instance_create_depth(xx, yy, -50, objBrokenRock);
		brokenRock.image_index = imageIndex;
		brokenRock.image_speed = imageSpeed;
	}
	global.playersList = ds_map_find_value(loadObjects, "players");
	show_debug_message("Player List Size");
	show_debug_message(string(ds_list_size(global.playersList)));
	for(var i = 0; i < ds_list_size(global.playersList); i++) {
		var thisPlayer = ds_list_find_value(global.playersList, i);
		var name = ds_map_find_value(thisPlayer, "name");
		if(name == serverPlayer.name) {
			var xx = ds_map_find_value(thisPlayer, "x");
			var yy = ds_map_find_value(thisPlayer, "y");
			var inventory = ds_map_find_value(thisPlayer, "inventory");
			var money = ds_map_find_value(thisPlayer, "money");
			serverPlayer.x = xx;
			serverPlayer.y = yy;
			serverPlayer.money = money;
			var inventoryRead = ds_grid_create(2, 16);
			ds_grid_read(inventoryRead, inventory);
			serverPlayer.inventory = inventoryRead;
		}
	}
} else {
	for(var i = 0; i < 35; i++) {
		for(var ii = 0; ii < 17; ii++) {
			var xToAdd = i*32;
			var yToAdd = ii*32;
			var crop = instance_create_depth(480 + xToAdd, 384 + yToAdd, -10, objCrop);
			crop.isHost = true;
			var randomChance = irandom(10);
			//show_debug_message("random = " + string(randomChance));
			if(randomChance == 1) {
				show_debug_message("Created wood");	
				instance_create_depth(480 + xToAdd, 384 + yToAdd, -50, objSmallTree);
			}
			if(randomChance == 2) {
				show_debug_message("Created rock");	
				instance_create_depth(480 + xToAdd, 384 + yToAdd, -50, objSmallRock);
			}
			if(randomChance == 3) {
				show_debug_message("Created grass");
				crop.isGrass = true;
			}
			//Just for test
			//crop.cropType = irandom_range(CROP_CORN, CROP_JASMINE);
			//crop.stage = 2;
		}
	}
}



globalMoney = false;

typeServer = network_socket_tcp;
global.port = 8000;
global.maxClients = 32;
global.server = network_create_server(typeServer, global.port, global.maxClients);

var size, typeBuffer, alignment;
size = 1024;
typeBuffer = buffer_fixed;
alignment = 1;

// Buffer the server will write and send to all the clients
global.bufferServerWrite = buffer_create(size, typeBuffer, alignment);

// Map of clients
global.clients = ds_map_create();
// List of sockets
global.socketList = ds_list_create();


