{
    "id": "932b57c7-d3be-44c5-a18d-12a483cb32a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTeleport",
    "eventList": [
        {
            "id": "398a2f93-549e-477b-8a73-23d07ac5538d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "932b57c7-d3be-44c5-a18d-12a483cb32a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "565c0c18-d969-41d4-963a-b567b609d3e9",
    "visible": false
}