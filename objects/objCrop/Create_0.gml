/// @description Insert description here
// You can write your code in this editor

image_speed = 0;
image_index = 0;
depth = -1 - (y/32); // believe me, this is needed

isDig = false;
isGrass = false;
water = 0; // not watered
stage = 0; // crops have 3 stages
cropType = 0; // no crop planted
//crop = irandom_range(CORN, PUMPKIN);

isHost = false; // if the crop is on the server or the client
//basically, the crop on the server is the only one which should
//be updating itself with the timer and stuff
//the one on the client will only change when the server tells him to
sendUpdate = false;
highlight = false;
cropID = -1; // Only for the client so he can say to the server exactly which crop he is looking at
timer = 0; //will only grow when timer is equal to the growth value

myPlayer = instance_find(objPlayer, 0);