{
    "id": "090bcf5f-4d3d-4ea5-af08-e1096f11212c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCrop",
    "eventList": [
        {
            "id": "ceff103b-a685-4a4d-87c4-0fd5258c495b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "090bcf5f-4d3d-4ea5-af08-e1096f11212c"
        },
        {
            "id": "17cbfdc8-6a7a-46bb-a7d4-452de1674342",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "090bcf5f-4d3d-4ea5-af08-e1096f11212c"
        },
        {
            "id": "75f58dfa-ff79-4d6c-b54c-1f549a86c83f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "090bcf5f-4d3d-4ea5-af08-e1096f11212c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
    "visible": true
}