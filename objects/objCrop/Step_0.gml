/// @description Insert description here
// You can write your code in this editor

if isHost {
	if (cropType != 0 and water and stage != 2) {
		timer++;
	}
	var oldSprite = sprite_index;
	var oldImage = image_index;
	if(cropType == 0) {
		
		sprite_index = sprPlot;
		if(isDig) {
			if(water) {
				image_index = 3;
			} else {
				image_index = 2;
			}
		} else if(isGrass) {
			if(water) {
				image_index = 7;
			} else {
				image_index = 6;
			}
		} else {
			if(water){
				image_index = 1;
			} else {
				image_index = 0;
			}
		}
	}
	if(cropType != 0) {
		if water and stage != 2 {
			if timer >= global.cropsTimer[cropType, stage] {
				timer = 0;
				stage++;
				sendUpdate = true;
				//water = 0;
			}
		}
		sprite_index = sprCrops;
		image_index = global.cropsImage[cropType, water] + stage;
	}
	if(oldSprite != sprite_index or oldImage != image_index){
		sendUpdate = true;
	}
	if sendUpdate {
		//send the updated crop to the clients
		ServerSendCropUpdate(id);
		sendUpdate = false;
	}
}