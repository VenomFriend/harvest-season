{
    "id": "368592bb-3449-4288-8158-6bf0cf335c52",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset1",
    "auto_tile_sets": [
        {
            "id": "f10642e2-a3bf-4796-9243-23a870d0591e",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_1",
            "tiles": [
                167,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                32,
                33,
                34,
                35,
                36,
                37,
                38,
                39,
                64,
                65,
                66,
                67,
                68,
                69,
                70,
                71,
                96,
                97,
                98,
                99,
                100,
                101,
                102,
                103,
                128,
                129,
                130,
                131,
                132,
                133,
                134,
                135,
                160,
                161,
                162,
                163,
                164,
                165,
                166
            ]
        },
        {
            "id": "f1027742-9382-479e-b571-63ebf3ea4c2e",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_2",
            "tiles": [
                192,
                193,
                194,
                195,
                196,
                197,
                198,
                199,
                224,
                225,
                226,
                227,
                228,
                229,
                230,
                231,
                256,
                257,
                258,
                259,
                260,
                261,
                262,
                263,
                288,
                289,
                290,
                291,
                292,
                293,
                294,
                295,
                320,
                321,
                322,
                323,
                324,
                325,
                326,
                327,
                352,
                353,
                354,
                355,
                356,
                357,
                358
            ]
        },
        {
            "id": "69c93faa-496d-456b-a6a5-3c41979767e9",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_3",
            "tiles": [
                384,
                385,
                386,
                387,
                388,
                389,
                390,
                391,
                416,
                417,
                418,
                419,
                420,
                421,
                422,
                423,
                448,
                449,
                450,
                451,
                452,
                453,
                454,
                455,
                480,
                481,
                482,
                483,
                484,
                485,
                486,
                487,
                512,
                513,
                514,
                515,
                516,
                517,
                518,
                519,
                544,
                545,
                546,
                547,
                548,
                549,
                550
            ]
        },
        {
            "id": "38db1426-8ec3-4187-9c3a-3a5cc2cb42e4",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_4",
            "tiles": [
                576,
                577,
                578,
                579,
                580,
                581,
                582,
                583,
                608,
                609,
                610,
                611,
                612,
                613,
                614,
                615,
                640,
                641,
                642,
                643,
                644,
                645,
                646,
                647,
                672,
                673,
                674,
                675,
                676,
                677,
                678,
                679,
                704,
                705,
                706,
                707,
                708,
                709,
                710,
                711,
                736,
                737,
                738,
                739,
                740,
                741,
                742
            ]
        },
        {
            "id": "92430dae-e259-4ac4-a02b-e744e4def349",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_5",
            "tiles": [
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                40,
                41,
                42,
                43,
                44,
                45,
                46,
                47,
                72,
                73,
                74,
                75,
                76,
                77,
                78,
                79,
                104,
                105,
                106,
                107,
                108,
                109,
                110,
                111,
                136,
                137,
                138,
                139,
                140,
                141,
                142,
                143,
                168,
                169,
                170,
                171,
                172,
                173,
                174
            ]
        },
        {
            "id": "c17f7d55-f86c-414c-9be4-91ee106a4889",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_6",
            "tiles": [
                200,
                201,
                202,
                203,
                204,
                205,
                206,
                207,
                232,
                233,
                234,
                235,
                236,
                237,
                238,
                239,
                264,
                265,
                266,
                267,
                268,
                269,
                270,
                271,
                296,
                297,
                298,
                299,
                300,
                301,
                302,
                303,
                328,
                329,
                330,
                331,
                332,
                333,
                334,
                335,
                360,
                361,
                362,
                363,
                364,
                365,
                366
            ]
        },
        {
            "id": "2dbbffb1-6973-4812-b7ef-1c72924291ff",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_7",
            "tiles": [
                392,
                393,
                394,
                395,
                396,
                397,
                398,
                399,
                424,
                425,
                426,
                427,
                428,
                429,
                430,
                431,
                456,
                457,
                458,
                459,
                460,
                461,
                462,
                463,
                488,
                489,
                490,
                491,
                492,
                493,
                494,
                495,
                520,
                521,
                522,
                523,
                524,
                525,
                526,
                527,
                552,
                553,
                554,
                555,
                556,
                557,
                558
            ]
        },
        {
            "id": "897744a8-e995-49b8-971d-e52c0c501268",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_8",
            "tiles": [
                584,
                585,
                586,
                587,
                588,
                589,
                590,
                591,
                616,
                617,
                618,
                619,
                620,
                621,
                622,
                623,
                648,
                649,
                650,
                651,
                652,
                653,
                654,
                655,
                680,
                681,
                682,
                683,
                684,
                685,
                686,
                687,
                712,
                713,
                714,
                715,
                716,
                717,
                718,
                719,
                744,
                745,
                746,
                747,
                748,
                749,
                750
            ]
        },
        {
            "id": "74d5d8c1-ecfa-4a07-bcd5-73946983674f",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_9",
            "tiles": [
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                48,
                49,
                50,
                51,
                52,
                53,
                54,
                55,
                80,
                81,
                82,
                83,
                84,
                85,
                86,
                87,
                112,
                113,
                114,
                115,
                116,
                117,
                118,
                119,
                144,
                145,
                146,
                147,
                148,
                149,
                150,
                151,
                176,
                177,
                178,
                179,
                180,
                181,
                182
            ]
        },
        {
            "id": "a7ae8a1a-e106-4461-9d95-bb16743515ef",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_10",
            "tiles": [
                208,
                209,
                210,
                211,
                212,
                213,
                214,
                215,
                240,
                241,
                242,
                243,
                244,
                245,
                246,
                247,
                272,
                273,
                274,
                275,
                276,
                277,
                278,
                279,
                304,
                305,
                306,
                307,
                308,
                309,
                310,
                311,
                336,
                337,
                338,
                339,
                340,
                341,
                342,
                343,
                368,
                369,
                370,
                371,
                372,
                373,
                374
            ]
        },
        {
            "id": "03dd2c41-78a5-4d25-af61-b6af1d42404f",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_11",
            "tiles": [
                400,
                401,
                402,
                403,
                404,
                405,
                406,
                407,
                432,
                433,
                434,
                435,
                436,
                437,
                438,
                439,
                464,
                465,
                466,
                467,
                468,
                469,
                470,
                471,
                496,
                497,
                498,
                499,
                500,
                501,
                502,
                503,
                528,
                529,
                530,
                531,
                532,
                533,
                534,
                535,
                560,
                561,
                562,
                563,
                564,
                565,
                566
            ]
        },
        {
            "id": "a4f79bb5-b4b0-4c56-bbb8-8eafca516b81",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_12",
            "tiles": [
                592,
                593,
                594,
                595,
                596,
                597,
                598,
                599,
                624,
                625,
                626,
                627,
                628,
                629,
                630,
                631,
                656,
                657,
                658,
                659,
                660,
                661,
                662,
                663,
                688,
                689,
                690,
                691,
                692,
                693,
                694,
                695,
                720,
                721,
                722,
                723,
                724,
                725,
                726,
                727,
                752,
                753,
                754,
                755,
                756,
                757,
                758
            ]
        },
        {
            "id": "b66cddb1-1600-4fd8-9e53-87712eb8c33f",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_13",
            "tiles": [
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31,
                56,
                57,
                58,
                59,
                60,
                61,
                62,
                63,
                88,
                89,
                90,
                91,
                92,
                93,
                94,
                95,
                120,
                121,
                122,
                123,
                124,
                125,
                126,
                127,
                152,
                153,
                154,
                155,
                156,
                157,
                158,
                159,
                184,
                185,
                186,
                187,
                188,
                189,
                190
            ]
        },
        {
            "id": "caf8f0f3-7782-44d9-b19c-4ddf9bda7621",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_14",
            "tiles": [
                216,
                217,
                218,
                219,
                220,
                221,
                222,
                223,
                248,
                249,
                250,
                251,
                252,
                253,
                254,
                255,
                280,
                281,
                282,
                283,
                284,
                285,
                286,
                287,
                312,
                313,
                314,
                315,
                316,
                317,
                318,
                319,
                344,
                345,
                346,
                347,
                348,
                349,
                350,
                351,
                376,
                377,
                378,
                379,
                380,
                381,
                382
            ]
        },
        {
            "id": "347df9f2-4935-44d3-b4e4-7e71338e6d42",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_15",
            "tiles": [
                408,
                409,
                410,
                411,
                412,
                413,
                414,
                415,
                440,
                441,
                442,
                443,
                444,
                445,
                446,
                447,
                472,
                473,
                474,
                475,
                476,
                477,
                478,
                479,
                504,
                505,
                506,
                507,
                508,
                509,
                510,
                511,
                536,
                537,
                538,
                539,
                540,
                541,
                542,
                543,
                568,
                569,
                570,
                571,
                572,
                573,
                574
            ]
        },
        {
            "id": "56c41a54-efb6-4db9-b7c4-e05016c91dac",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_16",
            "tiles": [
                600,
                601,
                602,
                603,
                604,
                605,
                606,
                607,
                632,
                633,
                634,
                635,
                636,
                637,
                638,
                639,
                664,
                665,
                666,
                667,
                668,
                669,
                670,
                671,
                696,
                697,
                698,
                699,
                700,
                701,
                702,
                703,
                728,
                729,
                730,
                731,
                732,
                733,
                734,
                735,
                760,
                761,
                762,
                763,
                764,
                765,
                766
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 28,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "af415734-c576-4dde-abf5-5f06fdcdec72",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            47,
            48,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,
            57,
            58,
            59,
            60,
            61,
            62,
            63,
            64,
            65,
            66,
            67,
            68,
            69,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
            79,
            80,
            81,
            82,
            83,
            84,
            85,
            86,
            87,
            88,
            89,
            90,
            91,
            92,
            93,
            94,
            95,
            96,
            97,
            98,
            99,
            100,
            101,
            102,
            103,
            104,
            105,
            106,
            107,
            108,
            109,
            110,
            111,
            112,
            113,
            114,
            115,
            116,
            117,
            118,
            119,
            120,
            121,
            122,
            123,
            124,
            125,
            126,
            127,
            128,
            129,
            130,
            131,
            132,
            133,
            134,
            135,
            136,
            137,
            138,
            139,
            140,
            141,
            142,
            143,
            144,
            145,
            146,
            147,
            148,
            149,
            150,
            151,
            152,
            153,
            154,
            155,
            156,
            157,
            158,
            159,
            160,
            161,
            162,
            163,
            164,
            165,
            166,
            167,
            168,
            169,
            170,
            171,
            172,
            173,
            174,
            175,
            176,
            177,
            178,
            179,
            180,
            181,
            182,
            183,
            184,
            185,
            186,
            187,
            188,
            189,
            190,
            191,
            192,
            193,
            194,
            195,
            196,
            197,
            198,
            199,
            200,
            201,
            202,
            203,
            204,
            205,
            206,
            207,
            208,
            209,
            210,
            211,
            212,
            213,
            214,
            215,
            216,
            217,
            218,
            219,
            220,
            221,
            222,
            223,
            224,
            225,
            226,
            227,
            228,
            229,
            230,
            231,
            232,
            233,
            234,
            235,
            236,
            237,
            238,
            239,
            240,
            241,
            242,
            243,
            244,
            245,
            246,
            247,
            248,
            249,
            250,
            251,
            252,
            253,
            254,
            255,
            256,
            257,
            258,
            259,
            260,
            261,
            262,
            263,
            264,
            265,
            266,
            267,
            268,
            269,
            270,
            271,
            272,
            273,
            274,
            275,
            276,
            277,
            278,
            279,
            280,
            281,
            282,
            283,
            284,
            285,
            286,
            287,
            288,
            289,
            290,
            291,
            292,
            293,
            294,
            295,
            296,
            297,
            298,
            299,
            300,
            301,
            302,
            303,
            304,
            305,
            306,
            307,
            308,
            309,
            310,
            311,
            312,
            313,
            314,
            315,
            316,
            317,
            318,
            319,
            320,
            321,
            322,
            323,
            324,
            325,
            326,
            327,
            328,
            329,
            330,
            331,
            332,
            333,
            334,
            335,
            336,
            337,
            338,
            339,
            340,
            341,
            342,
            343,
            344,
            345,
            346,
            347,
            348,
            349,
            350,
            351,
            352,
            353,
            354,
            355,
            356,
            357,
            358,
            359,
            360,
            361,
            362,
            363,
            364,
            365,
            366,
            367,
            368,
            369,
            370,
            371,
            372,
            373,
            374,
            375,
            376,
            377,
            378,
            379,
            380,
            381,
            382,
            383,
            384,
            385,
            386,
            387,
            388,
            389,
            390,
            391,
            392,
            393,
            394,
            395,
            396,
            397,
            398,
            399,
            400,
            401,
            402,
            403,
            404,
            405,
            406,
            407,
            408,
            409,
            410,
            411,
            412,
            413,
            414,
            415,
            416,
            417,
            418,
            419,
            420,
            421,
            422,
            423,
            424,
            425,
            426,
            427,
            428,
            429,
            430,
            431,
            432,
            433,
            434,
            435,
            436,
            437,
            438,
            439,
            440,
            441,
            442,
            443,
            444,
            445,
            446,
            447,
            448,
            449,
            450,
            451,
            452,
            453,
            454,
            455,
            456,
            457,
            458,
            459,
            460,
            461,
            462,
            463,
            464,
            465,
            466,
            467,
            468,
            469,
            470,
            471,
            472,
            473,
            474,
            475,
            476,
            477,
            478,
            479,
            480,
            481,
            482,
            483,
            484,
            485,
            486,
            487,
            488,
            489,
            490,
            491,
            492,
            493,
            494,
            495,
            496,
            497,
            498,
            499,
            500,
            501,
            502,
            503,
            504,
            505,
            506,
            507,
            508,
            509,
            510,
            511,
            512,
            513,
            514,
            515,
            516,
            517,
            518,
            519,
            520,
            521,
            522,
            523,
            524,
            525,
            526,
            527,
            528,
            529,
            530,
            531,
            532,
            533,
            534,
            535,
            536,
            537,
            538,
            539,
            540,
            541,
            542,
            543,
            544,
            545,
            546,
            547,
            548,
            549,
            550,
            551,
            552,
            553,
            554,
            555,
            556,
            557,
            558,
            559,
            560,
            561,
            562,
            563,
            564,
            565,
            566,
            567,
            568,
            569,
            570,
            571,
            572,
            573,
            574,
            575,
            576,
            577,
            578,
            579,
            580,
            581,
            582,
            583,
            584,
            585,
            586,
            587,
            588,
            589,
            590,
            591,
            592,
            593,
            594,
            595,
            596,
            597,
            598,
            599,
            600,
            601,
            602,
            603,
            604,
            605,
            606,
            607,
            608,
            609,
            610,
            611,
            612,
            613,
            614,
            615,
            616,
            617,
            618,
            619,
            620,
            621,
            622,
            623,
            624,
            625,
            626,
            627,
            628,
            629,
            630,
            631,
            632,
            633,
            634,
            635,
            636,
            637,
            638,
            639,
            640,
            641,
            642,
            643,
            644,
            645,
            646,
            647,
            648,
            649,
            650,
            651,
            652,
            653,
            654,
            655,
            656,
            657,
            658,
            659,
            660,
            661,
            662,
            663,
            664,
            665,
            666,
            667,
            668,
            669,
            670,
            671,
            672,
            673,
            674,
            675,
            676,
            677,
            678,
            679,
            680,
            681,
            682,
            683,
            684,
            685,
            686,
            687,
            688,
            689,
            690,
            691,
            692,
            693,
            694,
            695,
            696,
            697,
            698,
            699,
            700,
            701,
            702,
            703,
            704,
            705,
            706,
            707,
            708,
            709,
            710,
            711,
            712,
            713,
            714,
            715,
            716,
            717,
            718,
            719,
            720,
            721,
            722,
            723,
            724,
            725,
            726,
            727,
            728,
            729,
            730,
            731,
            732,
            733,
            734,
            735,
            736,
            737,
            738,
            739,
            740,
            741,
            742,
            743,
            744,
            745,
            746,
            747,
            748,
            749,
            750,
            751,
            752,
            753,
            754,
            755,
            756,
            757,
            758,
            759,
            760,
            761,
            762,
            763,
            764,
            765,
            766,
            767
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 768,
    "tileheight": 32,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}