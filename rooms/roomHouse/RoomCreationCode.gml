var farm = instance_create_depth(288, 448, -50, objTeleport);
var farm2 = instance_create_depth(320, 448, -50, objTeleport);

farm.teleport = roomFarm;
farm2.teleport = roomFarm;
farm.goX = 224;
farm.goY = 192;
farm2.goX = 224;
farm2.goY = 192;
farm.roomID = ROOM_FARM;
farm2.roomID = ROOM_FARM;