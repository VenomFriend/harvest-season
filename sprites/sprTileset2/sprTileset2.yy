{
    "id": "fb1a06da-a814-4e8e-9cc8-69aaadf1629f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileset2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f9977dad-f296-4334-afea-3d7ce9fa2847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb1a06da-a814-4e8e-9cc8-69aaadf1629f",
            "compositeImage": {
                "id": "923a4b56-b3a1-483b-94d4-25d42a0bbae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9977dad-f296-4334-afea-3d7ce9fa2847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1297aa76-02c1-440c-b089-de2c1f407397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9977dad-f296-4334-afea-3d7ce9fa2847",
                    "LayerId": "a3f2f351-6f85-4164-9842-802a4b83ad38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "a3f2f351-6f85-4164-9842-802a4b83ad38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb1a06da-a814-4e8e-9cc8-69aaadf1629f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}