{
    "id": "67ef1781-f47c-498d-beb2-2bbf3aa4e65d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprExterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ea2d0951-aab1-4e0a-a7a6-604490357175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ef1781-f47c-498d-beb2-2bbf3aa4e65d",
            "compositeImage": {
                "id": "96cc6a89-043c-45ee-9f2e-2a221b99fbb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea2d0951-aab1-4e0a-a7a6-604490357175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd3d00ad-e21a-4c56-94c1-13916d18bb1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea2d0951-aab1-4e0a-a7a6-604490357175",
                    "LayerId": "b05166b4-a178-4f95-ad80-0a89ff5b6891"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "b05166b4-a178-4f95-ad80-0a89ff5b6891",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67ef1781-f47c-498d-beb2-2bbf3aa4e65d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}