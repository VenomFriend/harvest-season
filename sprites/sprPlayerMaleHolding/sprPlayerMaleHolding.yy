{
    "id": "323a66d2-21da-4668-9d78-d73daf0d90f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleHolding",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "03fb1d4f-9c5c-4ca6-b948-aaab0c14fe3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "0576a5a7-ca41-405f-b92e-dfe7b8653bc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03fb1d4f-9c5c-4ca6-b948-aaab0c14fe3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "714f137a-0e26-4345-bf1c-09c80b04b134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fb1d4f-9c5c-4ca6-b948-aaab0c14fe3a",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "16b5c20a-3b31-4658-aaa5-ea671f9272fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fb1d4f-9c5c-4ca6-b948-aaab0c14fe3a",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "57385065-5e3d-48a4-98fa-d5657f1042aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fb1d4f-9c5c-4ca6-b948-aaab0c14fe3a",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "0b664261-c69e-4e10-8894-aab083f3e07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "517c19d7-9ce1-460b-afa6-93a029bc854c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b664261-c69e-4e10-8894-aab083f3e07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "723acc35-6c76-4772-af19-0e039a7640a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b664261-c69e-4e10-8894-aab083f3e07d",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "48cee388-ab2c-46f4-9f1b-fd69cb12049b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b664261-c69e-4e10-8894-aab083f3e07d",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "0d1d8896-0f8d-42a9-8b24-8df06659c424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b664261-c69e-4e10-8894-aab083f3e07d",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "e44d1c5a-6b7e-47c5-a981-ddce8062d501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "f4fd286e-482d-4e99-bd3e-2df918b438be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e44d1c5a-6b7e-47c5-a981-ddce8062d501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5fe817-a1da-445d-b927-bec07cfd8a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44d1c5a-6b7e-47c5-a981-ddce8062d501",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "43e534fa-00f9-4a7f-ba53-9850e8644970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44d1c5a-6b7e-47c5-a981-ddce8062d501",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "8a9533b5-dfd1-46e5-9bce-0918db85cfed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44d1c5a-6b7e-47c5-a981-ddce8062d501",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "fceb2ec2-c199-4033-922a-a7889b862698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "81c25b38-fcb0-4393-acee-962d38341e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fceb2ec2-c199-4033-922a-a7889b862698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7104a2-a34c-4391-a199-0e363e1ff7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceb2ec2-c199-4033-922a-a7889b862698",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "69e3a876-6211-447b-9a05-f7af89808cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceb2ec2-c199-4033-922a-a7889b862698",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "a26a20ae-7ca0-404f-ad5f-d7e8cb2267ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceb2ec2-c199-4033-922a-a7889b862698",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "8e890791-b151-4470-894d-a01920ecf0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "c1dd7dc4-1d2a-48e0-a391-f9ce4e3c1207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e890791-b151-4470-894d-a01920ecf0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aaa5d1f-b56c-4ef7-8cca-834406e91794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e890791-b151-4470-894d-a01920ecf0cd",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "b98f63df-5375-4127-a070-563d87b7ca31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e890791-b151-4470-894d-a01920ecf0cd",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "f92981b7-e45b-4cce-8a7e-7e42ffd1d39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e890791-b151-4470-894d-a01920ecf0cd",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "4e5334c1-59a4-4652-a9ae-346a89ea6931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "a534553d-69b8-4451-a1eb-406c77e650b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e5334c1-59a4-4652-a9ae-346a89ea6931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee352c8-ef7f-4b26-a2b2-00703a057106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e5334c1-59a4-4652-a9ae-346a89ea6931",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "04d49d05-f0d6-4900-aeea-b0625b8b370e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e5334c1-59a4-4652-a9ae-346a89ea6931",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "65ad989c-1de9-4906-8d2b-4282fca55b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e5334c1-59a4-4652-a9ae-346a89ea6931",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "6789e762-f708-4a38-9315-49ceac518173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "d8c97ff8-e79c-433e-a05b-0657fdac236c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6789e762-f708-4a38-9315-49ceac518173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0dd7682-8a85-40c4-a623-0ac2eb932bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6789e762-f708-4a38-9315-49ceac518173",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "6fde89d4-ab0f-463b-894b-181e93f5796e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6789e762-f708-4a38-9315-49ceac518173",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "1efa8ac5-26a3-4a8c-8fc8-cbc05c86ae21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6789e762-f708-4a38-9315-49ceac518173",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "89ec9ddc-f7dd-4f0b-bd36-98d24f8fe43b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "a5aa558d-8dc2-45bf-a203-99eb9d97c19a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ec9ddc-f7dd-4f0b-bd36-98d24f8fe43b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2e92189-e8c4-4ecf-ae5c-e49cdfbf5f9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ec9ddc-f7dd-4f0b-bd36-98d24f8fe43b",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "c07bbb22-99e9-4cb6-aa25-903b65d094a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ec9ddc-f7dd-4f0b-bd36-98d24f8fe43b",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "9dd5298a-2048-417b-860e-36f11e2ac837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ec9ddc-f7dd-4f0b-bd36-98d24f8fe43b",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "2e45b4e5-6909-4213-a764-e0911504fa38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "7d4f159a-5526-4d86-99cc-1042e1afea45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e45b4e5-6909-4213-a764-e0911504fa38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32ce54d-1c8c-4746-bff0-34b503a94447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e45b4e5-6909-4213-a764-e0911504fa38",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "acd2d3f0-ac4f-4a77-a9c0-f35f8ec7e450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e45b4e5-6909-4213-a764-e0911504fa38",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "a03d8f04-d790-46c4-978a-f933253da6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e45b4e5-6909-4213-a764-e0911504fa38",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "c020f73e-6d83-46c3-a76f-24298fae414e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "9b02ef44-2440-4c1a-89f8-62f1d4d90d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c020f73e-6d83-46c3-a76f-24298fae414e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a9d8d5d-4f42-49e9-ab12-a18d0aef43f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c020f73e-6d83-46c3-a76f-24298fae414e",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "fcdb5a55-701b-4b77-934a-2a9341a90b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c020f73e-6d83-46c3-a76f-24298fae414e",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "6e92b208-f618-408a-a059-937c2846adc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c020f73e-6d83-46c3-a76f-24298fae414e",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "ec74307e-ffad-4a30-98ec-cb156998bd9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "f3efdb2e-9c6b-4010-af95-78b93868e27a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec74307e-ffad-4a30-98ec-cb156998bd9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e266778-6fd7-4734-a169-266dcbdd16c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec74307e-ffad-4a30-98ec-cb156998bd9c",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "0d2b019a-6d24-4755-8f86-94727129a1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec74307e-ffad-4a30-98ec-cb156998bd9c",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "f76f8394-365c-4434-9be2-b901e78a6a02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec74307e-ffad-4a30-98ec-cb156998bd9c",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "dce92d0a-3824-4a2b-afe0-c6a82aa45c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "6f931f65-f9f4-4985-9443-c0926361690c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce92d0a-3824-4a2b-afe0-c6a82aa45c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164a7ee4-e783-4e77-be0c-147db82bd4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce92d0a-3824-4a2b-afe0-c6a82aa45c20",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "6dd73b77-30f3-4180-91ab-7525ab133363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce92d0a-3824-4a2b-afe0-c6a82aa45c20",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "e9654096-0269-43ea-9dc1-17f84cf0b084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce92d0a-3824-4a2b-afe0-c6a82aa45c20",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "7b7e05e8-62c3-40c7-953d-e543b6503e6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "349c6ede-933a-459b-a3fe-46b9adc8c5c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7e05e8-62c3-40c7-953d-e543b6503e6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3000dcbf-e56b-4c62-8cdc-645e20301af7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7e05e8-62c3-40c7-953d-e543b6503e6e",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "df43ce37-b313-4749-8c02-a06a8c820ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7e05e8-62c3-40c7-953d-e543b6503e6e",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "96697626-ac98-40fd-ba0d-b85b8f0f1c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7e05e8-62c3-40c7-953d-e543b6503e6e",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "4fc56591-fdd9-4e5c-b508-4dc3894ab705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "e4bd46b3-6a9c-4a3b-93fc-a7e8ce0c90db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fc56591-fdd9-4e5c-b508-4dc3894ab705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a391dc-a945-4c1a-8056-0400850a781c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc56591-fdd9-4e5c-b508-4dc3894ab705",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "58c97895-c0e5-475e-b563-9600ddfe5bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc56591-fdd9-4e5c-b508-4dc3894ab705",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "1f9fa0cb-d2f6-4799-8949-3005977a1072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc56591-fdd9-4e5c-b508-4dc3894ab705",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "b1285ee7-46e7-44ff-b722-59b30f015156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "dce314ae-6e7d-466c-8ac7-1637e63f3338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1285ee7-46e7-44ff-b722-59b30f015156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53733446-f32f-462c-b602-052770fdfb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1285ee7-46e7-44ff-b722-59b30f015156",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "9497edc6-497a-4fe7-9541-00206b811cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1285ee7-46e7-44ff-b722-59b30f015156",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "776e19d0-9e7e-421e-9a3f-aae1dcc9d2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1285ee7-46e7-44ff-b722-59b30f015156",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        },
        {
            "id": "e1f7c8de-da13-4d83-9308-1f64feaa2807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "compositeImage": {
                "id": "ab3b99e8-f845-4cdf-b048-6497b710e472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f7c8de-da13-4d83-9308-1f64feaa2807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60cd2383-8730-4e6b-9101-eea2d9f49997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f7c8de-da13-4d83-9308-1f64feaa2807",
                    "LayerId": "976f1020-26b0-44bb-b51a-9e73506d08fa"
                },
                {
                    "id": "396ef3b6-90d2-44ae-8c1a-1299f9c26293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f7c8de-da13-4d83-9308-1f64feaa2807",
                    "LayerId": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904"
                },
                {
                    "id": "90705130-adc9-4f4a-bd41-11629616a8e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f7c8de-da13-4d83-9308-1f64feaa2807",
                    "LayerId": "ebd4b02c-066b-45fa-a943-981d2ab80434"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 60,
    "layers": [
        {
            "id": "976f1020-26b0-44bb-b51a-9e73506d08fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e9fda4ea-2cf1-4d03-b7fa-38e004be0904",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ebd4b02c-066b-45fa-a943-981d2ab80434",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "323a66d2-21da-4668-9d78-d73daf0d90f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}