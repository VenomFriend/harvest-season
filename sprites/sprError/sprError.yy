{
    "id": "0e480cf8-590a-4eba-bcf0-a20025eca91f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprError",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 358,
    "bbox_right": 398,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ff5e406b-8867-454f-833c-7038c5628cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e480cf8-590a-4eba-bcf0-a20025eca91f",
            "compositeImage": {
                "id": "214af57f-95b8-4c35-931e-d1cd41a4b0a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5e406b-8867-454f-833c-7038c5628cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6edb6e8-592d-464b-aee1-c436cf22209e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5e406b-8867-454f-833c-7038c5628cee",
                    "LayerId": "ebb3f4a4-3a66-46ae-9cfc-ffee208ba815"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "ebb3f4a4-3a66-46ae-9cfc-ffee208ba815",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e480cf8-590a-4eba-bcf0-a20025eca91f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 399,
    "xorig": 199,
    "yorig": 0
}