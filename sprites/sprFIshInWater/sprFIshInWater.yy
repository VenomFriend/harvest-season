{
    "id": "2d74dfc7-162a-4da1-9725-37adbfc62267",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFIshInWater",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0fb8cd50-fee7-4079-9cbb-76f15f50e78f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d74dfc7-162a-4da1-9725-37adbfc62267",
            "compositeImage": {
                "id": "0b79bc7a-a2e6-481c-96a4-c9924aaedb32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb8cd50-fee7-4079-9cbb-76f15f50e78f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bd075e5-4c8c-4b27-8903-95575dcb47e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb8cd50-fee7-4079-9cbb-76f15f50e78f",
                    "LayerId": "4bd14636-b2e7-4c6c-a07a-ba5804c273ee"
                }
            ]
        },
        {
            "id": "dbc0f57a-98c1-45a1-8385-aad3539673f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d74dfc7-162a-4da1-9725-37adbfc62267",
            "compositeImage": {
                "id": "364f9771-4cd6-459e-99f8-8123c4447798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbc0f57a-98c1-45a1-8385-aad3539673f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18867ce2-d4d4-469f-97e9-291b9418f61a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbc0f57a-98c1-45a1-8385-aad3539673f9",
                    "LayerId": "4bd14636-b2e7-4c6c-a07a-ba5804c273ee"
                }
            ]
        },
        {
            "id": "cd0e20ee-58a0-485b-9ce3-23c85e7c9f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d74dfc7-162a-4da1-9725-37adbfc62267",
            "compositeImage": {
                "id": "bd93c001-1b73-49f4-8e1c-1998c1539890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0e20ee-58a0-485b-9ce3-23c85e7c9f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf3f719-6de1-4050-8da4-4daf96a09143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0e20ee-58a0-485b-9ce3-23c85e7c9f96",
                    "LayerId": "4bd14636-b2e7-4c6c-a07a-ba5804c273ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4bd14636-b2e7-4c6c-a07a-ba5804c273ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d74dfc7-162a-4da1-9725-37adbfc62267",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}