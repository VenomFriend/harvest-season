{
    "id": "af415734-c576-4dde-abf5-5f06fdcdec72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileset1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "60ba25a1-8bf9-4b7f-81d7-2fbdce86d3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af415734-c576-4dde-abf5-5f06fdcdec72",
            "compositeImage": {
                "id": "4de8d471-afc1-47f4-b383-ab475d5c9223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60ba25a1-8bf9-4b7f-81d7-2fbdce86d3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af12ca5-ffea-4306-98f9-395f363c47af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60ba25a1-8bf9-4b7f-81d7-2fbdce86d3d0",
                    "LayerId": "09c0a744-8193-4db9-b016-6cbe506e9b99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "09c0a744-8193-4db9-b016-6cbe506e9b99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af415734-c576-4dde-abf5-5f06fdcdec72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}