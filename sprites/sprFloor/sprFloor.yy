{
    "id": "fa544267-2229-4f33-8aac-a1d269725f71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f8461582-1429-4de8-8f53-7d55d4d05065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa544267-2229-4f33-8aac-a1d269725f71",
            "compositeImage": {
                "id": "ff4b4e8b-0dce-42b1-830d-00cf7920a823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8461582-1429-4de8-8f53-7d55d4d05065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d4d5b08-4d83-4d3b-bf77-5a63066acdf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8461582-1429-4de8-8f53-7d55d4d05065",
                    "LayerId": "c2b4f58e-f28a-43a2-9808-ea03949a88a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c2b4f58e-f28a-43a2-9808-ea03949a88a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa544267-2229-4f33-8aac-a1d269725f71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}