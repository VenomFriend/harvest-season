{
    "id": "109e1067-b2e5-4bc9-a8ab-2de98abccf9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWalls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 287,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "279a2f6a-86e4-4138-a19b-eeb66b8763e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "109e1067-b2e5-4bc9-a8ab-2de98abccf9f",
            "compositeImage": {
                "id": "07156db8-17b5-4126-b695-a1af0d7ab66c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "279a2f6a-86e4-4138-a19b-eeb66b8763e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823cbfc0-7edc-45f3-b273-9593557e7dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "279a2f6a-86e4-4138-a19b-eeb66b8763e8",
                    "LayerId": "a18c0beb-4e36-451a-94ab-d32217ae3103"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "a18c0beb-4e36-451a-94ab-d32217ae3103",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "109e1067-b2e5-4bc9-a8ab-2de98abccf9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}