{
    "id": "1039f5aa-6350-4204-9e94-e14fdbc8092c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprQuickslot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "395c3f34-1d59-4077-a94d-abd699708761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1039f5aa-6350-4204-9e94-e14fdbc8092c",
            "compositeImage": {
                "id": "71c69376-d82d-4277-8a21-a143bb0110df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "395c3f34-1d59-4077-a94d-abd699708761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b313d15b-243a-4749-8484-5030591b1213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "395c3f34-1d59-4077-a94d-abd699708761",
                    "LayerId": "354a6af1-52aa-44bf-b18d-bc624787e7a6"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 28,
    "layers": [
        {
            "id": "354a6af1-52aa-44bf-b18d-bc624787e7a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1039f5aa-6350-4204-9e94-e14fdbc8092c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 0,
    "yorig": 0
}