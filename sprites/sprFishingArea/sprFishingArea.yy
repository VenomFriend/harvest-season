{
    "id": "2c6abc06-8630-409f-8c31-dd854845e0df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFishingArea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9e923340-6215-46f5-9c7a-128365ea159d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c6abc06-8630-409f-8c31-dd854845e0df",
            "compositeImage": {
                "id": "1a6d87bf-8433-41dd-aa9e-e67958d9f2ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e923340-6215-46f5-9c7a-128365ea159d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0234bcf7-1915-45fc-819d-c196f40c457a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e923340-6215-46f5-9c7a-128365ea159d",
                    "LayerId": "dcca72c6-8016-4905-a9a3-78970e274b22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dcca72c6-8016-4905-a9a3-78970e274b22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c6abc06-8630-409f-8c31-dd854845e0df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}