{
    "id": "b8dd863b-1936-4784-a1b2-791f6035309c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleWalking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "797ff44c-4c9b-49a5-b795-057238e48418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "fc35ea27-16c6-48f2-9045-fe6ab1963a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797ff44c-4c9b-49a5-b795-057238e48418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9dfe98-c5c8-4d70-bed7-4df410465c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797ff44c-4c9b-49a5-b795-057238e48418",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "d29c8c58-3626-402e-a652-cbb96e1496ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "71feb040-0bd5-4ef1-a343-fcfb435a8117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29c8c58-3626-402e-a652-cbb96e1496ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702df106-a7cd-48c3-bb8d-00445b7ed2d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29c8c58-3626-402e-a652-cbb96e1496ca",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "abb1d994-7386-410d-a614-ef518a7fcb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "65273317-978e-47c6-b3c8-dc8a30778d44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb1d994-7386-410d-a614-ef518a7fcb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db30faf0-eedf-4c6e-b847-1cc7c3a5b947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb1d994-7386-410d-a614-ef518a7fcb46",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "ee6b851c-c73d-4c97-abc0-5708c9ceb978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "c7c83426-ca13-496d-95ee-bd33fc020fbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee6b851c-c73d-4c97-abc0-5708c9ceb978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6743be4a-9c43-4528-866b-99f3bbb1bdb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee6b851c-c73d-4c97-abc0-5708c9ceb978",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "6724c3f7-87e9-46f4-91ba-32ebe1d71a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "0994acbb-c405-4896-a96b-3012cdfd6b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6724c3f7-87e9-46f4-91ba-32ebe1d71a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e684a7-f710-4ce2-801b-12cdc8649c90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6724c3f7-87e9-46f4-91ba-32ebe1d71a7f",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "992d1c71-19b7-4729-9c11-442697bbe957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "550e0c49-6e52-4964-893f-068415b6e160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992d1c71-19b7-4729-9c11-442697bbe957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dedb168-63ec-4434-8497-427e9084197c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992d1c71-19b7-4729-9c11-442697bbe957",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "87b5f005-bce7-48de-ad7f-3fbe850db3b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "9a6c4679-f8b3-457d-88ab-1e97b61281d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b5f005-bce7-48de-ad7f-3fbe850db3b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b1a44b-3073-43c2-a298-c17569027f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b5f005-bce7-48de-ad7f-3fbe850db3b5",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "8aa442db-a9fa-4724-a6e2-af5ad1e292a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "66a86e22-496b-4e8d-a476-5001056baf53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa442db-a9fa-4724-a6e2-af5ad1e292a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4b9ba0b-0e39-495e-b5ff-03b534334ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa442db-a9fa-4724-a6e2-af5ad1e292a8",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "fc1760df-8d3c-4c60-a28e-9096896d9841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "85e0e07b-b241-4c9b-9fa6-e54c05bb251d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc1760df-8d3c-4c60-a28e-9096896d9841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1060f371-3b49-4d36-a0a1-2b7b29eab5af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc1760df-8d3c-4c60-a28e-9096896d9841",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "b26a3e23-5be0-43b2-ba2f-8985049c0b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "fffcced9-13ce-47b0-b2de-131bc6ddd300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b26a3e23-5be0-43b2-ba2f-8985049c0b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74441718-7df5-4e68-9638-1b0d68ae7f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b26a3e23-5be0-43b2-ba2f-8985049c0b30",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "ffba0187-f14e-44cb-bc71-f8a4bd9c3532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "c1e47ec8-2204-4d69-bf1c-ddc7e9f590c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffba0187-f14e-44cb-bc71-f8a4bd9c3532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1f24a6-f4fa-4256-9358-41aded74c0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffba0187-f14e-44cb-bc71-f8a4bd9c3532",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "0d320073-4dad-4161-8f02-f43a51adee6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "3a6d47c9-3906-4c3c-9121-71a695a1e29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d320073-4dad-4161-8f02-f43a51adee6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb6e377-f4fe-4358-8bcd-0ca1f11dbb9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d320073-4dad-4161-8f02-f43a51adee6b",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "ed936133-c38f-4cac-83ae-3fbce6a6f24f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "aa1f4dc7-da1c-4d5a-a7d4-717c6eab4825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed936133-c38f-4cac-83ae-3fbce6a6f24f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a99596f7-e15f-485a-82b6-2bc03a4ea9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed936133-c38f-4cac-83ae-3fbce6a6f24f",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "68cb3ac1-78d0-4b6e-93fe-03370ba81d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "518164b4-4f10-4075-a7f6-6906d6083b93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68cb3ac1-78d0-4b6e-93fe-03370ba81d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41b504a-346a-4cc3-bc33-34b893ab3c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68cb3ac1-78d0-4b6e-93fe-03370ba81d85",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "2785ee61-7362-4622-b356-65acaabd0cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "328b71ed-5180-4218-add6-730be463325f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2785ee61-7362-4622-b356-65acaabd0cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0600cb89-e84d-41f1-8c08-c71479d238d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2785ee61-7362-4622-b356-65acaabd0cda",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        },
        {
            "id": "e8c22df6-f454-4a13-af16-d92dd6362b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "compositeImage": {
                "id": "88c488a1-5561-4693-9363-3c196d2efb69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8c22df6-f454-4a13-af16-d92dd6362b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "074151e9-ca02-4902-a964-d6995d062c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8c22df6-f454-4a13-af16-d92dd6362b7d",
                    "LayerId": "5b99c148-a24b-4633-bef8-5f5248d8bcda"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 60,
    "layers": [
        {
            "id": "5b99c148-a24b-4633-bef8-5f5248d8bcda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8dd863b-1936-4784-a1b2-791f6035309c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}