{
    "id": "bf92f4e9-c734-4ab7-958f-018cd7eda3e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprExterior2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 543,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "64c95937-f3c8-49da-9916-e46888c465cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf92f4e9-c734-4ab7-958f-018cd7eda3e5",
            "compositeImage": {
                "id": "005a74cc-b8f0-443e-a040-5a4b69ff7a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c95937-f3c8-49da-9916-e46888c465cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d883b9-3051-49af-96cf-29781f2307cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c95937-f3c8-49da-9916-e46888c465cd",
                    "LayerId": "80519347-7d3e-475d-9d3e-7bcf05c926b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 544,
    "layers": [
        {
            "id": "80519347-7d3e-475d-9d3e-7bcf05c926b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf92f4e9-c734-4ab7-958f-018cd7eda3e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}