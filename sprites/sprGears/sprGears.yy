{
    "id": "19d2580c-ae98-4847-9987-68838c1ac226",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGears",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "726ade54-a189-4dda-952e-25c49afa942a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "dcece5e4-4a7c-4837-9f2c-cf731ee9c68c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "726ade54-a189-4dda-952e-25c49afa942a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed43c20-2e28-46ac-985d-419205516f7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "726ade54-a189-4dda-952e-25c49afa942a",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "2c44e4f0-c394-43cd-819c-488dd6e9bddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "0fd111e9-2df6-4ee3-8b89-076952cc8446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c44e4f0-c394-43cd-819c-488dd6e9bddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "516fb2a5-76d4-4240-8fcd-3678c745cf45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c44e4f0-c394-43cd-819c-488dd6e9bddc",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "a2f8ad14-ba27-499a-b84b-99dbc35ab33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "32d629fd-e2ba-4134-9ca6-cf5891bc0a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f8ad14-ba27-499a-b84b-99dbc35ab33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "436154e9-4727-4e6d-83a7-adeb1cbf89c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f8ad14-ba27-499a-b84b-99dbc35ab33c",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "59e90f2a-cb5d-4b05-9916-1b92f78ab9a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "89a27365-9277-4674-8dd7-03f4fbeb9d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59e90f2a-cb5d-4b05-9916-1b92f78ab9a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8875ce31-64c1-4f29-9fb1-a2b563227542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59e90f2a-cb5d-4b05-9916-1b92f78ab9a0",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "094db0ad-a9a4-4e8e-8fcb-d63f76f85353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "c5f1a850-48ad-406a-8049-e4116422b02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "094db0ad-a9a4-4e8e-8fcb-d63f76f85353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7cc57d-fe3c-4589-a762-a2628b88d863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "094db0ad-a9a4-4e8e-8fcb-d63f76f85353",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "3e15a517-1319-4ca1-93c4-704613812252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "c48b4743-8ee4-4eb0-a3b3-f5013ce482ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e15a517-1319-4ca1-93c4-704613812252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0bf7480-e1c9-4659-b131-edc992a0f755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e15a517-1319-4ca1-93c4-704613812252",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "13a86007-adcb-40c6-ad75-cabc5fe6322a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "9070e6c1-d262-4f21-8627-c43b3fa9e929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a86007-adcb-40c6-ad75-cabc5fe6322a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2469342-45c6-4130-be9e-0698dda718ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a86007-adcb-40c6-ad75-cabc5fe6322a",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        },
        {
            "id": "5dd9ac47-5056-4e78-9e98-881ad7a054cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "compositeImage": {
                "id": "a4e04184-8129-439c-8bb1-42333c6903da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd9ac47-5056-4e78-9e98-881ad7a054cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9947cc82-6f3a-489b-9dae-9162f8313fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd9ac47-5056-4e78-9e98-881ad7a054cd",
                    "LayerId": "a125f44d-e77a-4120-be3d-299258737c8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a125f44d-e77a-4120-be3d-299258737c8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19d2580c-ae98-4847-9987-68838c1ac226",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}