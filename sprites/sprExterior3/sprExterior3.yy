{
    "id": "2098ab4e-18e8-4232-9fad-8ac9b65637ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprExterior3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 543,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4526ff04-0828-4750-a42b-b8a6686de73b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2098ab4e-18e8-4232-9fad-8ac9b65637ef",
            "compositeImage": {
                "id": "5baa2930-0a1b-4c10-a712-c8e82b627df3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4526ff04-0828-4750-a42b-b8a6686de73b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42cba387-7e3f-4a0e-a09e-a8c7ce6a850c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4526ff04-0828-4750-a42b-b8a6686de73b",
                    "LayerId": "39702f02-d0ce-4976-9061-69d3686b1788"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 544,
    "layers": [
        {
            "id": "39702f02-d0ce-4976-9061-69d3686b1788",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2098ab4e-18e8-4232-9fad-8ac9b65637ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}