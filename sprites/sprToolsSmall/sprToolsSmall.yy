{
    "id": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprToolsSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d378a3be-d9ed-405c-a0bf-ab62593cd465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "40145373-df57-4e29-b322-7ffd11c1c341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d378a3be-d9ed-405c-a0bf-ab62593cd465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047a8e4d-30b6-4624-ae53-aad86fe706b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d378a3be-d9ed-405c-a0bf-ab62593cd465",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "a971a4fc-02c0-4bfa-8200-e674247bc4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7e3dee29-d8bb-4cd1-a1c2-096833663abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a971a4fc-02c0-4bfa-8200-e674247bc4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "989ff992-9a87-4bdd-a5fa-7c34df06057a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a971a4fc-02c0-4bfa-8200-e674247bc4c9",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "264a1b32-8c67-422f-95d6-30b3bee1240c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e8748f96-a185-4029-ba6d-956409ffbd93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "264a1b32-8c67-422f-95d6-30b3bee1240c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc69482-5f78-40af-81c4-152ff64ea2dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264a1b32-8c67-422f-95d6-30b3bee1240c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e0189a35-fa54-4582-8efc-01cdd83dfab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "0c568d76-a614-4c6d-9d43-a91e1576f977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0189a35-fa54-4582-8efc-01cdd83dfab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63720cbd-3a83-452a-9989-ac21651177e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0189a35-fa54-4582-8efc-01cdd83dfab9",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "bca842a3-40cf-4d3d-b6e9-f6b446273a52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b19188bc-baa4-45c7-8422-ccd14a4872d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca842a3-40cf-4d3d-b6e9-f6b446273a52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57fb3779-0c4b-4e46-9e5e-e56d86183772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca842a3-40cf-4d3d-b6e9-f6b446273a52",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0dbada01-6a9a-400d-b98c-4cd9622aec23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "dc370137-953d-42cb-b2d5-ec3be049d7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dbada01-6a9a-400d-b98c-4cd9622aec23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94414c51-1bd6-45ea-8cdd-b6c029ae4b73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dbada01-6a9a-400d-b98c-4cd9622aec23",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "148d07e4-04d0-420d-899c-39a574c1481f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "3e36cc52-3904-4f6a-a941-975a81057b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148d07e4-04d0-420d-899c-39a574c1481f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3494f5-5ff0-4257-93b6-a9c4a83051a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148d07e4-04d0-420d-899c-39a574c1481f",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ec098427-8b89-4b7d-8053-f9458b7dc29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4d773ded-7b5d-4a3a-a167-4405e0265dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec098427-8b89-4b7d-8053-f9458b7dc29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfb9ca1a-0339-4bd0-b5bb-792fb9f49e79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec098427-8b89-4b7d-8053-f9458b7dc29b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "43ee3a42-d4e4-48b4-b53d-66f507b102a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d263bc78-0048-45ab-a05f-65e9017a2c53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ee3a42-d4e4-48b4-b53d-66f507b102a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa2df7b7-7ae5-4c9d-96c0-5666fffda537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ee3a42-d4e4-48b4-b53d-66f507b102a4",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "1381f83f-724d-46b8-8479-56402b3ac910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "82b28822-0835-4312-9563-6ec2a6e42387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1381f83f-724d-46b8-8479-56402b3ac910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7160dbfe-0c3d-413c-aa74-6816c5e0a757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1381f83f-724d-46b8-8479-56402b3ac910",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7d422f37-d7e6-4a6e-8b0f-227a8c47b52d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "be531ae5-1440-43ea-9740-21f20145dbf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d422f37-d7e6-4a6e-8b0f-227a8c47b52d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b68f977-4ab6-4436-b1b6-ad2024793414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d422f37-d7e6-4a6e-8b0f-227a8c47b52d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "6d2b79d0-3e6a-45a0-bab6-ffe32d3b5726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "841ef7ee-d6b7-4b79-8fb9-e31cdb21fea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d2b79d0-3e6a-45a0-bab6-ffe32d3b5726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf11f99-428e-4dd7-ac74-f2c262827b2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d2b79d0-3e6a-45a0-bab6-ffe32d3b5726",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "f83c1e30-3c92-4b32-9fee-24a00f27ae4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "032fdd55-6aaf-4424-8259-2acf0b87e47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83c1e30-3c92-4b32-9fee-24a00f27ae4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6f9637-b275-4667-b136-030a4a2b6e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83c1e30-3c92-4b32-9fee-24a00f27ae4c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "f8321930-bbba-4f8e-bcf0-06b18940ce68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "753e6855-4260-4e80-b24d-9d4aee5a71f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8321930-bbba-4f8e-bcf0-06b18940ce68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b21b7f-2ee6-437d-a276-2c55fe944dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8321930-bbba-4f8e-bcf0-06b18940ce68",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "66ab9b85-9a93-441b-a885-2dc78c367125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "6c06c388-5bcb-4bcb-b6c5-a561fe524bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66ab9b85-9a93-441b-a885-2dc78c367125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acca3c35-923a-4f47-a1b8-3b4f6f7d6755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66ab9b85-9a93-441b-a885-2dc78c367125",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7ab57453-fc3e-41ec-8e27-140c433879b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "006c8f4d-1b94-4653-982d-c3c0e44222ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab57453-fc3e-41ec-8e27-140c433879b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d81d2c-6313-4572-b271-410edb270cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab57453-fc3e-41ec-8e27-140c433879b6",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9535bbfe-c814-4d68-9bbe-63194a6f6ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "3e74523b-18c1-4648-9b61-6f7741b130e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9535bbfe-c814-4d68-9bbe-63194a6f6ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82139b5-1980-4604-b4cc-ca89c040f7b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9535bbfe-c814-4d68-9bbe-63194a6f6ca2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "bae20d5f-49e4-4d7c-8d7b-91e04c1ddbd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "dfca7dbf-6a62-40b8-af99-dd97311c26db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae20d5f-49e4-4d7c-8d7b-91e04c1ddbd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5006b508-e3d5-45d1-b6d3-ae90c1150a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae20d5f-49e4-4d7c-8d7b-91e04c1ddbd1",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e5556df9-590f-4274-96e9-3e768a89fbf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "bcac59a9-1576-4f9b-96ef-bf4395cb88ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5556df9-590f-4274-96e9-3e768a89fbf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "558c37bf-5ceb-4498-ac06-98ce2d6cc737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5556df9-590f-4274-96e9-3e768a89fbf8",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c439a6d1-28ef-434f-a892-4820b64d855b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "17327b60-0990-4399-8182-ca898e77b655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c439a6d1-28ef-434f-a892-4820b64d855b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f335536e-02dd-48b7-a16a-80143352a4b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c439a6d1-28ef-434f-a892-4820b64d855b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9d88b0f0-3ff9-456c-9867-b03b31696031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "3a812d23-4375-4b0d-a6c1-6d85f90dd4eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d88b0f0-3ff9-456c-9867-b03b31696031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03277f1c-bc2e-481a-b591-e9f3f65b002a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d88b0f0-3ff9-456c-9867-b03b31696031",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0f98f288-13ec-4010-a918-f095ffa58e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "681ea5b0-715d-4896-84a0-e77431121fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f98f288-13ec-4010-a918-f095ffa58e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb03946-a709-4b5d-951b-4dfaf6b3241a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f98f288-13ec-4010-a918-f095ffa58e40",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "26dd8f64-c519-4966-a7b9-fc7168c450f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "bce9cee8-d93c-4cf0-8231-1d9de0f632ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26dd8f64-c519-4966-a7b9-fc7168c450f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "470dcd91-adb0-4201-a468-e053b4c7e397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26dd8f64-c519-4966-a7b9-fc7168c450f0",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e4c83754-7e2c-42b1-8c86-61cefa8d88eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "f7ea5a9d-98a6-4bdd-a1ad-1894b5d734e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4c83754-7e2c-42b1-8c86-61cefa8d88eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ac8271-6033-4d3a-81b3-5a1f5e6031d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4c83754-7e2c-42b1-8c86-61cefa8d88eb",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d440902e-0118-44d2-89ee-1b021236ee92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c90f998f-d601-42ee-b1ff-c83aadfa89ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d440902e-0118-44d2-89ee-1b021236ee92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e6881c-441f-466b-b17f-b62587e3a542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d440902e-0118-44d2-89ee-1b021236ee92",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "cadb535e-84f7-4468-a03a-fab33b2bc40d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a5b014c4-802c-4d0a-8acf-27e35291fc08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cadb535e-84f7-4468-a03a-fab33b2bc40d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0830d41-bb86-45e3-9535-a9ab8702293e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cadb535e-84f7-4468-a03a-fab33b2bc40d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "6ca928fa-d871-47f8-9263-e2b584dac628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b804e330-27d0-420b-8042-1afe2d1c4f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca928fa-d871-47f8-9263-e2b584dac628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4893188e-a053-4924-b8f8-cc5dd0ecf6f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca928fa-d871-47f8-9263-e2b584dac628",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7bd7d09c-4df9-4d72-bc14-1f19e1deb088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "230fe30a-d0ad-4fa2-a9f0-096aac313b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bd7d09c-4df9-4d72-bc14-1f19e1deb088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c913626d-4e62-433f-a581-d27aec6bb053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bd7d09c-4df9-4d72-bc14-1f19e1deb088",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "314d1045-a84f-481a-a65b-c44fa3701ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "57fb3720-ee49-4c24-9bef-8ada3aa2699f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "314d1045-a84f-481a-a65b-c44fa3701ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbc0a74-35a2-4f1a-a062-fe04d52e1454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "314d1045-a84f-481a-a65b-c44fa3701ad6",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "13a755e3-08d1-4253-85ea-2bc08d526913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a1421c6e-6715-457d-ada4-3ab9bb2bf15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a755e3-08d1-4253-85ea-2bc08d526913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "349ea799-abb8-4756-afdf-00d70eee0114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a755e3-08d1-4253-85ea-2bc08d526913",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9fb9cbd0-73b2-46c3-95e0-19842b4cf44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "fd023cb0-a53d-494a-9a8a-ac1d9cb410d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb9cbd0-73b2-46c3-95e0-19842b4cf44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f299de5-f8c3-4caf-999a-ebf25f872ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb9cbd0-73b2-46c3-95e0-19842b4cf44b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0da81851-5c08-4910-ad41-b4c6b50d65a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "bbbed9c3-a6e2-4125-ace6-1811a42bcae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0da81851-5c08-4910-ad41-b4c6b50d65a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44866eae-b10c-4d5c-96d7-6f4edb9a5e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da81851-5c08-4910-ad41-b4c6b50d65a0",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3f3960ac-ee5c-4a7e-879f-03826dee3066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "77099300-9a1b-4d6e-ab13-bf7b6fb086cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3960ac-ee5c-4a7e-879f-03826dee3066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad78ff80-b547-4e6f-8375-3317c47d7669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3960ac-ee5c-4a7e-879f-03826dee3066",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d00de509-f101-4c17-9f4f-67bee13d788a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a7514153-f452-462a-a952-529970baaaf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d00de509-f101-4c17-9f4f-67bee13d788a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "055e3b12-e66c-4a69-8a78-0768f32dbd7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d00de509-f101-4c17-9f4f-67bee13d788a",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "829cfeed-2204-4786-be49-20c2290ee593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "244eea17-67a0-4bb6-86eb-d23f4bb52007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "829cfeed-2204-4786-be49-20c2290ee593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "736e7b75-2fda-4d7d-aec4-9c9492465f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "829cfeed-2204-4786-be49-20c2290ee593",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3733ca7e-6c77-463e-9c98-66067652b8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "15802f96-54be-47df-b85c-f43fa02b8e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3733ca7e-6c77-463e-9c98-66067652b8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f174b642-54ce-4e95-a866-8fa5784c7578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3733ca7e-6c77-463e-9c98-66067652b8f7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3ac2d354-0abe-4cdf-b9e9-bc9a3f86c6c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "48c7ecf8-19c4-43a1-bdd7-fcf37de22e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ac2d354-0abe-4cdf-b9e9-bc9a3f86c6c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6e858e9-edc8-45f5-97ef-98b27a383ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ac2d354-0abe-4cdf-b9e9-bc9a3f86c6c0",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ec9c154a-57dc-46b2-93cb-f30fbd1f3240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "037d0cb2-21c2-41b5-b355-f862fa05ef39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9c154a-57dc-46b2-93cb-f30fbd1f3240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a730d3-fae2-4284-8d9d-7790fa93c8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9c154a-57dc-46b2-93cb-f30fbd1f3240",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "44d7e1c6-6718-4de9-bdb3-47ad2521a04c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "fe7eb189-91f6-4e40-9ee5-99270cb9a4c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d7e1c6-6718-4de9-bdb3-47ad2521a04c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce00962-e765-40d6-955f-0d823ff50d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d7e1c6-6718-4de9-bdb3-47ad2521a04c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "a31445d9-bd0e-4acb-ab48-8ed434f4ce03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e7385c0e-2385-42a8-a80b-ec7dc72e8dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31445d9-bd0e-4acb-ab48-8ed434f4ce03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c348349d-e7d5-4795-a010-4df152777ed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31445d9-bd0e-4acb-ab48-8ed434f4ce03",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2c89b2b0-83b8-44b8-8b5b-706f04f95607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "073aeba0-5011-4559-a017-6ac34cae66e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c89b2b0-83b8-44b8-8b5b-706f04f95607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "762ac89e-3117-48b6-a104-90cafa20db73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c89b2b0-83b8-44b8-8b5b-706f04f95607",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3f0602c4-356b-494d-a74b-dcf2d9412861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "551838c1-3d5d-4c54-80a6-a2b5d1fe9538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0602c4-356b-494d-a74b-dcf2d9412861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1385e38-a827-4fc7-a3af-32fa28f0f6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0602c4-356b-494d-a74b-dcf2d9412861",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "62e41ca8-66a5-4bfc-983a-a4a09c4410fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "abfa4db5-02a0-4eef-ac79-a6c00f03ab8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e41ca8-66a5-4bfc-983a-a4a09c4410fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04605003-044a-4dd2-aefc-5f5de7f019cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e41ca8-66a5-4bfc-983a-a4a09c4410fa",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "78760e7d-aec3-42c1-a140-ee91dc1fdf56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b7327be2-eb05-4134-8690-acd11a9ef5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78760e7d-aec3-42c1-a140-ee91dc1fdf56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62d0e5f-39e0-49bd-a196-470d2b6fbca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78760e7d-aec3-42c1-a140-ee91dc1fdf56",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ca35b4d6-acf5-4fc5-a477-943c6eae1f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e1c7c627-b737-4ad9-b79f-3a38bff98d0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca35b4d6-acf5-4fc5-a477-943c6eae1f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41508c6-6740-4615-befe-094f285fbdc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca35b4d6-acf5-4fc5-a477-943c6eae1f1c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e52d0adc-65eb-42b5-821a-fb5795e330e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d794b322-ad84-42f8-8441-fbffea0c6001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e52d0adc-65eb-42b5-821a-fb5795e330e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a22a9d-83d0-4e1d-b863-ecb43ddb42d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e52d0adc-65eb-42b5-821a-fb5795e330e4",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d5151a2c-ac1f-4377-8fe8-94899746cf5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a9d0cc67-54bd-47f0-ac6c-fb62fdb41d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5151a2c-ac1f-4377-8fe8-94899746cf5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c456489b-54b4-4037-9976-2f218fd4e1ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5151a2c-ac1f-4377-8fe8-94899746cf5e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d8e604b4-c2c5-4326-a130-d2136abd002a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "6f956ce1-2d03-4d58-89eb-f92ff5604ed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e604b4-c2c5-4326-a130-d2136abd002a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f931d6b6-b011-4eaf-a8a9-3e332d99728f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e604b4-c2c5-4326-a130-d2136abd002a",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "bcab7d40-5bf6-4ff1-ada7-d3a436ee69d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "fb020a98-54c6-4ecd-8d86-b5b25f2c1b8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcab7d40-5bf6-4ff1-ada7-d3a436ee69d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac1f27b-0be3-41dd-8263-0dab8c0baad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcab7d40-5bf6-4ff1-ada7-d3a436ee69d2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ac8bdb57-8a98-4bdd-8828-1691162370f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "f8b0ad17-ed7e-4316-be17-d997d1304a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8bdb57-8a98-4bdd-8828-1691162370f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51031243-84fe-4e35-b8b6-ad3d553a7825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8bdb57-8a98-4bdd-8828-1691162370f1",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "271e0159-d8fc-46e3-b3cd-c6770b2784a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "583ce46b-a545-4739-84ec-7f86cfc191e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "271e0159-d8fc-46e3-b3cd-c6770b2784a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d32bb8c-8768-43c7-9746-d9556ca966f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "271e0159-d8fc-46e3-b3cd-c6770b2784a1",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "1c3de0c8-d7ca-4a5a-888b-580001268107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "007a62f2-4531-4ec6-9c66-853dd043e98b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3de0c8-d7ca-4a5a-888b-580001268107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061d9167-d1ba-40f2-a0fd-a013e74a97c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3de0c8-d7ca-4a5a-888b-580001268107",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "aa8ce4cb-8d86-4f16-b91d-b8d19768176e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "dd54f808-ab5b-414d-9ea7-b4d0e4d1e95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa8ce4cb-8d86-4f16-b91d-b8d19768176e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05319b35-a8e2-48c6-8282-b6d86593b22c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa8ce4cb-8d86-4f16-b91d-b8d19768176e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d3ead565-c5f4-4417-b981-b5f388c2723e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "01b6e984-9281-40d9-be66-c7c58bdc53f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ead565-c5f4-4417-b981-b5f388c2723e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d724ef6-71a8-49e8-8e51-b2e289b1240d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ead565-c5f4-4417-b981-b5f388c2723e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0f9228b2-2c7c-4208-b48c-c71271d60696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b0a3a680-4528-4d21-85b6-ce69c86bf6be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f9228b2-2c7c-4208-b48c-c71271d60696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b2e260-fd62-475b-b5ac-e2d5d8326159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f9228b2-2c7c-4208-b48c-c71271d60696",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "cb7124b4-98eb-4873-9dc0-77acadc93642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "27d6b7f0-5451-4f16-88ab-2fa35a5f8161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7124b4-98eb-4873-9dc0-77acadc93642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97eeea00-dbaf-42f0-885d-e54965b4ea1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7124b4-98eb-4873-9dc0-77acadc93642",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7164d679-8dba-4bf6-b4b8-ccf43e771dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "02e7fc58-9d85-4135-817c-f28fbaf79ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7164d679-8dba-4bf6-b4b8-ccf43e771dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d4f33c-8731-4d6c-9060-888a4fdcae1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7164d679-8dba-4bf6-b4b8-ccf43e771dae",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "6f4245fc-120c-4088-a54a-e35f1153431a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "611264a9-88f6-4374-bf5a-197024cedfa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f4245fc-120c-4088-a54a-e35f1153431a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec8daacc-e5ec-4a44-ac3f-e0300112beb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f4245fc-120c-4088-a54a-e35f1153431a",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d4dc7845-d1f0-4ae7-bb14-f96b48085cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "8902fa26-2e20-423b-b026-be661e1208b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4dc7845-d1f0-4ae7-bb14-f96b48085cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3bc850c-e6a0-4a85-b570-8f3a13d84b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4dc7845-d1f0-4ae7-bb14-f96b48085cb0",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "720b0723-931d-4047-95d9-57f0b2d4255e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "20f34db9-a849-4891-a278-b4fdead512db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720b0723-931d-4047-95d9-57f0b2d4255e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a0f6733-b1ee-4b4c-8562-6d296e5c4dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720b0723-931d-4047-95d9-57f0b2d4255e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fa14eb8f-827e-4ef2-931d-234dabcd6685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c561d6c0-e6e7-4704-8b93-b58390031d66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa14eb8f-827e-4ef2-931d-234dabcd6685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9a5051d-5c99-480d-ab8c-fa8b75dc6b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa14eb8f-827e-4ef2-931d-234dabcd6685",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2e53d740-91dd-4b87-8f2d-928c9f14349a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "1d825b2f-6388-4aba-b6c0-86a9c803039b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e53d740-91dd-4b87-8f2d-928c9f14349a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80947f86-1c59-4eeb-a18f-b80a5b8fd616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e53d740-91dd-4b87-8f2d-928c9f14349a",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e04f0fc1-3522-4350-9be3-d7c8155a7068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "79fbaad4-0414-4e39-8ad7-ce84303c2b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e04f0fc1-3522-4350-9be3-d7c8155a7068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d56eadd-3cc5-4ce6-a5ab-ac3d76815060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e04f0fc1-3522-4350-9be3-d7c8155a7068",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "12c130fc-e4e2-4989-a023-7d29052df56b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "1401107c-0916-4753-8890-0e6454d90821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c130fc-e4e2-4989-a023-7d29052df56b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c0b561-c1c8-4516-ba6c-2e5464d05b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c130fc-e4e2-4989-a023-7d29052df56b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b925d90c-a3b6-4c3e-8c2a-ca1957c42935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "1ece46ba-ca78-4d9e-a5ef-4cc84cc6f92d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b925d90c-a3b6-4c3e-8c2a-ca1957c42935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ccc5719-20c2-446d-b7e6-4ed7b307a1a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b925d90c-a3b6-4c3e-8c2a-ca1957c42935",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "71ee3a45-55ee-40a5-9eb3-7e39ea2c742e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "0817b025-dbf4-4c06-8416-d4bee12042cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71ee3a45-55ee-40a5-9eb3-7e39ea2c742e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed8ec1f-98ee-4d71-88e7-5d8161d68e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71ee3a45-55ee-40a5-9eb3-7e39ea2c742e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d77b97bc-9611-400c-9e32-ed8f2035200d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c2c368cd-0e28-4d13-97b0-bfcbdf6151a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d77b97bc-9611-400c-9e32-ed8f2035200d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b2cea17-5e73-4e8e-9c41-eb92386cdd35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d77b97bc-9611-400c-9e32-ed8f2035200d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "385eff58-82bb-4d4f-936c-fb6d52a70602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "31054fe5-d8b7-49d4-a22f-1f0e4d1ee711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385eff58-82bb-4d4f-936c-fb6d52a70602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c353a3-8143-4778-9636-8f7fb6c1e02f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385eff58-82bb-4d4f-936c-fb6d52a70602",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b62ac535-0551-40f2-bbde-7b46e13989e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e44df0cb-2690-474f-9ae8-e8b9977fb3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62ac535-0551-40f2-bbde-7b46e13989e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ddfa22-9680-4ede-8b10-1a56e8e2c47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62ac535-0551-40f2-bbde-7b46e13989e4",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9eaf6315-f397-4a25-8413-94ccbb26ee24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "738066da-f558-4d89-b098-59de8e59a2fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eaf6315-f397-4a25-8413-94ccbb26ee24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f9b2e06-4204-424b-9bc9-cc104e247c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eaf6315-f397-4a25-8413-94ccbb26ee24",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2152fc9d-30a6-4355-9c4a-fcab8fd41d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e748cce3-770c-42a8-9bc2-0415ac916f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2152fc9d-30a6-4355-9c4a-fcab8fd41d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49788fe-ed02-42b2-9663-cb02537c582a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2152fc9d-30a6-4355-9c4a-fcab8fd41d7d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ae5e5e88-49c2-44cd-a59b-d99be15fab46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "279b9d4a-0018-474d-ada4-2819af0b2b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae5e5e88-49c2-44cd-a59b-d99be15fab46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511cd9b2-649f-40b4-8d8a-eded25f85293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5e5e88-49c2-44cd-a59b-d99be15fab46",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d52786cd-e0e0-469f-a811-0c72e23c6eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "57ecf3ef-d432-4f50-9106-d75e5faee8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d52786cd-e0e0-469f-a811-0c72e23c6eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28314ba3-e599-438e-ac6b-03ed46d523e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d52786cd-e0e0-469f-a811-0c72e23c6eae",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "65847fc1-bda5-43d4-a3d3-bbb0c13b7c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "92c1ee30-a3c4-4d80-a27f-d9f97f3f390e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65847fc1-bda5-43d4-a3d3-bbb0c13b7c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eac8505-7d58-48f5-bcf7-bfbbea13dc96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65847fc1-bda5-43d4-a3d3-bbb0c13b7c17",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0e3154e9-9863-4d2f-92ac-6ac6c054ddd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "cb5933da-de45-4051-b730-31cf894b7186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3154e9-9863-4d2f-92ac-6ac6c054ddd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e71547c-1991-4a2a-81f5-bb297d5b2edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3154e9-9863-4d2f-92ac-6ac6c054ddd8",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "432d5467-25a4-4936-a16a-685dbaeab261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "5584b01a-2faf-4382-a347-b02db9e52f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432d5467-25a4-4936-a16a-685dbaeab261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b6547e-759e-4b16-ac8a-4b59de1ffc9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432d5467-25a4-4936-a16a-685dbaeab261",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2c8b0a18-3a24-497d-ab02-575c52fbebac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ab134f7e-710c-4cfb-a5de-339024674b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c8b0a18-3a24-497d-ab02-575c52fbebac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511e2e96-51e9-434f-bf7d-dbbce45fc4cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c8b0a18-3a24-497d-ab02-575c52fbebac",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "50bc0e70-1225-4429-a57c-7c191b0c4ed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "944337c7-e4ce-416d-b478-b816341900e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50bc0e70-1225-4429-a57c-7c191b0c4ed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83771c3b-d61c-4eee-b941-6b548b95d962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50bc0e70-1225-4429-a57c-7c191b0c4ed5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "bb7fd2a1-f14a-453d-9116-c111f814aa62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "5b1ee8f6-069b-4332-a451-fd9a2bf11425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7fd2a1-f14a-453d-9116-c111f814aa62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c56de5-dcfc-4579-b625-e7c9637db5f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7fd2a1-f14a-453d-9116-c111f814aa62",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d2acb5cf-a66c-49f8-98fc-cc1e908b4ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "9fbeb204-bb80-4422-bfc4-4b3a6dd8bfca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2acb5cf-a66c-49f8-98fc-cc1e908b4ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff3999b-ed2e-4abf-86d1-6034b0c55416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2acb5cf-a66c-49f8-98fc-cc1e908b4ecf",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c664a8a6-0f01-4533-8f46-eb78361835e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "5df07573-d4ae-47fa-bbea-890fa1f67bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c664a8a6-0f01-4533-8f46-eb78361835e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45587079-3c82-48d8-8c61-4c29e741b183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c664a8a6-0f01-4533-8f46-eb78361835e6",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4d52e759-d1fe-4d75-81c1-304dc23aa606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "0b49ce1d-7dc8-43d9-b45e-2916c34af0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d52e759-d1fe-4d75-81c1-304dc23aa606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68086509-a068-4f39-9d25-5253ae88c7a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d52e759-d1fe-4d75-81c1-304dc23aa606",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7ecf9acb-6a96-4788-8613-89038dc3c2db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e49c178a-080b-47c4-93ac-5a409bd9986e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ecf9acb-6a96-4788-8613-89038dc3c2db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07dd09a0-1a4d-442d-8b52-a5f2ecfc09fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ecf9acb-6a96-4788-8613-89038dc3c2db",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3211cfbe-212e-41a2-a995-7fd0f5af3ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b6791bce-af6d-49a2-ad74-a27ba8aa2082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3211cfbe-212e-41a2-a995-7fd0f5af3ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8e81c0-6b3f-4434-846f-873c1ded8386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3211cfbe-212e-41a2-a995-7fd0f5af3ce5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "35f4d9dd-76bb-4351-8dff-bc35cb075c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "3f7b0c56-db60-44ba-8b32-92f83c8a4331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f4d9dd-76bb-4351-8dff-bc35cb075c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a709e50-8480-4cfc-ae5e-003e977d9a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f4d9dd-76bb-4351-8dff-bc35cb075c0f",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "eb6fd654-59c9-4e50-9533-7b957c02cddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c2cbb8bf-a403-47d7-9f50-5cc0e8a8dc2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb6fd654-59c9-4e50-9533-7b957c02cddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7c603b-dc96-42bc-a535-88a945a74823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb6fd654-59c9-4e50-9533-7b957c02cddb",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4b696556-fc64-4baf-83a7-4941fbee3da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "59ac700f-bd63-4cb9-b06c-faf3c6562d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b696556-fc64-4baf-83a7-4941fbee3da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9a692e-b796-4c31-9518-f606180edd7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b696556-fc64-4baf-83a7-4941fbee3da6",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2c8862ae-379e-4117-ae2c-e1d924006292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "6417c40a-3bbd-40e0-9cb4-8c58f405c194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c8862ae-379e-4117-ae2c-e1d924006292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd973ed-d8d2-4753-b675-fdda6ec3e6ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c8862ae-379e-4117-ae2c-e1d924006292",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4d8c6de1-89b7-473c-b9c3-e6a44cf2c47c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "276ab194-d725-4417-b12f-9069e397be1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8c6de1-89b7-473c-b9c3-e6a44cf2c47c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc238c88-ebc4-4989-805b-cf15d4ae2593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8c6de1-89b7-473c-b9c3-e6a44cf2c47c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2c6ee0f9-9362-4448-83b0-1362af86677e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "10c0c951-2a73-43ce-92a3-ae50c7f82b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c6ee0f9-9362-4448-83b0-1362af86677e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ee9c32-e1f1-4ed7-b097-ce4906bf66e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6ee0f9-9362-4448-83b0-1362af86677e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "1299d2aa-1fd5-4051-ac82-3fa344f806ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "5e9905be-d1e8-4903-80f2-c0b017fdcb18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1299d2aa-1fd5-4051-ac82-3fa344f806ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec14c2c9-92fa-485f-b208-50d0b4aad4e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1299d2aa-1fd5-4051-ac82-3fa344f806ae",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d40db712-e3bc-4689-a75f-b1cae113fe31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "45e1a7d0-8b4d-47ee-9af7-1ab692d5b180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d40db712-e3bc-4689-a75f-b1cae113fe31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c965e024-54f1-45b0-abb6-808d4ae006ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d40db712-e3bc-4689-a75f-b1cae113fe31",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "61cb2f4d-e5ef-4d95-906a-2b47dc103d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "378c488e-af4e-44a1-a6ae-e105e483e17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61cb2f4d-e5ef-4d95-906a-2b47dc103d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e60fcc-dd35-46c6-ad4c-e63efb5c634b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61cb2f4d-e5ef-4d95-906a-2b47dc103d65",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d641ab69-e2a3-41c7-bc61-77383ee7e753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a81dff4f-172f-4a4d-8a07-23000aba6bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d641ab69-e2a3-41c7-bc61-77383ee7e753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aac3143-0765-444c-b665-bc1e2b71f320",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d641ab69-e2a3-41c7-bc61-77383ee7e753",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "cfbee26f-ea66-497c-8c37-7662b6cbc864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "f20b1696-b03e-458a-b8a9-fa83e8464816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfbee26f-ea66-497c-8c37-7662b6cbc864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8939de78-d715-4df6-bb4f-0dec60a58ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfbee26f-ea66-497c-8c37-7662b6cbc864",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "f4dc107c-4ed9-42fe-8702-f4124daf8fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "385e29fd-8678-4f4b-a20c-d1f9ab7380ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4dc107c-4ed9-42fe-8702-f4124daf8fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951c17c4-1ef8-4d88-931b-e679182ff2ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4dc107c-4ed9-42fe-8702-f4124daf8fff",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "59910394-2cbe-48eb-8923-dc0a84a8f121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "37470f50-822a-4f0b-ab71-7f3bde8b20e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59910394-2cbe-48eb-8923-dc0a84a8f121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba8d8d32-cdef-41d5-bd45-1fb9d949bf1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59910394-2cbe-48eb-8923-dc0a84a8f121",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e0421767-2082-4493-bdfb-13f372205833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "1578f913-ff8c-44f8-94fb-ac92b4c1e165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0421767-2082-4493-bdfb-13f372205833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a00aeb3-7f66-4b9d-afe5-ce6a3f94f8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0421767-2082-4493-bdfb-13f372205833",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "73f7d103-1626-402c-9399-14df50e567b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4f43af12-232a-4bc9-9a62-21215c73a750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f7d103-1626-402c-9399-14df50e567b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e6f1c46-2eef-4f42-a48f-7bafff5ab2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f7d103-1626-402c-9399-14df50e567b4",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9db5992f-39a7-44c2-acfd-2dcbf46c65a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "27969ad7-6738-4cbb-acc3-9641799e0abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db5992f-39a7-44c2-acfd-2dcbf46c65a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68034d0-8399-4d7e-9c84-cdc50e70822b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db5992f-39a7-44c2-acfd-2dcbf46c65a1",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9168f9db-99c7-415e-a351-f94fa61e89d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "66210edc-1a3f-4b44-bcdd-c15a927eb1ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9168f9db-99c7-415e-a351-f94fa61e89d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1d49766-c1f0-49db-ae96-c709bd9b2cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9168f9db-99c7-415e-a351-f94fa61e89d1",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "38b6bd21-17ad-4a4a-9146-27a9dad1a41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a89ea517-20d5-4172-ad15-9f69433affd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b6bd21-17ad-4a4a-9146-27a9dad1a41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc9b1ba0-d7f0-4b5f-811f-4f392b6caebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b6bd21-17ad-4a4a-9146-27a9dad1a41e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "12ab6066-db15-48fd-a6e3-55134a07799d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b5e275f5-4b7e-4e5a-bb13-7ec0c80f1eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ab6066-db15-48fd-a6e3-55134a07799d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044d7e2e-d2da-41da-b51f-185e93d0824c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ab6066-db15-48fd-a6e3-55134a07799d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4c69a8e6-a5d8-47a9-b411-cc42ac661590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "546a981a-8593-46db-96f5-58a8a87cbca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c69a8e6-a5d8-47a9-b411-cc42ac661590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770901de-fbf9-4c5d-8a39-36216007f105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c69a8e6-a5d8-47a9-b411-cc42ac661590",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ea56e740-8198-48dc-9b07-82497a2c10be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "293e28ca-b396-41ce-84eb-25f27e9ec347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea56e740-8198-48dc-9b07-82497a2c10be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b185df3-e441-40bf-90ba-9924e5ade498",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea56e740-8198-48dc-9b07-82497a2c10be",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c27d9e47-6be5-41f0-9530-43d8f87360f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "94be9df0-4242-49f6-92ad-a840090273bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27d9e47-6be5-41f0-9530-43d8f87360f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154b9828-b4d6-4757-9c3e-423e1c742e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27d9e47-6be5-41f0-9530-43d8f87360f2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "20edc03c-f9f0-4ef2-9609-872ae58658d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "0f4acc0f-5083-4fa8-b47b-a28419fede47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20edc03c-f9f0-4ef2-9609-872ae58658d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62132510-27a4-45df-84de-7992dc4a1804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20edc03c-f9f0-4ef2-9609-872ae58658d2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b5957a52-aa1c-406a-b836-aa7ceadba57a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c1488bbe-dadf-4361-9dc4-1eff46f3d2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5957a52-aa1c-406a-b836-aa7ceadba57a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e97ab0-4c7f-4458-be73-3a535002aa06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5957a52-aa1c-406a-b836-aa7ceadba57a",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "5120c8f1-f640-4fb6-90cd-b44d53afaf49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a0a7c491-a431-4ef3-a687-f730dc7f6ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5120c8f1-f640-4fb6-90cd-b44d53afaf49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e73c5770-0595-44dc-9bad-f87e5267753d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5120c8f1-f640-4fb6-90cd-b44d53afaf49",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "497f8196-11d5-4093-b89a-077e072f2620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ec28baad-0150-4f09-833a-2b04a34a50dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "497f8196-11d5-4093-b89a-077e072f2620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf270d2-0145-40a2-8a36-37b24b7ed099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "497f8196-11d5-4093-b89a-077e072f2620",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "f0bca7e9-735d-4ac2-be22-ad8d0d7fb244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "438df6da-7fa4-4bdf-b52f-ab36ffef892b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0bca7e9-735d-4ac2-be22-ad8d0d7fb244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b659816-7823-400f-b768-e2092bdb690e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0bca7e9-735d-4ac2-be22-ad8d0d7fb244",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "27de8d03-461d-49e4-8277-e5791851967b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "8c4dedb0-e8b2-4007-abba-104722fce13c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27de8d03-461d-49e4-8277-e5791851967b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c582642-b707-4e63-ab7f-47b681e50ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27de8d03-461d-49e4-8277-e5791851967b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b58db69e-4d2e-48f6-b639-3e40706be5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ec67b1b4-8593-4af6-ac2e-aaeb598d6658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b58db69e-4d2e-48f6-b639-3e40706be5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efe14be-7929-46c0-8968-4268a3ad6048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b58db69e-4d2e-48f6-b639-3e40706be5f2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "9962e615-e559-4d4a-8987-7623482fd5e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "0767df28-68a5-42e9-a546-765058bf246b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9962e615-e559-4d4a-8987-7623482fd5e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6cb1d6-080b-4495-97b9-d1fd8599831e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9962e615-e559-4d4a-8987-7623482fd5e0",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "263fbd87-5c45-488d-be3a-3d064814e151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "db9f90b3-f2db-4042-918a-9715e67dbb54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "263fbd87-5c45-488d-be3a-3d064814e151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c0d334d-033f-48b3-aaec-462e0691160c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "263fbd87-5c45-488d-be3a-3d064814e151",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4dd9f79b-19f2-4e54-a4c8-d0fd90a32d77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "518401e2-49b9-48be-8e73-90545ae83d5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd9f79b-19f2-4e54-a4c8-d0fd90a32d77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "229cae9f-190f-4646-9e70-f8a617108364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd9f79b-19f2-4e54-a4c8-d0fd90a32d77",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "5f9ffe55-03b9-4733-b3ef-881ca6f3a50e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "0bcbecb8-f3de-4389-8c1c-13133b183ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f9ffe55-03b9-4733-b3ef-881ca6f3a50e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01bf6d4d-378f-4b29-b783-00870aca1e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f9ffe55-03b9-4733-b3ef-881ca6f3a50e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ff978db8-2f9c-410a-8ebf-e0116ad9224c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "b519f960-932d-433e-bfa1-151af189637e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff978db8-2f9c-410a-8ebf-e0116ad9224c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17e3b76d-3c01-4d10-9a7e-a1a8ca433ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff978db8-2f9c-410a-8ebf-e0116ad9224c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "a7e54871-4705-4dfd-8165-63f60578205c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d2bd71af-47f1-4039-bb40-4d81457fd04b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e54871-4705-4dfd-8165-63f60578205c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12c94d39-4edd-4b8e-b48b-d09374bf2f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e54871-4705-4dfd-8165-63f60578205c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4141977c-95ce-4b58-bc61-3624d60ffe36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "eb5afc95-ae90-4eca-b857-5d70d32239ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4141977c-95ce-4b58-bc61-3624d60ffe36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138ee815-8b5a-43c4-9869-4b3413def57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4141977c-95ce-4b58-bc61-3624d60ffe36",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "8184a358-5988-4dbc-abc4-b13a9032cdc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "050a9cc1-1601-45c8-941c-5bc91c6b12f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8184a358-5988-4dbc-abc4-b13a9032cdc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "437071a2-2cca-4ae0-ab17-b5e9568c237c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8184a358-5988-4dbc-abc4-b13a9032cdc4",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b860f6ca-bc65-43ec-b1c0-1e24feb0e500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "3c889fb2-323b-4fbf-87d0-2e98d9417e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b860f6ca-bc65-43ec-b1c0-1e24feb0e500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd91e536-9cf5-4b8c-8049-15a0fb004018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b860f6ca-bc65-43ec-b1c0-1e24feb0e500",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "a41ef605-ecca-4be6-8f01-2357c5467112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "50505340-5fbb-41ac-8513-480db777afc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41ef605-ecca-4be6-8f01-2357c5467112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2214c309-e880-4ba2-af2e-86fe9dd1b9af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41ef605-ecca-4be6-8f01-2357c5467112",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2819be83-c26c-481f-bb0c-de53b5d17ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "aa6a0911-36d1-48d7-b087-f04d5ba73d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2819be83-c26c-481f-bb0c-de53b5d17ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91493b6b-bad0-436d-bff3-acd2a3983014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2819be83-c26c-481f-bb0c-de53b5d17ff1",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e7541fa9-4112-4ddd-8b6f-cdee4cb063da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "1b5efa06-94e8-4e09-a9c7-09033e762d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7541fa9-4112-4ddd-8b6f-cdee4cb063da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7772ad2d-454e-4f50-b136-626eb3b32ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7541fa9-4112-4ddd-8b6f-cdee4cb063da",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "1da5b5e3-2a04-4832-9f28-474041dddb3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "2e428188-502e-42cd-a457-024fa5ca95bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da5b5e3-2a04-4832-9f28-474041dddb3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78fbf444-5762-40d4-9b50-7a1949399b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da5b5e3-2a04-4832-9f28-474041dddb3e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "63d70bc3-446a-4a49-af44-c72144edc2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "993ede6f-6e27-45f4-8d23-b7cafceb1648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63d70bc3-446a-4a49-af44-c72144edc2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9017236a-b7c2-422d-999e-94ebfc0d1fc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63d70bc3-446a-4a49-af44-c72144edc2c3",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "23ec8eed-bf65-4709-848c-e663b4eb7497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c5f33355-1db5-4145-b315-d503a82a5b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23ec8eed-bf65-4709-848c-e663b4eb7497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c44f8b19-e238-4bb8-b7c0-deef1e0e56e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23ec8eed-bf65-4709-848c-e663b4eb7497",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "f8cf630f-5f28-42a3-aa38-0ba90f4ad090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "277fb097-e723-4342-aab6-8e6598e41716",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8cf630f-5f28-42a3-aa38-0ba90f4ad090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f32ee6e-3c3f-4683-8764-13b60dd94c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8cf630f-5f28-42a3-aa38-0ba90f4ad090",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d512c0ea-e521-476f-91a6-aa67ae2730ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7ec67da0-6066-4bea-910f-1fe1f1deab26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d512c0ea-e521-476f-91a6-aa67ae2730ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e2d55d-4985-422c-b688-eddff9f60b27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d512c0ea-e521-476f-91a6-aa67ae2730ec",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0a5b85f4-a704-4e04-aa9e-187c8e47e283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ccb032d7-049c-4128-b1f9-2b509f73690b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a5b85f4-a704-4e04-aa9e-187c8e47e283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc3358f1-de9e-4698-9ceb-e2bc4e018f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5b85f4-a704-4e04-aa9e-187c8e47e283",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fd8a7dd6-b70a-4aa8-92e2-b71ca071bb07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4842e917-3a6e-4439-a867-6c7f956492a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd8a7dd6-b70a-4aa8-92e2-b71ca071bb07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d24c1a-de58-4363-8dde-df95f1ce4222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd8a7dd6-b70a-4aa8-92e2-b71ca071bb07",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "83900861-fbc3-43a1-b77d-ac6c79d745bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e0e2f870-c2ae-4690-a687-daa052f32916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83900861-fbc3-43a1-b77d-ac6c79d745bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "231b2011-f1c2-49de-9bd4-b52c05da338c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83900861-fbc3-43a1-b77d-ac6c79d745bb",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4a1efdf5-547b-4b22-a13e-a7c7509d8c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7838134d-67b8-48da-b341-f446b49a4e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a1efdf5-547b-4b22-a13e-a7c7509d8c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19da3e77-9daf-4d33-a2f5-e160f6abb2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a1efdf5-547b-4b22-a13e-a7c7509d8c82",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "1f981bd4-d2cd-4d59-840d-1b7948e4cd17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "183a319c-4ce7-4ea3-ab4c-9815118b19b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f981bd4-d2cd-4d59-840d-1b7948e4cd17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1becc34-e86c-4449-8077-8ddfdaa8ffa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f981bd4-d2cd-4d59-840d-1b7948e4cd17",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "47f33742-345d-42cb-8e07-a1b8ea2592fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "abd1c6d7-c0e7-467e-8288-37d38d64781e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f33742-345d-42cb-8e07-a1b8ea2592fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ae6c86-67e6-4f57-87ff-86a31f614534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f33742-345d-42cb-8e07-a1b8ea2592fd",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "4369d86c-82a5-4ee1-993a-4568d8cf4d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "af7dd258-e45c-4a4a-9a20-f1848635f48f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4369d86c-82a5-4ee1-993a-4568d8cf4d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5126cba-e64b-4212-a4c2-28eac799b5d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4369d86c-82a5-4ee1-993a-4568d8cf4d7c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "417d2e2f-60b2-4d13-b35a-b0e91010f8b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "8470a50d-eaa8-4cf4-9222-59212e894514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417d2e2f-60b2-4d13-b35a-b0e91010f8b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "431aaa3a-43e7-469f-8a41-838578ac650f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417d2e2f-60b2-4d13-b35a-b0e91010f8b9",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d43bec26-875a-4359-ab76-693828a85733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d9ff117b-ae6b-4df6-89ba-f1c2ead2b7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43bec26-875a-4359-ab76-693828a85733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8492f75e-6873-4648-8deb-181016c88945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43bec26-875a-4359-ab76-693828a85733",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2c89dd70-dc36-4d7f-b42d-0629db4753c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "3301a7da-1b33-4493-8080-97d4c4fa81f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c89dd70-dc36-4d7f-b42d-0629db4753c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e0ce60-ba5b-4c26-97ea-d1b1735c8057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c89dd70-dc36-4d7f-b42d-0629db4753c7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "76ecbd33-1401-4b51-a44b-1aa834b2f5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7eeb73a5-0857-4b53-bee8-19bf81efd59f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ecbd33-1401-4b51-a44b-1aa834b2f5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "107929ea-5950-4e4d-b115-798cb4605c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ecbd33-1401-4b51-a44b-1aa834b2f5d7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "21e750a5-4854-446f-b591-d6155c029d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e75d795f-aba7-4b3b-87c7-432e95f98597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e750a5-4854-446f-b591-d6155c029d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf349c8-6e7e-4b7c-8fae-233b628ad37f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e750a5-4854-446f-b591-d6155c029d83",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7b917a6b-4dfe-46a6-8d24-ce48fa6351b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "58526a44-2a11-4de6-a99e-2fba2fc72443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b917a6b-4dfe-46a6-8d24-ce48fa6351b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c0f0c70-8e94-4cf6-a5c6-e23103b2ee6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b917a6b-4dfe-46a6-8d24-ce48fa6351b8",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "79291735-e86f-44ae-81e5-618168c52fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a586d238-139f-4fdc-967e-c34133f1e6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79291735-e86f-44ae-81e5-618168c52fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5a77ea-17b1-4f98-b44a-f466feb9187f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79291735-e86f-44ae-81e5-618168c52fa6",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "571fd46b-bea8-4ff9-afc3-bae1d6a8b5d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ae867954-9e23-43ce-888a-7da7bf8f4a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "571fd46b-bea8-4ff9-afc3-bae1d6a8b5d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e0aa3c-2e21-4f42-9fbe-be35fb24ecd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "571fd46b-bea8-4ff9-afc3-bae1d6a8b5d5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d4fd4302-9366-4f34-bb04-ca8e3779ab54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4da51106-9a85-41b7-8d7f-1da256c558e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4fd4302-9366-4f34-bb04-ca8e3779ab54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c41c45-daa8-4573-9375-43d1e9f80e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4fd4302-9366-4f34-bb04-ca8e3779ab54",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fd789fe6-72ac-4694-95be-39e5f769cc97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d73d8cf7-0577-45ec-8f5d-f80b0058a1d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd789fe6-72ac-4694-95be-39e5f769cc97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2af3419-e2d1-4c2d-b5cf-54f9dd80e861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd789fe6-72ac-4694-95be-39e5f769cc97",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c4b610c1-4345-4b49-aef7-337c00f85345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "9eaad24d-5d5f-45ba-95cf-35a39f6be6ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b610c1-4345-4b49-aef7-337c00f85345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf02b56-41d9-4cc2-a18b-1c173c808e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b610c1-4345-4b49-aef7-337c00f85345",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "af9a1095-6b9f-4b46-ace7-f46a2b499966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "843ba953-cf53-4529-a5db-ef49d29d6d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9a1095-6b9f-4b46-ace7-f46a2b499966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba954569-a0d9-4874-9d37-d5e2ec5b18eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9a1095-6b9f-4b46-ace7-f46a2b499966",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "cf51658c-3efe-483f-a343-19eb9ae4d333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c71bf58e-01cf-4157-a8e5-2b839637e90d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf51658c-3efe-483f-a343-19eb9ae4d333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf4c8e23-7e0b-42aa-95b8-fed20a526da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf51658c-3efe-483f-a343-19eb9ae4d333",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fa892533-8504-403f-9c4e-3227509d2dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ac926af6-f8ae-4fee-b878-b00ee7ec35b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa892533-8504-403f-9c4e-3227509d2dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7db2be2-1acb-40b8-823c-96f9f473e397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa892533-8504-403f-9c4e-3227509d2dc9",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "5644d8f3-f556-4533-9dee-c86f56403480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ace917f7-7857-4aa7-84dd-365f555cdbab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5644d8f3-f556-4533-9dee-c86f56403480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb547e6-100f-4abb-849c-eb8b8eb67770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5644d8f3-f556-4533-9dee-c86f56403480",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2b98b177-34f6-48b2-b515-dc4f2cfaeff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4da5e187-bb0c-4ba4-ba5d-20bb5bde44d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b98b177-34f6-48b2-b515-dc4f2cfaeff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2665d803-3e1b-46cf-a87e-b89304d6b215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b98b177-34f6-48b2-b515-dc4f2cfaeff2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0065b11c-7695-4bed-8fdc-6da4139bbe77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "909954a7-a5e7-4539-8b80-3c0c40f47dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0065b11c-7695-4bed-8fdc-6da4139bbe77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8005a055-fd4e-4004-b9b1-50fbcd251ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0065b11c-7695-4bed-8fdc-6da4139bbe77",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7431c204-cd25-4aa7-b1c7-efb1792203ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a95b42a8-fb14-4145-bddc-386d23423749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7431c204-cd25-4aa7-b1c7-efb1792203ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476bc9f7-8c36-4ba2-924e-3789eef9ffc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7431c204-cd25-4aa7-b1c7-efb1792203ad",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c6b1487a-c7fc-4b54-abe8-3d2aa1c2086e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "8e42b4e3-adbc-42c0-bb0b-3e3585fa506b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6b1487a-c7fc-4b54-abe8-3d2aa1c2086e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4131f777-ac75-41b1-bc4e-47156092b15b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6b1487a-c7fc-4b54-abe8-3d2aa1c2086e",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e989a699-fe02-40cf-b86f-36708cafe958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "28a6c5a0-007e-4cf1-895e-9eb05948e4c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e989a699-fe02-40cf-b86f-36708cafe958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09e55737-5a1b-45cc-bebd-4eafa9f963d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e989a699-fe02-40cf-b86f-36708cafe958",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7acec1cb-d847-4ea2-84df-5653d78f9ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "cf8504db-b4d7-4c12-928d-95c6baacf9ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7acec1cb-d847-4ea2-84df-5653d78f9ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9c826c-31db-465d-a8c4-a1935634bcb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7acec1cb-d847-4ea2-84df-5653d78f9ef5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c9ec73b8-68e5-4113-99b2-8a5880554f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4013c957-6faa-44c4-869a-1c1066872d01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ec73b8-68e5-4113-99b2-8a5880554f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c708f22f-2fa3-4dd4-8ad1-3f8854ca4395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ec73b8-68e5-4113-99b2-8a5880554f33",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "636f0fa2-73fe-4690-be73-9cf35ca6762a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "36124d6c-ff2e-4ff3-83e4-3961cb3644d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "636f0fa2-73fe-4690-be73-9cf35ca6762a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1a44c4-c40b-47f5-8ed2-4c54f4e83068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "636f0fa2-73fe-4690-be73-9cf35ca6762a",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fb9cffce-5cec-4e2f-be5e-6920d3f89894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "40b4ec65-f940-4bf9-aa2e-d152516c0c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9cffce-5cec-4e2f-be5e-6920d3f89894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5be58f7-beca-4d80-8a9e-69540cf68d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9cffce-5cec-4e2f-be5e-6920d3f89894",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "338a854c-3650-4097-9b7b-1785cbc27b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d94c6c8b-c00c-479d-b703-f59bb42acf88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338a854c-3650-4097-9b7b-1785cbc27b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e390a6fe-ddb2-411e-9e9b-656dcf4b4f5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338a854c-3650-4097-9b7b-1785cbc27b7d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "65091700-2e2f-42cf-9bd8-e44bdab85436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "e174be5c-3e3d-4cea-9c16-5628f1fc42c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65091700-2e2f-42cf-9bd8-e44bdab85436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7c5a48-0352-472e-92dc-638b39c13612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65091700-2e2f-42cf-9bd8-e44bdab85436",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "bde7a0a7-35f8-4308-8b03-b3a335564096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "2a4defc8-8d45-471d-aab8-d0cde2bdd0fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde7a0a7-35f8-4308-8b03-b3a335564096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0eb0cb6-0d6a-4076-8584-2f6ed51f12ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde7a0a7-35f8-4308-8b03-b3a335564096",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "82958bf6-a530-4e37-90c0-eed239f833f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "63d29b74-9d3e-4d08-b7b5-7496665ab3b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82958bf6-a530-4e37-90c0-eed239f833f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c5ebd4-3ac2-412a-acca-c77bac8e436e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82958bf6-a530-4e37-90c0-eed239f833f2",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fe7ba7ff-9e0b-4460-8a0c-465621eff5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7a9dbd87-ca73-4f84-80dc-2a590a0ca672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe7ba7ff-9e0b-4460-8a0c-465621eff5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b8113d-2762-4d6e-8d49-0a3ef1e3dfec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe7ba7ff-9e0b-4460-8a0c-465621eff5d7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "88ec07bb-04e2-4ecd-b634-8005543b04e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "44042662-4f22-451a-a137-894765d02bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ec07bb-04e2-4ecd-b634-8005543b04e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bc44af8-43ae-4d83-ba06-192a8ab642cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ec07bb-04e2-4ecd-b634-8005543b04e9",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "760c5f9b-32ae-40e4-8cdb-a32bf5076dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c7174bce-faa6-40e1-bb2c-6a73b70ff2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760c5f9b-32ae-40e4-8cdb-a32bf5076dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd5033e5-2d84-4f33-b962-1d4f7d973d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760c5f9b-32ae-40e4-8cdb-a32bf5076dbe",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "16b6fe5f-5215-4e38-b1be-4c2d47d5e253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "a178bfed-af75-47fb-a9bd-d1639eb16010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b6fe5f-5215-4e38-b1be-4c2d47d5e253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3423b0da-a50f-4d38-a21e-6496f91fe19b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b6fe5f-5215-4e38-b1be-4c2d47d5e253",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c8a3d14c-3621-47f1-8c6e-88ed32270f6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7b58c277-3b28-40b8-8d26-6c4d104282b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a3d14c-3621-47f1-8c6e-88ed32270f6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b63ef8-22de-44c8-b07e-40e37de43ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a3d14c-3621-47f1-8c6e-88ed32270f6c",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "54412d71-342a-4080-8183-f6d7d7d93222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "213bae61-47f9-47da-855c-ea7885321d83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54412d71-342a-4080-8183-f6d7d7d93222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779b2703-918d-4852-8f1f-ef302e003c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54412d71-342a-4080-8183-f6d7d7d93222",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b8dc1de3-fee6-4045-9cc3-93066de0d6d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4f298af5-8f95-4e51-a60c-8403518f7e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8dc1de3-fee6-4045-9cc3-93066de0d6d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf59e4f-a202-4f8f-ba6a-dcf501cfd786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8dc1de3-fee6-4045-9cc3-93066de0d6d5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "da82c5c2-d309-4527-bcc8-edf35d8dbff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4dc72118-c9f0-4687-9b1f-2a59cff0e2dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da82c5c2-d309-4527-bcc8-edf35d8dbff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f87993-dece-4bce-8a87-13eeacd11e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da82c5c2-d309-4527-bcc8-edf35d8dbff3",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3ed6c4a9-0a69-4b30-9473-204e44fbfe2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c76d4f2c-649a-4a84-97fa-bfa26246ad2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed6c4a9-0a69-4b30-9473-204e44fbfe2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea13c09-dede-4784-966e-f41605d020ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed6c4a9-0a69-4b30-9473-204e44fbfe2f",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d5369c53-7451-40a7-84f1-239f7d13a1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "62edcd84-f16a-47d0-a635-12fa63c1e6e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5369c53-7451-40a7-84f1-239f7d13a1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75345a2-4d50-45a8-ba01-b054cd306e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5369c53-7451-40a7-84f1-239f7d13a1a3",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2c2784d1-7229-4281-b776-957f4c64340f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "6691b8b3-bb32-453e-9e46-bbd0f08af2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c2784d1-7229-4281-b776-957f4c64340f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "700e2320-44dc-4227-aeda-ae9421994fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c2784d1-7229-4281-b776-957f4c64340f",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "727cc479-46d5-4c37-8ef5-132a1befe7fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "1b55d373-4a39-48b6-aa3b-088a222db10f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727cc479-46d5-4c37-8ef5-132a1befe7fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e9cbce-a76a-43e4-93b3-f3356424336d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727cc479-46d5-4c37-8ef5-132a1befe7fe",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2493fae9-2b75-4441-a3a7-146eff74abb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "16030458-3591-49ca-8d43-acaa32177a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2493fae9-2b75-4441-a3a7-146eff74abb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845d8561-683a-4151-8d39-caa0037949ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2493fae9-2b75-4441-a3a7-146eff74abb7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c47f3893-6c9c-42c2-b1d4-d2d4771222d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "baf1b02e-adcb-48ab-a297-d50e2274fc59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c47f3893-6c9c-42c2-b1d4-d2d4771222d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b17765-e9a9-405b-9a97-bcf3375cfc8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c47f3893-6c9c-42c2-b1d4-d2d4771222d3",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "8c8fee9c-345f-4020-a117-7d153a4f03d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "cfe376bd-5f46-4d7d-8b18-a55531658c7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8fee9c-345f-4020-a117-7d153a4f03d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66ef2f89-4042-49e3-8604-110abfab3637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8fee9c-345f-4020-a117-7d153a4f03d5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b7ec323f-44fd-4803-88b8-e9ecea3ae427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "9a356c0c-4836-4c40-89e1-4ae6975660fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ec323f-44fd-4803-88b8-e9ecea3ae427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "516aa6ed-1d7a-40ad-b82e-59384999768b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ec323f-44fd-4803-88b8-e9ecea3ae427",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fcf7963a-306e-4671-9660-9612a0519d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "863a4947-acf1-46f2-913e-3a8495fefe86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf7963a-306e-4671-9660-9612a0519d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f5a29a-0cae-41a1-a79c-5bf93479d203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf7963a-306e-4671-9660-9612a0519d88",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c2bb6c90-0383-4c22-881a-a19e5a090b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "24b7ed7a-eaed-4eda-8bef-82bff23f779a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2bb6c90-0383-4c22-881a-a19e5a090b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa05523d-94e1-450b-8f24-3f23ae3d5ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2bb6c90-0383-4c22-881a-a19e5a090b4f",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2144848f-de4e-4507-a937-24a053700eaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c903fea5-f20e-4d64-96aa-719f21bde3ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2144848f-de4e-4507-a937-24a053700eaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda587bd-ebb9-4129-8c6a-2716b6deaf12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2144848f-de4e-4507-a937-24a053700eaa",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "3c649ede-5a9c-4aba-93e4-1c7235582cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "bbeefb77-cae1-4e9f-85dd-70fa463cb20a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c649ede-5a9c-4aba-93e4-1c7235582cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eed76eba-9e4c-47b8-b431-4c85a6955ef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c649ede-5a9c-4aba-93e4-1c7235582cf3",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "b479b4a3-b39c-4440-8dc7-78cf8c011b71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "4898bd2d-01bb-4fce-827f-f9e306eba09a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b479b4a3-b39c-4440-8dc7-78cf8c011b71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a353b361-ed24-424b-8ef0-867a1625fd77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b479b4a3-b39c-4440-8dc7-78cf8c011b71",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "e26645b9-ecae-4ac0-b92f-a09fd5bde6a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ce06f4d3-cd09-4b86-a1f3-cd7534a3a1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26645b9-ecae-4ac0-b92f-a09fd5bde6a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6660e17-1ef8-40e6-87cc-ebecbc9dfeff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26645b9-ecae-4ac0-b92f-a09fd5bde6a4",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "36e1559d-af56-4c14-ba4b-9bf291a77ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "729f0c18-1805-4ac6-9a94-bb922bd28937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36e1559d-af56-4c14-ba4b-9bf291a77ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8848716-a184-450a-aa1c-964e3d831d6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36e1559d-af56-4c14-ba4b-9bf291a77ee7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "399a8389-d488-4f7c-a2eb-bf6b2af5be73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "377e5b77-af8c-486f-a304-5012ccf16356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "399a8389-d488-4f7c-a2eb-bf6b2af5be73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d54159-9cdf-463b-a590-b144075a0d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "399a8389-d488-4f7c-a2eb-bf6b2af5be73",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "c048e748-a116-4b3b-97be-aa27ff5cf79d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "f9341874-3858-429a-a79c-caf0826de1d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c048e748-a116-4b3b-97be-aa27ff5cf79d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06380514-a30f-443d-8c5b-99dffaa5d997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c048e748-a116-4b3b-97be-aa27ff5cf79d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "87ac17ef-65a1-4d48-836d-1381923f5892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "af7bbb94-a9c7-43a3-a5ce-65559fe4c944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ac17ef-65a1-4d48-836d-1381923f5892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491f6f9b-10d9-4693-96ea-9eb2ed8b511f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ac17ef-65a1-4d48-836d-1381923f5892",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "fc2ada27-084f-4bcc-8c14-78f9c8458f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "483c8939-6e45-404d-8174-bab93d915edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2ada27-084f-4bcc-8c14-78f9c8458f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad75aba-0859-45c8-82c1-46a6434d5b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2ada27-084f-4bcc-8c14-78f9c8458f5b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "32684e9c-9c5e-408e-a458-276d8e2e4d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "bea5e771-b0c3-4a95-8dd5-762b0b2361dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32684e9c-9c5e-408e-a458-276d8e2e4d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d9effb-c914-4cc1-b94e-9d9be2c4d016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32684e9c-9c5e-408e-a458-276d8e2e4d59",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "0f34df78-0a97-4347-bd64-e239f8bbb4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "9349478c-d16f-4191-aee0-e4f2cbb44519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f34df78-0a97-4347-bd64-e239f8bbb4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70edc2f-a804-422f-8fd3-42531c593652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f34df78-0a97-4347-bd64-e239f8bbb4e0",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "514c25c3-3336-41e4-813b-4a0b5d440d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "ef2f0fb7-d17a-4a5b-8987-1a4b0ca9d08d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514c25c3-3336-41e4-813b-4a0b5d440d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63e0bec9-8a61-4af6-8b93-873e64f11185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514c25c3-3336-41e4-813b-4a0b5d440d03",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "d8d2ba8a-44d9-42aa-8d6a-06d04c8d7228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "72c3d36c-cff0-42bb-934a-ad86fd139919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8d2ba8a-44d9-42aa-8d6a-06d04c8d7228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed679da-c59c-4944-bd1b-233a39fbbded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8d2ba8a-44d9-42aa-8d6a-06d04c8d7228",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "33f1e0c3-814b-4eac-957c-51ef7665ef7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "d851d046-e118-4f76-8a44-74f691348c3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f1e0c3-814b-4eac-957c-51ef7665ef7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a8069c6-69fc-4829-83b8-a2db85f45d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f1e0c3-814b-4eac-957c-51ef7665ef7b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "cad952d3-ddc8-4905-ae9b-ac8a0446f38b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "770e43d6-e416-477d-98e6-8939a4ffaafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cad952d3-ddc8-4905-ae9b-ac8a0446f38b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5150ce30-9495-43b4-8527-5c8c2b73a09b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad952d3-ddc8-4905-ae9b-ac8a0446f38b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "1c76100b-e7e0-489c-8534-854315d52e46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "789878a2-aeac-4a23-993a-e83f4dd72ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c76100b-e7e0-489c-8534-854315d52e46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3cd7de2-d4b8-46a9-afcc-6c889a9ad1c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c76100b-e7e0-489c-8534-854315d52e46",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "2a3abece-02e7-45c8-84a3-27f1a57b5501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "396217fb-5820-4c1f-85be-ff2813f182d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3abece-02e7-45c8-84a3-27f1a57b5501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fcf57bd-cb87-49bf-898e-f71b5007a984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3abece-02e7-45c8-84a3-27f1a57b5501",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "50af3971-25d2-4b98-b36d-17dbebd5571d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "6a16bcd8-edc9-4443-a1a2-3ebd03a878da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50af3971-25d2-4b98-b36d-17dbebd5571d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5f3f38-bef1-4eea-975d-dffbbeca2e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50af3971-25d2-4b98-b36d-17dbebd5571d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "ccc85276-900d-4c44-815e-bc32b702f358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "cba2bc5b-cc36-407b-90b4-800962cdcae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc85276-900d-4c44-815e-bc32b702f358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ddf225e-8550-4370-9716-39f1c06401e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc85276-900d-4c44-815e-bc32b702f358",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "68d72bab-9ce7-4e07-beff-94e78b3b3fe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "c7ca2f82-44af-4f24-9caa-011cb6f1df5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d72bab-9ce7-4e07-beff-94e78b3b3fe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5636278b-2164-4aba-a418-be92348dbe3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d72bab-9ce7-4e07-beff-94e78b3b3fe9",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "79c419c6-d802-45cd-b223-8be423098c2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "452d9906-ff76-4ff8-8c7e-089830d3d4a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c419c6-d802-45cd-b223-8be423098c2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d156010f-7d2a-40a5-af2e-1e7c6d95f43d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c419c6-d802-45cd-b223-8be423098c2d",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "36448e9c-6ac1-4109-a714-f4c7ca890b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "7252a66e-8727-462e-bb42-f345842ef6bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36448e9c-6ac1-4109-a714-f4c7ca890b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f23375a9-71b8-45c9-8002-8a9917aee0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36448e9c-6ac1-4109-a714-f4c7ca890b0b",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "13b328ed-ca75-4684-954c-8dffc36d08a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "42079992-470a-4b97-b960-eb2397827c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13b328ed-ca75-4684-954c-8dffc36d08a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e88722-c74c-4145-94ee-f62b23c49af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b328ed-ca75-4684-954c-8dffc36d08a7",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "7ec1905c-67da-4a92-98ab-888ac17c2518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "234ee1b7-34c9-4862-ad62-a98eb5f4bf44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec1905c-67da-4a92-98ab-888ac17c2518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e749ad-2202-493f-b922-c709d0529562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec1905c-67da-4a92-98ab-888ac17c2518",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        },
        {
            "id": "6619d813-5f14-45b3-ae3e-a7571448d8c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "compositeImage": {
                "id": "20137c45-c4a9-4190-a6a1-722a85159c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6619d813-5f14-45b3-ae3e-a7571448d8c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ade885-9bac-4806-876d-23216a1168bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6619d813-5f14-45b3-ae3e-a7571448d8c5",
                    "LayerId": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab"
                }
            ]
        }
    ],
    "gridX": 12,
    "gridY": 12,
    "height": 24,
    "layers": [
        {
            "id": "6137eabe-11cc-4bcf-a2ff-4cd24e9cc2ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e75515f3-c0c0-46ba-bb8a-0f11292d7208",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}