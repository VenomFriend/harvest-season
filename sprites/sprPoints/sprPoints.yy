{
    "id": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPoints",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3b028e23-8f4a-4103-85ff-c215d91de672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "2d6916f1-a842-42e3-92ab-9285d72e18a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b028e23-8f4a-4103-85ff-c215d91de672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd5dba1-c0fd-401c-9976-a1ab4318e521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b028e23-8f4a-4103-85ff-c215d91de672",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "c3aaf340-82a1-46e6-929e-9f3d22edc085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "04d7dac8-a708-471b-a461-631be58e95bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3aaf340-82a1-46e6-929e-9f3d22edc085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e03e295f-daf5-427d-b84f-42a4e5f189ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3aaf340-82a1-46e6-929e-9f3d22edc085",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "f507c23f-4965-40b8-ad85-182a43fbc1f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "3d51ae65-820c-4296-8907-d0ace83219a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f507c23f-4965-40b8-ad85-182a43fbc1f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4347ae-53b1-45e7-9de2-516256cfa602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f507c23f-4965-40b8-ad85-182a43fbc1f6",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "51b007a0-519e-40a9-a2b7-41244dadf2d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "9c6af3bd-03a4-460f-aec1-4c0e8d239efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b007a0-519e-40a9-a2b7-41244dadf2d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df610236-5a08-4fc4-a6c9-dc4d6e1a155b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b007a0-519e-40a9-a2b7-41244dadf2d2",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "99eef4eb-665d-468b-840c-3b9ac81e2311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "bc0584c3-b89f-438a-b537-7ac00cf75db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99eef4eb-665d-468b-840c-3b9ac81e2311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8c8c3e5-1b31-491f-9f8b-ec563cd86a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99eef4eb-665d-468b-840c-3b9ac81e2311",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "3163eca0-568d-4b28-a4a0-1622c269b1e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "3c8b20b9-e8a2-4e62-9d5c-48f764ba49a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3163eca0-568d-4b28-a4a0-1622c269b1e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67452802-b675-46e5-a1fd-c9c7cc69f3ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3163eca0-568d-4b28-a4a0-1622c269b1e9",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "7d2e222f-c288-4c28-b54c-2a288915f530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "d12a0089-a6b9-4fd9-92cd-866e985368e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2e222f-c288-4c28-b54c-2a288915f530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7b0aac-c33b-4fad-bf30-20960fe25c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2e222f-c288-4c28-b54c-2a288915f530",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        },
        {
            "id": "dd749be2-6311-4ed9-848d-d64af079ba98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "compositeImage": {
                "id": "0411060f-7002-4a5e-b1fa-f076bbc51625",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd749be2-6311-4ed9-848d-d64af079ba98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb33dc61-aaaf-4a4d-ae26-685558a65c28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd749be2-6311-4ed9-848d-d64af079ba98",
                    "LayerId": "26271934-276e-4914-b5c0-5ff61cffa042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "26271934-276e-4914-b5c0-5ff61cffa042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2a27d3d-aebf-49c6-8f6f-0bd43db524a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}