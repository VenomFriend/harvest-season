{
    "id": "95195553-edf2-478c-bf5f-9356020c59b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStoreQuantity",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "06778517-05ba-4cad-9b38-91671eee67e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95195553-edf2-478c-bf5f-9356020c59b0",
            "compositeImage": {
                "id": "6c2a4e3b-10ad-4dd4-9fe4-4bca55fb6b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06778517-05ba-4cad-9b38-91671eee67e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a346ef5-d231-4cfd-a244-f15c7d0ca507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06778517-05ba-4cad-9b38-91671eee67e6",
                    "LayerId": "9cdeb92a-fdc5-4407-8d08-49f79db609c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "9cdeb92a-fdc5-4407-8d08-49f79db609c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95195553-edf2-478c-bf5f-9356020c59b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}