{
    "id": "455885a9-d50a-4bdd-976c-f085be1779f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "16267808-a4fc-49e6-bcd6-d5c5b23461fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "e2f8eaf7-03a7-4b8d-8d04-d1b7b6e3eab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16267808-a4fc-49e6-bcd6-d5c5b23461fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97de6a97-f86a-405b-a9e1-17706fccc5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16267808-a4fc-49e6-bcd6-d5c5b23461fa",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "43ba7d00-1750-47d7-b8a9-f7c6d8299e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "a9730418-1a76-4231-a916-ba7ee58aa63a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ba7d00-1750-47d7-b8a9-f7c6d8299e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e93bfd-105c-432d-bc0c-27943bfd0b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ba7d00-1750-47d7-b8a9-f7c6d8299e5d",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "9e0b6167-4cee-4ac0-9c35-ac7a635ee760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "315b5885-2f4b-4ebf-88ee-719a3f84b5d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0b6167-4cee-4ac0-9c35-ac7a635ee760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e1615a-3f72-4fc7-8958-d3ab24e2185f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0b6167-4cee-4ac0-9c35-ac7a635ee760",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "a0bbd726-5dfb-47e0-a2d4-2b64d43360af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "9c6a5c93-b04b-43fb-9085-16de8871c7bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bbd726-5dfb-47e0-a2d4-2b64d43360af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6188c584-764f-44f5-ac71-48226f1ae2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bbd726-5dfb-47e0-a2d4-2b64d43360af",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "0a73d9f7-05b0-4189-af2b-7c940a8794f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "16e7ca05-7f7f-4760-a48e-a8270d175511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a73d9f7-05b0-4189-af2b-7c940a8794f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a0aaa63-93dc-4cf8-bd6f-c540ec9b2c61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a73d9f7-05b0-4189-af2b-7c940a8794f6",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "3276016b-1f01-4172-8387-2ae505c4135f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "fb77fc66-d645-4f66-8206-0223d490a8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3276016b-1f01-4172-8387-2ae505c4135f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8910f0-9e47-46fe-8379-b40b20defc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3276016b-1f01-4172-8387-2ae505c4135f",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "2cf491f0-e467-4fee-9ce0-bf59f48882df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "a3c44242-f1e2-45b7-a5ba-430fa2f9a5e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf491f0-e467-4fee-9ce0-bf59f48882df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e2e7b13-7f51-45c3-bf69-ec4c5624eb14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf491f0-e467-4fee-9ce0-bf59f48882df",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        },
        {
            "id": "9cd0b2a4-c793-4f98-9559-1887ce890102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "compositeImage": {
                "id": "2f5672b2-109c-4ea9-be19-f68db81a3325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd0b2a4-c793-4f98-9559-1887ce890102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac6c141-b282-4c6a-b563-28e77485fd07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd0b2a4-c793-4f98-9559-1887ce890102",
                    "LayerId": "c779e914-0eab-4926-8b09-d4e04a0988dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c779e914-0eab-4926-8b09-d4e04a0988dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "455885a9-d50a-4bdd-976c-f085be1779f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}