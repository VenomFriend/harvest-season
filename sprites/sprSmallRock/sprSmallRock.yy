{
    "id": "64eb5b2d-3552-482e-8af3-1f68bf372e29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSmallRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7b50e119-89c0-4fde-8041-480e8ab23faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64eb5b2d-3552-482e-8af3-1f68bf372e29",
            "compositeImage": {
                "id": "c51f5ad8-fac1-482c-a1d3-cccc8ff5ce5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b50e119-89c0-4fde-8041-480e8ab23faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff52db2-8d21-4ddd-8608-19cc290e05b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b50e119-89c0-4fde-8041-480e8ab23faa",
                    "LayerId": "4311c750-22c9-4827-b797-ddb6a59a1433"
                }
            ]
        },
        {
            "id": "2b62612b-9bd0-401e-b6cd-528297eda0e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64eb5b2d-3552-482e-8af3-1f68bf372e29",
            "compositeImage": {
                "id": "957e675a-2490-4b1d-8b95-47bd37293f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b62612b-9bd0-401e-b6cd-528297eda0e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452b080f-0f76-4782-a401-6193b8c9fce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b62612b-9bd0-401e-b6cd-528297eda0e5",
                    "LayerId": "4311c750-22c9-4827-b797-ddb6a59a1433"
                }
            ]
        },
        {
            "id": "1a43ffad-a1c7-40bf-9124-f25494782231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64eb5b2d-3552-482e-8af3-1f68bf372e29",
            "compositeImage": {
                "id": "b0597440-a09d-4f26-a24d-038a6c88ad47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a43ffad-a1c7-40bf-9124-f25494782231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40915f6-c60e-4b5a-813a-d78eb560a5d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a43ffad-a1c7-40bf-9124-f25494782231",
                    "LayerId": "4311c750-22c9-4827-b797-ddb6a59a1433"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4311c750-22c9-4827-b797-ddb6a59a1433",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64eb5b2d-3552-482e-8af3-1f68bf372e29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}