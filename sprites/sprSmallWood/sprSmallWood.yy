{
    "id": "7f30503c-1189-4fd2-b924-9b42eb2c4428",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSmallWood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "53e178b0-dcb3-4ae6-8d0e-0fccb0a71d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f30503c-1189-4fd2-b924-9b42eb2c4428",
            "compositeImage": {
                "id": "2daa1c6c-e975-41ab-b6c4-02b471c6ab9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e178b0-dcb3-4ae6-8d0e-0fccb0a71d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa80e09-f566-4cb6-a7e5-b93ab6322f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e178b0-dcb3-4ae6-8d0e-0fccb0a71d84",
                    "LayerId": "9bf9cd0f-5117-403c-bdc3-1930ada58a14"
                }
            ]
        },
        {
            "id": "8fe2ac3a-5d9f-47a7-9e37-71d59fa08149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f30503c-1189-4fd2-b924-9b42eb2c4428",
            "compositeImage": {
                "id": "051304dd-e3de-4496-b131-d758e71a1211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fe2ac3a-5d9f-47a7-9e37-71d59fa08149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39267093-9b5d-4f73-9d16-a6f284a0a63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fe2ac3a-5d9f-47a7-9e37-71d59fa08149",
                    "LayerId": "9bf9cd0f-5117-403c-bdc3-1930ada58a14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9bf9cd0f-5117-403c-bdc3-1930ada58a14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f30503c-1189-4fd2-b924-9b42eb2c4428",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}