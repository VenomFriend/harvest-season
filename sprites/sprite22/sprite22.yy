{
    "id": "1bf2585f-b51d-4c61-a66b-981f721d3329",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "466913d4-bbc7-43ce-825c-8c5b99db60bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bf2585f-b51d-4c61-a66b-981f721d3329",
            "compositeImage": {
                "id": "70d7e3fa-b910-435b-90bc-228c16229932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "466913d4-bbc7-43ce-825c-8c5b99db60bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e064c72b-11be-4d4d-ad48-721b64935ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "466913d4-bbc7-43ce-825c-8c5b99db60bd",
                    "LayerId": "c3c91edd-ca0c-4232-9824-e1404c01339d"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 64,
    "layers": [
        {
            "id": "c3c91edd-ca0c-4232-9824-e1404c01339d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bf2585f-b51d-4c61-a66b-981f721d3329",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}