{
    "id": "d346bb6a-24c4-44c3-85bf-358ecda2c396",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7d0b32fa-8087-473f-b91b-67aa93fdb39c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d346bb6a-24c4-44c3-85bf-358ecda2c396",
            "compositeImage": {
                "id": "2b7dd1ef-3384-4947-9003-728393f42722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0b32fa-8087-473f-b91b-67aa93fdb39c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35cb5b0-4a72-4b87-a172-3589ecd1f295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0b32fa-8087-473f-b91b-67aa93fdb39c",
                    "LayerId": "4ead613d-b25c-48ba-aa7b-f4cd88729bfe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "4ead613d-b25c-48ba-aa7b-f4cd88729bfe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d346bb6a-24c4-44c3-85bf-358ecda2c396",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}