{
    "id": "7e58fe4e-891b-4302-9959-bc0ffa90901b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "aec8838d-fa7d-45fe-9694-ce368b342c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e58fe4e-891b-4302-9959-bc0ffa90901b",
            "compositeImage": {
                "id": "df3ae6cf-68d6-49f0-9bff-87ae3ed7671e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec8838d-fa7d-45fe-9694-ce368b342c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38976f56-fd29-4f9e-9419-d0489d86a783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec8838d-fa7d-45fe-9694-ce368b342c1a",
                    "LayerId": "30b3dd5a-6feb-4e70-bc2f-6609cdf1e220"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 28,
    "layers": [
        {
            "id": "30b3dd5a-6feb-4e70-bc2f-6609cdf1e220",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e58fe4e-891b-4302-9959-bc0ffa90901b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}