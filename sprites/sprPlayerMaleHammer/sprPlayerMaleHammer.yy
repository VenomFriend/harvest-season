{
    "id": "e880777b-c5c6-4f81-8152-badaee152f60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleHammer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "61bacfdc-4f12-4200-8391-712c63ee0300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "e9d7d460-18fe-44cc-a590-f7a6b5518739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61bacfdc-4f12-4200-8391-712c63ee0300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45be5fda-f1e9-46e9-a7f2-06052d184467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61bacfdc-4f12-4200-8391-712c63ee0300",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "a51c8721-b0f8-4a6e-9915-4f81c8c21291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "7581883c-a75b-465a-9c94-17f2e6cb4831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51c8721-b0f8-4a6e-9915-4f81c8c21291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "525c9959-d125-4b19-ac13-b04910fe321e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51c8721-b0f8-4a6e-9915-4f81c8c21291",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "dbf89de1-b49a-49ea-a82a-236f4969ce60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "c4c3c161-fbd4-4410-b074-e2fe51ca5341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf89de1-b49a-49ea-a82a-236f4969ce60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c673adcb-70dc-499d-ad8a-df0b13f83e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf89de1-b49a-49ea-a82a-236f4969ce60",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "b24bbfe0-d3fa-490d-9747-207e2d862fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "224bd8c7-68d2-42d0-a94f-fd4c62eff841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b24bbfe0-d3fa-490d-9747-207e2d862fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b0c1f1-2efa-46ac-aa0b-0ca97696aa1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b24bbfe0-d3fa-490d-9747-207e2d862fbf",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "0a43b0fc-ae33-495d-81c4-50fb624ae136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "6bd5f8cb-769c-4cb6-8711-5db00e7801ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a43b0fc-ae33-495d-81c4-50fb624ae136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "360fecb4-2750-40aa-bb6e-400726f19e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a43b0fc-ae33-495d-81c4-50fb624ae136",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "8618c8b2-f6f4-460d-b96e-3677286395a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "f330aa65-212d-4aa0-aca4-0a915a4a5d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8618c8b2-f6f4-460d-b96e-3677286395a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c4b9993-168e-45f4-a493-de724f1e6253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8618c8b2-f6f4-460d-b96e-3677286395a2",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "0c68b334-2615-4a6a-ab20-92349b6f808d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "18548c77-d11b-45a8-8031-37f8f2ffc4a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c68b334-2615-4a6a-ab20-92349b6f808d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef415806-cdf3-4613-bfa0-2225bd69bc99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c68b334-2615-4a6a-ab20-92349b6f808d",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "fb10e22a-a22e-48e7-8d74-230aec7c28cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "257f0bfe-22d4-4f5c-80a3-edff4f587914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb10e22a-a22e-48e7-8d74-230aec7c28cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd3213a0-d8f2-4d3d-b41e-1126a3f85d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb10e22a-a22e-48e7-8d74-230aec7c28cc",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "52859391-578e-4d42-8b8b-c1c627693cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "a2b0a284-b82d-44e4-85d4-74ddbdd4c31c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52859391-578e-4d42-8b8b-c1c627693cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e10a02-453d-49c4-a245-dde8ce86f876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52859391-578e-4d42-8b8b-c1c627693cdb",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "8b437957-f5be-46e7-8598-955718ae2683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "fda3338f-a337-4c84-8eba-bdcaa43faff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b437957-f5be-46e7-8598-955718ae2683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "502b73cc-78f1-4041-9bd9-e813bb838569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b437957-f5be-46e7-8598-955718ae2683",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "ec6e8c03-ecdb-438b-bf4b-2f56e5fac8a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "2f9bc6bb-d6c0-4b10-bd9e-0c8158af60ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6e8c03-ecdb-438b-bf4b-2f56e5fac8a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47effb9-a7c9-4d98-a575-e60950825400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6e8c03-ecdb-438b-bf4b-2f56e5fac8a0",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "1eb1b77b-73a9-42b3-8b4c-03a5ff5db76b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "f73aa8ae-803b-47ce-8bd4-3e87be1bf1a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eb1b77b-73a9-42b3-8b4c-03a5ff5db76b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9e7e97-c322-43a8-af2d-d19832f0fd47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eb1b77b-73a9-42b3-8b4c-03a5ff5db76b",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "61436e8d-a93b-4505-bd3a-ed9a9ef64e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "b89092f0-7abe-4a04-a466-f8f775a168c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61436e8d-a93b-4505-bd3a-ed9a9ef64e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c51326-c1c6-4802-982c-f71eeeb645f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61436e8d-a93b-4505-bd3a-ed9a9ef64e3c",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "ba1e2e4c-f3ee-4f7b-875c-7850f28b88ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "b25724fa-1622-4b8c-b0af-dbb017310d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba1e2e4c-f3ee-4f7b-875c-7850f28b88ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f9d874-fab2-410f-adf5-d26833664904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba1e2e4c-f3ee-4f7b-875c-7850f28b88ba",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "b08af3c8-8f62-45f0-8c4d-ed306914fea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "7f34b8cb-67e0-4b18-a2b8-caeca759587e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08af3c8-8f62-45f0-8c4d-ed306914fea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c64f2c2-598f-4696-8086-142ed2a39c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08af3c8-8f62-45f0-8c4d-ed306914fea9",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        },
        {
            "id": "a44aca28-15d3-4763-bd39-b28607ae9661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "compositeImage": {
                "id": "4858319b-75c4-4ca5-b448-be9720c89809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44aca28-15d3-4763-bd39-b28607ae9661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d01ae5-162f-46c4-8ed2-6359cb6e8254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44aca28-15d3-4763-bd39-b28607ae9661",
                    "LayerId": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "3f52572c-6dd4-44ff-8df6-8eb5a1da0f87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e880777b-c5c6-4f81-8152-badaee152f60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}