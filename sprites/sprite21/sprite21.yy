{
    "id": "1e533a0b-0069-492d-a205-747ce67edf62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8b419d94-a65d-4edf-b930-cff15f14b356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e533a0b-0069-492d-a205-747ce67edf62",
            "compositeImage": {
                "id": "0eb4d6ec-867b-499d-89ed-f882c45bfc82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b419d94-a65d-4edf-b930-cff15f14b356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658670b4-cd17-460b-8f48-edbe6816beb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b419d94-a65d-4edf-b930-cff15f14b356",
                    "LayerId": "65f43ae5-e3be-4835-9f35-1882f312dfee"
                }
            ]
        },
        {
            "id": "1cf880f4-1f1b-4be3-9f3e-ebf2abffcffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e533a0b-0069-492d-a205-747ce67edf62",
            "compositeImage": {
                "id": "98cfadef-dd37-4fbb-ba52-9dab3b943738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf880f4-1f1b-4be3-9f3e-ebf2abffcffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17ad0d90-05fa-4cb6-9eaf-5a61475b0646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf880f4-1f1b-4be3-9f3e-ebf2abffcffd",
                    "LayerId": "65f43ae5-e3be-4835-9f35-1882f312dfee"
                }
            ]
        },
        {
            "id": "9d87890f-4011-48a0-84b4-ffa2d53834d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e533a0b-0069-492d-a205-747ce67edf62",
            "compositeImage": {
                "id": "a1bed029-1eed-43aa-b64f-618b484eb3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d87890f-4011-48a0-84b4-ffa2d53834d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05385b7-ef75-4b38-a712-588e901407ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d87890f-4011-48a0-84b4-ffa2d53834d4",
                    "LayerId": "65f43ae5-e3be-4835-9f35-1882f312dfee"
                }
            ]
        },
        {
            "id": "b6bee19a-0726-4935-86c8-bd1b39b76ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e533a0b-0069-492d-a205-747ce67edf62",
            "compositeImage": {
                "id": "0d6b5b3e-e410-4dd5-b268-c0c973256ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6bee19a-0726-4935-86c8-bd1b39b76ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e8d547-76bb-4018-8be6-16118c304a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6bee19a-0726-4935-86c8-bd1b39b76ff2",
                    "LayerId": "65f43ae5-e3be-4835-9f35-1882f312dfee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "65f43ae5-e3be-4835-9f35-1882f312dfee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e533a0b-0069-492d-a205-747ce67edf62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 0
}