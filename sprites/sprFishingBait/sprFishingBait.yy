{
    "id": "bc571a91-09c1-46f1-a8d5-4b0a1c19743f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprFishingBait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "615963f9-dc73-46db-bfbe-d1f046c5045f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc571a91-09c1-46f1-a8d5-4b0a1c19743f",
            "compositeImage": {
                "id": "b422664a-3bfc-4aed-b3d4-a930ca365459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "615963f9-dc73-46db-bfbe-d1f046c5045f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec91771-4200-41e2-b79a-057f2a8ca558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "615963f9-dc73-46db-bfbe-d1f046c5045f",
                    "LayerId": "5809b72c-5f4d-4b99-b8ba-ee03d9d60821"
                }
            ]
        },
        {
            "id": "9701a5ec-eab4-4651-9f0a-e3802f050ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc571a91-09c1-46f1-a8d5-4b0a1c19743f",
            "compositeImage": {
                "id": "e8d031af-1b29-4c97-94b6-270bca77061c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9701a5ec-eab4-4651-9f0a-e3802f050ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8554a5-842e-475c-b111-68d94faf38c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9701a5ec-eab4-4651-9f0a-e3802f050ba2",
                    "LayerId": "5809b72c-5f4d-4b99-b8ba-ee03d9d60821"
                }
            ]
        },
        {
            "id": "94a2ca44-86bf-4bf5-9e37-764f8d6f85ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc571a91-09c1-46f1-a8d5-4b0a1c19743f",
            "compositeImage": {
                "id": "206472c9-f884-4046-abae-5432ff4fef42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a2ca44-86bf-4bf5-9e37-764f8d6f85ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9701c270-1d3e-430c-a173-66daca131ecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a2ca44-86bf-4bf5-9e37-764f8d6f85ff",
                    "LayerId": "5809b72c-5f4d-4b99-b8ba-ee03d9d60821"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5809b72c-5f4d-4b99-b8ba-ee03d9d60821",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc571a91-09c1-46f1-a8d5-4b0a1c19743f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}