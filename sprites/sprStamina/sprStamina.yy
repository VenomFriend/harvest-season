{
    "id": "29382a50-02dc-4c39-88f0-75dfe10d2539",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStamina",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3f64ec57-4ab6-49af-8be4-f687a9c08e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "2d20e5f5-7ae4-4a65-a626-dc2f61b73c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f64ec57-4ab6-49af-8be4-f687a9c08e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42148177-65b4-40ca-b611-c6b32627b80f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f64ec57-4ab6-49af-8be4-f687a9c08e77",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "41331a5d-a8b5-41f8-803d-187f9379415e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "646b4c42-32f8-4608-bbeb-7817f872377a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41331a5d-a8b5-41f8-803d-187f9379415e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d01a0dd-fc9c-41c5-a46b-db298c685645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41331a5d-a8b5-41f8-803d-187f9379415e",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "d8c94dc0-864d-40a9-b55e-4595440843de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "f5e59175-9ab2-41a7-8e6e-e1890f96f6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8c94dc0-864d-40a9-b55e-4595440843de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db553e8-8c68-44ad-b527-f772f41a5d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8c94dc0-864d-40a9-b55e-4595440843de",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "8d5f2113-897c-4c2c-83e7-221d2ffc15d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "bb71b7e4-aa70-45ef-bf21-398167d6512a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5f2113-897c-4c2c-83e7-221d2ffc15d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c861c82d-5b62-41b2-a205-80c41d187c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5f2113-897c-4c2c-83e7-221d2ffc15d9",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "df70a6a5-969b-44a4-90fd-bb922a1c9ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "38d141db-671b-44a6-87f8-aa9ac1b32d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df70a6a5-969b-44a4-90fd-bb922a1c9ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7573056-1a77-47af-95fb-5249488b1c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df70a6a5-969b-44a4-90fd-bb922a1c9ff0",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "a871bac1-198a-4248-9f39-e4ff7908d2e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "35fbfaff-c1af-4f9c-9c83-1c68e5e835f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a871bac1-198a-4248-9f39-e4ff7908d2e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d450946-069c-4941-b09e-39e2b8d14607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a871bac1-198a-4248-9f39-e4ff7908d2e9",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "a2fb52fc-f6bb-47de-afbc-4a186e39b725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "f0133d7c-7bdf-4a43-9540-2fb30d45f79b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2fb52fc-f6bb-47de-afbc-4a186e39b725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24a8fba-4472-49af-be19-09bf69a9d798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2fb52fc-f6bb-47de-afbc-4a186e39b725",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        },
        {
            "id": "799f04fa-78a3-4b3f-b270-b47d4f02a3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "compositeImage": {
                "id": "18f801fe-19a4-47c8-ba0a-1270dc60347d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799f04fa-78a3-4b3f-b270-b47d4f02a3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1abdc8e-5bb2-4cc5-bf5b-5454127734b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799f04fa-78a3-4b3f-b270-b47d4f02a3be",
                    "LayerId": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0899cc1a-cd95-42e4-8f53-bd0f18c74c98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29382a50-02dc-4c39-88f0-75dfe10d2539",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}