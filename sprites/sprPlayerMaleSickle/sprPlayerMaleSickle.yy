{
    "id": "54a204c9-457a-403b-bf80-4a8e601e5421",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleSickle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "64799eb0-fb5c-4c61-a821-a2b9504c7b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "081fefe2-1683-47b8-94c7-02f225dcd5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64799eb0-fb5c-4c61-a821-a2b9504c7b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b64149d-1ce1-4bb2-9ce8-1985bdb7a974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64799eb0-fb5c-4c61-a821-a2b9504c7b47",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "6592974e-ea24-4ff8-8e55-529a9e2100d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "0cdb5f01-a0b9-4068-a2e2-e903929edf0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6592974e-ea24-4ff8-8e55-529a9e2100d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "109ed988-a108-4361-a036-6ae2a8e8184b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6592974e-ea24-4ff8-8e55-529a9e2100d7",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "4f46b7ca-ded9-4b74-abba-315e2a7aebd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "05c11edd-d38a-4d06-8548-2b63f00b5fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f46b7ca-ded9-4b74-abba-315e2a7aebd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2b33375-5c9f-43cb-a848-a4554704680c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f46b7ca-ded9-4b74-abba-315e2a7aebd2",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "c8fb11fc-cf70-499a-b82b-a5cc036ff0dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "a89cb7cc-9878-43f0-92bb-1f089d00a234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8fb11fc-cf70-499a-b82b-a5cc036ff0dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871bba3e-7ed5-4fe4-b6e5-e05e2bbab750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8fb11fc-cf70-499a-b82b-a5cc036ff0dc",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "143b5bd5-307e-4eda-ad5a-80df8541e6f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "c11aadf1-0352-48f7-bcfd-ecf344077d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143b5bd5-307e-4eda-ad5a-80df8541e6f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4535a76b-835c-4285-96b7-3ff0ad4b7c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143b5bd5-307e-4eda-ad5a-80df8541e6f0",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "97158a8d-d7cb-46f7-8cc8-375e534d6881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "3dbb0842-06b9-467d-bf59-0f4aebfc8828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97158a8d-d7cb-46f7-8cc8-375e534d6881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21c77cd-5b28-43e7-b91a-edabdde059c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97158a8d-d7cb-46f7-8cc8-375e534d6881",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "39bd8acc-672f-42bd-90a6-fa5c4173248a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "c716a920-d07d-4fca-9432-12686106ff1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39bd8acc-672f-42bd-90a6-fa5c4173248a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89212c4f-d9c7-4a87-b486-a37ea993854f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39bd8acc-672f-42bd-90a6-fa5c4173248a",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "7761c1c3-9bbb-4cea-9773-1c62e0601452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "cb9f0bd5-41c0-4b39-ac95-c61d10a410ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7761c1c3-9bbb-4cea-9773-1c62e0601452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52926fea-7f33-48b7-bc76-99e0b4c1cc09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7761c1c3-9bbb-4cea-9773-1c62e0601452",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "aff0926f-b4b3-45ff-bb9a-e8eb8d83bfd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "44ca6b9e-4375-44cc-86b4-a99a5e70489b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aff0926f-b4b3-45ff-bb9a-e8eb8d83bfd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5923788-e7e0-4d9f-ba0f-57bb7751841c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aff0926f-b4b3-45ff-bb9a-e8eb8d83bfd8",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "7cbd6ac3-353d-4616-b64e-be06a9b33931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "522f17af-51c4-4976-9363-a845d2e77336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cbd6ac3-353d-4616-b64e-be06a9b33931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e46e5e-4f6d-4662-bf0b-c009924d32f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cbd6ac3-353d-4616-b64e-be06a9b33931",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "bd004c56-2e1e-49ef-9889-5c4c5993d3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "22a099ae-a025-4333-91c9-c034bb42750a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd004c56-2e1e-49ef-9889-5c4c5993d3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25bef978-37b4-4bba-a10f-1718b5d217c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd004c56-2e1e-49ef-9889-5c4c5993d3e5",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "25bbbaba-7820-4b5a-bddc-2156bb096029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "7f016101-c978-4f43-88e3-4e5fc2a64cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25bbbaba-7820-4b5a-bddc-2156bb096029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b1038d-7417-4063-8f05-3fb1cba712ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25bbbaba-7820-4b5a-bddc-2156bb096029",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "dc0dc0da-40c9-4422-b140-859dc9a79a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "ae4b1508-2f72-47e8-8963-a68bb2dbd896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc0dc0da-40c9-4422-b140-859dc9a79a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58fdf002-da00-4b26-a77d-97b2e78d04a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc0dc0da-40c9-4422-b140-859dc9a79a27",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "20177b60-5160-4ef9-a635-8d9675a7c6ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "c69cbb03-1593-47fc-a892-108df714362c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20177b60-5160-4ef9-a635-8d9675a7c6ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfd0fa5-c6d4-4d80-be4f-148d204d014a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20177b60-5160-4ef9-a635-8d9675a7c6ab",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "278b5123-5498-42ff-813e-4c8b9f5a2c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "5a65bef7-cd51-4a4e-95b6-e7f577bc90bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "278b5123-5498-42ff-813e-4c8b9f5a2c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fe7d00-8c8d-4e55-bc73-a42f17014d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "278b5123-5498-42ff-813e-4c8b9f5a2c13",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        },
        {
            "id": "521ea409-db83-4500-bd5c-4e83c5598739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "compositeImage": {
                "id": "e5e42c89-02a9-4e62-8d4b-09ed956a811d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "521ea409-db83-4500-bd5c-4e83c5598739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19686322-753c-4120-97d5-878e32c35c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "521ea409-db83-4500-bd5c-4e83c5598739",
                    "LayerId": "10856b9a-5226-4cab-9640-362ba5de3398"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "10856b9a-5226-4cab-9640-362ba5de3398",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54a204c9-457a-403b-bf80-4a8e601e5421",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}