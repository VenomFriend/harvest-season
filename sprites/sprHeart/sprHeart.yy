{
    "id": "373ba00e-e1e3-4fa9-abee-ea7f7975d2e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 21,
    "bbox_right": 73,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d9867178-3b57-424a-9872-0d0a2d635c0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373ba00e-e1e3-4fa9-abee-ea7f7975d2e6",
            "compositeImage": {
                "id": "cc025bcb-2ed0-4b9e-9478-943bca65d8b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9867178-3b57-424a-9872-0d0a2d635c0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa28a875-c5b7-413a-9890-521992255733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9867178-3b57-424a-9872-0d0a2d635c0d",
                    "LayerId": "4df4f17c-1a6a-4c85-b2b9-58ccbc9ca009"
                }
            ]
        },
        {
            "id": "6ff72303-4b9a-4692-be9d-3f1cc1299001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373ba00e-e1e3-4fa9-abee-ea7f7975d2e6",
            "compositeImage": {
                "id": "60ee6512-afdc-427b-b1b7-d0cc6f2f4e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff72303-4b9a-4692-be9d-3f1cc1299001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff2ca53-f140-4ee0-86d2-7d6747c54445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff72303-4b9a-4692-be9d-3f1cc1299001",
                    "LayerId": "4df4f17c-1a6a-4c85-b2b9-58ccbc9ca009"
                }
            ]
        },
        {
            "id": "6b5400dd-eb70-41db-afda-1099a02dafeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373ba00e-e1e3-4fa9-abee-ea7f7975d2e6",
            "compositeImage": {
                "id": "5e77d149-f38b-4dd3-9707-33d757fe5916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b5400dd-eb70-41db-afda-1099a02dafeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a977236-d8d3-44b3-92a8-4d621e8a5881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b5400dd-eb70-41db-afda-1099a02dafeb",
                    "LayerId": "4df4f17c-1a6a-4c85-b2b9-58ccbc9ca009"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4df4f17c-1a6a-4c85-b2b9-58ccbc9ca009",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "373ba00e-e1e3-4fa9-abee-ea7f7975d2e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}