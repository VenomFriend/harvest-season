{
    "id": "914054fa-27dd-4d3e-bb42-c180f3b4e768",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileset0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a01c117b-bbc4-49d9-8022-8f23de7f93b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "914054fa-27dd-4d3e-bb42-c180f3b4e768",
            "compositeImage": {
                "id": "b1fa2a7a-0ad1-497c-b807-1ce8bf1292ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01c117b-bbc4-49d9-8022-8f23de7f93b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50c4705f-45b8-4b49-a823-dffea3e4a092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01c117b-bbc4-49d9-8022-8f23de7f93b3",
                    "LayerId": "931f800d-e245-4e65-8aeb-9be533a54077"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "931f800d-e245-4e65-8aeb-9be533a54077",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "914054fa-27dd-4d3e-bb42-c180f3b4e768",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}