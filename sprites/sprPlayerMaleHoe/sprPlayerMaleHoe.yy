{
    "id": "67674d39-b183-4e11-9e96-5b082bf286f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleHoe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c3ab1449-1196-4da8-b7dd-bfce484a18d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "a5c4afd7-f4ac-41f6-915c-bbeb21b3035e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ab1449-1196-4da8-b7dd-bfce484a18d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e6107c3-0068-4956-9334-d8bdbe6bcb04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ab1449-1196-4da8-b7dd-bfce484a18d0",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "c3a076f2-ce8f-4826-80ea-8b8273fd17d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "726a42f0-1a8a-4fda-b2e6-742d12d82e4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a076f2-ce8f-4826-80ea-8b8273fd17d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad154e9-184d-4dec-954f-3e68a588ab9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a076f2-ce8f-4826-80ea-8b8273fd17d3",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "5e21bdee-a7b3-4a2b-9516-14161e61f3d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "b2aec159-d1c2-4b44-899b-6295bdfe044d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e21bdee-a7b3-4a2b-9516-14161e61f3d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bbe2a04-4bb5-4a67-9834-b4ce3b064fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e21bdee-a7b3-4a2b-9516-14161e61f3d6",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "91cc9502-88e9-47b1-8253-86a3589b5685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "a5db8de6-9405-4883-80b5-3cd74c0df186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91cc9502-88e9-47b1-8253-86a3589b5685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09de2670-5782-43b2-b6f8-038f0237ff01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91cc9502-88e9-47b1-8253-86a3589b5685",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "1ac3d051-9f6e-47a2-a71b-54d1908cb82a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "5685128e-a84b-41f6-a8f7-9a9d0b879b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac3d051-9f6e-47a2-a71b-54d1908cb82a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bcc6dfe-0322-4d74-8328-9566642b1d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac3d051-9f6e-47a2-a71b-54d1908cb82a",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "86964bfb-2326-4f3b-a18a-0394e982bd64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "cf682711-9cfc-4143-9f7a-a9e59bcb04d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86964bfb-2326-4f3b-a18a-0394e982bd64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797b2f27-c086-4ed2-9e0f-9b8bb7be5602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86964bfb-2326-4f3b-a18a-0394e982bd64",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "67288fbd-8959-4f24-a33f-4091c27a4174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "019fb75b-234b-4a1a-a404-0f733501b8aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67288fbd-8959-4f24-a33f-4091c27a4174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34fca74f-77be-48f1-92d7-ddd631cd2bcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67288fbd-8959-4f24-a33f-4091c27a4174",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "7ab52747-3a75-4a9f-9cff-0de57e2e765c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "e35d1e4e-ef21-41ab-bb1c-66f96e74c547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab52747-3a75-4a9f-9cff-0de57e2e765c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f763fd58-d8e6-4bff-9a28-9626273cc1a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab52747-3a75-4a9f-9cff-0de57e2e765c",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "23a38d58-01bd-413e-826c-3d8f5e394212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "8379b218-502d-4d1e-9c70-b3f7d1b2b9fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a38d58-01bd-413e-826c-3d8f5e394212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e770ff-955e-456a-8aeb-dec950296218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a38d58-01bd-413e-826c-3d8f5e394212",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "591e01e8-1868-4095-927e-11e4584761ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "4c2bee68-6c4a-464a-ba36-a3d428e51f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591e01e8-1868-4095-927e-11e4584761ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f7c4b5-db8f-49d6-b4b9-e33678a024fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591e01e8-1868-4095-927e-11e4584761ac",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "8467a18c-e00f-4b18-b965-2eda190ae301",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "5273abc9-1d2d-4f8f-8f2f-3c938ff1e51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8467a18c-e00f-4b18-b965-2eda190ae301",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e572a597-35c6-41da-a633-ced92206228f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8467a18c-e00f-4b18-b965-2eda190ae301",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "7b3a59eb-6745-40be-b4b7-2c4e5fd08ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "0c3e5eb5-1241-437d-9e6a-e2f98bbb304c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3a59eb-6745-40be-b4b7-2c4e5fd08ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650b0353-2de3-4139-a2dd-0ba2be4e8602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3a59eb-6745-40be-b4b7-2c4e5fd08ced",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "97f3bf66-01af-4cd5-8607-5da6415afbae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "28586c62-08fe-4b49-9b19-72c2b42d6039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f3bf66-01af-4cd5-8607-5da6415afbae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a11fb14-7935-44c6-a941-485978968b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f3bf66-01af-4cd5-8607-5da6415afbae",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "42709074-1061-4c0b-ae01-9e4adfe8f456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "a068eb48-539c-4112-895d-093bb9935941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42709074-1061-4c0b-ae01-9e4adfe8f456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c418926-f708-4e67-9cf6-b884144bbd87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42709074-1061-4c0b-ae01-9e4adfe8f456",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "a3648617-32dc-4a7b-a5f2-5a2757383612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "aacbad24-22c6-4191-9b80-5895668063bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3648617-32dc-4a7b-a5f2-5a2757383612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83df68d2-e431-422c-a987-8b79ef39217b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3648617-32dc-4a7b-a5f2-5a2757383612",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        },
        {
            "id": "226feb8c-ebce-42ed-ab1c-71d05c3595db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "compositeImage": {
                "id": "169bdbce-f5ee-4e7f-b9ee-89cedc2295d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "226feb8c-ebce-42ed-ab1c-71d05c3595db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de566af-4cbd-48e3-a45a-68d26836888f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "226feb8c-ebce-42ed-ab1c-71d05c3595db",
                    "LayerId": "8b20cc0e-b53c-41be-a18c-867c5cddb295"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 60,
    "layers": [
        {
            "id": "8b20cc0e-b53c-41be-a18c-867c5cddb295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67674d39-b183-4e11-9e96-5b082bf286f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}