{
    "id": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleFishing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5fd3da10-fafe-43e1-aab7-ec3261778cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "cccd4e3f-5797-4d65-8747-8fe2b5fa9130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd3da10-fafe-43e1-aab7-ec3261778cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b954cd6-d207-4bde-9aba-98fceb7ad6ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd3da10-fafe-43e1-aab7-ec3261778cef",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "d1297884-098a-4931-a543-bba287dd8425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "04269256-5d6f-4b52-b1b7-9f2e47b6e7ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1297884-098a-4931-a543-bba287dd8425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dcaa493-9791-4338-8de2-8c8458aa8120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1297884-098a-4931-a543-bba287dd8425",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "ba8a183d-7c12-48cc-ad70-f3ae6b0f0a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "8b7a7ec1-b5e7-44c6-ad8d-9ad97f471585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8a183d-7c12-48cc-ad70-f3ae6b0f0a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc19d531-7c5d-4880-95b4-dd3909b325eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8a183d-7c12-48cc-ad70-f3ae6b0f0a0c",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "3cd76cb7-f657-4fb3-9322-02fb1e95fc52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "63a3c153-378a-4e6e-b771-a0ebce6e250f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd76cb7-f657-4fb3-9322-02fb1e95fc52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "848c5995-457c-4c2a-bc19-bebb2462f46b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd76cb7-f657-4fb3-9322-02fb1e95fc52",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "67f99fa4-ea4b-4a06-ad36-58ba1fbf7767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "80cfa2ad-8a32-4f14-8ac8-88cf0fbc763f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67f99fa4-ea4b-4a06-ad36-58ba1fbf7767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c5bc05b-6d2e-4e5e-b7a0-b3daad092d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67f99fa4-ea4b-4a06-ad36-58ba1fbf7767",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "2ce5fed7-85ab-4b7e-b3b4-57a91da5efd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "f16c1afe-c9c6-466d-880e-ef83ac0b4ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce5fed7-85ab-4b7e-b3b4-57a91da5efd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1607423d-1a4f-4df8-af9d-d4817a13ed17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce5fed7-85ab-4b7e-b3b4-57a91da5efd4",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "2f99ed66-0ae6-4444-93db-3a4dddbc06e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "be9a096d-1b10-45f7-996a-b7396d951d7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f99ed66-0ae6-4444-93db-3a4dddbc06e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1f9d0e4-1758-4605-932c-2fd59d9c4fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f99ed66-0ae6-4444-93db-3a4dddbc06e2",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "fc3c06b3-5bb3-43a8-ad66-893fe9deaf66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "3127a3a0-632b-4e7e-a5f7-d98e84a44949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3c06b3-5bb3-43a8-ad66-893fe9deaf66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a3c9d6b-3c0e-47b1-8043-514105ea5ab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3c06b3-5bb3-43a8-ad66-893fe9deaf66",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "730addb9-686d-49b6-b6e1-d21cb1e47751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "3822cc29-b9d4-4dbe-bb5b-ac90380d7974",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "730addb9-686d-49b6-b6e1-d21cb1e47751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f3ba2e-1c59-4c63-b7f7-f309bccd7c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "730addb9-686d-49b6-b6e1-d21cb1e47751",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "1a796c11-beb0-4832-a081-3616faf6e207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "0e66ed9a-a457-402b-91ea-f6002eb26a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a796c11-beb0-4832-a081-3616faf6e207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7881e5b0-1283-4459-84b2-060486c671ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a796c11-beb0-4832-a081-3616faf6e207",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "14d7eea6-1f46-4769-a86e-577f8f1e8f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "814024b6-4cdd-40cf-9b01-21938cad93f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d7eea6-1f46-4769-a86e-577f8f1e8f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8389122-9f98-4cec-b2c1-99df6c2bda80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d7eea6-1f46-4769-a86e-577f8f1e8f7f",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "23b33ac2-ad54-41a3-a9f4-30154d589731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "17ce40de-bf2f-44ca-b849-b12296c58fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b33ac2-ad54-41a3-a9f4-30154d589731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa802629-a386-4cdc-ae49-2680a79f10b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b33ac2-ad54-41a3-a9f4-30154d589731",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "08b460df-a659-4f16-ae8f-6c8b5f483a88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "c424d90e-61e4-4026-9d03-3dc36ff61ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08b460df-a659-4f16-ae8f-6c8b5f483a88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e6a396b-7605-4eee-8f4f-9a5d9fe192a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08b460df-a659-4f16-ae8f-6c8b5f483a88",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "f18058f8-be36-4619-90c9-4b11bd1f34bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "efef9394-af98-4fd7-bb30-7e0f48fcff78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18058f8-be36-4619-90c9-4b11bd1f34bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea9015ec-0ab9-4d80-9695-845f1f98a2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18058f8-be36-4619-90c9-4b11bd1f34bf",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "472c5860-16ea-4b00-ab55-9b8477b26a95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "f07bd375-f787-4e02-9ab4-2f421fd6c33e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472c5860-16ea-4b00-ab55-9b8477b26a95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0578cbb1-3f89-4e59-ac1c-9ec8285748a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472c5860-16ea-4b00-ab55-9b8477b26a95",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        },
        {
            "id": "d97f0bfc-ca71-4512-8e66-719140107c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "compositeImage": {
                "id": "49fa25a0-3529-4022-9b14-8395bad2ea0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97f0bfc-ca71-4512-8e66-719140107c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d632adda-fa0f-4cbf-a242-7bb590ebc753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97f0bfc-ca71-4512-8e66-719140107c08",
                    "LayerId": "c15af779-8da4-460b-ae9b-1858a86c124c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "c15af779-8da4-460b-ae9b-1858a86c124c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3069eb39-b8a9-410b-948f-93bc6dd9c2a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}