{
    "id": "869a65c2-ec61-4982-9257-39f51905c55d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleAxe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7d299cd6-72da-4dfd-bec2-0afc9ceeb12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "19730fd9-7a15-4f90-8f36-3ab2e0c65bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d299cd6-72da-4dfd-bec2-0afc9ceeb12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d7aa64-b773-4bd5-a6a4-85fcdc26155c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d299cd6-72da-4dfd-bec2-0afc9ceeb12c",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "6dee9547-3b3d-4cdd-a2fc-9c01bdede9fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "dfa29353-9899-429e-beda-19e633810316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dee9547-3b3d-4cdd-a2fc-9c01bdede9fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f888ec-65dc-4ddf-805d-8e65ace078a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dee9547-3b3d-4cdd-a2fc-9c01bdede9fb",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "a68c6260-d129-4fa5-aa76-1815b1d24025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "d9c845ac-de59-495f-8db5-8f2fec194eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68c6260-d129-4fa5-aa76-1815b1d24025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11df1086-c860-4e15-b31e-e35b32de485d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68c6260-d129-4fa5-aa76-1815b1d24025",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "1502b011-1aeb-41ea-b681-1c9140559465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "b48cc8af-e909-4596-93e8-3115f51f012f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1502b011-1aeb-41ea-b681-1c9140559465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95dce87b-e7ec-4119-896e-e761ae72d4df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1502b011-1aeb-41ea-b681-1c9140559465",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "4b669230-87a4-4b30-85c5-1799e0be3a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "906a61db-8563-4b6c-bbc1-a27c635e1b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b669230-87a4-4b30-85c5-1799e0be3a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e6952f-dfdf-4a04-8cd4-f4ff82704c49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b669230-87a4-4b30-85c5-1799e0be3a8e",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "9a8c2ad7-248d-45fb-8191-994bcf5bd746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "c0bd3460-7862-48a0-b84b-2dde5efebce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a8c2ad7-248d-45fb-8191-994bcf5bd746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9087d6b-ea62-455f-b01a-7e9b90d2e6fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a8c2ad7-248d-45fb-8191-994bcf5bd746",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "d387c633-4cf9-4c45-8fbf-3348058688fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "f08ba6ff-bc2d-430c-8be0-c1de054dbde7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d387c633-4cf9-4c45-8fbf-3348058688fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cf86e76-9761-465a-b7aa-9050104f0d25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d387c633-4cf9-4c45-8fbf-3348058688fd",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "353715ec-2cb7-4f57-a497-c79eb40e3820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "3ec46138-a103-4016-97e1-e8700952cac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "353715ec-2cb7-4f57-a497-c79eb40e3820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a8d46a-a1f4-493e-8f04-6a072a776db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "353715ec-2cb7-4f57-a497-c79eb40e3820",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "c57aa9fd-c2db-4a75-bd61-81aafccc3eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "cd56367a-8c7e-4195-9dcb-7acc30fd67f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c57aa9fd-c2db-4a75-bd61-81aafccc3eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9cb00d4-a2e4-4a7a-8e21-6ead6b986402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c57aa9fd-c2db-4a75-bd61-81aafccc3eaf",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "da8778d1-569b-44be-a526-7afcc5bd9d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "a19dab81-f1cb-44c1-b1c1-c6cb523869e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da8778d1-569b-44be-a526-7afcc5bd9d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ec5ad7-0993-4939-8e4c-d51b622fbde1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da8778d1-569b-44be-a526-7afcc5bd9d97",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "e6aba4d6-9a23-424a-a5ae-0836f7c22935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "d0a3bc01-da21-4c22-8dd8-5e6762365ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6aba4d6-9a23-424a-a5ae-0836f7c22935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b185d7a1-7e9c-44b2-9472-9ab5a56f9ad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6aba4d6-9a23-424a-a5ae-0836f7c22935",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "7a5ecd8c-6ceb-4079-9d21-8079a6fdcb35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "847e8ddd-6f8f-40b0-a1f1-6378fb46498e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a5ecd8c-6ceb-4079-9d21-8079a6fdcb35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8d4645b-79bf-44d1-9381-52d7d47775d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a5ecd8c-6ceb-4079-9d21-8079a6fdcb35",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "03db1baf-7c75-4005-8d23-f23ccebd4b29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "d67a7755-55f4-4acb-a4b5-7eb758075170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03db1baf-7c75-4005-8d23-f23ccebd4b29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b0c402-b8d1-4500-a5a0-134e1d404444",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03db1baf-7c75-4005-8d23-f23ccebd4b29",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "057d976f-fb1a-4b05-b0ff-d4d52bc01c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "b2ec89a7-9923-41cf-a247-85b8393722be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057d976f-fb1a-4b05-b0ff-d4d52bc01c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8adb568-eb81-4245-8d90-da2c48000674",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057d976f-fb1a-4b05-b0ff-d4d52bc01c05",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "5514f66f-e217-4910-a37d-4ae3c7891e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "78ba91b3-5efa-47cd-a342-e9ff3aeb920b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5514f66f-e217-4910-a37d-4ae3c7891e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a733ac3-4a3d-400e-8c19-2c8386b15f98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5514f66f-e217-4910-a37d-4ae3c7891e7a",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        },
        {
            "id": "702dfdb8-f8ae-4ef9-949c-aabdf1a8bb62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "compositeImage": {
                "id": "63b3d460-b814-4208-ab78-aa7c50c4c2d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "702dfdb8-f8ae-4ef9-949c-aabdf1a8bb62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c014c829-74d6-4d98-9192-900b2fc3b284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "702dfdb8-f8ae-4ef9-949c-aabdf1a8bb62",
                    "LayerId": "ca2662b8-27b7-42cb-980f-c7052abdd00d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "ca2662b8-27b7-42cb-980f-c7052abdd00d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "869a65c2-ec61-4982-9257-39f51905c55d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}