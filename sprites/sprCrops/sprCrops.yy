{
    "id": "a6449f79-f62b-429d-8731-cc1244d1ceca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCrops",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2cbcf7d4-940c-420d-8a9b-215d30f2f5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2f8c1c62-81c2-464b-bc64-cb7d8758da41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbcf7d4-940c-420d-8a9b-215d30f2f5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a9428bd-6e4b-4d41-922a-44ee6eb53f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbcf7d4-940c-420d-8a9b-215d30f2f5f2",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5a7216a7-8fe6-4c09-b7c5-1dd00089061f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "109706fe-cd9a-498b-bacf-14772a3cf5c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a7216a7-8fe6-4c09-b7c5-1dd00089061f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd89165-b76b-4678-bf7a-45d9d85f1662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7216a7-8fe6-4c09-b7c5-1dd00089061f",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e7967813-968a-463e-9b4f-21028c00b953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "5d3f3a8d-a46f-44fc-896c-dd096f08a731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7967813-968a-463e-9b4f-21028c00b953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b5a949-9d46-4d94-986c-8d3e0201abdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7967813-968a-463e-9b4f-21028c00b953",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b9772798-ac40-4586-8143-e7e462bc1768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "966d235a-43b9-4ba1-83e9-0acf47c5de44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9772798-ac40-4586-8143-e7e462bc1768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c801da50-240a-4ce2-856a-de4ca2a4a216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9772798-ac40-4586-8143-e7e462bc1768",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a0702c71-636c-481e-9b45-118b4ac49eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d818e09a-23cd-4be4-8353-72a0b8b24463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0702c71-636c-481e-9b45-118b4ac49eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d53afc-5363-476c-be0d-2ec3b0908f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0702c71-636c-481e-9b45-118b4ac49eac",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "bc2a6d9d-056f-49dd-8bc4-6bec37abcb54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "7cc2eaf0-ef42-42c1-a1c7-ed84bdc581bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc2a6d9d-056f-49dd-8bc4-6bec37abcb54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b662da0-f051-475b-b6e1-4a7f2b51eef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc2a6d9d-056f-49dd-8bc4-6bec37abcb54",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "944c6900-d6a0-41ca-b82d-598590ec04f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d07a3a24-afdc-4dcc-8ab0-f361bb6f3685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "944c6900-d6a0-41ca-b82d-598590ec04f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "593c2946-5df6-4d3c-8418-3ccb8657aa87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944c6900-d6a0-41ca-b82d-598590ec04f0",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5ddc9fa5-fff0-4531-949d-fb763e06c10e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "a5ae4131-6bb4-4aba-9fcd-cde91ebade76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ddc9fa5-fff0-4531-949d-fb763e06c10e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cba368-6f35-417c-8c26-fa2bcc479b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ddc9fa5-fff0-4531-949d-fb763e06c10e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "77fe82e7-768f-48f6-8b05-d21115934f2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2dff7849-9bc1-46d4-bfa2-ab676e639090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77fe82e7-768f-48f6-8b05-d21115934f2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0caac4-21ba-4046-9c35-abc5bb023771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77fe82e7-768f-48f6-8b05-d21115934f2e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a6e3d0ac-f533-402d-bc6e-c7f3105dd3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "230d793e-23ed-4cbb-8216-ae8140f83db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6e3d0ac-f533-402d-bc6e-c7f3105dd3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "127526a4-97b0-4e92-837f-d70b2b6bf958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6e3d0ac-f533-402d-bc6e-c7f3105dd3e0",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "ab468fd2-316e-48f8-aa52-fd279ca3ae67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "66eff168-5234-4661-8bdf-15e20a812e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab468fd2-316e-48f8-aa52-fd279ca3ae67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3094c297-a2bf-41dc-aeda-a1ca725cdddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab468fd2-316e-48f8-aa52-fd279ca3ae67",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "f4ab217b-a325-4728-96d8-489bdb3f519d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b0dca748-eaf5-49da-a253-4ee430ffc8f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4ab217b-a325-4728-96d8-489bdb3f519d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a687ca9e-7be6-4e93-ad11-ac0f1530a7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ab217b-a325-4728-96d8-489bdb3f519d",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "ebe8f6c7-f992-4b64-99e7-718fe12a7478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "972a2214-e327-460d-9057-a7c41da97668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe8f6c7-f992-4b64-99e7-718fe12a7478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5edba82-7936-4095-944a-01eb367ad9dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe8f6c7-f992-4b64-99e7-718fe12a7478",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b6137ffb-dd10-4983-b96b-decf0272d54b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "203b98f6-d090-4e5a-ab33-c2054b2d1ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6137ffb-dd10-4983-b96b-decf0272d54b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da4954c-f701-49ce-9ab4-04f5f7da251b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6137ffb-dd10-4983-b96b-decf0272d54b",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "722f2db6-c6a1-42fc-b1e6-bbedd246a4c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "9fbe7108-3a8a-41ab-a948-b8a6c0ea5f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722f2db6-c6a1-42fc-b1e6-bbedd246a4c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cba50366-661a-4751-8ada-e694bf8b79dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722f2db6-c6a1-42fc-b1e6-bbedd246a4c8",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "2aea1371-9c86-498a-852e-29284779c2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "ea2354fd-a5d3-41cb-bf22-a5cff3f63fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aea1371-9c86-498a-852e-29284779c2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f04be980-3a02-4232-a9e5-cf0a4c9c2fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aea1371-9c86-498a-852e-29284779c2f2",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b2b0ea04-0822-42f0-a266-79a27b12df79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "4cce8f8c-85d9-4faa-b8f5-b70ef1d03cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2b0ea04-0822-42f0-a266-79a27b12df79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e0ebf16-adb1-48e4-b70a-5adf0760cf2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b0ea04-0822-42f0-a266-79a27b12df79",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5cc544c6-311d-4c76-806c-6e7c018e799b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "de7c0746-0042-4fb9-ad2e-6ac703a8297a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc544c6-311d-4c76-806c-6e7c018e799b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52661894-3fa8-4bcb-8836-e6b804b50106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc544c6-311d-4c76-806c-6e7c018e799b",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "c30a7418-9555-489d-99ed-93271cdb8a54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "13e97490-3aea-4aca-8ae5-5214f2c0068e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30a7418-9555-489d-99ed-93271cdb8a54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d079b2ec-6eaa-4179-abe5-88edf9f07e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30a7418-9555-489d-99ed-93271cdb8a54",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b444ddf4-19f8-49f7-b26a-6ed0c57ec1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "371dd306-21ec-4212-baef-270fa959798d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b444ddf4-19f8-49f7-b26a-6ed0c57ec1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e494c9-4f09-4fcb-9b68-2b30d2283077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b444ddf4-19f8-49f7-b26a-6ed0c57ec1c5",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "80994525-c89b-4520-a2da-14e65db3eda4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "c4214a71-1dff-4b99-949a-0b68c8727274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80994525-c89b-4520-a2da-14e65db3eda4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf51c8fa-c868-4f42-bb5a-9b8e44c992ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80994525-c89b-4520-a2da-14e65db3eda4",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "ca7bc6c9-e153-493c-995d-7cbe435c440a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "71070e64-fa7b-4084-983f-936654408fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7bc6c9-e153-493c-995d-7cbe435c440a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6362291-1903-4d05-8b6e-33ed049cb0b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7bc6c9-e153-493c-995d-7cbe435c440a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a4d79516-2a53-4ba6-86ac-eb77411e10e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "a13c3a2f-a2de-468a-ab5d-8c1337268a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d79516-2a53-4ba6-86ac-eb77411e10e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ffdb14-02ca-4202-8896-3aec65c6ed32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d79516-2a53-4ba6-86ac-eb77411e10e4",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "c9966004-60f5-483d-baa8-33a07df3a9d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2de37a56-c664-4f6b-b005-d04dd88a6905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9966004-60f5-483d-baa8-33a07df3a9d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29a8d5d0-bb6a-439a-8a3b-b0878185e822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9966004-60f5-483d-baa8-33a07df3a9d6",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5ed610f9-cc1b-4eff-9f99-f60189808a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "20803104-aac4-4822-8b4c-94ed9051cba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ed610f9-cc1b-4eff-9f99-f60189808a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49766964-7d24-4835-92f2-4020437b163c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ed610f9-cc1b-4eff-9f99-f60189808a11",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "f396c85e-ace0-4f3c-ba76-1a630316a3e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "e7fd51fd-a079-4a94-a738-d1eac6c353eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f396c85e-ace0-4f3c-ba76-1a630316a3e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17ba504-1e38-4580-916a-6eea5b526f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f396c85e-ace0-4f3c-ba76-1a630316a3e8",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "c8f94e95-f81f-4e46-bbc9-c9315bc2fb83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "8fc142ba-3b6c-438f-b578-7dee9fed4875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f94e95-f81f-4e46-bbc9-c9315bc2fb83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf91b132-a0c6-4adf-a6aa-cee6628508e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f94e95-f81f-4e46-bbc9-c9315bc2fb83",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "ffcb5f6a-b6a7-4656-8662-f6dc98aec152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "43dffdd1-d1a6-40b0-a0f7-4d6bf9044bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffcb5f6a-b6a7-4656-8662-f6dc98aec152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "628e3204-abf9-4d37-892d-0e681ed94751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffcb5f6a-b6a7-4656-8662-f6dc98aec152",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "6388e7b7-a334-42ea-a2b0-500a2c28568a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "234498b1-717e-4a27-abc0-966d9553633c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6388e7b7-a334-42ea-a2b0-500a2c28568a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "991af19a-e610-476d-8ee7-f4e61a730c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6388e7b7-a334-42ea-a2b0-500a2c28568a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a9ed3517-a117-4503-a6fc-f773909c096e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "6d5c3079-edf3-4591-8888-fded040701fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ed3517-a117-4503-a6fc-f773909c096e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed244c86-09e5-43ab-8cc2-a616993fa2e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ed3517-a117-4503-a6fc-f773909c096e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "6739244f-af82-4cad-a71d-7f7df87c4a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d0ca6fd3-6276-45d3-8de5-40c3f834f63b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6739244f-af82-4cad-a71d-7f7df87c4a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb17bbf-d361-4e33-9243-c14207bc046f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6739244f-af82-4cad-a71d-7f7df87c4a10",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "cdfa8251-7e36-4091-9667-2b8149c00cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d847715c-e895-4010-afb3-d625a0d3ee1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdfa8251-7e36-4091-9667-2b8149c00cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a811ccee-02d7-42dd-84ff-be8d5cd1b364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdfa8251-7e36-4091-9667-2b8149c00cdb",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "3359a7e3-cf24-47d3-966c-b7debb219a37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d5811b3e-882e-4968-821e-272b3f17cbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3359a7e3-cf24-47d3-966c-b7debb219a37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8de1446-7d7a-4785-b899-560cc7ee713a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3359a7e3-cf24-47d3-966c-b7debb219a37",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "84acce36-9f55-463f-9749-8ae62fa09732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "078c4afa-4a08-471e-ba93-8caeba51ee4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84acce36-9f55-463f-9749-8ae62fa09732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c636be3f-9952-41f7-8b71-d1e04a02d30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84acce36-9f55-463f-9749-8ae62fa09732",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "3ab9a5f4-ee6c-4163-8a35-0835d7b8d7c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "09185d74-2f3c-463c-89a0-2a538d9ecadf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab9a5f4-ee6c-4163-8a35-0835d7b8d7c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb6a064-b40b-4cac-ba0e-3cadec5c77b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab9a5f4-ee6c-4163-8a35-0835d7b8d7c6",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "4e1be075-ffdf-47f7-a5e0-790de64d6399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "5fc424bd-c3ae-4a59-a0b8-08900e0bc849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e1be075-ffdf-47f7-a5e0-790de64d6399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37978f0a-ad2c-468e-8564-d97bae592f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e1be075-ffdf-47f7-a5e0-790de64d6399",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "58ba25e9-31fa-4382-8500-10a98eb3df7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b7c94177-6b05-4466-a50f-ae5bc1b8e0a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ba25e9-31fa-4382-8500-10a98eb3df7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27fb5838-fbc8-429f-8092-69e4b1605afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ba25e9-31fa-4382-8500-10a98eb3df7c",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "fb7c2ed9-cbe0-4120-af49-36838d78d0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "f933b525-23b2-48a6-988f-b733786da412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb7c2ed9-cbe0-4120-af49-36838d78d0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383146b7-a588-479a-99e9-27b9a05f533b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7c2ed9-cbe0-4120-af49-36838d78d0b5",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "85c108b3-703b-4020-b532-90e09702a44f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "bb0cdef7-60f7-480b-b7bf-a16f28e1673f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85c108b3-703b-4020-b532-90e09702a44f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f4001c-7286-4ce5-ada9-0e1c87c6c8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85c108b3-703b-4020-b532-90e09702a44f",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "9a5595e7-a843-4434-b40d-f5309f6a0d81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "6d41a14f-9cd7-4684-807f-3610c9d1273a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5595e7-a843-4434-b40d-f5309f6a0d81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbf71d0-8518-43af-80ca-6353040bad53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5595e7-a843-4434-b40d-f5309f6a0d81",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "da03f273-4fd0-4919-98cd-a2d76640e1d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "f41853df-b12b-4258-88a9-0b10f82b15bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da03f273-4fd0-4919-98cd-a2d76640e1d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf8edd8-80b0-44f9-92ab-1ee3287e3429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da03f273-4fd0-4919-98cd-a2d76640e1d1",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "8aa07ea7-65d3-49a1-9eeb-6ba49e2c80c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "32827fa4-f7f4-46ba-a78a-e924a8f6b997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa07ea7-65d3-49a1-9eeb-6ba49e2c80c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e14a4b97-9484-455a-b47e-5311d903258b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa07ea7-65d3-49a1-9eeb-6ba49e2c80c8",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "3c464a96-c712-4438-b2ca-6ea2d02e6001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "4c964859-e793-4e80-a968-915dceec071f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c464a96-c712-4438-b2ca-6ea2d02e6001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496de9fc-1c59-41fb-ae66-8df6dca3409d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c464a96-c712-4438-b2ca-6ea2d02e6001",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "14990ef3-5baf-4774-9feb-35c260e3ef72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2359d90d-dec9-489d-8e6b-4b03e8176a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14990ef3-5baf-4774-9feb-35c260e3ef72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d45437ca-bb06-4f13-9e49-9cd18f25f5ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14990ef3-5baf-4774-9feb-35c260e3ef72",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "f7f7209c-6c98-4bca-8be7-80fbbc2a7f2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "ff5cbd4b-02ba-4f2b-ae29-be73fb02db94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f7209c-6c98-4bca-8be7-80fbbc2a7f2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928ed899-aca6-4b9d-89ca-44ead634f99d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f7209c-6c98-4bca-8be7-80fbbc2a7f2e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a7eb0508-219c-4e7f-bc8f-57f82c17b2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "f7f55358-3ec3-451f-8bfd-94e1214b4b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7eb0508-219c-4e7f-bc8f-57f82c17b2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "885c749e-c5fb-4135-b466-d89f73bd497a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7eb0508-219c-4e7f-bc8f-57f82c17b2f2",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "88a8703c-90c5-4c08-b4a5-8bd440b8a425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "096bf155-1285-4e14-bafd-ff2131202bca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a8703c-90c5-4c08-b4a5-8bd440b8a425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e168c434-b5d5-484e-9dfd-7ad282d5842f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a8703c-90c5-4c08-b4a5-8bd440b8a425",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "cc1a8641-162b-454e-9836-1bbda73e64a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "bf6de710-0a8d-46b1-8d0d-2a17fc8f94af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc1a8641-162b-454e-9836-1bbda73e64a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e18bd9b-21fa-448f-84ea-69877d4af1d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc1a8641-162b-454e-9836-1bbda73e64a9",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b3974870-a7ad-4c69-b689-410fecb7c472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "3b9ed296-4ce8-4fdd-902f-92d9b8fce596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3974870-a7ad-4c69-b689-410fecb7c472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d1594a6-44e4-48db-b8b5-dea95eaf892a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3974870-a7ad-4c69-b689-410fecb7c472",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a98c3d51-3faa-46bf-90b1-2fedff12fbeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "0e554bd3-a821-45f2-8f30-b1b9149b09e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a98c3d51-3faa-46bf-90b1-2fedff12fbeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43bfd64f-9e25-415a-aded-12dc6982c2a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a98c3d51-3faa-46bf-90b1-2fedff12fbeb",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e7dfb60b-a594-4964-b18e-97a2a50a8db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "97d22dcd-26be-434f-93b8-4bccf3a61954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7dfb60b-a594-4964-b18e-97a2a50a8db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8604ac0e-6657-4d1c-b41b-dccab72db327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7dfb60b-a594-4964-b18e-97a2a50a8db7",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "8de494ae-8df3-4231-a5fb-c8944fd57d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "40d8f445-3a4c-4415-ac69-ec503c1164f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de494ae-8df3-4231-a5fb-c8944fd57d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383dc761-54eb-44ad-9b64-7cee1f2edb55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de494ae-8df3-4231-a5fb-c8944fd57d63",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "75540c2d-44af-4cfe-bda4-56235f118c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "db0be818-5fec-444c-8efc-6673ce15b2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75540c2d-44af-4cfe-bda4-56235f118c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca758776-c517-4c9b-8cbd-307440ed8169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75540c2d-44af-4cfe-bda4-56235f118c82",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "933b424f-5459-4e69-97af-7ef0eddd62da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "09997435-1283-4dc2-8fca-2f47da3b4d90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933b424f-5459-4e69-97af-7ef0eddd62da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a9d1cf3-1468-4d8e-90d0-7e90ba4eab22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933b424f-5459-4e69-97af-7ef0eddd62da",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b019f916-80c1-4d54-8e06-2fd3b0ca89d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "ff6ca31c-45a9-469e-bafd-85f8a8174f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b019f916-80c1-4d54-8e06-2fd3b0ca89d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a20ff7-fe65-47cb-b2c8-5a4ccee59bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b019f916-80c1-4d54-8e06-2fd3b0ca89d5",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5ddc2263-88c9-468e-a851-fcc7053528b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "44013aab-e3b2-4579-9203-c3e5cc877e15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ddc2263-88c9-468e-a851-fcc7053528b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3174d9e8-72e3-4b85-a89f-c556f9d8f93c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ddc2263-88c9-468e-a851-fcc7053528b3",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b90e9324-e07a-4824-a06c-d561844d41dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "cb13ef5b-24e7-4cf1-aafe-06e900bbe798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90e9324-e07a-4824-a06c-d561844d41dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5500fd-e587-4ece-b83d-b699e99c5804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90e9324-e07a-4824-a06c-d561844d41dc",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "225e30f9-e71a-4620-9c51-7af4f5254e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "5108ab78-d3b9-4a3a-b3b7-430277cb63ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "225e30f9-e71a-4620-9c51-7af4f5254e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9142405f-f7c5-4562-b141-7fa22e42b6d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "225e30f9-e71a-4620-9c51-7af4f5254e82",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "6fc33188-900a-405d-b17a-3c5f3ffd1e55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "157f3340-9d2d-40c1-8a83-0207ed3787d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc33188-900a-405d-b17a-3c5f3ffd1e55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d441d4-918f-4db1-a800-0409d683025f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc33188-900a-405d-b17a-3c5f3ffd1e55",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "2f08867e-934a-474a-aab3-54ff8ed84ce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "7bf6bf6c-7749-4db3-aa65-bf2f7af4e4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f08867e-934a-474a-aab3-54ff8ed84ce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b474f8d-c245-4356-9003-a9271085e9ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f08867e-934a-474a-aab3-54ff8ed84ce0",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d5f1012b-42d5-468e-83e5-e0db8f7645b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "115ed292-4df4-4f4b-986c-7cba953ef849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f1012b-42d5-468e-83e5-e0db8f7645b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21cc7bec-afe7-4b10-ae7b-5ab010347e14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f1012b-42d5-468e-83e5-e0db8f7645b4",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "91da9d94-c57e-49e5-a9cf-e3c68971595e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "c4d7e35c-b09b-4c7c-8844-a2bfea6de114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91da9d94-c57e-49e5-a9cf-e3c68971595e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d17dd6-5a3c-478f-b28e-1a5ac41fd107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91da9d94-c57e-49e5-a9cf-e3c68971595e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "053be1da-5a31-4c1b-9f0b-bed0b9c3dddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "14579a9b-327b-49bc-ab08-1a5db0797667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053be1da-5a31-4c1b-9f0b-bed0b9c3dddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b302949-5aee-411f-895c-1937adb66194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053be1da-5a31-4c1b-9f0b-bed0b9c3dddb",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "f986741d-b54e-4e07-8f62-df965789de1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2b572bfb-f227-4f8d-9271-cdce71f8aaef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f986741d-b54e-4e07-8f62-df965789de1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4ef582-2ec5-4448-b4a2-2cbe7ae4810c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f986741d-b54e-4e07-8f62-df965789de1e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "db147a57-b230-41ee-9564-d2c2c680fe8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "8ec97274-98e0-4357-8aac-48242d9e1c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db147a57-b230-41ee-9564-d2c2c680fe8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0637d947-3b02-43ee-81b0-ed65acc04f0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db147a57-b230-41ee-9564-d2c2c680fe8f",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d6c562f0-a0b4-43df-a1f6-4bdc9fa215d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "10e95d54-74c3-4c3e-ad8f-d9fde558e50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c562f0-a0b4-43df-a1f6-4bdc9fa215d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a118c5ef-3734-4799-8827-bf57e7afc415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c562f0-a0b4-43df-a1f6-4bdc9fa215d5",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "6d16fb7a-3abe-440c-8a21-aac31dba397a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "0999c341-ac91-495f-a572-d88c4714603c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d16fb7a-3abe-440c-8a21-aac31dba397a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15857bd2-2730-4097-83e0-947fdb91efb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d16fb7a-3abe-440c-8a21-aac31dba397a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d4440b9f-2e1e-4a64-baa0-b9dcd19e4790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "22bb979e-f213-4dd8-b9cc-d6162cec886e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4440b9f-2e1e-4a64-baa0-b9dcd19e4790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d2fa7e-a8c3-4c84-8d03-a55d7cde624c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4440b9f-2e1e-4a64-baa0-b9dcd19e4790",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "0805fe76-10c2-4761-8004-f013f695ed15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "3b07fbc3-1e98-48b7-8226-32bb2049e995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0805fe76-10c2-4761-8004-f013f695ed15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f886f4bf-4757-4186-a751-3bc1add9620e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0805fe76-10c2-4761-8004-f013f695ed15",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e716f545-1ffd-4a1f-83fe-0e3279bd7cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2cc46975-fe27-4bd2-8437-65cb4a5150f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e716f545-1ffd-4a1f-83fe-0e3279bd7cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da830d44-31d8-4e89-a575-02c029feedfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e716f545-1ffd-4a1f-83fe-0e3279bd7cc2",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d4ec2265-cccc-4472-92d3-d76158356aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "729449de-f806-4e28-ace3-f87d46c79a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ec2265-cccc-4472-92d3-d76158356aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6304fb8-56cb-4246-9dbf-25dce782084f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ec2265-cccc-4472-92d3-d76158356aad",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e0d3cff1-290f-4285-89dd-9671d931dfdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "5b28b973-916b-4d96-a959-b2d98dd45378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d3cff1-290f-4285-89dd-9671d931dfdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f43420c-ba7b-4d7d-99a2-e88b0a866032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d3cff1-290f-4285-89dd-9671d931dfdb",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d5dc6614-e0ae-44f3-893a-1e251502ae1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "e82b0e8c-25c0-444c-90fa-1e76900c5084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5dc6614-e0ae-44f3-893a-1e251502ae1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bca7c32-c677-4459-924a-59dbbaab0981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5dc6614-e0ae-44f3-893a-1e251502ae1e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "cc2cda5b-2a2f-4926-a3e3-9cd883d14530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b87be385-807b-425f-b39c-45c8ff81a822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc2cda5b-2a2f-4926-a3e3-9cd883d14530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4d8fdf-3973-4b51-9edc-15474e1d47e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc2cda5b-2a2f-4926-a3e3-9cd883d14530",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "47ffd2a9-9208-40a6-b0f7-bc98d864d9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "5c3a0fa8-9804-4830-ad33-969bca0c6249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47ffd2a9-9208-40a6-b0f7-bc98d864d9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad4fbc28-2c07-4020-b411-1786522998f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47ffd2a9-9208-40a6-b0f7-bc98d864d9ea",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "55f2c985-4e9a-414b-914e-868253668149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "dea77f2c-6cc0-4b36-aa3a-85c2925fae00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f2c985-4e9a-414b-914e-868253668149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290f80bf-45c3-4f5e-8fcf-9886b449d871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f2c985-4e9a-414b-914e-868253668149",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b6db08ef-443c-4c60-8b2d-9adf18ed5834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "0811c537-8b7e-4f68-ae01-5bbc4197411c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6db08ef-443c-4c60-8b2d-9adf18ed5834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517e883e-045a-4523-abc0-2522b939c3ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6db08ef-443c-4c60-8b2d-9adf18ed5834",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "99fc863a-c44b-49c2-9033-efd92fec6e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "bba7e2ae-9995-4ba6-8b57-21a7f37e17b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99fc863a-c44b-49c2-9033-efd92fec6e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8863eb-5de6-4d9d-a6f9-25cc2855a6c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fc863a-c44b-49c2-9033-efd92fec6e97",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "430bbd97-9b2c-4167-bf41-5b844448a412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b68da06d-ae27-4411-a618-57c9c803cb98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430bbd97-9b2c-4167-bf41-5b844448a412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e48133b7-1ca2-4e15-a562-ab118ec55158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430bbd97-9b2c-4167-bf41-5b844448a412",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e6511345-c3c2-495c-a995-bfcaf5899ede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "47c540d7-5fdd-4170-9e5b-0bec4e083786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6511345-c3c2-495c-a995-bfcaf5899ede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38cc23c3-4c2d-4d85-9ca8-1866e71cd549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6511345-c3c2-495c-a995-bfcaf5899ede",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "343559ef-3f8d-4888-8013-6e5ab1e07103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "de5807a6-63e7-4b3f-8fe8-28a98636674e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "343559ef-3f8d-4888-8013-6e5ab1e07103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9da9052e-830f-47ac-b686-52902f320363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "343559ef-3f8d-4888-8013-6e5ab1e07103",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5fe27b5b-dba5-4a86-859f-0321ef3f84e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "19a165b0-b65a-40d9-b216-2b32978149d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe27b5b-dba5-4a86-859f-0321ef3f84e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c7b9df-cbe2-4cbd-99f8-a68a511d8b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe27b5b-dba5-4a86-859f-0321ef3f84e4",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "6db8e3b7-673e-4afc-8555-782475795c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "26e04bb0-1e40-4836-baae-d2d004762882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db8e3b7-673e-4afc-8555-782475795c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73de1592-1ebd-4248-abd2-e5588f653f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db8e3b7-673e-4afc-8555-782475795c7f",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "36cd6ca4-50df-4b72-81a8-510685e6883f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b425bc9a-f186-4495-a461-689a6352a6e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36cd6ca4-50df-4b72-81a8-510685e6883f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0ddc91-d0e0-41d5-b3b8-649e618a462e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36cd6ca4-50df-4b72-81a8-510685e6883f",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e8d7dc30-158a-4685-a405-7d56a93bae13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "218ee7da-98af-4220-93ad-ad99213bbd5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d7dc30-158a-4685-a405-7d56a93bae13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b53ebbc-eb9b-47c7-8649-8833ea7cc7dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d7dc30-158a-4685-a405-7d56a93bae13",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "c1712959-20e2-4a0c-8ea1-8fbd8e941a7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1b27551a-8cb3-4b1a-a6eb-0953e58759a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1712959-20e2-4a0c-8ea1-8fbd8e941a7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b95a36e-62c8-44f4-ac8d-269f36bf8629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1712959-20e2-4a0c-8ea1-8fbd8e941a7d",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "1b979d1f-869f-465b-b475-857d91eef091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1c954050-596e-4949-9c92-bb4f8f72f539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b979d1f-869f-465b-b475-857d91eef091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30f44fcd-6c97-45f2-a402-407351fd8896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b979d1f-869f-465b-b475-857d91eef091",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "283251ff-c1b6-483a-bbf7-4db687bde118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "8294e1c9-dadd-4347-be3d-459bfcfaaedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "283251ff-c1b6-483a-bbf7-4db687bde118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0310d4f5-b010-4c40-8c69-3f6ace26ee1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "283251ff-c1b6-483a-bbf7-4db687bde118",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e9e4d317-fca6-4cb0-8863-b19d0e807c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "40765138-0e05-4549-9a2d-8bbd4968ed34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9e4d317-fca6-4cb0-8863-b19d0e807c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3cc44e-e004-4ec3-90f1-54fb4b7c661e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9e4d317-fca6-4cb0-8863-b19d0e807c53",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "7ff37858-8417-41b2-9cfc-9800455f7bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "9062b7fb-7a12-40f1-ad67-c56041548de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ff37858-8417-41b2-9cfc-9800455f7bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5845af7c-a830-423e-bbfe-c9191c23babc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff37858-8417-41b2-9cfc-9800455f7bba",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a3756281-1225-4fca-922b-40043ffe1dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "6369707f-95b7-4270-8ceb-c127a6a0bfb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3756281-1225-4fca-922b-40043ffe1dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dbdd038-2df9-4ee2-85e0-9edc48638912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3756281-1225-4fca-922b-40043ffe1dd4",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "53aa99b2-bb89-43f1-b637-08f8cb147eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1e03faf7-478b-40fc-9673-3d511dea1bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53aa99b2-bb89-43f1-b637-08f8cb147eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f58120-da00-4fa4-9685-684c4135b32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53aa99b2-bb89-43f1-b637-08f8cb147eea",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "67a78c48-5ebf-4c65-8db0-82b373606921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "98120173-029f-43bc-a981-ffe57646dace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a78c48-5ebf-4c65-8db0-82b373606921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61361df0-866a-4b31-8bb5-b41ccce7fb27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a78c48-5ebf-4c65-8db0-82b373606921",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "2b665fd8-892b-43df-9190-387ab0df0031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "dfcce71b-822f-4705-aab3-4b77fa46ef51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b665fd8-892b-43df-9190-387ab0df0031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ca1eb0-84f3-46b1-90e0-51e6580bef8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b665fd8-892b-43df-9190-387ab0df0031",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d9358a5b-733a-43dd-bd7b-0f2dc1151f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "34c91d15-0bc4-4f21-af88-cf448ed45cff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9358a5b-733a-43dd-bd7b-0f2dc1151f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a629c0dc-9fc1-47b6-8cfb-b28ff9da3a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9358a5b-733a-43dd-bd7b-0f2dc1151f21",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "9d3883d9-60d0-4e08-a6ea-1b4066bfb267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "08d60b06-e058-4624-81db-a3b56153c0c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d3883d9-60d0-4e08-a6ea-1b4066bfb267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b158ae4-cb82-46c5-b735-741b486d4f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d3883d9-60d0-4e08-a6ea-1b4066bfb267",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "86d975cd-778f-4445-b908-d7f5fd7f5b73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "8fc050ec-6005-410f-b7e9-e32ff7c790e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86d975cd-778f-4445-b908-d7f5fd7f5b73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38d3c81c-3141-4ac1-a3b3-b3c26f6b9cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86d975cd-778f-4445-b908-d7f5fd7f5b73",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "9be41a2f-54d3-4f05-9ef7-b09defb2f588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "ddfb1276-f476-4917-85c8-20efa63d3160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be41a2f-54d3-4f05-9ef7-b09defb2f588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "485e2da2-98bd-43b3-be84-b21892ecdeeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be41a2f-54d3-4f05-9ef7-b09defb2f588",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "667066e4-6902-44d4-90e8-a0690ad68347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "a1d2f104-f85f-412a-b7b7-83d812e62c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "667066e4-6902-44d4-90e8-a0690ad68347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20fa480-bc63-4589-b8ea-0cb1f8f3b0f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "667066e4-6902-44d4-90e8-a0690ad68347",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "76e3a460-288f-4afc-8b1c-edec84cb59fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "48265de8-37b4-46e5-ab09-1c6e150e48c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e3a460-288f-4afc-8b1c-edec84cb59fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a63a7ae2-130b-4d5a-a465-8424632f5687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e3a460-288f-4afc-8b1c-edec84cb59fc",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "50e4175b-e0fe-4c32-acc8-f594159ea0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "f1081eaa-c07d-4ff5-8409-8b13840be16d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50e4175b-e0fe-4c32-acc8-f594159ea0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "590cc060-e61e-4cf7-bcc3-b3283315851d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50e4175b-e0fe-4c32-acc8-f594159ea0cd",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a29ad4b0-9e67-475d-92a9-87b083511dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "c207a0f6-d279-4093-a0d3-340b63e6bee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a29ad4b0-9e67-475d-92a9-87b083511dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5118166-ee0f-47e5-a12d-eb5d17fe412e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a29ad4b0-9e67-475d-92a9-87b083511dbf",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "601af71f-5773-48f5-9fce-269ba0338fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "a803c658-3b46-4f3c-bd69-48b6506bad97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "601af71f-5773-48f5-9fce-269ba0338fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00427950-67c9-4370-be8d-f0eaafdd1db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "601af71f-5773-48f5-9fce-269ba0338fb0",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d6846f2f-3bb3-40f3-85d9-079e6f56f30c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "041cd6eb-8ebd-40f6-bb42-d9d0f1422297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6846f2f-3bb3-40f3-85d9-079e6f56f30c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3269b0-bbf8-4cfd-8375-4be3b7aa3450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6846f2f-3bb3-40f3-85d9-079e6f56f30c",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d6951577-8f20-4997-9b18-7dbd9f666d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "147c5dcc-ba6b-473b-afdc-db88f7329992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6951577-8f20-4997-9b18-7dbd9f666d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd328ad2-e2a6-4a35-9474-c8ad8ff5386c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6951577-8f20-4997-9b18-7dbd9f666d85",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "3f2a4db1-4363-42ce-91ef-5964f4102834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "4e335aed-158b-4f74-a429-2d35f5721cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2a4db1-4363-42ce-91ef-5964f4102834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e02dacf-b8c6-4b4c-911d-d73cb062840f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2a4db1-4363-42ce-91ef-5964f4102834",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5e46dc47-9ae3-4fc0-bf20-22f2a4acada3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "109f9db6-b2ca-4775-815f-443d4d68bf45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e46dc47-9ae3-4fc0-bf20-22f2a4acada3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ced27e-3687-46ab-ac61-a11991d4302a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e46dc47-9ae3-4fc0-bf20-22f2a4acada3",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "4fd5b4aa-b9c7-451a-b31f-c9f8cdeb979a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "decd1135-1428-4241-9f9b-7fcb1a19ebec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd5b4aa-b9c7-451a-b31f-c9f8cdeb979a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86d9b85d-4913-4a11-aa11-f9522dd8c27c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd5b4aa-b9c7-451a-b31f-c9f8cdeb979a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5d8ff29f-c0e1-4f23-8ad8-fa6aacb2fac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "23cf891b-b90e-4784-a2d6-abda3c63d3a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8ff29f-c0e1-4f23-8ad8-fa6aacb2fac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0231a8c-62a4-41bb-8f3d-cc49ec69de83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8ff29f-c0e1-4f23-8ad8-fa6aacb2fac7",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "29a24af4-4a1c-4f80-9687-5232aa0da581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "72b959fb-6929-4a39-84ca-0ff44f9859e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a24af4-4a1c-4f80-9687-5232aa0da581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aa47d32-7f86-4f86-b129-d696cf1d46e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a24af4-4a1c-4f80-9687-5232aa0da581",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e89016a4-6309-4d16-98e2-512a173092bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "a3185a2a-b4c8-4894-898f-5c76407bcf5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89016a4-6309-4d16-98e2-512a173092bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b70388d-1086-48ce-9247-7f3b93949f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89016a4-6309-4d16-98e2-512a173092bb",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "e4426cea-0d99-4617-9cca-cadc295a4343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1208fd1b-8c26-4db9-8bca-32aeb59fd770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4426cea-0d99-4617-9cca-cadc295a4343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b52f557-77f1-477c-8196-5d8e3995dfe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4426cea-0d99-4617-9cca-cadc295a4343",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "166b238d-bbf1-4c3a-95da-ff1a981b4895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "c2e0ddfb-42db-4ea8-be3b-9da9f69e572d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "166b238d-bbf1-4c3a-95da-ff1a981b4895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee43a6ba-198f-4230-922a-0ca417bdf23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "166b238d-bbf1-4c3a-95da-ff1a981b4895",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "41c52006-f918-4f01-b4e8-771ef2551af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "30d3d497-f3d9-49cb-a725-f305771591d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c52006-f918-4f01-b4e8-771ef2551af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac33e37c-fe5c-454f-956f-42ce8026a95a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c52006-f918-4f01-b4e8-771ef2551af1",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b50fbbe6-9cf8-43f3-8bb2-4fdc5a16b4c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "98698f17-dae5-4772-a97c-5452c409cd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50fbbe6-9cf8-43f3-8bb2-4fdc5a16b4c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a5a658-0b8d-4cf8-bbcd-accf78b0f1d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50fbbe6-9cf8-43f3-8bb2-4fdc5a16b4c8",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "77ac6331-af97-4765-9f0e-4d85c118ab1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "63e74714-4ef9-4f98-bdbf-f5dcba46d926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ac6331-af97-4765-9f0e-4d85c118ab1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6cac7ab-54e5-4de6-89fd-9a6a3c31e79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ac6331-af97-4765-9f0e-4d85c118ab1e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "4dbf5455-7d46-4449-8386-a2907240fe0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1a8aaf58-1254-468c-8328-4e2c62ca4579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dbf5455-7d46-4449-8386-a2907240fe0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14208f9e-40fc-4072-9588-ac046a8616a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dbf5455-7d46-4449-8386-a2907240fe0a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "9f3f3e1f-894f-4789-94bb-9dfc3d5b21d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "5ce1a5a3-75c1-4d4b-b144-9b363522706e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3f3e1f-894f-4789-94bb-9dfc3d5b21d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca390eab-1a3d-4014-a642-1c146d97e5bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3f3e1f-894f-4789-94bb-9dfc3d5b21d9",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "457b03cd-0aba-49ce-b8bc-1e1d8d064591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "76f85790-415a-4094-812c-9387f7c147b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457b03cd-0aba-49ce-b8bc-1e1d8d064591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7035721-e11f-43ac-ad09-76a09d4aadac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457b03cd-0aba-49ce-b8bc-1e1d8d064591",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "80895ce7-b76b-4ec2-a9f6-5850f1374531",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "e5020399-6636-4421-ab5a-529bc8cdd72a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80895ce7-b76b-4ec2-a9f6-5850f1374531",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a7eb145-2040-4ecc-8fb5-13a751ffb435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80895ce7-b76b-4ec2-a9f6-5850f1374531",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "4f7a76b2-dce6-4b53-bede-2880129690cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "adb037ad-4c95-454d-8080-55ebf989259f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7a76b2-dce6-4b53-bede-2880129690cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08eef8b-0f91-4b70-ac40-49815afcd41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7a76b2-dce6-4b53-bede-2880129690cf",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "5ce9f8d6-b79e-49c1-b1dd-215ea27cde21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "78cc99bb-8873-45bf-8dc7-f06b6fb9a6de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce9f8d6-b79e-49c1-b1dd-215ea27cde21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1edb7f5-a36e-489b-a17b-cb00872f0e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce9f8d6-b79e-49c1-b1dd-215ea27cde21",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "483f396e-a506-493f-811a-a187a9d19d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "9f590095-e149-4311-855c-d6ced67596ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "483f396e-a506-493f-811a-a187a9d19d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6c69cb-5a3c-4b8c-acf3-653c948dd34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "483f396e-a506-493f-811a-a187a9d19d0b",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d8af6d1d-cfc4-41bd-96a0-cd9e8ff6c888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "e749d8df-ea8d-4dc8-a34a-8f5658052900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8af6d1d-cfc4-41bd-96a0-cd9e8ff6c888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e067e39a-f392-4ff8-b9b3-ca86086c2d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8af6d1d-cfc4-41bd-96a0-cd9e8ff6c888",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "194c8d1e-4b2b-4b4a-a95d-52a0f72f8ba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "918d6b6c-f5a2-41e7-a389-17ad34a4df82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "194c8d1e-4b2b-4b4a-a95d-52a0f72f8ba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5786e376-4002-4217-bed5-7c5ffd00c68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "194c8d1e-4b2b-4b4a-a95d-52a0f72f8ba7",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "c2f2568c-7daa-40a0-9871-ffdc32588712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "93a6aa20-219f-496a-aca9-22dc4bddd916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f2568c-7daa-40a0-9871-ffdc32588712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19606ff7-7fa2-40d7-a7ec-e2f508996e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f2568c-7daa-40a0-9871-ffdc32588712",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "9504918f-f6fe-41a8-80f6-38c1a2f67e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "26ba72ce-7bef-4abd-b6f7-66cfdee6bb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9504918f-f6fe-41a8-80f6-38c1a2f67e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1e2049-7594-4a82-bfd9-80f7ed799e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9504918f-f6fe-41a8-80f6-38c1a2f67e5f",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "8659d104-7999-4d54-8346-78c7929f316e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "51cae373-100e-477f-8324-8e72857c5032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8659d104-7999-4d54-8346-78c7929f316e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b25e0212-f1cf-4e58-bd93-546bf97ec607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8659d104-7999-4d54-8346-78c7929f316e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "375b83e4-a850-4573-b33f-04146375ebdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d53b665a-55af-4d2a-ac8d-ab5c6a945221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375b83e4-a850-4573-b33f-04146375ebdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21fb79f-8dcf-4b23-b9aa-4700d111fa2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375b83e4-a850-4573-b33f-04146375ebdf",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "76e4a302-28c6-4157-b697-5a125722be6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1693e82f-4b06-4645-b815-76a2c60c8c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e4a302-28c6-4157-b697-5a125722be6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eda1d70-b6b0-4d7e-8b8e-bb393e148c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e4a302-28c6-4157-b697-5a125722be6a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b812fcc4-95b0-433c-b56d-14a1ca578e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "27459e42-b613-4911-8efc-8be33fc93838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b812fcc4-95b0-433c-b56d-14a1ca578e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "285e1659-376c-400f-8dd0-e5504f42825c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b812fcc4-95b0-433c-b56d-14a1ca578e8d",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d3ec5ce3-d4b7-4ff9-ba49-2519f9911375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b4130a39-c554-471f-96b1-c21f352dfd9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ec5ce3-d4b7-4ff9-ba49-2519f9911375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8ec150-d753-4f28-8102-8b49ffae90a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ec5ce3-d4b7-4ff9-ba49-2519f9911375",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b17bd2e5-55a1-410c-b1f6-28ca0bc4ddab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "3d5b9a68-8831-41fa-a610-5f787012b256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b17bd2e5-55a1-410c-b1f6-28ca0bc4ddab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bf1471-f2c1-481f-923f-4c1269cff1b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17bd2e5-55a1-410c-b1f6-28ca0bc4ddab",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "55367b01-e28f-49ab-90f4-bfb1bc44a4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "145cf21a-9f96-4d7a-a516-94d6878d5197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55367b01-e28f-49ab-90f4-bfb1bc44a4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b888067a-72b9-46dd-9d4f-dd4185282f1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55367b01-e28f-49ab-90f4-bfb1bc44a4e6",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "8cc71677-9869-4001-afae-eff2c7b874f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "1acaa4cb-5b73-4553-a18f-1e3641a9f148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc71677-9869-4001-afae-eff2c7b874f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847786bd-def6-4e5d-94d4-6c98d3f649ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc71677-9869-4001-afae-eff2c7b874f3",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "b4959c12-b417-49c6-8f23-778f650a0ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "0b50f97e-d404-49f7-ae03-2ee78bb2ca3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4959c12-b417-49c6-8f23-778f650a0ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69fef7b6-83e0-4aed-b493-57e190e605a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4959c12-b417-49c6-8f23-778f650a0ec1",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "f0cddc79-2e91-4b29-8829-be9c94dab180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "2fa0661e-a8ba-42c1-87dd-283c9526eec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cddc79-2e91-4b29-8829-be9c94dab180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e6b70d-50b6-48e7-b828-5b39d46271e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cddc79-2e91-4b29-8829-be9c94dab180",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "953c3521-1a92-433a-a138-0431e27d5f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "513c5a1b-2512-4472-ba95-8ca91788e108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "953c3521-1a92-433a-a138-0431e27d5f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26d2e38-578e-4789-9483-248afd9644be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "953c3521-1a92-433a-a138-0431e27d5f73",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "a17a865f-cf73-4c42-9b0d-ed7621c90b26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "cf58cc0f-e593-4940-8d5a-977fd4bc75da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a17a865f-cf73-4c42-9b0d-ed7621c90b26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec92d0c6-81cf-4b84-ac8c-b0249e4bce45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a17a865f-cf73-4c42-9b0d-ed7621c90b26",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "6ffc56ae-6259-4c9e-928e-f2421bc7642a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "d8297fe6-7095-46fb-99a6-7d8f14a0c972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ffc56ae-6259-4c9e-928e-f2421bc7642a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5cbed6-64c3-4175-b575-20c49bc78094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ffc56ae-6259-4c9e-928e-f2421bc7642a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "7d2e6e6b-6490-4725-b625-9f3c1ef77e13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "a3b5f239-403c-4e35-97af-d43a44a82e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2e6e6b-6490-4725-b625-9f3c1ef77e13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e44d3cc-c37c-44c3-ab57-6663f141bbf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2e6e6b-6490-4725-b625-9f3c1ef77e13",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d4850e01-0870-4968-80d4-224ecd5f84aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "b308d045-0890-4d27-a695-409fb0403787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4850e01-0870-4968-80d4-224ecd5f84aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb86696-17a6-4168-8385-03fb551ea693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4850e01-0870-4968-80d4-224ecd5f84aa",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "fd57d5f3-7968-4c93-8a88-8ee3c4c23992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "4f04dd3c-9e95-45d0-9b8b-920765190e61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd57d5f3-7968-4c93-8a88-8ee3c4c23992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be8674b-589e-4d5b-a524-675a3ddfe6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd57d5f3-7968-4c93-8a88-8ee3c4c23992",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "55f9bb1d-8214-4f02-9c51-411a7667321e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "ff6be704-abef-4142-95f9-2fa48e30e893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f9bb1d-8214-4f02-9c51-411a7667321e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff6a5ef-3594-43ed-ae64-01ed638a9bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f9bb1d-8214-4f02-9c51-411a7667321e",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "79668807-6ad9-4063-8efd-df6d1c5fecdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "e307b1b2-0995-4fbc-a147-b33ab935da75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79668807-6ad9-4063-8efd-df6d1c5fecdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8579c9-2bf0-431f-a833-b68b97edb7d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79668807-6ad9-4063-8efd-df6d1c5fecdf",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "edc98239-07a1-48ce-be69-263c9951c32a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "06fd4172-1c1c-44de-8207-20cdf403896f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edc98239-07a1-48ce-be69-263c9951c32a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c563263-da42-4ff4-aeb5-bc45e4f5d3a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edc98239-07a1-48ce-be69-263c9951c32a",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "403860ce-7977-45ce-92c4-2e71fc6e0d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "0bb85068-da18-483d-b755-7c193c76f133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "403860ce-7977-45ce-92c4-2e71fc6e0d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48489255-3d31-4f96-b166-941a54ec5d18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "403860ce-7977-45ce-92c4-2e71fc6e0d9b",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "52b13866-af77-4887-bd6a-736615481829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "775b1261-6702-467b-905b-e9a02ed47d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b13866-af77-4887-bd6a-736615481829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26dd601c-9131-4867-88d8-9f6b40697b2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b13866-af77-4887-bd6a-736615481829",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "d80a484d-0615-4a06-bd5d-d5a5390bb4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "ef016461-6133-4a9b-b8be-a62544c94dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d80a484d-0615-4a06-bd5d-d5a5390bb4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eac2e9dd-e1df-43b4-aaf4-7eb0001875d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d80a484d-0615-4a06-bd5d-d5a5390bb4c4",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        },
        {
            "id": "7d9f0b05-9089-41dc-8f2f-c7c273895957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "compositeImage": {
                "id": "8806f4aa-fcf4-4057-8b69-13945d6e32f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d9f0b05-9089-41dc-8f2f-c7c273895957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d62adf00-cafe-4c0d-aaa6-fb9f7f1c5e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d9f0b05-9089-41dc-8f2f-c7c273895957",
                    "LayerId": "6c66cff9-f239-48cb-8a52-3f5702ef2b37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6c66cff9-f239-48cb-8a52-3f5702ef2b37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6449f79-f62b-429d-8731-cc1244d1ceca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}