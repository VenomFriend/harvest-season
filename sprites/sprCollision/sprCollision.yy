{
    "id": "02df5a04-710d-404e-be6b-33468d36363d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0f7caa20-adcb-4f88-a8bb-9d5042c68ff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02df5a04-710d-404e-be6b-33468d36363d",
            "compositeImage": {
                "id": "abac9ebb-87b4-47bb-9609-76461d4c48d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7caa20-adcb-4f88-a8bb-9d5042c68ff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c4c8002-6ecd-40d2-852d-73b6b8c24b19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7caa20-adcb-4f88-a8bb-9d5042c68ff3",
                    "LayerId": "73f92a3e-59ae-4d80-a6a5-628d387566d9"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "73f92a3e-59ae-4d80-a6a5-628d387566d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02df5a04-710d-404e-be6b-33468d36363d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}