{
    "id": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerMaleWateringCan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 38,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2d9ced52-bb6d-455d-bf56-235442b3b347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "1469788b-9532-4f12-8a44-0034acdb6354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d9ced52-bb6d-455d-bf56-235442b3b347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4849aae9-06eb-4424-a8ee-dd3ce6007ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d9ced52-bb6d-455d-bf56-235442b3b347",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "3d7ce2fc-576d-4392-9cfe-b8743c13fb5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "6449f401-335e-4023-bb3f-98c10a9565ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d7ce2fc-576d-4392-9cfe-b8743c13fb5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412140c6-17ea-4d0a-b669-b5415d326f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d7ce2fc-576d-4392-9cfe-b8743c13fb5f",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "13751967-f142-45c3-94de-94a89dddc035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "2968e907-4b99-4fc0-9923-aee6298eec08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13751967-f142-45c3-94de-94a89dddc035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3deb033-1c82-4c81-bfae-4cd666ae3273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13751967-f142-45c3-94de-94a89dddc035",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "cfac3f3a-f81a-4a22-b3be-793779abf287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "9fca4ae0-bed4-488d-a318-c8e96d4ecb4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfac3f3a-f81a-4a22-b3be-793779abf287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec7afb5-fa23-441b-adab-f2dab75e0cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfac3f3a-f81a-4a22-b3be-793779abf287",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "4c46dc13-4fde-4bba-a608-1f7df3d93b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "fccd86a6-65db-4128-bf8e-ced9ad8b05c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c46dc13-4fde-4bba-a608-1f7df3d93b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f506115-7a7a-4867-9f59-b0febee37a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c46dc13-4fde-4bba-a608-1f7df3d93b84",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "f18ecf47-27bf-402a-b407-1379e13db2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "17797344-3e4b-415d-9385-a39ac4bbccf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18ecf47-27bf-402a-b407-1379e13db2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f5f77e-b95c-41df-a7a3-4007ce70118b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18ecf47-27bf-402a-b407-1379e13db2f4",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "9f33fbeb-50d3-4d81-a447-0ac40513399d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "2fd36541-8563-471a-8e1c-66753f4db576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f33fbeb-50d3-4d81-a447-0ac40513399d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fd34589-fffc-487b-9615-01f8b5770e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f33fbeb-50d3-4d81-a447-0ac40513399d",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "3fe76d20-aadd-4e06-b846-7a34d3f88962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "23a3b6a0-58db-4c84-8afe-bfd9ac45e51f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fe76d20-aadd-4e06-b846-7a34d3f88962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0ea968-f7db-4a6d-8bb5-fac23bb5cb53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fe76d20-aadd-4e06-b846-7a34d3f88962",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "fe5ccb47-4869-45d4-b100-dc560c777573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "f09d78f0-3a70-4d1b-b2aa-319e1b17fa04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5ccb47-4869-45d4-b100-dc560c777573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e856d11d-e58e-4317-937c-194343afdf3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5ccb47-4869-45d4-b100-dc560c777573",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "fc63d7f5-2158-4932-9874-43184620dbf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "a27237e2-749a-43c9-820b-47098657763b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc63d7f5-2158-4932-9874-43184620dbf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1e4406-0edd-4800-a799-26217eba4f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc63d7f5-2158-4932-9874-43184620dbf0",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "73a606ce-19be-4e5a-bb85-220d975669d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "f2127217-a641-43b2-a672-f422146195f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a606ce-19be-4e5a-bb85-220d975669d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ebdf9f-c55e-4454-b26b-353eb367bf58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a606ce-19be-4e5a-bb85-220d975669d2",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "e753501e-8af4-4be1-8986-d90ccfc4e9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "51ca8409-dd00-4913-bbe3-5bd9a3f8e741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e753501e-8af4-4be1-8986-d90ccfc4e9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f525f56-7b8b-4ce9-a7cd-cc27174acf42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e753501e-8af4-4be1-8986-d90ccfc4e9c3",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "40b45475-9344-4d6c-b0d6-26019fbb6ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "ae171254-aba7-4a51-940e-3ba83741e206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b45475-9344-4d6c-b0d6-26019fbb6ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9df4d8eb-d6ef-4acd-80a8-b8dcd3526cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b45475-9344-4d6c-b0d6-26019fbb6ed3",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "c1967d3e-ef91-4587-add2-3ccea882b42d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "a6fb9120-122b-43c8-a464-c423af64dac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1967d3e-ef91-4587-add2-3ccea882b42d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e32ded33-1cf0-41ad-b062-48db91c5d80d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1967d3e-ef91-4587-add2-3ccea882b42d",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "57fe2abe-e0c9-4b51-b260-096178dd7cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "f0935070-1f14-4bf5-8efb-720e12baf4fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57fe2abe-e0c9-4b51-b260-096178dd7cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b9a922-c7a3-4b06-a3f5-5b06c73c48a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57fe2abe-e0c9-4b51-b260-096178dd7cfd",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        },
        {
            "id": "2fc4e9a4-b64e-49d0-b48c-e470244683dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "compositeImage": {
                "id": "64e51c01-7fd6-4327-b105-df273b7f1a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fc4e9a4-b64e-49d0-b48c-e470244683dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64b11144-26a1-4c7f-aea5-47cae3ff4e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fc4e9a4-b64e-49d0-b48c-e470244683dd",
                    "LayerId": "e0523f84-04c5-46d2-ac7d-a2998b754710"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 60,
    "layers": [
        {
            "id": "e0523f84-04c5-46d2-ac7d-a2998b754710",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbb520c5-b732-4cca-8494-ff5e1b1e6df7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 23,
    "yorig": 39
}