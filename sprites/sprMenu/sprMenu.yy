{
    "id": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 293,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "010fc015-6cd4-4db6-8526-63e23d792100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "91e72ae8-c875-4e1f-9139-12a59a2f25e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010fc015-6cd4-4db6-8526-63e23d792100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dc6201e-13e6-4f5d-a226-99e75ab50cd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010fc015-6cd4-4db6-8526-63e23d792100",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "f98b0c26-8fa3-4b9a-b79f-35f9284d1e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "e30e7cc7-6b04-4eae-ad37-4a242503f803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98b0c26-8fa3-4b9a-b79f-35f9284d1e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd346ab-4cff-4376-ae2c-3d98ad531c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98b0c26-8fa3-4b9a-b79f-35f9284d1e12",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "e2158326-4fdd-4c97-ab43-32ec5c586afd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "2083936f-de98-40fe-a947-fb1593e2ae90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2158326-4fdd-4c97-ab43-32ec5c586afd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5150e83-449d-448c-9c7e-dac7738d97e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2158326-4fdd-4c97-ab43-32ec5c586afd",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "b9071240-e8e8-4b22-a390-9a09aad68af0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "6d0aa9a8-7884-45ed-b2d5-4f74b8bba505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9071240-e8e8-4b22-a390-9a09aad68af0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0819c5-65ae-41f6-9dd4-e9146d4165a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9071240-e8e8-4b22-a390-9a09aad68af0",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "8295da9f-268a-49f9-a7c0-1a27417e6c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "25c815ff-f88d-46c0-ba45-6c7c5ab1412c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8295da9f-268a-49f9-a7c0-1a27417e6c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6101d944-b7ea-4e84-812d-1e078c689a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8295da9f-268a-49f9-a7c0-1a27417e6c05",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "47095e7e-b282-4c49-b389-c632c5bd5bfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "0a6a07c5-b674-46b0-827b-bc9aac1e84e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47095e7e-b282-4c49-b389-c632c5bd5bfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d14cfb-28e3-4bd4-988d-d9fae23dedaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47095e7e-b282-4c49-b389-c632c5bd5bfc",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "4bc2850c-c82c-4943-8970-ee6e7964dfd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "1423840b-ef1e-47ae-9cd2-c6d99e0c4dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc2850c-c82c-4943-8970-ee6e7964dfd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7749ee-145b-4583-9004-b359b1e28512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc2850c-c82c-4943-8970-ee6e7964dfd2",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "7505d798-9613-42f7-a50e-04a0b412c743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "0ea28edb-7772-456c-b331-b0d280cb6ead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7505d798-9613-42f7-a50e-04a0b412c743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6053c8c2-bb7a-41c8-b2b9-e865fb578daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7505d798-9613-42f7-a50e-04a0b412c743",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        },
        {
            "id": "13db01ff-9e5b-4867-ad04-8e7a6cea5f07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "compositeImage": {
                "id": "befcd567-93e0-41ff-bf5c-3f1c23779aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13db01ff-9e5b-4867-ad04-8e7a6cea5f07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be21b6de-bf63-4c8b-9527-afeaccbebeff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13db01ff-9e5b-4867-ad04-8e7a6cea5f07",
                    "LayerId": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "465ae6b3-f970-4fc1-92fd-7a010e1aa82e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eba5bae5-d4bc-49fd-af96-521408f9e28d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 294,
    "xorig": 147,
    "yorig": 0
}