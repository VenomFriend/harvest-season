{
    "id": "2fa7b0ca-1de4-4282-ad2f-181778527bd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite221",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "30e4180b-cfc4-4429-9c8b-edd920e0e2fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fa7b0ca-1de4-4282-ad2f-181778527bd2",
            "compositeImage": {
                "id": "2b77b2e9-6d83-49c5-af54-dd18f813ff20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e4180b-cfc4-4429-9c8b-edd920e0e2fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c080fa-56d1-4fc7-9633-0451c02cddbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e4180b-cfc4-4429-9c8b-edd920e0e2fd",
                    "LayerId": "af8f44b7-146d-4562-9cf2-25f03333ebee"
                },
                {
                    "id": "087f3489-d71e-42f4-adbb-65bc6750ec7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e4180b-cfc4-4429-9c8b-edd920e0e2fd",
                    "LayerId": "c67ac793-4500-47cc-9244-e0b92d556b9e"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 64,
    "layers": [
        {
            "id": "c67ac793-4500-47cc-9244-e0b92d556b9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fa7b0ca-1de4-4282-ad2f-181778527bd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "af8f44b7-146d-4562-9cf2-25f03333ebee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fa7b0ca-1de4-4282-ad2f-181778527bd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}