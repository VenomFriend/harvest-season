{
    "id": "66518895-bc1b-4a25-a6e6-4c112c3137d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileset3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e92dd034-62c9-4bd0-bded-1bb8dee72823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66518895-bc1b-4a25-a6e6-4c112c3137d7",
            "compositeImage": {
                "id": "9da287ad-2b61-4951-be5d-0d75efa853d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e92dd034-62c9-4bd0-bded-1bb8dee72823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac9d45d-8075-46ab-bc27-42226e23e88e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e92dd034-62c9-4bd0-bded-1bb8dee72823",
                    "LayerId": "0d0c68b4-d4f8-4cc0-9cd0-c3e2efd3c495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "0d0c68b4-d4f8-4cc0-9cd0-c3e2efd3c495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66518895-bc1b-4a25-a6e6-4c112c3137d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}