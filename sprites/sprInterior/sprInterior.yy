{
    "id": "5b444dfe-a29e-45ae-8bbb-3d23f6a1ef7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprInterior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5e952b7a-f916-488f-a5eb-a972b9d2953d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b444dfe-a29e-45ae-8bbb-3d23f6a1ef7a",
            "compositeImage": {
                "id": "c71d8874-0184-4aff-93c2-d0d6877ee866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e952b7a-f916-488f-a5eb-a972b9d2953d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51245ba5-92b6-49ce-9def-a1b38eac4645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e952b7a-f916-488f-a5eb-a972b9d2953d",
                    "LayerId": "84900c6c-33b0-4bcf-bed0-ec4359d49acc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "84900c6c-33b0-4bcf-bed0-ec4359d49acc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b444dfe-a29e-45ae-8bbb-3d23f6a1ef7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}