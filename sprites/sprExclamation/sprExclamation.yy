{
    "id": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprExclamation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d4b902c7-6e07-4cdc-98cf-308f7d688c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "52ca8e09-4f26-4b08-a7d4-195cd0e99000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4b902c7-6e07-4cdc-98cf-308f7d688c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d49ca08-9e85-424d-8ae9-0c6d37abd1d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4b902c7-6e07-4cdc-98cf-308f7d688c35",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "de321e7c-d5c2-49f1-b31e-cda9453c95a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "1272c255-7b0f-4a4c-a684-0921b4a9b15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de321e7c-d5c2-49f1-b31e-cda9453c95a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5dfc386-e53e-44db-9a9d-16c00e410b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de321e7c-d5c2-49f1-b31e-cda9453c95a2",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "0c91adb0-fea9-4726-a62b-140773e3d50a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "5e7e92f2-ca85-44e1-a141-5d1698997df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c91adb0-fea9-4726-a62b-140773e3d50a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1816ae1a-d2ee-40f4-bf94-5d416c9d9ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c91adb0-fea9-4726-a62b-140773e3d50a",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "6fe6b972-fead-43cc-a501-30b5c819aa32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "3e482b11-e46b-4fe0-9775-b882dd905b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe6b972-fead-43cc-a501-30b5c819aa32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6225fe-9d59-43d4-a209-8ab1a79adcfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe6b972-fead-43cc-a501-30b5c819aa32",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "80aacc35-bed3-4f77-9bc5-5a1d7ce55e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "4705de93-2757-41f5-b643-d70b19009e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80aacc35-bed3-4f77-9bc5-5a1d7ce55e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffc2b96-a789-4297-a31f-c0d9ff5d072b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80aacc35-bed3-4f77-9bc5-5a1d7ce55e40",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "d14cb7da-e634-470d-9bf3-fce55fcfd88a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "465fdb32-e78d-441b-b4be-749be3061186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d14cb7da-e634-470d-9bf3-fce55fcfd88a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "734d9fb0-7cc2-45d0-a0dd-067a64cb79ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d14cb7da-e634-470d-9bf3-fce55fcfd88a",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "0ced03d3-2e4b-4f73-8780-4b7833cbae83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "0dda5a4c-350a-4d81-9452-982a9fff2a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ced03d3-2e4b-4f73-8780-4b7833cbae83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79f006c-3f09-44a4-8514-b98e6e85b818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ced03d3-2e4b-4f73-8780-4b7833cbae83",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        },
        {
            "id": "64c55a8f-47cb-4382-a2d5-53a321bb16bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "compositeImage": {
                "id": "f256caa8-414f-4f4e-af25-312627d69c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c55a8f-47cb-4382-a2d5-53a321bb16bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1adf498f-9bc6-401f-bfdc-22cd348380f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c55a8f-47cb-4382-a2d5-53a321bb16bd",
                    "LayerId": "189d2fc9-1406-4bea-b2d0-d2e42847e640"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "189d2fc9-1406-4bea-b2d0-d2e42847e640",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adb18c40-56a1-4a22-ba3f-b5c72cd3203f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}