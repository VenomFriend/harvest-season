{
    "id": "548fe4c8-cce6-4efc-8f3d-a0cf36d826de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b7fe1e73-1fd7-4bea-b029-6345c41595c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "548fe4c8-cce6-4efc-8f3d-a0cf36d826de",
            "compositeImage": {
                "id": "4041d151-b108-44b3-84bc-59e3f26d6bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7fe1e73-1fd7-4bea-b029-6345c41595c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "550be7a3-f826-44ea-afc5-fda89e4e766f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7fe1e73-1fd7-4bea-b029-6345c41595c5",
                    "LayerId": "e7fdf826-5dd6-4a90-b12f-f2cb57c551dc"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 54,
    "layers": [
        {
            "id": "e7fdf826-5dd6-4a90-b12f-f2cb57c551dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "548fe4c8-cce6-4efc-8f3d-a0cf36d826de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 0,
    "yorig": 0
}