{
    "id": "2b57c56f-b03c-4616-a032-acc1511ee130",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCropSelected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "43adf924-289b-4a40-a13c-d67283745563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b57c56f-b03c-4616-a032-acc1511ee130",
            "compositeImage": {
                "id": "bcb278e0-28ef-48c9-8bf4-0d74c5263a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43adf924-289b-4a40-a13c-d67283745563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9eaecb1-c17a-45a7-a2bc-baab14d8e5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43adf924-289b-4a40-a13c-d67283745563",
                    "LayerId": "a8f5dd89-3c6d-4227-98b5-9fcfb05c325c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8f5dd89-3c6d-4227-98b5-9fcfb05c325c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b57c56f-b03c-4616-a032-acc1511ee130",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}