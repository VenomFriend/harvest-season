{
    "id": "565c0c18-d969-41d4-963a-b567b609d3e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTeleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9c3692e8-4d4d-4592-b22d-c43c1ece4308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565c0c18-d969-41d4-963a-b567b609d3e9",
            "compositeImage": {
                "id": "f06cde1e-b360-4592-baed-46134e4057d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c3692e8-4d4d-4592-b22d-c43c1ece4308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604bb9d3-c502-4ae3-abec-5dfd0a5401c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c3692e8-4d4d-4592-b22d-c43c1ece4308",
                    "LayerId": "d717a63a-4376-4a0d-93b8-118493a18b62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d717a63a-4376-4a0d-93b8-118493a18b62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "565c0c18-d969-41d4-963a-b567b609d3e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}