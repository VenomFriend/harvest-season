{
    "id": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprToolsIcons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 16,
    "bbox_right": 79,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3120ba41-655d-4bee-8d05-a6a46730b995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "5e8d7c36-2303-465b-9552-4bf710de891a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3120ba41-655d-4bee-8d05-a6a46730b995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969b5d70-b8fc-4fd5-9583-9f945d3442b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3120ba41-655d-4bee-8d05-a6a46730b995",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "531ecb81-d04f-4ea1-ad6d-f7e355a38953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "3ff9d076-2cf3-4bff-baef-94be3ecd904d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "531ecb81-d04f-4ea1-ad6d-f7e355a38953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "423438e9-b5af-450f-a62e-ccfa3a79f2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "531ecb81-d04f-4ea1-ad6d-f7e355a38953",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "e24d62fd-6c81-4aaa-b7f4-42632ba8bbf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "44d8c986-032b-4867-bdc3-c830e555f3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e24d62fd-6c81-4aaa-b7f4-42632ba8bbf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0cfd762-4237-47e7-ab44-93f91f4a2a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e24d62fd-6c81-4aaa-b7f4-42632ba8bbf5",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f25faae6-c95d-4549-aead-4f3993e3653e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "760a55be-216c-41bd-89f7-8f71d1ba2543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25faae6-c95d-4549-aead-4f3993e3653e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0ef44e0-837d-4882-8d4c-591aba325de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25faae6-c95d-4549-aead-4f3993e3653e",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "31a683df-f373-4d9b-b82c-57962a943315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "99075b7d-a465-4047-8e5e-60f1589560ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a683df-f373-4d9b-b82c-57962a943315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31065417-8cc3-4f51-a7e9-6de312ce8318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a683df-f373-4d9b-b82c-57962a943315",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "4e7e463c-f895-45e3-b917-fc727b4cb13f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "0d2dfccb-2957-479b-9579-c1f1ec1b0762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e7e463c-f895-45e3-b917-fc727b4cb13f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932afd0c-079f-418a-a4f3-fc1cc513897f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e7e463c-f895-45e3-b917-fc727b4cb13f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "1ad04f20-f1f7-43ac-a44f-2ecc6d34f7dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "3496cb32-ce58-4234-a647-0b4eec61baf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ad04f20-f1f7-43ac-a44f-2ecc6d34f7dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef843e19-b0bd-44d8-9c0d-6692819925e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ad04f20-f1f7-43ac-a44f-2ecc6d34f7dc",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "bddc417b-234a-463c-9d68-08955099735a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "0de69023-ad7c-4253-a743-ea9f3c3ea77a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bddc417b-234a-463c-9d68-08955099735a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "513227d8-bfbc-407c-8d68-3aae2bc139e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bddc417b-234a-463c-9d68-08955099735a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "a976757a-fb92-48aa-be9f-ba7a594e8b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "291d17a4-7518-4cb5-9dd9-a4b3a98ed69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a976757a-fb92-48aa-be9f-ba7a594e8b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffea6fe5-9be2-4317-9d4c-4e1cb069b298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a976757a-fb92-48aa-be9f-ba7a594e8b5c",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f84a6cc5-c640-4538-9c58-4e3194854780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a9b16850-6c67-49cf-82e3-83a672176807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f84a6cc5-c640-4538-9c58-4e3194854780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "911567a3-5f67-4767-81a6-d1e7ab8f4baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84a6cc5-c640-4538-9c58-4e3194854780",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "3c548b41-d18c-4b2d-a95c-591d857d06bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "8c40f656-f7ca-4125-957a-bf24f0fd90e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c548b41-d18c-4b2d-a95c-591d857d06bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d6705da-30e9-42f5-9426-addafedb380a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c548b41-d18c-4b2d-a95c-591d857d06bc",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "7bb861c2-d33c-456d-8fbd-63b87c6f87c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "31609a2d-a45f-4f40-8833-48dcf2395adf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bb861c2-d33c-456d-8fbd-63b87c6f87c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "248fabac-bc1a-488f-8ac1-7a6dc8a257fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bb861c2-d33c-456d-8fbd-63b87c6f87c6",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "9153816a-9596-4ad7-8a53-63d4eb151b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "5d4cc3e4-774c-4032-95da-b5a9ef59b3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9153816a-9596-4ad7-8a53-63d4eb151b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a8e150e-82f1-464f-b79a-82a821ff3b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9153816a-9596-4ad7-8a53-63d4eb151b1d",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "59635a0d-b5f4-47d6-8eac-5ecc334a329c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "2007c1b9-9e60-4491-b3d2-b04a77bad893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59635a0d-b5f4-47d6-8eac-5ecc334a329c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c516c97b-2af5-4f36-bb1c-ae558b3de9de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59635a0d-b5f4-47d6-8eac-5ecc334a329c",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "5d601e1c-170c-4ae5-bbd7-ea774a715cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "42f6ba8d-4943-4b60-a38c-450bc038421e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d601e1c-170c-4ae5-bbd7-ea774a715cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fad0ccd-aaaa-4eb0-a45d-dcc199b03bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d601e1c-170c-4ae5-bbd7-ea774a715cde",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "78030dd6-6e2f-4037-8d14-aa83961daf4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "090a8634-cb8b-41c4-9303-37db99b735b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78030dd6-6e2f-4037-8d14-aa83961daf4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ac3474-3d0a-4b41-891b-f02021aafad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78030dd6-6e2f-4037-8d14-aa83961daf4a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "b2ce141f-7c7d-4bc0-ab75-04d286bef6c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "30d094a3-ae18-4cd4-8771-100ab9e2f62c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ce141f-7c7d-4bc0-ab75-04d286bef6c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32fae036-8910-49cb-ae16-0b9a4b01928d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ce141f-7c7d-4bc0-ab75-04d286bef6c3",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "14dc59e1-2fa8-47fa-9592-36d6e2f8955a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "af3dc518-4e88-43fd-abe5-ef8b3d0b547e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14dc59e1-2fa8-47fa-9592-36d6e2f8955a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb67ff0-bc76-4d57-95f9-61dab8ec7899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14dc59e1-2fa8-47fa-9592-36d6e2f8955a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "7b363998-cef3-4f8c-bf85-8116cb81aa0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "96dbf789-8ead-4dec-aeb5-69ee79d222e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b363998-cef3-4f8c-bf85-8116cb81aa0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b3bf08a-7b06-4a0b-859f-dffd184905fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b363998-cef3-4f8c-bf85-8116cb81aa0d",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "15668bc4-dfac-4846-9c0a-33d8ba1ef5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "c79cf7cc-35f5-475b-af5b-d9c77cd6dc9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15668bc4-dfac-4846-9c0a-33d8ba1ef5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4662f40a-1948-4f07-9ba0-7c5b4d046617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15668bc4-dfac-4846-9c0a-33d8ba1ef5d7",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "c987198b-5231-46a6-97c5-9e04558299d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "3ce3809b-8828-4b8b-823e-c43fc42b9ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c987198b-5231-46a6-97c5-9e04558299d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a9e748-f790-4a67-b2f9-bdaf9490bff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c987198b-5231-46a6-97c5-9e04558299d2",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "82fe0f65-a3fd-4a27-b148-0d6a3f2b7d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "2e87e688-24ce-4697-9dca-a7143ba92c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82fe0f65-a3fd-4a27-b148-0d6a3f2b7d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d266dee-207d-42e9-9f41-99b5a71be1e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82fe0f65-a3fd-4a27-b148-0d6a3f2b7d32",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "06dd2de6-c69d-4aa1-9127-13fc9aeb8f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "13d81260-7bd6-4424-afa2-7845172ce0bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06dd2de6-c69d-4aa1-9127-13fc9aeb8f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fa177d8-fb6a-4b5f-877f-d39f69de659a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06dd2de6-c69d-4aa1-9127-13fc9aeb8f35",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "74c8cae8-16b5-4c04-b615-d87955c571c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "40a590bc-b672-4bc9-bf0f-f6b1e8523a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c8cae8-16b5-4c04-b615-d87955c571c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900aebbd-9f8d-4e9a-898a-ef73ce74a28e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c8cae8-16b5-4c04-b615-d87955c571c3",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "c1aa77fd-75b6-4544-bbb4-532691ab07bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "e8c3afae-1e3e-45cf-806d-cdbe891cbad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1aa77fd-75b6-4544-bbb4-532691ab07bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac67c0fd-e12f-40eb-a6f5-966f91110ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1aa77fd-75b6-4544-bbb4-532691ab07bf",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "103aaa3c-b2ff-4aea-8993-81ac8beef0f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "7188bd0b-1b4c-4744-b946-41141a7cce0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103aaa3c-b2ff-4aea-8993-81ac8beef0f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70e3ce23-d563-4604-bd4d-993592b27396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103aaa3c-b2ff-4aea-8993-81ac8beef0f8",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "2c6a6863-5438-4ed5-8434-274c6fdf82d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "7d800d08-6874-46dd-8a20-41cf473718c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c6a6863-5438-4ed5-8434-274c6fdf82d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8d71a9-e2cd-4efc-a000-1112c73fd4a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6a6863-5438-4ed5-8434-274c6fdf82d9",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "ef22f154-793b-49ce-936c-74b1fce26ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "9dcea394-0c03-42a9-aee3-5db8af07627a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef22f154-793b-49ce-936c-74b1fce26ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68d721f5-5ac8-461c-baea-cb4442f778a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef22f154-793b-49ce-936c-74b1fce26ebd",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "3c485095-2222-41ca-a428-a406ba83977e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "7d150d83-2688-4aa9-a02c-e9d0f5fcf8ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c485095-2222-41ca-a428-a406ba83977e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7973e30f-de33-4b6b-bc59-b5f11089db0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c485095-2222-41ca-a428-a406ba83977e",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "4884d482-7f1a-46d1-86a0-44757d33660e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "69402ed5-6b74-4e52-bbb6-c048581201de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4884d482-7f1a-46d1-86a0-44757d33660e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f675e5-a774-46ba-87b2-d7e74b25a558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4884d482-7f1a-46d1-86a0-44757d33660e",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "c0a26270-e785-426f-ade3-3bb0f77cb412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "4570edc6-d744-4f4e-8b82-6bd6b1c81e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a26270-e785-426f-ade3-3bb0f77cb412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae8fae3-6951-4a1d-bb38-26c5b4f9b35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a26270-e785-426f-ade3-3bb0f77cb412",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "bab72277-14f6-4a2f-b3fc-693e11f5b764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "3bfe29f4-a6a8-4fb1-8c2c-e31b75932537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bab72277-14f6-4a2f-b3fc-693e11f5b764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19272f0e-d86c-4a63-a80c-de7efe991b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bab72277-14f6-4a2f-b3fc-693e11f5b764",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "1243254b-9214-4bdb-8714-1a20e6ca0b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "6505d379-2eaa-45bd-96a9-fa90099b1713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1243254b-9214-4bdb-8714-1a20e6ca0b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6981467b-3f14-43cb-bd3e-30ad3ea81252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1243254b-9214-4bdb-8714-1a20e6ca0b1e",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "da87cc57-aba7-4694-b032-5eb4fb929317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "d88c992e-76e2-4612-aac5-717e32a0a641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da87cc57-aba7-4694-b032-5eb4fb929317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b6d258-2d6c-44f9-9f67-b2e1f1eb7289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da87cc57-aba7-4694-b032-5eb4fb929317",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "400778c7-6841-4242-aad0-fcc628d08c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "7de4b993-2f9b-4b4b-a2a9-200e5a6c8b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "400778c7-6841-4242-aad0-fcc628d08c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1afc6ae-48e3-4ca5-bae1-604e7da01bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "400778c7-6841-4242-aad0-fcc628d08c58",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "fc06d709-fbf5-4f7c-bbba-3af382f1104d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "dd4237ad-465f-43db-b4e6-bc38fc2dbca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc06d709-fbf5-4f7c-bbba-3af382f1104d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d2ac26-fb4c-403f-805c-ff4553deeaeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc06d709-fbf5-4f7c-bbba-3af382f1104d",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "a0797379-3894-44a8-80f6-20fe22870a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "7a79681e-f679-4d83-b5e1-f927bbe497df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0797379-3894-44a8-80f6-20fe22870a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25696676-635b-496a-946a-a0daa8630f34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0797379-3894-44a8-80f6-20fe22870a99",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f921c227-4ccd-43a0-9092-423ae2243d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "784ab84c-5c03-43f4-abb2-f753872ca700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f921c227-4ccd-43a0-9092-423ae2243d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15131a1b-4c73-4a5b-8d44-954aecdbd409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f921c227-4ccd-43a0-9092-423ae2243d18",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "273c6a70-450f-43fb-be93-d2b3af2e3ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "95bd5934-f84c-421b-979f-c87aae41ac52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273c6a70-450f-43fb-be93-d2b3af2e3ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d4a301-3dff-4515-9260-67bfb70df161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273c6a70-450f-43fb-be93-d2b3af2e3ec3",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "07a01c68-c420-47f4-a601-7a037dd3bb9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "50b2c69f-0676-40a1-abbc-0549d5454788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a01c68-c420-47f4-a601-7a037dd3bb9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cbf6a3-3b3e-4fa6-b7dc-e899ebc709a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a01c68-c420-47f4-a601-7a037dd3bb9a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "75a30ce4-cb2f-4b21-8db3-327d08e55010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "e6834b02-adc5-4553-8406-84d7e55af427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a30ce4-cb2f-4b21-8db3-327d08e55010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24389890-5024-4f21-bbfb-c6180310a8d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a30ce4-cb2f-4b21-8db3-327d08e55010",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "53b47814-daf0-4b89-9056-fddb2d77b1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "de161d73-69d8-415d-99a6-951001775318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53b47814-daf0-4b89-9056-fddb2d77b1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d748df20-3ece-4d37-9372-43e0f034350c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53b47814-daf0-4b89-9056-fddb2d77b1f9",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "757ad418-fb2a-4247-a114-4e4fc0729442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "281bcaa6-75b5-40f7-b565-1833df315f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757ad418-fb2a-4247-a114-4e4fc0729442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c0ca51c-3f60-4b4c-96bb-1ff1e91e4d44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757ad418-fb2a-4247-a114-4e4fc0729442",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "7a4018a2-2797-4010-b5ca-5b416677a346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b9896365-2b98-4c30-9cb4-e1ad82fca022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4018a2-2797-4010-b5ca-5b416677a346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37c807a-6253-4ad5-8788-b76300fc6072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4018a2-2797-4010-b5ca-5b416677a346",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "2f828e8d-813e-4935-817e-a2aeb1e6d4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "fb75b84c-2af6-4229-8988-f1353c8ee6d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f828e8d-813e-4935-817e-a2aeb1e6d4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c06ecdcd-baae-48f0-8a27-f2ea3ba7e922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f828e8d-813e-4935-817e-a2aeb1e6d4c3",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "35b53768-a171-4513-b109-bc6f11c10902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "ac9e4d6d-88a8-480b-91e7-9e33a2bb17ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35b53768-a171-4513-b109-bc6f11c10902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998c2a5a-4580-4c2e-87a4-0854d7dba7e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b53768-a171-4513-b109-bc6f11c10902",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "254fda27-202e-4cae-a027-896dee247305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "92f4d79d-82bc-46be-a24f-773a74f54fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254fda27-202e-4cae-a027-896dee247305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d347b1-5a32-41ec-a54f-af4899d9ba9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254fda27-202e-4cae-a027-896dee247305",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "027654fa-c085-4e94-a57a-52439e530b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "051facd9-d1d7-4945-95d2-0f6b27129bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027654fa-c085-4e94-a57a-52439e530b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893bc6f3-007e-49f5-be8a-a288c79f7ff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027654fa-c085-4e94-a57a-52439e530b86",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "58ead50c-cda3-4a9f-8aa4-b0fab13d7b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "662f7835-669e-4a83-b39d-e3aeaa0b5535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ead50c-cda3-4a9f-8aa4-b0fab13d7b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34c31758-b501-4e08-a67e-9f7ba15a3d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ead50c-cda3-4a9f-8aa4-b0fab13d7b9a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "894d79db-76c3-48f6-a9cd-3a58bc53f1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "48860b7b-1bab-42c8-a85d-499f308658e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "894d79db-76c3-48f6-a9cd-3a58bc53f1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bc60535-59c9-4cb2-a7ec-279097695ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "894d79db-76c3-48f6-a9cd-3a58bc53f1cf",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "70110ceb-bbb6-49c3-ab58-f495372a2a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "11e2046b-a133-4fd7-9ed7-52f853a6137f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70110ceb-bbb6-49c3-ab58-f495372a2a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a32eb6f-77ea-4561-a925-3a62bd1d866b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70110ceb-bbb6-49c3-ab58-f495372a2a5b",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "208e8661-911b-4092-a3e1-9d920590f810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "98376350-f4cf-4f9c-9870-c6bb68bd402c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208e8661-911b-4092-a3e1-9d920590f810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b7e2acc-326f-495f-98d6-568f9a6c5e06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208e8661-911b-4092-a3e1-9d920590f810",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "6ccb208f-0b7a-46da-b8b2-480f104b722e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "c2a76fe1-3438-4af8-8855-f6180baebccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ccb208f-0b7a-46da-b8b2-480f104b722e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca3a7c7-8b13-44d9-9432-4b799414c309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ccb208f-0b7a-46da-b8b2-480f104b722e",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "25baf3cd-8389-4abe-961c-4dcfa04cc243",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a9fc3c69-9597-441c-b3ab-f9c777301115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25baf3cd-8389-4abe-961c-4dcfa04cc243",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11bbab9-b4fa-45a4-8530-68d8c216cf6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25baf3cd-8389-4abe-961c-4dcfa04cc243",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "9310a8a4-301c-41e9-a700-c4f59295d5d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "df4af104-20d0-4317-ada0-f41e12b97318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9310a8a4-301c-41e9-a700-c4f59295d5d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d17833-0439-41fd-bdc1-24c8c3ab1a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9310a8a4-301c-41e9-a700-c4f59295d5d6",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "0e4e9193-5d69-48d0-adc5-87afa73a0fb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "d4f80259-cfbd-4c1a-adb8-a3fb0876184a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4e9193-5d69-48d0-adc5-87afa73a0fb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c7a5e9-5f42-4482-a38f-661afe194d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4e9193-5d69-48d0-adc5-87afa73a0fb7",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "be479ca5-3b1a-4882-9dcf-a769df3e2b17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "bbc647bb-6329-4fe6-9fe9-295606a215af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be479ca5-3b1a-4882-9dcf-a769df3e2b17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f06e24-bbc7-4468-84b2-603bff66e93c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be479ca5-3b1a-4882-9dcf-a769df3e2b17",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f43b4227-7736-4b32-9fb4-edc1645a82ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "171e7c7d-c96d-4ac3-bf5f-2b0cb074a89a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f43b4227-7736-4b32-9fb4-edc1645a82ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b9a7506-3b4c-4961-9171-1434509af60a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f43b4227-7736-4b32-9fb4-edc1645a82ab",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "18dc774e-f1bf-4442-8699-e1c5f4537ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b038daaa-c0ed-4c1e-be7f-4986610fd116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18dc774e-f1bf-4442-8699-e1c5f4537ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3e3996-ef98-4477-b3fe-a18c9b8f2e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18dc774e-f1bf-4442-8699-e1c5f4537ee8",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "b339bb06-deae-40cd-be05-6f77fb17c37f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "1060c020-479e-48a8-b17a-bf0f0b56dab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b339bb06-deae-40cd-be05-6f77fb17c37f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c7ee096-5c42-48db-a768-95357f8f9845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b339bb06-deae-40cd-be05-6f77fb17c37f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "323d2167-1ee5-4361-98c8-8fd3f2aa8dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "19c4e3da-748e-4158-9386-d472902bebcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "323d2167-1ee5-4361-98c8-8fd3f2aa8dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fe6c6a2-5133-4e6f-9bdd-4fd6390b9cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "323d2167-1ee5-4361-98c8-8fd3f2aa8dfa",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f7dc66f5-6377-4e63-8adc-938c144cf5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b730c9e0-dc9f-4f8c-84fe-7b472562ae07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7dc66f5-6377-4e63-8adc-938c144cf5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0fefb2e-618d-4578-8223-ab9b3a12171c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7dc66f5-6377-4e63-8adc-938c144cf5be",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "6f60980e-c848-4dd5-92a9-364a62efe833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "3e4307ba-344d-4ca0-a426-0a1828002700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f60980e-c848-4dd5-92a9-364a62efe833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e4542e-6d3a-4ab4-943c-57804f2d7fd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f60980e-c848-4dd5-92a9-364a62efe833",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "2ddeb5e9-5ef7-4350-90a4-0a2ff0dcce4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "34177b4d-48bc-4649-90f5-ab9562b14327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ddeb5e9-5ef7-4350-90a4-0a2ff0dcce4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34b60cb-2345-488b-8c54-4009263008cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ddeb5e9-5ef7-4350-90a4-0a2ff0dcce4b",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "cff05860-3497-44ca-b960-65d01b9e989d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "7dc44891-f3f3-4ef3-b399-d230476d50c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cff05860-3497-44ca-b960-65d01b9e989d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07cbef1c-5dea-4947-95e5-6b4d744835d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cff05860-3497-44ca-b960-65d01b9e989d",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "c7dfc3e0-ec79-4bfc-ba5e-b5bfae762f58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a5171904-0103-4abc-86dc-9ed7ba236caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7dfc3e0-ec79-4bfc-ba5e-b5bfae762f58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61dd847-0fc9-4448-8403-575656341cfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7dfc3e0-ec79-4bfc-ba5e-b5bfae762f58",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "03e2ae63-9576-49f2-bede-d64ceeb55bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "5c655324-e340-4b84-8bfb-16ecd5673bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e2ae63-9576-49f2-bede-d64ceeb55bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6eb5963-196d-4f46-b1bf-29f3b31b60aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e2ae63-9576-49f2-bede-d64ceeb55bc1",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "4fe302c4-8e75-42ee-9c8c-b7e8db525f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "0b5b7732-0ed7-4da6-8bde-218ba703fc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe302c4-8e75-42ee-9c8c-b7e8db525f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "684fd609-a615-453e-8a93-99ddc04713c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe302c4-8e75-42ee-9c8c-b7e8db525f13",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "3b961278-4e29-45f1-b366-d9369f24f84d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a3c76534-643d-4f41-95bc-430322f7929b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b961278-4e29-45f1-b366-d9369f24f84d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70488c7-a059-4065-a382-6510707f5f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b961278-4e29-45f1-b366-d9369f24f84d",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "ef2e6b81-8c8a-4d29-85ae-d38822c12173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "1d1341f3-a3f4-40a2-a0ed-fcc55a7794cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2e6b81-8c8a-4d29-85ae-d38822c12173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5082e33-89f5-4627-b04b-4d031171f325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2e6b81-8c8a-4d29-85ae-d38822c12173",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "7cae5fae-2fa3-43b9-872d-51e42eca65a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "fd7dc60a-4e77-4ea1-b422-7347763146b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cae5fae-2fa3-43b9-872d-51e42eca65a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c007482-6fd1-4f61-9192-0368650a4a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cae5fae-2fa3-43b9-872d-51e42eca65a4",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f8d0ff6d-6603-4d58-a4a3-b8aa189f2298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "01bb3ef4-661a-4b4a-b401-d4d5c41a04a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d0ff6d-6603-4d58-a4a3-b8aa189f2298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d835796-8ebc-4478-b922-c6e303ed0100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d0ff6d-6603-4d58-a4a3-b8aa189f2298",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "16871d31-f7bb-413a-82c4-4891f0a96801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "31d784fc-6522-4659-b8f9-834d5b5e1944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16871d31-f7bb-413a-82c4-4891f0a96801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3eb3f0f-32d0-4bc4-9812-66914874c27a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16871d31-f7bb-413a-82c4-4891f0a96801",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "d3f5d1c4-1055-47c1-98ec-8d4dcb624f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "5d3f12f4-169a-4ee2-a9cf-9ac0314b8421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3f5d1c4-1055-47c1-98ec-8d4dcb624f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0976ccc6-e450-4c6f-84bf-993000464bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3f5d1c4-1055-47c1-98ec-8d4dcb624f3c",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "476ad67a-37d5-4e16-a134-24e3a3a7ace8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "923c24a5-8eef-46e8-a68e-18708057e322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "476ad67a-37d5-4e16-a134-24e3a3a7ace8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5343a5e0-811d-4f5d-a588-c4962b143bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "476ad67a-37d5-4e16-a134-24e3a3a7ace8",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "973e1e51-d3e9-43e1-be5b-8d3ffc97f56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "1285f19e-c7de-4744-ad6e-c6dc7432d576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973e1e51-d3e9-43e1-be5b-8d3ffc97f56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b06f1c69-c505-4991-b142-182fb6edf757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973e1e51-d3e9-43e1-be5b-8d3ffc97f56f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "98cf7bc6-ce1a-4483-adbb-13130a530979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b06487ad-596d-435c-a57d-0e34f83cdc76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98cf7bc6-ce1a-4483-adbb-13130a530979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddcfc828-0afe-4af9-b729-c4ee9363c40f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98cf7bc6-ce1a-4483-adbb-13130a530979",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "68102296-6c0c-488f-a18d-e4c2798fd946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "cd9ce558-6c41-4b5c-9e69-7baa51716b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68102296-6c0c-488f-a18d-e4c2798fd946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8e677d-1a95-48e6-989e-adf96c4c7b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68102296-6c0c-488f-a18d-e4c2798fd946",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "41a4196c-6fcc-452d-9408-115665c8468f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "1961e9da-c24c-4205-aa7f-96c1cf4196d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a4196c-6fcc-452d-9408-115665c8468f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd92dda-b405-4c4b-a5cc-c1cbec7f8755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a4196c-6fcc-452d-9408-115665c8468f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "78b79902-8372-451a-8cb0-6847bc7f61cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "026a5ada-6ff3-49e8-978d-5048797d244b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b79902-8372-451a-8cb0-6847bc7f61cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56975eed-6b5e-4786-866c-c2e804942e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b79902-8372-451a-8cb0-6847bc7f61cc",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f4ed79d0-2df3-4c25-9879-6e1383072179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "50becd0a-438c-4788-a1d4-ccae625ac99b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4ed79d0-2df3-4c25-9879-6e1383072179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2822ce7f-a29f-4b1e-add0-f458941142e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ed79d0-2df3-4c25-9879-6e1383072179",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "91d3a321-38d5-438d-93fd-876bf42b307a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "066b3536-2da9-46e9-9dfb-42d84d088aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d3a321-38d5-438d-93fd-876bf42b307a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d050a72-f13b-4ac0-8ab9-b09c49792bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d3a321-38d5-438d-93fd-876bf42b307a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "9ac9f253-365a-4ba2-a468-e70df5916d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "9b4c77da-c164-4760-89ad-d8a10aa11f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac9f253-365a-4ba2-a468-e70df5916d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d66496-e78f-4a85-a17f-54e932152e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac9f253-365a-4ba2-a468-e70df5916d4b",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "217ac1d1-bb09-4e10-8cf7-e1239fcbc087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "170cfc6d-1355-4a36-bd39-4c13fd6d6009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "217ac1d1-bb09-4e10-8cf7-e1239fcbc087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "930f9f27-8a8d-4ee6-a5ab-2845683a9df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "217ac1d1-bb09-4e10-8cf7-e1239fcbc087",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "b14d37d4-0095-4eba-8a26-d7ac405f0c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "cdd96a75-7358-46f8-ae0f-55c04b504b4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14d37d4-0095-4eba-8a26-d7ac405f0c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccd08c82-6e6a-48c3-9ce7-2ae4aede09f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14d37d4-0095-4eba-8a26-d7ac405f0c5b",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f0bcc48a-f63c-47e2-8c27-06ef9508cee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "0ec8686c-6bb2-46e1-8b2f-2c4ec0d128c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0bcc48a-f63c-47e2-8c27-06ef9508cee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6023741b-0cb7-4ecd-a649-888b8705b77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0bcc48a-f63c-47e2-8c27-06ef9508cee7",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "a828a68c-2192-4324-8565-eb4eb2af14d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "c993c679-74dc-4d92-a579-1023a909a064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a828a68c-2192-4324-8565-eb4eb2af14d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a83d30e-773b-4c67-84d1-a9e49a8e8d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a828a68c-2192-4324-8565-eb4eb2af14d9",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "cdcf4d71-03ae-47b6-b464-39cacfb02c28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "156de541-4925-4494-8ab2-9cd365d07b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdcf4d71-03ae-47b6-b464-39cacfb02c28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f46a761-72a4-4e27-b7ac-7a4fff4e314e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdcf4d71-03ae-47b6-b464-39cacfb02c28",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "24023475-29b9-4d99-b885-92a7de91f85e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "8ca3d60e-c3b8-4230-be00-f7889c5f783b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24023475-29b9-4d99-b885-92a7de91f85e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca46cb05-b7f6-415c-9bb1-5545cbe163a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24023475-29b9-4d99-b885-92a7de91f85e",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f74afd8b-9ab5-4377-9a06-416b8363f3d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "58aeedf6-0062-4a5a-bd76-e9dcdfe6b5d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f74afd8b-9ab5-4377-9a06-416b8363f3d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9278df-8497-47ba-b463-c1c79885a1f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74afd8b-9ab5-4377-9a06-416b8363f3d7",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "f9af9e51-57d6-4dbf-b142-64e3c2f11075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "83420e18-9a51-45f1-ab17-22f8ee556c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9af9e51-57d6-4dbf-b142-64e3c2f11075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e8873d-3678-4983-bdde-e8df0f72f940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9af9e51-57d6-4dbf-b142-64e3c2f11075",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "1c588631-0491-4417-b2ee-495f067b9294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a9bcd3e7-5e7f-40c1-8e64-11f17f7677cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c588631-0491-4417-b2ee-495f067b9294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa5c8d6-2c3e-4c3f-a884-8cf4e648a870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c588631-0491-4417-b2ee-495f067b9294",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "044d8b46-6274-45bc-87e1-e4985e7346d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "1acba5fa-4ed9-4a7c-ab58-c244b61a3f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "044d8b46-6274-45bc-87e1-e4985e7346d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f50bc5-a241-49de-afbc-029a8fca3a4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "044d8b46-6274-45bc-87e1-e4985e7346d8",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "a1b23974-620b-4e56-83da-06a33f02ff8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "47688a15-21f6-44ad-994e-9beb450b28fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1b23974-620b-4e56-83da-06a33f02ff8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d51d0e1-80a5-44e5-a85d-62da5f1c4e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1b23974-620b-4e56-83da-06a33f02ff8f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "3390d761-f45f-4353-b7d2-34f8269e6c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "e18bc2e4-68e4-42e1-9d18-f2420a068995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3390d761-f45f-4353-b7d2-34f8269e6c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa026379-d71d-42d5-8ffa-793bf42686e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3390d761-f45f-4353-b7d2-34f8269e6c16",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "1ac7686e-28b0-4338-929a-6b7085b10426",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "d2efc7a2-99de-4789-b8f2-ddcca833ddc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac7686e-28b0-4338-929a-6b7085b10426",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d2279d-ba10-49a8-a326-af059fdfd6ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac7686e-28b0-4338-929a-6b7085b10426",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "e7cc40f3-2a73-4ae1-861e-b14d09e5640f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "124c47a9-8b19-40b6-8f6c-d1198be28f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7cc40f3-2a73-4ae1-861e-b14d09e5640f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89402578-a494-4749-a84b-a00016568b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7cc40f3-2a73-4ae1-861e-b14d09e5640f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "d56e3ea1-87b6-49e7-9144-a6fb49488858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "0e60f76e-5091-44a5-8c44-2a8dc88591d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56e3ea1-87b6-49e7-9144-a6fb49488858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a128264-df49-426f-9ac6-f107338455e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56e3ea1-87b6-49e7-9144-a6fb49488858",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "b361d976-33f8-4475-ba3f-470210231f4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "3930527b-828f-4e84-8433-2467eb7a4945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b361d976-33f8-4475-ba3f-470210231f4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67b0ed7-69d0-4d19-817f-74720638b12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b361d976-33f8-4475-ba3f-470210231f4f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "6d14b245-3709-48cb-a932-b5cc216bb340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a6d06d2e-faa6-48b0-b0ff-b9b54ccb7c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d14b245-3709-48cb-a932-b5cc216bb340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8ef48f-bdea-45c3-a00b-45b8c8e9b948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d14b245-3709-48cb-a932-b5cc216bb340",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "ff047037-6d7b-4526-a5c0-a8a035cab03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "055f00d7-353d-42f2-9691-5818ccc1532b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff047037-6d7b-4526-a5c0-a8a035cab03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad501d9f-f7e7-4417-9d79-5beda1d7330c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff047037-6d7b-4526-a5c0-a8a035cab03b",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "3e9c5dce-ba04-4891-98e1-3d1cbdcd49b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "28b6f89e-c34b-4a50-81bd-34450894f21c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9c5dce-ba04-4891-98e1-3d1cbdcd49b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dbaefc-fd3c-47a3-b394-fc33bcae9078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9c5dce-ba04-4891-98e1-3d1cbdcd49b6",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "058a8a87-9236-4848-adf6-349b76ed51ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "6f3ac5ad-97ea-41da-81a4-d09305a7b3f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "058a8a87-9236-4848-adf6-349b76ed51ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f89fa53-42f8-43f8-8111-8bd9281a0e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "058a8a87-9236-4848-adf6-349b76ed51ad",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "fdbd0c2e-007e-4a63-8d1d-72612c018304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "8c954e72-70f5-4a78-bfe2-c12126a44951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdbd0c2e-007e-4a63-8d1d-72612c018304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff6d8c42-8a7a-4f6b-ad9a-0809bc382a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdbd0c2e-007e-4a63-8d1d-72612c018304",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "c26644d7-3027-4299-895a-45787e1ac79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b38af99a-e4cb-4ae6-915f-6a8f95e7b986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26644d7-3027-4299-895a-45787e1ac79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f001ef-e981-463e-85d6-ffefa0baa005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26644d7-3027-4299-895a-45787e1ac79b",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "17149ca8-897d-4d7b-84ab-5b1de74a65ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "6ad7c630-3e73-4b27-a5f7-2961b40d2fcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17149ca8-897d-4d7b-84ab-5b1de74a65ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb741343-42b4-4332-9276-7f55673cfdcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17149ca8-897d-4d7b-84ab-5b1de74a65ec",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "06498e17-bcca-413c-ac4c-b53f20ee4621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "de2018fd-cfd8-4f48-af6c-03b17d1bcbbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06498e17-bcca-413c-ac4c-b53f20ee4621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bcf6b0a-be13-42b4-88c0-53ac176224e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06498e17-bcca-413c-ac4c-b53f20ee4621",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "480950b3-9661-43b4-b255-c2616ff40daa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "c95ffcdd-ab0a-4d72-97c7-676dc5c2b3b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480950b3-9661-43b4-b255-c2616ff40daa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f404e5-cd95-4749-b1ac-914195019efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480950b3-9661-43b4-b255-c2616ff40daa",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "e133379d-577d-4cf7-b17e-f59dfdd9ae46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "ff1317b4-4390-4424-8443-0ca45186bcf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e133379d-577d-4cf7-b17e-f59dfdd9ae46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f11b2112-fcd9-42dc-af64-233d4c65711d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e133379d-577d-4cf7-b17e-f59dfdd9ae46",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "87d17fa3-e86b-4d8f-a0b1-ff07783c135a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "fd23e316-2a2b-4799-8a50-55cc769f5242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d17fa3-e86b-4d8f-a0b1-ff07783c135a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3411fd4-f868-4200-a1f7-6f823b58dac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d17fa3-e86b-4d8f-a0b1-ff07783c135a",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "0bfcb27e-81fd-440d-9ab9-362dbbcd4212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "247232f6-0ce3-42a4-9eda-1f5ef6ca651e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfcb27e-81fd-440d-9ab9-362dbbcd4212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6853c8-f635-4aff-b5d5-655475ed4c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfcb27e-81fd-440d-9ab9-362dbbcd4212",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "d8ed9db6-328a-4e01-b473-1d58e0bdf898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "5865c1a5-e4a0-46c2-86c8-14de923c3acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8ed9db6-328a-4e01-b473-1d58e0bdf898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50ccd2e-d256-4076-8182-680fc75ea621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8ed9db6-328a-4e01-b473-1d58e0bdf898",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "deb5ce15-59a6-4098-9612-31689d0b1c95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "9fc4fd36-3d91-460d-8398-1adc92f8fc59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deb5ce15-59a6-4098-9612-31689d0b1c95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d54c1cb-036c-4188-898b-fa6ccb9c926c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deb5ce15-59a6-4098-9612-31689d0b1c95",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "50c678a7-3c23-4ba6-8834-63f985d8a586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "16340e29-4847-4502-b3ad-55bc7d020449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c678a7-3c23-4ba6-8834-63f985d8a586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4ddcd95-acd2-4c09-ac5b-5bb399f6b9cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c678a7-3c23-4ba6-8834-63f985d8a586",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "88c7fcf6-0ee8-42cd-9daa-61fecfd94821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "9c508ec0-1702-41b4-9efa-ef0f0aee6be9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88c7fcf6-0ee8-42cd-9daa-61fecfd94821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d575272-fdf4-4bc9-a311-6fee0fa938e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88c7fcf6-0ee8-42cd-9daa-61fecfd94821",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "7af01cef-7ac6-4232-9cab-43808d340883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "a88adcad-812f-4a60-acf5-26d289a23d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7af01cef-7ac6-4232-9cab-43808d340883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e943bebe-95da-40dd-a1cb-b967ea73f912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7af01cef-7ac6-4232-9cab-43808d340883",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "91c3f06e-a4b8-4d1e-b3ff-af9a70466b56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "686cd693-7c2b-4e06-9e3e-789e210ebd68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c3f06e-a4b8-4d1e-b3ff-af9a70466b56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c48956d3-f800-4ba0-8630-b09c4fa28217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c3f06e-a4b8-4d1e-b3ff-af9a70466b56",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "656d1013-ec4e-4f85-800a-8702fcdaf4e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b781b0c0-caa8-4c3a-a321-65d48cbd7f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656d1013-ec4e-4f85-800a-8702fcdaf4e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e61c1c07-797a-41e0-b7fd-784d8a8c77e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656d1013-ec4e-4f85-800a-8702fcdaf4e2",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "4781f613-e91d-4ce2-a7e5-74441f10a423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "b030b219-361c-4fff-a67c-2ea2ef703877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4781f613-e91d-4ce2-a7e5-74441f10a423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7efd31dd-d6f1-4fb8-8e47-867a4e86168a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4781f613-e91d-4ce2-a7e5-74441f10a423",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "bd397421-0b10-48d7-a775-1c16559567c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "dc53e4cd-d487-494d-beba-21e013dae6fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd397421-0b10-48d7-a775-1c16559567c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c52c45-d4a4-4926-9480-61f3192de9b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd397421-0b10-48d7-a775-1c16559567c1",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        },
        {
            "id": "5db70fff-eb5a-44ff-ab27-4a0b632c3f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "compositeImage": {
                "id": "2f46493d-216b-4d56-adf0-4e79b535d16e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db70fff-eb5a-44ff-ab27-4a0b632c3f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd22565d-4d38-44f1-927f-82c57038c0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db70fff-eb5a-44ff-ab27-4a0b632c3f7f",
                    "LayerId": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c9ab0e58-1ec8-44ca-95a4-bd31b05edef5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "740c1fe4-4545-43d7-b1aa-d00e97b151e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}