isMoving = moveUp or moveDown or moveLeft or moveRight;

if(runPressed) {
	isRunning = true;
	runPressed = 0;
}
if(runReleased) {
	isRunning = false;
	runReleased = 0;
}

if moveUp {
	facing = "up";
	if(isRunning) {
		y -= playerRunSpeed;
	} else {
		y -= playerSpeed;
	}
	
	var t1 = tilemap_get_at_pixel(global.tilemap, bbox_left, bbox_top) & tile_index_mask;
	var t2 = tilemap_get_at_pixel(global.tilemap, bbox_right, bbox_top) & tile_index_mask;
	
	
	if(t1 != 0 || t2 != 0) {
		y = ((bbox_top + 32) & ~31) - sprite_bbox_top;
	}
	if(room == roomFarm) {
		var tree = place_meeting(x, y, objSmallTree);
		var rock = place_meeting(x, y, objSmallRock);
		if(tree || rock) {
			y = yprevious;
		}
	}
	
} else if moveDown {
	facing = "down";
	if(isRunning) {
		y += playerRunSpeed;
	} else {
		y += playerSpeed;
	}
	var t1 = tilemap_get_at_pixel(global.tilemap, bbox_left, bbox_bottom) & tile_index_mask;
	var t2 = tilemap_get_at_pixel(global.tilemap, bbox_right, bbox_bottom) & tile_index_mask;
	
	if(t1 != 0 || t2 != 0) {
		y = ((bbox_bottom & ~31) - 1) - sprite_bbox_bottom;
	}
	if(room == roomFarm) {
		var tree = place_meeting(x, y, objSmallTree);
		var rock = place_meeting(x, y, objSmallRock);
		if(tree || rock) {
			y = yprevious;
		}
	}
}

if moveLeft {
	facing = "left";
	if(isRunning) {
		x -= playerRunSpeed;
	} else {
		x -= playerSpeed;
	}
	var t1 = tilemap_get_at_pixel(global.tilemap, bbox_left, bbox_top) & tile_index_mask;
	var t2 = tilemap_get_at_pixel(global.tilemap, bbox_left, bbox_bottom) & tile_index_mask;

	if(t1 != 0 || t2 != 0) {
		x = ((bbox_left + 32) & ~31) - sprite_bbox_left;
	}
	if(room == roomFarm) {
		var tree = place_meeting(x, y, objSmallTree);
		var rock = place_meeting(x, y, objSmallRock);
		if(tree || rock) {
			x = xprevious;
		}
	}
} else if moveRight {
	facing = "right";
	if(isRunning) {
		x += playerRunSpeed;
	} else {
		x += playerSpeed;
	}
	var t1 = tilemap_get_at_pixel(global.tilemap, bbox_right, bbox_top) & tile_index_mask;
	var t2 = tilemap_get_at_pixel(global.tilemap, bbox_right, bbox_bottom) & tile_index_mask;
	
	if(t1 != 0 || t2 != 0) {
		x = ((bbox_right & ~31) - 1) - sprite_bbox_right;
	}
	if(room == roomFarm) {
		var tree = place_meeting(x, y, objSmallTree);
		var rock = place_meeting(x, y, objSmallRock);
		if(tree || rock) {
			x = xprevious;
		}
	}
}