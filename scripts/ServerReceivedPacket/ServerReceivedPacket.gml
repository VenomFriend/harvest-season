/// @param buffer
/// @param socket

var buffer = argument[0];
var socket = argument[1];
var msgid = buffer_read(buffer, buffer_u8);

switch(msgid) {
	// Send the key pressed by the other player to all the players, and update the player on the server
	case MSG_KEY:
		var keyPressed = buffer_read(buffer, buffer_u8);
		var otherPlayerToMove = ds_map_find_value(global.clients, socket);
		ServerSendKey(keyPressed, socket);
		MovePlayer(otherPlayerToMove, keyPressed);
		break;
	// Update the player x,y and image, in case there was some desynch
	case MSG_UPDATE_PLAYER_POSITION:
		var playerX = buffer_read(buffer, buffer_u16);
		var playerY = buffer_read(buffer, buffer_u16);
		var playerImage = buffer_read(buffer, buffer_u16);
		var playerRoom = buffer_read(buffer, buffer_u16);
		var playerStamina = buffer_read(buffer, buffer_u16);
		var otherPlayerToUpdate = ds_map_find_value(global.clients, socket);
		
		otherPlayerToUpdate.x = playerX;
		otherPlayerToUpdate.y = playerY;
		otherPlayerToUpdate.image_index = playerImage;
		otherPlayerToUpdate.myRoom = playerRoom;
		otherPlayerToUpdate.stamina = playerStamina;
		ServerSendPlayerUpdate(otherPlayerToUpdate);		
		break;
	// Make the player use the tools
	case MSG_TOOL_ACTION:
		var itemToUpdate = buffer_read(buffer, buffer_u32);
		var action = buffer_read(buffer, buffer_u8);
		switch(action){
			case TOOL_HOE:
				itemToUpdate.isDig = true;
				break;
			case TOOL_WATERING_CAN:
				itemToUpdate.water = true;
				break;
			case TOOL_SICKLE:
				itemToUpdate.cropType = 0;
				itemToUpdate.stage = 0;
				itemToUpdate.sendUpdate = true;
				break;
			case TOOL_HAMMER:
				var xx = itemToUpdate.x;
				var yy = itemToUpdate.y;
				
				ServerSendDelete(ITEM_ROCK, itemToUpdate);
				with(itemToUpdate) {
					instance_destroy();
				}
				var newBrokenRock = instance_create_depth(xx, yy, -50, objBrokenRock);
				ServerSendGeneric(objBrokenRock, newBrokenRock);
				break;
			case TOOL_AXE:
				var xx = itemToUpdate.x;
				var yy = itemToUpdate.y;
				
				ServerSendDelete(ITEM_TREE, itemToUpdate);
				with(itemToUpdate) {
					instance_destroy();
				}
				var newWood = instance_create_depth(xx, yy, -50, objWood);
				ServerSendGeneric(objWood, newWood);
				break;
			case GET_CROP:
				var otherPlayerToUpdate = ds_map_find_value(global.clients, socket);
				//scriptGetCrop(itemToUpdate, otherPlayerToUpdate);
				scriptGetItem(itemToUpdate.cropType, itemToUpdate, otherPlayerToUpdate);
				break;
			case GET_WOOD:
				var otherPlayerToUpdate = ds_map_find_value(global.clients, socket);
				scriptGetItem(ITEM_WOOD, itemToUpdate, otherPlayerToUpdate);
				break;
			case GET_ROCK:
				var otherPlayerToUpdate = ds_map_find_value(global.clients, socket);
				scriptGetItem(ITEM_BROKEN_ROCK, itemToUpdate, otherPlayerToUpdate);
				break;
			default:
				if(global.itemTypes[? action] == "Seed") {
					if (itemToUpdate.isDig == true and itemToUpdate.cropType == 0){
						if(action == SEED_RANDOM){ 
							itemToUpdate.cropType = irandom_range(CROP_CORN, CROP_PUMPKIN);
						} else {
							itemToUpdate.cropType = action + 60; // The crop number is the seed number + 60
						}
						itemToUpdate.sendUpdate = true;
					}
					break;
				}
		}
		break;
	// Update the current equipped item of the player
	case MSG_ITEM_EQUIPPED:
		var itemEquipped = buffer_read(buffer, buffer_u16);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		otherPlayer.itemEquipped = itemEquipped;
		ServerSendEquip(socket, itemEquipped);
		break;
	// Make the player gain money for selling crops
	case MSG_SELL:
		var itemToSell = buffer_read(buffer, buffer_u16);
		var quantity = buffer_read(buffer, buffer_u16);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		if(globalMoney) {
			serverPlayer.money += global.itemPricesSell[? itemToSell] * quantity;
			ServerSendMoneyGlobal(serverPlayer.money);
		} else {
			otherPlayer.money += global.itemPricesSell[? itemToSell] * quantity;
			ServerSendMoney(socket, otherPlayer.money);
		}
		otherPlayer.showMoney = true;
		otherPlayer.showMoneyValue = global.itemPricesSell[? itemToSell];
		otherPlayer.showMoneyY = 16;
		ServerSendShowMoney(socket, global.itemPricesSell[? itemToSell] * quantity);
		break;
	// Make the player lose money for buying stuff
	case MSG_BUY:
		var price = buffer_read(buffer, buffer_u16);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		if(globalMoney) {
			serverPlayer.money -= price;
			ServerSendMoneyGlobal(serverPlayer.money);
		} else {
			otherPlayer.money -= price;
			ServerSendMoney(socket, otherPlayer.money);
		}
		break;
	// Update the player's name, check if there's a save of him on the server
	// and, if there is, send the save to him 
	case MSG_NAME:
		var playerName = buffer_read(buffer, buffer_string);
		var otherPlayerToName = ds_map_find_value(global.clients, socket);
		otherPlayerToName.name = playerName;
		// send the player name to everybody
		ServerSendName(socket, playerName);
		// try to load the player save, if it exists, and send it to the player
		LoadPlayer(socket, playerName);
		break;
	// Show the text the player just sent on the chat, and send it to the other players
	case MSG_CHAT:
		var text = buffer_read(buffer, buffer_string);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		scrAddChat(otherPlayer.name, text);
		ServerSendChat(otherPlayer, text);
		break;
	// Update the current inventory of the player
	case MSG_PLAYER_INVENTORY:
		var inventoryString = buffer_read(buffer, buffer_string);
		var handItem = buffer_read(buffer, buffer_u32);
		var handQuantity = buffer_read(buffer, buffer_u32);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		ds_grid_read(otherPlayer.inventory, inventoryString);
		otherPlayer.handItem = handItem;
		otherPlayer.handQuantity = handQuantity;
		break;
	// Update the current hold item of the player
	case MSG_HAND_EQUIPPED:
		var handItem = buffer_read(buffer, buffer_u32);
		var handQuantity = buffer_read(buffer, buffer_u32);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		otherPlayer.handItem = handItem;
		otherPlayer.handQuantity = handQuantity;
		ServerSendHand(socket, handItem, handQuantity);
		break;
	case MSG_FISH_ON_ROD:
		var fishOnRod = buffer_read(buffer, buffer_bool);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		otherPlayer.fishOnRod = fishOnRod;
		ServerSendFishOnRod(otherPlayer, fishOnRod);
		break;
	case MSG_STATE:
		var playerState = buffer_read(buffer, buffer_u32);
		var playerSprite = buffer_read(buffer, buffer_u32);
		var otherPlayer = ds_map_find_value(global.clients, socket);
		otherPlayer.state = playerState;
		otherPlayer.sprite_index = playerSprite;
		ServerSendChangeState(otherPlayer, playerState, playerSprite);
		break;
}