var object = argument[0];
var spd = argument[1];
var dir = argument[2];

switch(dir) {
	case "up":
		if(place_meeting(x, y - spd, object)) {
			while(!place_meeting(x, y - spd, object)) {
				y -= sign(spd);
			}
			return true;
		} 
		break;
	case "down":
		if(place_meeting(x, y + spd, object)) {
			while(!place_meeting(x, y + spd, object)) {
				y += sign(spd);
			}
			return true;
		}
		break;
	case "left":
		if(place_meeting(x - spd, y, object)) {
			while(!place_meeting(x - spd, y, object)) {
				x -= sign(spd);
			}
			return true;
		}
		break;
	case "right":
		if(place_meeting(x + spd, y, object)) {
			while(!place_meeting(x + spd, y, object)) {
				x += sign(spd);
			}
			return true;
		}
		break;
}
return false;