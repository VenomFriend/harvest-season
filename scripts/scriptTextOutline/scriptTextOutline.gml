/// @param text
/// @param xx
/// @param yy

var text = argument[0];
var xx = argument[1];
var yy = argument[2];

draw_text_color(xx - 1, yy, text, c_black, c_black, c_black, c_black, 1);
draw_text_color(xx + 1, yy, text, c_black, c_black, c_black, c_black, 1);

draw_text_color(xx, yy - 1, text, c_black, c_black, c_black, c_black, 1);
draw_text_color(xx, yy + 1, text, c_black, c_black, c_black, c_black, 1);

draw_text_color(xx - 1, yy - 1, text, c_black, c_black, c_black, c_black, 1);
draw_text_color(xx - 1, yy + 1, text, c_black, c_black, c_black, c_black, 1);

draw_text_color(xx + 1, yy - 1, text, c_black, c_black, c_black, c_black, 1);
draw_text_color(xx + 1, yy + 1, text, c_black, c_black, c_black, c_black, 1);

draw_text(xx, yy, text);