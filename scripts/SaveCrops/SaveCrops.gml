var list = argument[0];
var map = ds_map_create();

ds_map_add(map, "x", x);
ds_map_add(map, "y", y);
ds_map_add(map, "isDig", isDig);
ds_map_add(map, "isGrass", isGrass);
ds_map_add(map, "water", water);
ds_map_add(map, "stage", stage);
ds_map_add(map, "cropType", cropType);
ds_map_add(map, "timer", timer);

ds_list_add(list, map);
ds_list_mark_as_map(list, ds_list_size(list) - 1);