/// @func ServerSendShowMoney(socket, money)
/// @param socket
/// @param money

var socketSize = ds_list_size(global.socketList);
var socket = argument[0];
var money = argument[1];


buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_SHOW_MONEY);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
buffer_write(global.bufferServerWrite, buffer_u16, money);
for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}