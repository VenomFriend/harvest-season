/// @param player

var playerToUpdate = argument[0];
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_PLAYER_POSITION);
buffer_write(global.bufferServerWrite, buffer_u32, playerToUpdate.playerSocket);
buffer_write(global.bufferServerWrite, buffer_u16, playerToUpdate.x);
buffer_write(global.bufferServerWrite, buffer_u16, playerToUpdate.y);
buffer_write(global.bufferServerWrite, buffer_u16, playerToUpdate.image_index);
buffer_write(global.bufferServerWrite, buffer_u16, playerToUpdate.myRoom);
buffer_write(global.bufferServerWrite, buffer_u16, playerToUpdate.stamina);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != playerToUpdate.playerSocket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}