/// @param buffer

var bufferClientReceived = argument[0]
var msgid = buffer_read(bufferClientReceived, buffer_u8);

switch(msgid) {
	// Create my own player, with his socket and stuff
	case MSG_CREATE_ID:
		var myPlayer = instance_create_depth(320, 320, -1000, objPlayer);
		instance_create_depth(0, 0, -2000, objClock);
		myPlayer.myPlayer = true;
		myPlayer.playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		myPlayer.alarm[0] = room_speed * 5;
		with(myPlayer) {
			audio_listener_orientation(0,1,0,0,0,1);
		}
		// Add myself to the list of players
		ds_map_add(global.players, myPlayer.playerSocket, myPlayer);	
		
		// Send my player name to the server
		ClientSendName(myPlayer.name);
		break;
	// Create the other player(s)
	case MSG_CREATE_PLAYER:
		var numberOfPlayers = buffer_read(bufferClientReceived, buffer_u8);
		for(var i = 0; i < numberOfPlayers; i++) {
			var newPlayerSocket = buffer_read(bufferClientReceived, buffer_u32);
			var newPlayerX = buffer_read(bufferClientReceived, buffer_u16);
			var newPlayerY = buffer_read(bufferClientReceived, buffer_u16);
			var newPlayerImage = buffer_read(bufferClientReceived, buffer_u16);
			var newPlayerToolEquipped = buffer_read(bufferClientReceived, buffer_u8);
			var newPlayerName = buffer_read(bufferClientReceived, buffer_string);
			var newPlayerState = buffer_read(bufferClientReceived, buffer_u16);
			if (is_undefined(ds_map_find_value(global.players, newPlayerSocket))){
	            var newPlayer = instance_create_depth(newPlayerX, newPlayerY, -1000, objPlayer);
				newPlayer.image_index = newPlayerImage;
				newPlayer.itemEquipped = newPlayerToolEquipped;
				newPlayer.name = newPlayerName;
				newPlayer.state = newPlayerState;
				ds_map_add(global.players, newPlayerSocket, newPlayer);
	        }
		}
		break;
	// Delete the player that has disconnected
	case MSG_DELETE_PLAYER:
		var playerSocketToDelete = buffer_read(bufferClientReceived, buffer_u32);
		if (!is_undefined(ds_map_find_value(global.players, playerSocketToDelete))){
            var otherPlayerToDelete = ds_map_find_value(global.players, playerSocketToDelete);
            ds_map_delete(global.players, playerSocketToDelete);
            with (otherPlayerToDelete) {
                instance_destroy();
            }
        }
		break;
	// See which key the other player pressed, and press it here too
	case MSG_KEY:
		var playerSocketToMove = buffer_read(bufferClientReceived, buffer_u32);
		var keyPressed = buffer_read(bufferClientReceived, buffer_u8);
		if (!is_undefined(ds_map_find_value(global.players, playerSocketToMove))){
			var otherPlayerToMove = ds_map_find_value(global.players, playerSocketToMove);
			MovePlayer(otherPlayerToMove, keyPressed);
		}
		break;
	// Update the other players position
	case MSG_UPDATE_PLAYER_POSITION:	
		var playerSocketToUpdate = buffer_read(bufferClientReceived, buffer_u32);
		var playerX = buffer_read(bufferClientReceived, buffer_u16);
		var playerY = buffer_read(bufferClientReceived, buffer_u16);
		var playerImage = buffer_read(bufferClientReceived, buffer_u16);
		var playerRoom = buffer_read(bufferClientReceived, buffer_u16);
		var playerStamina = buffer_read(bufferClientReceived, buffer_u16);
		if (!is_undefined(ds_map_find_value(global.players, playerSocketToUpdate))){
			var otherPlayerToUpdate = ds_map_find_value(global.players, playerSocketToUpdate);
			otherPlayerToUpdate.x = playerX;
			otherPlayerToUpdate.y = playerY;
			otherPlayerToUpdate.image_index = playerImage;
			otherPlayerToUpdate.myRoom = playerRoom;
			otherPlayerToUpdate.stamina = playerStamina;
		}
		break;
	// Update the crop sprite/etc
	case MSG_CROP:
		show_debug_message("HERE!");
		var cropID = buffer_read(bufferClientReceived, buffer_u32);
		var cropX = buffer_read(bufferClientReceived, buffer_u32);
		var cropY = buffer_read(bufferClientReceived, buffer_u32);
		var cropSprite = buffer_read(bufferClientReceived, buffer_u32);
		var cropImage = buffer_read(bufferClientReceived, buffer_u32);
		var cropStage = buffer_read(bufferClientReceived, buffer_u8);
		var cropIsDig = buffer_read(bufferClientReceived, buffer_bool);
		var cropIsGrass = buffer_read(bufferClientReceived, buffer_bool);
		var cropType = buffer_read(bufferClientReceived, buffer_u8);
		if (is_undefined(ds_map_find_value(global.crops, cropID))){
			var cropToUpdate = instance_create_depth(cropX, cropY, -10, objCrop);
			ds_map_add(global.crops, cropID, cropToUpdate);
		} else {
			var cropToUpdate = ds_map_find_value(global.crops, cropID);
			cropToUpdate.x = cropX;
			cropToUpdate.y = cropY;
		}
		cropToUpdate.sprite_index = cropSprite;
		cropToUpdate.image_index = cropImage;
		cropToUpdate.cropID = cropID;
		cropToUpdate.stage = cropStage;
		cropToUpdate.isDig = cropIsDig;
		cropToUpdate.isGrass = cropIsGrass;
		cropToUpdate.cropType = cropType;
		break;
	// Update my inventory with the crop that I clicked to get
	case MSG_UPDATE_INVENTORY:
		var inventory = buffer_read(bufferClientReceived, buffer_string);
		var handItem = buffer_read(bufferClientReceived, buffer_u32);
		var handQuantity = buffer_read(bufferClientReceived, buffer_u32);
		//var ypos = buffer_read(bufferClientReceived, buffer_u32);
		//var cropToAdd = buffer_read(bufferClientReceived, buffer_u32);
		//var quantity = buffer_read(bufferClientReceived, buffer_u32);
		var myPlayer = instance_find(objPlayer, 0);
		myPlayer.handItem = handItem;
		myPlayer.handQuantity = handQuantity;
		//ds_grid_set(myPlayer.inventory, 0, ypos, cropToAdd);
		//ds_grid_set(myPlayer.inventory, 1, ypos, quantity);	
		ds_grid_read(myPlayer.inventory, inventory);	
		// Send my inventory to the server
		ClientSendInventory(myPlayer);
		break;
	// Change the equipped item of the player, so when he clicks on the action button it will do the right thing
	case MSG_ITEM_EQUIPPED:
		var playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		var itemEquipped = buffer_read(bufferClientReceived, buffer_u16);
		if (!is_undefined(ds_map_find_value(global.players, playerSocket))){
			var otherPlayer = ds_map_find_value(global.players, playerSocket);
			otherPlayer.itemEquipped = itemEquipped;
		}
		break;
	// Update my money
	case MSG_SELL:
		var money = buffer_read(bufferClientReceived, buffer_u16);
		var myPlayer = instance_find(objPlayer, 0);
		myPlayer.money = money;
		break;
	// A little animation showing the money you just got by selling an item
	case MSG_SHOW_MONEY:
		var playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		var moneyToShow = buffer_read(bufferClientReceived, buffer_u16);
		if (!is_undefined(ds_map_find_value(global.players, playerSocket))){
			var otherPlayer = ds_map_find_value(global.players, playerSocket);
			otherPlayer.showMoney = true;
			otherPlayer.showMoneyY = 16;
			otherPlayer.showMoneyValue = moneyToShow;
		}
		break;
	// The name of the player
	case MSG_NAME:
		var playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		var playerName = buffer_read(bufferClientReceived, buffer_string);
		if (!is_undefined(ds_map_find_value(global.players, playerSocket))){
			var otherPlayer = ds_map_find_value(global.players, playerSocket);
			otherPlayer.name = playerName;
		}
		break;
	// Chat to display
	case MSG_CHAT:
		var playerName = buffer_read(bufferClientReceived, buffer_string);
		var text = buffer_read(bufferClientReceived, buffer_string);
		scrAddChat(playerName, text);
		break;
	// Load my player from the server
	case MSG_LOAD_PLAYER:
		var xx = buffer_read(bufferClientReceived, buffer_u32);
		var yy = buffer_read(bufferClientReceived, buffer_u32);
		var inventoryString = buffer_read(bufferClientReceived, buffer_string);
		var money = buffer_read(bufferClientReceived, buffer_u32);
		var myPlayer = instance_find(objPlayer, 0);
		myPlayer.x = xx;
		myPlayer.y = yy;
		var inventory = ds_grid_create(2, 16);
		ds_grid_read(inventory, inventoryString);
		myPlayer.inventory = inventory;
		myPlayer.money = money;
		break;
	// Create the generic object
	case MSG_OBJECT:
		var objectType = buffer_read(bufferClientReceived, buffer_u32);
		var objectID = buffer_read(bufferClientReceived, buffer_u32);
		var objectX = buffer_read(bufferClientReceived, buffer_u32);
		var objectY = buffer_read(bufferClientReceived, buffer_u32);
		var objectImageSpeed = buffer_read(bufferClientReceived, buffer_u32);
		var objectImageIndex = buffer_read(bufferClientReceived, buffer_u32);
		
		switch(objectType) {
			case objSmallTree:
				var mapFind = ds_map_find_value(global.trees, objectID);
				break;
			case objWood:
				var mapFind = ds_map_find_value(global.woods, objectID);
				break;
			case objSmallRock:
				var mapFind = ds_map_find_value(global.rocks, objectID);
				break;
			case objBrokenRock:
				var mapFind = ds_map_find_value(global.brokenRocks, objectID);
				break;
		}
		
		if (is_undefined(mapFind)){
			var objectToUpdate = instance_create_depth(objectX, objectY, -50, objectType);
			switch(objectType) {
				case objSmallTree:
					ds_map_add(global.trees, objectID, objectToUpdate);
					break;
				case objWood:
					ds_map_add(global.woods, objectID, objectToUpdate);
					break;
				case objSmallRock:
					ds_map_add(global.rocks, objectID, objectToUpdate);
					break;
				case objBrokenRock:
					ds_map_add(global.brokenRocks, objectID, objectToUpdate);
					break;
			}
		} else {
			switch(objectType) {
				case objSmallTree:
					var objectToUpdate = ds_map_find_value(global.trees, objectID);
					break;
				case objWood:
					var objectToUpdate = ds_map_find_value(global.woods, objectID);
					break;
				case objSmallRock:
					var objectToUpdate = ds_map_find_value(global.rocks, objectID);
					break;
				case objBrokenRock:
					var objectToUpdate = ds_map_find_value(global.brokenRocks, objectID);
					break;
			}
		}
		objectToUpdate.x = objectX
		objectToUpdate.y = objectY;
		objectToUpdate.myID = objectID;
		objectToUpdate.image_speed = objectImageSpeed;
		objectToUpdate.image_index = objectImageIndex;
		break;
	// Delete the generic object
	case MSG_OBJECT_DELETE:
		var objectType = buffer_read(bufferClientReceived, buffer_u32);
		var objectID = buffer_read(bufferClientReceived, buffer_u32);
		switch(objectType) {
			case ITEM_TREE:
				var mapFind = ds_map_find_value(global.trees, objectID);
				if (!is_undefined(mapFind)){
					ds_map_delete(global.trees, objectID);
					with(mapFind) {
						instance_destroy();
					}
				}
				break;
			case ITEM_WOOD:
				var mapFind = ds_map_find_value(global.woods, objectID);
				if (!is_undefined(mapFind)){
					ds_map_delete(global.woods, objectID);
					with(mapFind) {
						instance_destroy();
					}
				}
				break;
			case ITEM_ROCK:
				var mapFind = ds_map_find_value(global.rocks, objectID);
				if (!is_undefined(mapFind)){
					ds_map_delete(global.rocks, objectID);
					with(mapFind) {
						instance_destroy();
					}
				}
				break;
			case ITEM_BROKEN_ROCK:
				var mapFind = ds_map_find_value(global.brokenRocks, objectID);
				if (!is_undefined(mapFind)){
					ds_map_delete(global.brokenRocks, objectID);
					with(mapFind) {
						instance_destroy();
					}
				}
				break;
		}
		break;
	// Change the hand item of the player
	case MSG_HAND_EQUIPPED:
		var playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		var handItem = buffer_read(bufferClientReceived, buffer_u32);
		var handQuantity = buffer_read(bufferClientReceived, buffer_u32);
		if (!is_undefined(ds_map_find_value(global.players, playerSocket))){
			var otherPlayer = ds_map_find_value(global.players, playerSocket);
			otherPlayer.handItem = handItem;
			otherPlayer.handQuantity = handQuantity;
		}
		break;
	case MSG_FISH_ON_ROD:
		var playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		var fishOnRod = buffer_read(bufferClientReceived, buffer_bool);
		if (!is_undefined(ds_map_find_value(global.players, playerSocket))){
			var otherPlayer = ds_map_find_value(global.players, playerSocket);
			otherPlayer.fishOnRod = fishOnRod;
		}
		break;
	case MSG_STATE:
		var playerSocket = buffer_read(bufferClientReceived, buffer_u32);
		var playerState = buffer_read(bufferClientReceived, buffer_u32);
		var playerSprite = buffer_read(bufferClientReceived, buffer_u32);
		if (!is_undefined(ds_map_find_value(global.players, playerSocket))){
			var otherPlayer = ds_map_find_value(global.players, playerSocket);
			otherPlayer.state = playerState;
			otherPlayer.sprite_index = playerSprite;
		}
		break;
	case MSG_CLOCK:
		var hour = buffer_read(bufferClientReceived, buffer_u8);
		var minute = buffer_read(bufferClientReceived, buffer_u8);
		var timePassed = buffer_read(bufferClientReceived, buffer_u8);
		var clock = instance_find(objClock, 0);
		clock.hour = hour;
		clock.minute = minute;
		clock.timePassed = timePassed;
		break;
}