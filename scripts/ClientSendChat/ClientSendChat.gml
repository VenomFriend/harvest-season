/// @func ClientSendChat(text)
/// @param text

var chat = argument[0];

// Send the chat to the server
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_CHAT);
buffer_write(global.bufferClientWrite, buffer_string, chat);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));