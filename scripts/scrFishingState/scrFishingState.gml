if(myPlayer) {
	var fishPrevious = fishOnRod;
	if(timeToFish <= -360) {
		fishOnRod = false;
		timeToFish = irandom_range(360, 500);
	} else if(timeToFish > - 360 and timeToFish <= 0) {
		fishOnRod = true;
	} else {
		fishOnRod = false;
	}
	
	if(fishPrevious != fishOnRod) {
		if(isHost) {
			ServerSendFishOnRod(playerSocket, fishOnRod);
		} else {
			ClientSendFishOnRod(fishOnRod);
		}
	}

	timeToFish--;
	if(useItemPressed) {
		if(fishOnRod) {
			fishOnRod = false;
			timeToFish = -1;
			handItem = ITEM_FISH;
			handQuantity = 1;
			if(isHost) {
				ServerSendHand(playerSocket, handItem, handQuantity);
				ServerSendFishOnRod(playerSocket, fishOnRod);
			} else {
				ClientSendHand(handItem, handQuantity);
				ClientSendFishOnRod(fishOnRod);
			}
		}
		state = states.moving;
		sprite_index = sprPlayerMaleWalking;
		if(isHost) {
			ServerSendChangeState(playerSocket, state, sprite_index);
		} else {
			ClientSendChangeState(state, sprite_index);
		}
	}
}