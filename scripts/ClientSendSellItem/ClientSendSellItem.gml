/// @func ClientSendSellItem(item, quantity)
/// @param item
/// @param quantity

var itemToSell = argument[0];
var quantity = argument[1];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_SELL);
buffer_write(global.bufferClientWrite, buffer_u16, itemToSell);
buffer_write(global.bufferClientWrite, buffer_u16, quantity);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));