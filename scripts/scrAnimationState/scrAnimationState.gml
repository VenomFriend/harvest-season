var finishAnimation = false;
var xToCheck = 0;
var yToCheck = 0;
switch(facing) {
	case "up":
		xToCheck = x + 8;
		yToCheck = y - 8;
		break;
	case "down":
		xToCheck = x + 8;
		yToCheck = y + 24;
		break;
	case "left":
		xToCheck = x - 8;
		yToCheck = y + 8;
		break;
	case "right":
		xToCheck = x + 24
		yToCheck = y + 8;
		break;
}
if(global.itemTypes[? itemEquipped] == "Tool") {
	if(itemEquipped == TOOL_FISHING_ROD) {
		image_speed = 0.5;
		if(image_index mod 4 >= 3.5) {
			image_speed = 0;
			finishAnimation = true;
			usingItem = false;
			if(position_meeting(xToCheck, yToCheck, objFishingArea)) {
				state = states.fishing;
			} else {
				sprite_index = sprPlayerMaleWalking;
				state = states.moving;
			}
		}
	} else {
		image_speed = 0.5;
		if(image_index mod 4 >= 3.5) {
			usingItem = false;
			sprite_index = sprPlayerMaleWalking;
			finishAnimation = true;
			state = states.moving;
		}
	}
} else if (global.itemTypes[? itemEquipped] == "Seed" or global.itemTypes[? itemEquipped] == "Misc") {
		usingItem = false;
		finishAnimation = true;
		state = states.moving;
}

var woodToDestroy = noone;
var rockToDestroy = noone;

if(room == roomFarm) {
	woodToDestroy = instance_position(xToCheck, yToCheck, objSmallTree);
	rockToDestroy = instance_position(xToCheck, yToCheck, objSmallRock);
	waterToCheck = instance_position(xToCheck, yToCheck, objWater);
}

//Only my player will send stuff to the server/clients
if(myPlayer and finishAnimation) {
	switch(itemEquipped){
		case TOOL_HOE:
			if(plotToHighlight != noone and plotToHighlight.isGrass == false and woodToDestroy == noone and rockToDestroy == noone) {
				if(isHost) {
					plotToHighlight.isDig = true;
				} else { 
					ClientSendToolAction(plotToHighlight.cropID, TOOL_HOE);
				}
			}
			break;
		case TOOL_WATERING_CAN:
			if(plotToHighlight != noone and woodToDestroy == noone and rockToDestroy == noone and waterToCheck == noone) {
				if(isHost) {
					if(waterLevel > 0) {
						plotToHighlight.water = true;
						waterLevel--;
					}
				} else { 
					if(waterLevel > 0) {
						ClientSendToolAction(plotToHighlight.cropID, TOOL_WATERING_CAN);
						waterLevel--;
					}
				}
			} else if(waterToCheck != noone) {
				waterLevel = 20;
			}
			break;
		case TOOL_SICKLE:
			if(plotToHighlight != noone and woodToDestroy == noone and rockToDestroy == noone) {
				if(isHost) {
					if(plotToHighlight.cropType != 0) {
						plotToHighlight.cropType = 0;
						plotToHighlight.stage = 0;
						plotToHighlight.sendUpdate = true;
					} else if(plotToHighlight.isGrass) {
						plotToHighlight.isGrass = false;
					}
				} else { 
					ClientSendToolAction(plotToHighlight.cropID, TOOL_SICKLE);
				}
			}
			break;
		case TOOL_HAMMER:
			if(rockToDestroy != noone) {
				if(isHost) {
					var xx = rockToDestroy.x;
					var yy = rockToDestroy.y;
					ServerSendDelete(ITEM_ROCK, rockToDestroy);
					with(rockToDestroy) {
						instance_destroy();
					}
					var newBrokenRock = instance_create_depth(xx, yy, -50, objBrokenRock);
					ServerSendGeneric(objBrokenRock, newBrokenRock);
					
				} else {
					ClientSendToolAction(rockToDestroy.myID, TOOL_HAMMER);
				}
			}
			break;
		case TOOL_AXE:
			if(woodToDestroy != noone) {
				if(isHost) {
					var xx = woodToDestroy.x;
					var yy = woodToDestroy.y;
					ServerSendDelete(ITEM_TREE, woodToDestroy);
					with(woodToDestroy) {
						instance_destroy();
					}
					var newWood = instance_create_depth(xx, yy, -50, objWood);
					ServerSendGeneric(objWood, newWood);
					
				} else {
					ClientSendToolAction(woodToDestroy.myID, TOOL_AXE);
				}
			}
			break;
		case TOOL_FISHING_ROD:
			if(position_meeting(xToCheck, yToCheck, objFishingArea)) {
				timeToFish = irandom_range(360, 500);
			}
			break;
		default:
			if(global.itemTypes[? itemEquipped] == "Seed" and woodToDestroy == noone) {
				if(plotToHighlight != noone) {
					if (plotToHighlight.isDig == true and plotToHighlight.cropType == 0 and plotToHighlight.isGrass == false){
						if(isHost) {
							if(itemEquipped == SEED_RANDOM){
								plotToHighlight.cropType = irandom_range(CROP_CORN, CROP_PUMPKIN);
							} else {
								plotToHighlight.cropType = itemEquipped + 60; // The crop number is the seed number + 60
							}
							plotToHighlight.sendUpdate = true;
						} else {
							ClientSendToolAction(plotToHighlight.cropID, itemEquipped);
						}
						var itemQuantity = ds_grid_get(inventory, 1, itemSelected);
						if itemQuantity == 1 {
							ds_grid_set(inventory, 0, itemSelected, NO_ITEM);
							itemEquipped = NO_ITEM;
							// If the tool changed to none, tell everybody that you have nothing equipped now
							if(isHost){
								ServerSendEquip(playerSocket, NO_ITEM);
							} else {
								ClientSendEquip(NO_ITEM);
							}
						}
						ds_grid_set(inventory, 1, itemSelected, itemQuantity-1);
						if(!isHost) {
							// send my updated inventory to the server
							ClientSendInventory(inventory);
						}
					}
				}
			}
			break;
	}
}