var foundPlayer = false;
for(var i = 0; i < ds_list_size(global.playersList); i++) {
	var thisPlayerMap = ds_list_find_value(global.playersList, i);
	var playerName = ds_map_find_value(thisPlayerMap, "name");
	if(name == playerName) {
		foundPlayer = true;
		ds_map_replace(thisPlayerMap, "money", money);
		ds_map_replace(thisPlayerMap, "x", x);
		ds_map_replace(thisPlayerMap, "y", y);
		ds_map_replace(thisPlayerMap, "inventory", ds_grid_write(inventory));
		break;
	}
}
if(!foundPlayer) {
	var map = ds_map_create();
	ds_map_add(map, "name", name);
	ds_map_add(map, "money", money);
	ds_map_add(map, "x", x);
	ds_map_add(map, "y", y);
	ds_map_add(map, "inventory", ds_grid_write(inventory));
	ds_list_add(global.playersList, map);
	ds_list_mark_as_map(global.playersList, ds_list_size(global.playersList) - 1);
}