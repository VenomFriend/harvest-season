/// @func ServerSendName(socket, name)
/// @param socket
/// @param name

var playerSocket = argument[0];
var name = argument[1];
var socketSize = ds_list_size(global.socketList);

// Tell everyone that there's a new playe in town
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_NAME);
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);
buffer_write(global.bufferServerWrite, buffer_string, name);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != playerSocket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}