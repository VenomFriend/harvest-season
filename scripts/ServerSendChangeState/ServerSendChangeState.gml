/// @func ServerSendChangeState(socket, state, sprite)
/// @param socket
/// @param state
/// @param sprite

var socket = argument[0];
var playerState = argument[1];
var playerSprite = argument[2];

var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_STATE);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
buffer_write(global.bufferServerWrite, buffer_u32, playerState);
buffer_write(global.bufferServerWrite, buffer_u32, playerSprite);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != socket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}