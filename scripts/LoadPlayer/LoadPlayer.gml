var socket = argument[0];
var name = argument[1];

/*
var file = file_text_open_read("server.json");
if(file) {
	var jsonData = "";
	while (!file_text_eof(file))
	{
	    jsonData += file_text_read_string(file);
	    file_text_readln(file);
	}
	file_text_close(file);
	var loadObjects = json_decode(jsonData);
	
	var playerList = ds_map_find_value(loadObjects, "players");
	show_debug_message("Player List Size");
	show_debug_message(string(ds_list_size(playerList)));
	for(var i = 0; i < ds_list_size(playerList); i++) {
		var thisPlayer = ds_list_find_value(playerList, i);
		var name = ds_map_find_value(thisPlayer, "name");
		if(name == playerName) {
			var xx = ds_map_find_value(thisPlayer, "x");
			var yy = ds_map_find_value(thisPlayer, "y");
			var inventoryString = ds_map_find_value(thisPlayer, "inventory");
			var money = ds_map_find_value(thisPlayer, "money");
			ServerSendPlayerSave(socket, xx, yy, inventoryString, money);
		}
	}
}
*/
for(var i = 0; i < ds_list_size(global.playersList); i++) {
	var thisPlayerMap = ds_list_find_value(global.playersList, i);
	var playerName = ds_map_find_value(thisPlayerMap, "name");
	if(name == playerName) {
		var xx = ds_map_find_value(thisPlayerMap, "x");
		var yy = ds_map_find_value(thisPlayerMap, "y");
		var inventoryString = ds_map_find_value(thisPlayerMap, "inventory");
		var money = ds_map_find_value(thisPlayerMap, "money");
		var thisPlayer = ds_map_find_value(global.clients, socket);
		thisPlayer.x = xx;
		thisPlayer.y = yy;
		thisPlayer.money = money;
		ds_grid_read(thisPlayer.inventory, inventoryString);
		ServerSendPlayerSave(socket, xx, yy, inventoryString, money);
		break;
	}
}