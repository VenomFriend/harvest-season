/// @func ServerSendClock(hour, minute, timePassed)
/// @param hour
/// @param minute
/// @param timePassed

var hour = argument[0];
var minute = argument[1];
var timePassed = argument[2];

var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CLOCK);
buffer_write(global.bufferServerWrite, buffer_u8, hour);
buffer_write(global.bufferServerWrite, buffer_u8, minute);
buffer_write(global.bufferServerWrite, buffer_u8, timePassed);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}