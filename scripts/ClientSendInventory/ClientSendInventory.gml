/// @func ClientSendInventory(player)
/// @param player

var player = argument[0];

var inventoryString = ds_grid_write(player.inventory);

// Send my inventory to the server, as a string
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_PLAYER_INVENTORY);
buffer_write(global.bufferClientWrite, buffer_string, inventoryString);
buffer_write(global.bufferClientWrite, buffer_u32, player.handItem);
buffer_write(global.bufferClientWrite, buffer_u32, player.handQuantity);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));