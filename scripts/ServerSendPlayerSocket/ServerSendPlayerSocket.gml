/// @func ServerSendPlayerSocket(socketOfPlayer);
/// @param socket

var socket = argument[0];

// Tell the player who just connected his socket, which will be used as an ID
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_ID);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));