/// @func ServerSendChat(player, text);
/// @param player
/// @param text

var player = argument[0];
var text = argument[1];
var socketSize = ds_list_size(global.socketList);

// Tell everyone the new chat message
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CHAT);
buffer_write(global.bufferServerWrite, buffer_string, player.name);
buffer_write(global.bufferServerWrite, buffer_string, text);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != player.playerSocket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}