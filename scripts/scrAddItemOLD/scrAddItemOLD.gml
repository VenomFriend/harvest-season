var player = argument[0];
var itemToAdd = argument[1];
var itemQuantity = argument[2];

var gridHeight = ds_grid_height(player.inventory);

for(var i = 0; i < gridHeight; i++) {
	if(player.inventory[# 0, i] == itemToAdd) {
		var inventoryQuantity = player.inventory[# 1, i];
		if(itemQuantity + inventoryQuantity <= 99) {
			player.inventory[# 1, i] = itemQuantity + inventoryQuantity;
			return true;
		}
	}
}

for(var i = 0; i < gridHeight; i++) {
	if(player.inventory[# 0, i] == NO_ITEM) {
		player.inventory[# 0, i] = itemToAdd;
		player.inventory[# 1, i] = itemQuantity;
		return true;
	}
}

return false;

// TODO: make it so, for example, if you have 50 of one item and buy 80 more, instead of having one stack of 50 and one of 80, you have one with 99 and one with 31