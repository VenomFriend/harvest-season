/// @func ServerSendMoney(socket, money)
/// @param socket
/// @param money

var socket = argument[0];
var money = argument[1];


buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_SELL);
buffer_write(global.bufferServerWrite, buffer_u16, money);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));