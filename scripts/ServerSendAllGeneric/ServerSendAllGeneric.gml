/// @function ServerSendAllGeneric(socket, objectType)
/// @param socket
/// @param objectType

var playerSocket = argument[0];
var objectType = argument[1];

var objectNumber = instance_number(objectType);

// Send all the objects to the player
if(objectNumber > 0) {
	for(var i = 0; i < objectNumber; i++) {
		var object = instance_find(objectType, i);
		buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
		buffer_write(global.bufferServerWrite, buffer_u8, MSG_OBJECT);
		buffer_write(global.bufferServerWrite, buffer_u32, objectType);
		buffer_write(global.bufferServerWrite, buffer_u32, object.id); // we will use his id to identify it
		buffer_write(global.bufferServerWrite, buffer_u32, object.x);
		buffer_write(global.bufferServerWrite, buffer_u32, object.y);
		buffer_write(global.bufferServerWrite, buffer_u32, object.image_speed);
		buffer_write(global.bufferServerWrite, buffer_u32, object.image_index);
		network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}