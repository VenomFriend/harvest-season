/// @func ClientSendEquip(handItem, handQuantity)
/// @param handItem
/// @param handQuantity

var handItem = argument[0];
var handQuantity = argument[1];

// Send the currently equipped item to the server
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_HAND_EQUIPPED);
buffer_write(global.bufferClientWrite, buffer_u32, handItem);
buffer_write(global.bufferClientWrite, buffer_u32, handQuantity);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));