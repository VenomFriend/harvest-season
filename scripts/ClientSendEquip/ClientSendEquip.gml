/// @func ClientSendEquip(itemEquipped)
/// @param itemEquipped

var itemEquipped = argument[0];

// Send the currently equipped item to the server
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_ITEM_EQUIPPED);
buffer_write(global.bufferClientWrite, buffer_u16, itemEquipped);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));