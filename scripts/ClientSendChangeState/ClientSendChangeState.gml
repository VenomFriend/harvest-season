/// @function ClientSendChangeState(state, sprite);
/// @param state
/// @param sprite

var stateToSend = argument[0];
var spriteIndex = argument[1];

// Send the new state to the server
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_STATE);
buffer_write(global.bufferClientWrite, buffer_u32, stateToSend);
buffer_write(global.bufferClientWrite, buffer_u32, spriteIndex);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));