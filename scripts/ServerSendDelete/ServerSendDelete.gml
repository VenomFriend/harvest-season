/// @function ServerSendDelete(objectType, object)
/// @param objectType
/// @param object

var objectType = argument[0];
var object = argument[1];

var socketSize = ds_list_size(global.socketList);

// Send the object to be deleted to all the players
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_OBJECT_DELETE);
buffer_write(global.bufferServerWrite, buffer_u32, objectType);
buffer_write(global.bufferServerWrite, buffer_u32, object.id); // we will use his id to identify it
for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}