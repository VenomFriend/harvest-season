/// @function scrAddItem(firstItem, player, itemToAdd, itemQuantity)

// This is a recursive function that will add the items you bought to your inventory
// It works by checking first if you already have the same item in your inventory, in other stacks
// Then it will try to add the item you bought to those stacks, and if it still has any
// left, it will see if there's an empty slot to put it
// It will only return true if all the items you bought can be filled in your inventory
// and only then it will add them to your inventory as well

var first = argument[0];
var player = argument[1];
var itemToAdd = argument[2];
var itemQuantity = argument[3];

var gridHeight = ds_grid_height(player.inventory);

for(var i = first; i < gridHeight; i++) {
	if(player.inventory[# 0, i] == itemToAdd) {
		var inventoryQuantity = player.inventory[# 1, i];
		if(itemQuantity + inventoryQuantity <= 99) {
			player.inventory[# 1, i] = itemQuantity + inventoryQuantity;
			return true;
		} else {
			var canAdd = 99 - inventoryQuantity;
			itemQuantity = itemQuantity - canAdd;
			var addRest = scrAddItem(i + 1, player, itemToAdd, itemQuantity);
			// Only add the items if you was able to add the rest of the items first
			if(addRest) {
				player.inventory[# 1, i] += canAdd;
				return true;
			}
		}
	}
}

// If there are no stacks of the item already in your inventory, or if the stacks already have 99
// check if there's an empty slot and put it there
for(var i = 0; i < gridHeight; i++) {
	if(player.inventory[# 0, i] == NO_ITEM) {
		player.inventory[# 0, i] = itemToAdd;
		player.inventory[# 1, i] = itemQuantity;
		return true;
	}
}

return false;