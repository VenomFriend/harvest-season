/// @func ClientSendToolAction(itemID, action)
/// @param itemID
/// @param action

var itemID = argument[0];
var action = argument[1];

// Send the tool action to the server
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_TOOL_ACTION);
buffer_write(global.bufferClientWrite, buffer_u32, itemID);
buffer_write(global.bufferClientWrite, buffer_u8, action);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));