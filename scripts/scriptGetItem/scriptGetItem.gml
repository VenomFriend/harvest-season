/// @func scriptGetItem(itemType, itemID, playerGetting);

var itemToGet = argument[0];
var itemID = argument[1];
var playerGetting = argument[2];

playerGetting.handItem = itemToGet;
playerGetting.handQuantity = 1;
//var deleteItem = scrAddItem(0, playerGetting, itemToGet, 1);
//if(deleteItem){
	ServerSendInventoryUpdate(playerGetting, playerGetting.inventory);
	// If the item is a crop, don't delete the object, just update it
	if(global.itemTypes[? itemToGet] == "Crop") {
		itemID.isDig = false;
		itemID.water = false;
		itemID.stage = 0;
		itemID.cropType = 0;
		itemID.sendUpdate = true;
	} else {
		ServerSendDelete(itemToGet, itemID);
		with(itemID) {
			instance_destroy();
		}
	}
//}