/// @func ServerSendInventoryUpdate(player, inventory)
/// @param player
/// @param inventory

var playerUpdated = argument[0];
var playerInventory = argument[1];
//var ypos = argument[1];
//var itemToAdd = argument[2];
//var quantity = argument[3];
var inventoryString = ds_grid_write(playerInventory);
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_INVENTORY);
buffer_write(global.bufferServerWrite, buffer_string, inventoryString);
buffer_write(global.bufferServerWrite, buffer_u32, playerUpdated.handItem);
buffer_write(global.bufferServerWrite, buffer_u32, playerUpdated.handQuantity);
//buffer_write(global.bufferServerWrite, buffer_u32, ypos);
//buffer_write(global.bufferServerWrite, buffer_u32, itemToAdd);
//buffer_write(global.bufferServerWrite, buffer_u32, quantity);

network_send_packet(playerUpdated.playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));