var list = argument[0];
var map = ds_map_create();

ds_map_add(map, "x", x);
ds_map_add(map, "y", y);
ds_map_add(map, "image_index", image_index);
ds_map_add(map, "image_speed", image_speed);

ds_list_add(list, map);
ds_list_mark_as_map(list, ds_list_size(list) - 1);