var player = argument[0];

if player.isHost {
	if(player.moveLeftPressed) {
        ServerSendKey(KEY_LEFT_PRESSED, player.playerSocket);
    }
    if(player.moveRightPressed) {
        ServerSendKey(KEY_RIGHT_PRESSED, player.playerSocket);
    }
    if(player.moveUpPressed) {
        ServerSendKey(KEY_UP_PRESSED, player.playerSocket);
    }
    if(player.moveDownPressed) {
        ServerSendKey(KEY_DOWN_PRESSED, player.playerSocket);
    }
	if(player.moveLeftReleased) {
        ServerSendKey(KEY_LEFT_RELEASED, player.playerSocket);
    }
    if(player.moveRightReleased) {
        ServerSendKey(KEY_RIGHT_RELEASED, player.playerSocket);
    }
    if(player.moveUpReleased) {
        ServerSendKey(KEY_UP_RELEASED, player.playerSocket);
    }
    if(player.moveDownReleased) {
        ServerSendKey(KEY_DOWN_RELEASED, player.playerSocket);
    }
	if(player.useItemPressed) {
        ServerSendKey(KEY_ITEM_PRESSED, player.playerSocket);
    }
    if(player.useItemReleased) {
        ServerSendKey(KEY_ITEM_RELEASED, player.playerSocket);
    }
	if(player.openBagPressed) {
		if(player.state = states.inventory) {
			ServerSendKey(KEY_INVENTORY_OPEN, player.playerSocket);
		} else {
			ServerSendKey(KEY_INVENTORY_CLOSE, player.playerSocket);
		}
    }
	if(player.runPressed) {
        ServerSendKey(KEY_RUN_PRESSED, player.playerSocket);
    }
    if(player.runReleased) {
        ServerSendKey(KEY_RUN_RELEASED, player.playerSocket);
    }
	if(player.chatPressed) {
        ServerSendKey(KEY_CHAT_PRESSED, player.playerSocket);
    }
	

} else {
	if(player.moveLeftPressed) {
        ClientSendKey(KEY_LEFT_PRESSED);
    }
    if(player.moveRightPressed) {
        ClientSendKey(KEY_RIGHT_PRESSED);
    }
    if(player.moveUpPressed) {
        ClientSendKey(KEY_UP_PRESSED);
    }
    if(player.moveDownPressed) {
        ClientSendKey(KEY_DOWN_PRESSED);
    }
	if(player.moveLeftReleased) {
        ClientSendKey(KEY_LEFT_RELEASED);
    }
    if(player.moveRightReleased) {
        ClientSendKey(KEY_RIGHT_RELEASED);
    }
    if(player.moveUpReleased) {
        ClientSendKey(KEY_UP_RELEASED);
    }
    if(player.moveDownReleased) {
        ClientSendKey(KEY_DOWN_RELEASED);
    }
	if(player.useItemPressed) {
        ClientSendKey(KEY_ITEM_PRESSED);
    }
    if(player.useItemReleased) {
        ClientSendKey(KEY_ITEM_RELEASED);
    }
	if(player.openBagPressed) {
		if(player.state == states.inventory) {
			ClientSendKey(KEY_INVENTORY_OPEN);
		} else {
			ClientSendKey(KEY_INVENTORY_CLOSE);
		}
    }
	if(player.runPressed) {
        ClientSendKey(KEY_RUN_PRESSED);
    }
    if(player.runReleased) {
        ClientSendKey(KEY_RUN_RELEASED);
    }
	if(player.chatPressed) {
        ClientSendKey(KEY_CHAT_PRESSED);
    }
	
}