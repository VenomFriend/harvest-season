/// @function ServerSendAllCrops(socket)
/// @param socket

var playerSocket = argument[0];

var cropNumber = instance_number(objCrop);

// Send all the crops to the new player
if(cropNumber > 0) {
	for(var i = 0; i < cropNumber; i++) {
		var crop = instance_find(objCrop, i);
		buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
		buffer_write(global.bufferServerWrite, buffer_u8, MSG_CROP);
		buffer_write(global.bufferServerWrite, buffer_u32, crop.id); // we will use his id to identify it
		buffer_write(global.bufferServerWrite, buffer_u32, crop.x);
		buffer_write(global.bufferServerWrite, buffer_u32, crop.y);
		buffer_write(global.bufferServerWrite, buffer_u32, crop.sprite_index);
		buffer_write(global.bufferServerWrite, buffer_u32, crop.image_index);
		buffer_write(global.bufferServerWrite, buffer_u8, crop.stage);
		buffer_write(global.bufferServerWrite, buffer_bool, crop.isDig);
		buffer_write(global.bufferServerWrite, buffer_bool, crop.isGrass);
		buffer_write(global.bufferServerWrite, buffer_u8, crop.cropType);
		network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}

	
}