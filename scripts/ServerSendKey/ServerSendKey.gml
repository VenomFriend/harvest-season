/// @func ServerSendKey(key, socket)
/// @param key
/// @param socket

var key = argument[0];
var socket = argument[1];

var socketSize = ds_list_size(global.socketList);

// Send the key press to all the players
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_KEY);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
buffer_write(global.bufferServerWrite, buffer_u8, key);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != socket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}