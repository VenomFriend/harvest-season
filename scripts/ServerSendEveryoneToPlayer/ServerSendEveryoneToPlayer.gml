/// @function ServerSendEveryoneToPlayer(socket)
/// @param socket

var socket = argument[0];

var playerCount = ds_list_size(global.socketList);
//I don't need to add +1 for the server player,
//because if there's the server and 1 player, the playerCount will be one and only one 
//player will be sent(the server)

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u8, playerCount);

//Write a packet with the server's player info
var serverPlayer = instance_find(objPlayer, 0);
buffer_write(global.bufferServerWrite, buffer_u32, serverPlayer.playerSocket);
buffer_write(global.bufferServerWrite, buffer_u16, serverPlayer.x);
buffer_write(global.bufferServerWrite, buffer_u16, serverPlayer.y);
buffer_write(global.bufferServerWrite, buffer_u16, serverPlayer.image_index);
buffer_write(global.bufferServerWrite, buffer_u8, serverPlayer.itemEquipped);
buffer_write(global.bufferServerWrite, buffer_string, serverPlayer.name);
buffer_write(global.bufferServerWrite, buffer_u16, serverPlayer.state);

//Write a really big packet with everybody information
for(var i = 0; i < playerCount-1; i++) {
    var thisplayerSocket = ds_list_find_value(global.socketList, i);
    var thisPlayer = ds_map_find_value(global.clients, thisplayerSocket);
    //I really don't need to send to the player his own information, right?
    if (thisplayerSocket != socket) {
        buffer_write(global.bufferServerWrite, buffer_u32, thisplayerSocket);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.x);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.y);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.image_index);
		buffer_write(global.bufferServerWrite, buffer_u8, thisPlayer.itemEquipped);
		buffer_write(global.bufferServerWrite, buffer_string, thisPlayer.name);
		buffer_write(global.bufferServerWrite, buffer_u16, serverPlayer.state);
    }
}

network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));