/// @func ServerSendEquip(socket, item)
/// @param socket
/// @param item

var socket = argument[0];
var itemEquipped = argument[1];

var socketSize = ds_list_size(global.socketList);

// Send the equipped item of the player to everyone
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_ITEM_EQUIPPED);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
buffer_write(global.bufferServerWrite, buffer_u16, itemEquipped);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != socket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}