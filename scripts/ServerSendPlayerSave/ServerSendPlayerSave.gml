/// @func ServerSendPlayerSave(socket, x, y, inventory, money)
/// @param socket
/// @param x
/// @param y
/// @param inventory
/// @param money

var socket = argument[0];
var xx = argument[1];
var yy = argument[2];
var inventory = argument[3];
var money = argument[4];

// Send the player who just connected the save of his player
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_LOAD_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u32, xx);
buffer_write(global.bufferServerWrite, buffer_u32, yy);
buffer_write(global.bufferServerWrite, buffer_string, inventory);
buffer_write(global.bufferServerWrite, buffer_u32, money);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));