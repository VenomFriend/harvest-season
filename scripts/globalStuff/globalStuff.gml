global.ipAddress = "127.0.0.1";
global.port = 8000;
global.name = "Joe";
global.showName = false;
global.roomID = ROOM_FARM;

// Tile map info
layer_set_target_room(roomFarm);
var playerLayer = layer_get_id("collision_layer");
global.tilemap = layer_tilemap_get_id(playerLayer);


global.cropsTimer[CROP_CORN, 0] = 300; //stage 0
global.cropsTimer[CROP_CORN, 1] = 300; // stage 1
global.cropsImage[CROP_CORN, 0] = 0; //not watered
global.cropsImage[CROP_CORN, 1] = 12; // watered

global.cropsTimer[CROP_TOMATO, 0] = 300; //stage 0
global.cropsTimer[CROP_TOMATO, 1] = 300; // stage 1
global.cropsImage[CROP_TOMATO, 0] = 3; //not watered
global.cropsImage[CROP_TOMATO, 1] = 15; // watered

global.cropsTimer[CROP_CARROT, 0] = 300; //stage 0
global.cropsTimer[CROP_CARROT, 1] = 300; // stage 1
global.cropsImage[CROP_CARROT, 0] = 6; //not watered
global.cropsImage[CROP_CARROT, 1] = 18; // watered

global.cropsTimer[CROP_TURNIP, 0] = 300; //stage 0
global.cropsTimer[CROP_TURNIP, 1] = 300; // stage 1
global.cropsImage[CROP_TURNIP, 0] = 9; //not watered
global.cropsImage[CROP_TURNIP, 1] = 21; // watered

global.cropsTimer[CROP_POTATO, 0] = 300; //stage 0
global.cropsTimer[CROP_POTATO, 1] = 300; // stage 1
global.cropsImage[CROP_POTATO, 0] = 24; //not watered
global.cropsImage[CROP_POTATO, 1] = 36; // watered

global.cropsTimer[CROP_EGGPLANT, 0] = 300; //stage 0
global.cropsTimer[CROP_EGGPLANT, 1] = 300; // stage 1
global.cropsImage[CROP_EGGPLANT, 0] = 27; //not watered
global.cropsImage[CROP_EGGPLANT, 1] = 39; // watered

global.cropsTimer[CROP_ONION, 0] = 300; //stage 0
global.cropsTimer[CROP_ONION, 1] = 300; // stage 1
global.cropsImage[CROP_ONION, 0] = 30; //not watered
global.cropsImage[CROP_ONION, 1] = 42; // watered

global.cropsTimer[CROP_PUMPKIN, 0] = 300; //stage 0
global.cropsTimer[CROP_PUMPKIN, 1] = 300; // stage 1
global.cropsImage[CROP_PUMPKIN, 0] = 33; //not watered
global.cropsImage[CROP_PUMPKIN, 1] = 45; // watered

global.cropsTimer[CROP_STRAWBERRY, 0] = 300; //stage 0
global.cropsTimer[CROP_STRAWBERRY, 1] = 300; // stage 1
global.cropsImage[CROP_STRAWBERRY, 0] = 48; //not watered
global.cropsImage[CROP_STRAWBERRY, 1] = 60; // watered

global.cropsTimer[CROP_RADISH, 0] = 300; //stage 0
global.cropsTimer[CROP_RADISH, 1] = 300; // stage 1
global.cropsImage[CROP_RADISH, 0] = 51; //not watered
global.cropsImage[CROP_RADISH, 1] = 63; // watered

global.cropsTimer[CROP_CUCUMBER, 0] = 300; //stage 0
global.cropsTimer[CROP_CUCUMBER, 1] = 300; // stage 1
global.cropsImage[CROP_CUCUMBER, 0] = 54; //not watered
global.cropsImage[CROP_CUCUMBER, 1] = 66; // watered

global.cropsTimer[CROP_CAULIFLOWER, 0] = 300; //stage 0
global.cropsTimer[CROP_CAULIFLOWER, 1] = 300; // stage 1
global.cropsImage[CROP_CAULIFLOWER, 0] = 57; //not watered
global.cropsImage[CROP_CAULIFLOWER, 1] = 69; // watered

global.cropsTimer[CROP_CHILIPEPPER, 0] = 300; //stage 0
global.cropsTimer[CROP_CHILIPEPPER, 1] = 300; // stage 1
global.cropsImage[CROP_CHILIPEPPER, 0] = 72; //not watered
global.cropsImage[CROP_CHILIPEPPER, 1] = 84; // watered

global.cropsTimer[CROP_MUSHROOM, 0] = 300; //stage 0
global.cropsTimer[CROP_MUSHROOM, 1] = 300; // stage 1
global.cropsImage[CROP_MUSHROOM, 0] = 75; //not watered
global.cropsImage[CROP_MUSHROOM, 1] = 87; // watered

global.cropsTimer[CROP_SUGARCANE, 0] = 300; //stage 0
global.cropsTimer[CROP_SUGARCANE, 1] = 300; // stage 1
global.cropsImage[CROP_SUGARCANE, 0] = 78; //not watered
global.cropsImage[CROP_SUGARCANE, 1] = 90; // watered

global.cropsTimer[CROP_LEEK, 0] = 300; //stage 0
global.cropsTimer[CROP_LEEK, 1] = 300; // stage 1
global.cropsImage[CROP_LEEK, 0] = 81; //not watered
global.cropsImage[CROP_LEEK, 1] = 93; // watered

global.cropsTimer[CROP_WATERMELON, 0] = 300; //stage 0
global.cropsTimer[CROP_WATERMELON, 1] = 300; // stage 1
global.cropsImage[CROP_WATERMELON, 0] = 96; //not watered
global.cropsImage[CROP_WATERMELON, 1] = 108; // watered

global.cropsTimer[CROP_BROCCOLI, 0] = 300; //stage 0
global.cropsTimer[CROP_BROCCOLI, 1] = 300; // stage 1
global.cropsImage[CROP_BROCCOLI, 0] = 99; //not watered
global.cropsImage[CROP_BROCCOLI, 1] = 111; // watered

global.cropsTimer[CROP_WHEAT, 0] = 300; //stage 0
global.cropsTimer[CROP_WHEAT, 1] = 300; // stage 1
global.cropsImage[CROP_WHEAT, 0] = 102; //not watered
global.cropsImage[CROP_WHEAT, 1] = 114; // watered

global.cropsTimer[CROP_CABBAGE, 0] = 300; //stage 0
global.cropsTimer[CROP_CABBAGE, 1] = 300; // stage 1
global.cropsImage[CROP_CABBAGE, 0] = 105; //not watered
global.cropsImage[CROP_CABBAGE, 1] = 117; // watered

global.cropsTimer[CROP_LETTUCE, 0] = 300; //stage 0
global.cropsTimer[CROP_LETTUCE, 1] = 300; // stage 1
global.cropsImage[CROP_LETTUCE, 0] = 120; //not watered
global.cropsImage[CROP_LETTUCE, 1] = 132; // watered

global.cropsTimer[CROP_BELLPEPPER, 0] = 300; //stage 0
global.cropsTimer[CROP_BELLPEPPER, 1] = 300; // stage 1
global.cropsImage[CROP_BELLPEPPER, 0] = 123; //not watered
global.cropsImage[CROP_BELLPEPPER, 1] = 135; // watered

global.cropsTimer[CROP_PEANUT, 0] = 300; //stage 0
global.cropsTimer[CROP_PEANUT, 1] = 300; // stage 1
global.cropsImage[CROP_PEANUT, 0] = 126; //not watered
global.cropsImage[CROP_PEANUT, 1] = 138; // watered

global.cropsTimer[CROP_GRAPES, 0] = 300; //stage 0
global.cropsTimer[CROP_GRAPES, 1] = 300; // stage 1
global.cropsImage[CROP_GRAPES, 0] = 129; //not watered
global.cropsImage[CROP_GRAPES, 1] = 141; // watered

global.cropsTimer[CROP_JASMINE, 0] = 300; //stage 0
global.cropsTimer[CROP_JASMINE, 1] = 300; // stage 1
global.cropsImage[CROP_JASMINE, 0] = 144; //not watered
global.cropsImage[CROP_JASMINE, 1] = 147; // watered