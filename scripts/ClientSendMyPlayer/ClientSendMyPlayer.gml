/// @func ClientSendMyPlayer(player)
/// @param player

var myPlayer = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_UPDATE_PLAYER_POSITION);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.x);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.y);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.image_index);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.myRoom);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.stamina);
var packet = network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));

if(packet < 0) {
	if(!instance_exists(objError)) {
		var camX = camera_get_view_x(view_camera[0]);
		var camY = camera_get_view_y(view_camera[0]);
		var camWidth = camera_get_view_width(view_camera[0]);
		var camHeight = camera_get_view_height(view_camera[0]);
		instance_create_depth(camX + camWidth/2, camY + camHeight/2, -5000, objError);
	}
}