/*
	Everything that has a check for myPlayer are stuff that you only need to check for your 
	own player, not for the other players on the game
*/
#region X and Y variables to check
var xToCheck = 0;
var yToCheck = 0;
switch(facing) {
	case "up":
		xToCheck = x + 8;
		yToCheck = y - 8;
		break;
	case "down":
		xToCheck = x + 8;
		yToCheck = y + 24;
		break;
	case "left":
		xToCheck = x - 8;
		yToCheck = y + 8;
		break;
	case "right":
		xToCheck = x + 24
		yToCheck = y + 8;
		break;
}
#endregion

if (inventoryKey != -1) {
	itemSelected = inventoryKey;
}


if(openBagPressed) {
	state = states.inventory;
	if(myPlayer){
		bagSlotSelected = 0;
		bagItemClicked = -1;
		bagItemSelected = ds_grid_get(inventory, 0, bagSlotSelected);
	}
	openBagPressed = 0;
	exit;
}



// only enter the action state if it's actually equipped with a tool/seed, otherwise use this button to sell all items
if(useItemPressed) {
	if(global.itemTypes[? handItem] == "Crop" or global.itemTypes[? handItem] == "Misc") {
		scrSellAllItems();
	} else {
		state = states.action;
		scrActionState();
		exit;
	}
}

if(storeItemPressed) {
	var itemToEquip = ds_grid_get(inventory, 0, itemSelected);
	if(handItem != NO_ITEM) {
		var itemStored = scrAddItem(0, id, handItem, handQuantity);
		if(itemStored) {
			handItem = NO_ITEM;
			handQuantity = 0;
		}
	} else if(global.itemTypes[? itemToEquip] == "Crop" or global.itemTypes[? itemToEquip] == "Misc") {
		// If the equipped item is not a tool or seed, it will be equipped on the player hand
		var tempHand = handItem;
		var tempQuantity = handQuantity;
		handItem = itemToEquip;
		handQuantity = ds_grid_get(inventory, 1, itemSelected);
		ds_grid_set(inventory, 0, itemSelected, tempHand);
		ds_grid_set(inventory, 1, itemSelected, tempQuantity);
	}
	if(isHost) {
		ServerSendHand(playerSocket, handItem, handQuantity);
	} else {
		ClientSendHand(handItem, handQuantity);
	}
}

if(chatPressed) {
	state = states.chat;
	// clear the keyboard and make the | appear
	if(myPlayer) {
		io_clear();
		alarm[2] = 10;
	}
	exit;
}

// Only my player will check this, but I don't actually need to check for myPlayer
// because the changeItem key is not send to the server/client when I click it anyway
if(changeItemPressed){
	if(itemSelected < quickSlotSize-1) {
		itemSelected++;
	} else {
		itemSelected = 0;
	}
}

// Only my own player needs to check this
if(myPlayer) {
	var itemEquippedPrevious = itemEquipped;
	var itemToEquip = ds_grid_get(inventory, 0, itemSelected);
	if(global.itemTypes[? itemToEquip] == "Seed" or global.itemTypes[? itemToEquip] == "Tool" or global.itemTypes[? itemToEquip] == "None") {
		itemEquipped = itemToEquip;
	}
	
	if(itemEquippedPrevious != itemEquipped) {
		if(isHost) {
			ServerSendEquip(playerSocket, itemEquipped);
		} else {
			ClientSendEquip(itemEquipped);
		}
	}
	if(actionPressed) {
		if(global.itemTypes[? handItem] == "Crop" or global.itemTypes[? handItem] == "Misc") {
			scrSellItem();
		} else {
			
			// Objects to check
			var storeToCheck = noone;
			var woodToCheck = noone;
			var brokenRockToCheck = noone;
			var teleportToCheck = instance_position(xToCheck, yToCheck, objTeleport);;
			if(room == roomFarm) {
				storeToCheck = instance_position(xToCheck, yToCheck, objStore);
				woodToCheck = instance_position(xToCheck, yToCheck, objWood);
				brokenRockToCheck = instance_position(xToCheck, yToCheck, objBrokenRock);
			}
			
			show_debug_message("woodToCheck = " + string(woodToCheck));
			if(plotToHighlight != noone) {
				if (plotToHighlight.stage == 2 and handItem == NO_ITEM) {
					if(isHost){
						scriptGetItem(plotToHighlight.cropType, plotToHighlight, id);
					} else{
						// server will do this and tell the player
						ClientSendToolAction(plotToHighlight.cropID, GET_CROP);
					}
					exit;
				}
			} 
			if(storeToCheck != noone) {
				show_debug_message("Entered store to check");
				storeToCheck.open = true;
				state = states.store;
				// need this
				keyboard_clear(ord("A"));
				exit;
			}
			if(woodToCheck != noone and handItem == NO_ITEM) {
				show_debug_message("Entered add wood");
				if(isHost) {
					handItem = ITEM_WOOD;
					handQuantity = 1;
					//var addedItem = scrAddItem(0, id, ITEM_WOOD, 1);
					//if(addedItem) {
						ServerSendDelete(ITEM_WOOD, woodToCheck);
						with(woodToCheck) {
							instance_destroy();
						}
					//}
				} else {
					ClientSendToolAction(woodToCheck.myID, GET_WOOD);
				}
			}
			if(brokenRockToCheck != noone and handItem == NO_ITEM) {
				show_debug_message("Entered add rock");
				if(isHost) {
					handItem = ITEM_BROKEN_ROCK;
					handQuantity = 1;
					//var addedItem = scrAddItem(0, id, ITEM_BROKEN_ROCK, 1);
					//if(addedItem) {
						ServerSendDelete(ITEM_BROKEN_ROCK, brokenRockToCheck);
						with(brokenRockToCheck) {
							instance_destroy();
						}
					//}
				} else {
					ClientSendToolAction(brokenRockToCheck.myID, GET_ROCK);
				}
			}
			if(teleportToCheck != noone) {
				x = teleportToCheck.goX;
				y = teleportToCheck.goY;
				alarm[1] = 1;
				myRoom = teleportToCheck.roomID;
				global.roomID = myRoom;
				alarm[0] = 1;
				room_goto(teleportToCheck.teleport);
			}
		}
	}
}




// Only highlight the crop if the player looking at it is mine
// I don't need to see it highlighted when other players are looking at it
if(myPlayer) {
	var cropToCheck = noone;
	if(room == roomFarm) {
		cropToCheck = instance_position(xToCheck, yToCheck, objCrop);
	}
	if(plotToHighlight != noone) {
		plotToHighlight.highlight = false;
		plotToHighlight = noone;
	}
	if(cropToCheck != noone) {
		plotToHighlight = cropToCheck;
		plotToHighlight.highlight = true;
	}
}

scrMovement();

