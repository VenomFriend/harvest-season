if(useItemPressed) {
	var store = instance_find(objStore, 0);
	if(store != noone) {
		store.open = false;
		store.itemSelected = 0;
	}
	state = states.moving;
}