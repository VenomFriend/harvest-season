
// Sell those items
// You don't need to release the key to sell it, just click
// So it doesn't go to the animation state(there's no animation for now), it just sells it and go backs to the moving state

var hasSellBox = false;
switch(facing){
	case "up":
		image_index = 9;
		hasSellBox = place_meeting(x, y - 16, objSell);
		break;
	case "down":
		image_index = 0;
		hasSellBox = place_meeting(x, y + 16, objSell);
		break;
	case "left":
		image_index = 3;
		hasSellBox = place_meeting(x - 16, y, objSell);
		break;
	case "right":
		image_index = 6;
		hasSellBox = place_meeting(x + 16, y, objSell);
		break;
}
if(myPlayer and hasSellBox) {
	if(isHost) {
		//Show a little animation of the money
		showMoney = true;
		showMoneyValue = global.itemPricesSell[? handItem] * handQuantity;
		money += showMoneyValue;
		showMoneyY = 16;
		ServerSendShowMoney(playerSocket, showMoneyValue);
	} else {
		ClientSendSellItem(handItem, handQuantity);
	}
	handItem = NO_ITEM;
	handQuantity = 0;
	// send my updated inventory to the server
	if(!isHost) {
		ClientSendInventory(id);
	}
}
