/// @function ClientSendFishOnRod(fishOnRod)
/// @param fishOnRod

var fishOnRod = argument[0];

// Send the key press to the server
buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_FISH_ON_ROD);
buffer_write(global.bufferClientWrite, buffer_bool, fishOnRod);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));