/// @func scrAddChat(playerName, text)

var playerName = argument[0];
var text = argument[1];

var chat = instance_find(objChat, 0);
chat.messages[2, 0] = chat.messages[1, 0];
chat.messages[2, 1] = chat.messages[1, 1];
chat.messages[1, 0] = chat.messages[0, 0];
chat.messages[1, 1] = chat.messages[0, 1];

chat.messages[0, 0] = playerName;
chat.messages[0, 1] = text;
chat.show = true;
chat.alarm[0] = 10 * room_speed;