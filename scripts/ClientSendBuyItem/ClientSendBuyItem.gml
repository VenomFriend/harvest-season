/// @func ClientSendBuyItem(price)
/// @param price

var price = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_BUY);
buffer_write(global.bufferClientWrite, buffer_u16, price);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));