/// @func ServerSendPlayerToAll(socketOfPlayer, player);
/// @param socket
/// @param player

var playerSocket = argument[0];
var playerToSend = argument[1];
var socketSize = ds_list_size(global.socketList);

// Tell everyone that there's a new playe in town
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u8, 1); //only one player to send
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);
buffer_write(global.bufferServerWrite, buffer_u16, playerToSend.x);
buffer_write(global.bufferServerWrite, buffer_u16, playerToSend.y);
buffer_write(global.bufferServerWrite, buffer_u16, playerToSend.image_index);
buffer_write(global.bufferServerWrite, buffer_u8, playerToSend.itemEquipped);
buffer_write(global.bufferServerWrite, buffer_string, playerToSend.name);
buffer_write(global.bufferServerWrite, buffer_u16, serverPlayer.state);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	if(thisSocket != playerSocket) {
		network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
	}
}