/// @function ServerSendGeneric(objectType, object)
/// @param objectType
/// @param object

var objectType = argument[0];
var object = argument[1];

var socketSize = ds_list_size(global.socketList);

// Send the object to the players
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_OBJECT);
buffer_write(global.bufferServerWrite, buffer_u32, objectType);
buffer_write(global.bufferServerWrite, buffer_u32, object.id); // we will use his id to identify it
buffer_write(global.bufferServerWrite, buffer_u32, object.x);
buffer_write(global.bufferServerWrite, buffer_u32, object.y);
buffer_write(global.bufferServerWrite, buffer_u32, object.image_speed);
buffer_write(global.bufferServerWrite, buffer_u32, object.image_index);
for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}