with(objPlayer){
	instance_destroy();
}
with(objClient) {
	instance_destroy();
}
with(objCrop) {
	instance_destroy();
}
with(objClock){
	instance_destroy();
}
with(objChat) {
	instance_destroy();
}
with(objSmallRock){
	instance_destroy();
}
with(objSmallTree){
	instance_destroy();
}
with(objWood) {
	instance_destroy();
}
with(objBrokenRock){
	instance_destroy();
}