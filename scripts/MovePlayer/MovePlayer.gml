var playerToMove = argument[0];
var keyChanged = argument[1];

switch(keyChanged) {
	case KEY_UP_PRESSED:
		playerToMove.moveUp = 1;
		break;
	case KEY_UP_RELEASED:
		playerToMove.moveUp = 0;
		break;
	case KEY_DOWN_PRESSED:
		playerToMove.moveDown = 1;
		break;
	case KEY_DOWN_RELEASED:
		playerToMove.moveDown = 0;
		break;
	case KEY_LEFT_PRESSED:
		playerToMove.moveLeft = 1;
		break;
	case KEY_LEFT_RELEASED:
		playerToMove.moveLeft = 0;
		break;
	case KEY_RIGHT_PRESSED:
		playerToMove.moveRight = 1;
		break;
	case KEY_RIGHT_RELEASED:
		playerToMove.moveRight = 0;
		break;	
	case KEY_ITEM_PRESSED:
		playerToMove.useItemPressed = 1;
		break;
	case KEY_ITEM_RELEASED:
		playerToMove.useItemPressed = 0;
		playerToMove.useItemReleased = 1;
		break;
	case KEY_INVENTORY_OPEN:
		playerToMove.moveUp = 0;
		playerToMove.moveDown = 0;
		playerToMove.moveLeft = 0;
		playerToMove.moveRight = 0;
		playerToMove.openBagPressed = 1;
		break;
	case KEY_INVENTORY_CLOSE:
		playerToMove.moveUp = 0;
		playerToMove.moveDown = 0;
		playerToMove.moveLeft = 0;
		playerToMove.moveRight = 0;
		playerToMove.openBagPressed = 0;
		break
	case KEY_RUN_PRESSED:
		playerToMove.runPressed = 1;
		//playerToMove.isRunning = true;
		break;
	case KEY_RUN_RELEASED:
		playerToMove.runPressed = 0;
		playerToMove.runReleased = 1;
		//playerToMove.isRunning = false;
		break;
	case KEY_CHAT_PRESSED:
		playerToMove.chatPressed = 1;
		break;
}