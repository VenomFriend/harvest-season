if(myPlayer) {
	chatText = keyboard_string;
	var chat = instance_find(objChat, 0);
	chat.show = true;
}

if(chatPressed) {
	if(myPlayer){
		chat.alarm[0] = 10 * room_speed;
		if(chatText != "") {
			scrAddChat(name, chatText);
			if(isHost) {
				ServerSendChat(id, chatText);
			} else {
				ClientSendChat(chatText);
			}
		}
	}
	state = states.moving;
	chatPressed = 0;
	chatText = "";
}