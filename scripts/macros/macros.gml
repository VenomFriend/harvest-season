// Network Messages
#macro MSG_CREATE_ID 0
#macro MSG_DELETE_PLAYER 1
#macro MSG_CREATE_PLAYER 2
#macro MSG_KEY 3
#macro MSG_UPDATE_PLAYER_POSITION 4
#macro MSG_CROP 5
#macro MSG_TOOL_ACTION 6
#macro MSG_UPDATE_INVENTORY 7
#macro MSG_ITEM_EQUIPPED 8
#macro MSG_SELL 9
#macro MSG_SHOW_MONEY 10
#macro MSG_BUY 11
#macro MSG_NAME 12
#macro MSG_CHAT 13
#macro MSG_LOAD_PLAYER 14
#macro MSG_PLAYER_INVENTORY 15
#macro MSG_OBJECT 16
#macro MSG_OBJECT_DELETE 17
#macro MSG_HAND_EQUIPPED 18
#macro MSG_FISH_ON_ROD 19
#macro MSG_STATE 20
#macro MSG_CLOCK 21

// Keys Pressed
#macro KEY_UP_PRESSED 20
#macro KEY_DOWN_PRESSED 21
#macro KEY_LEFT_PRESSED 22
#macro KEY_RIGHT_PRESSED 23
#macro KEY_UP_RELEASED 24
#macro KEY_DOWN_RELEASED 25
#macro KEY_LEFT_RELEASED 26
#macro KEY_RIGHT_RELEASED 27
#macro KEY_RUN_PRESSED 28
#macro KEY_RUN_RELEASED 29
#macro KEY_ITEM_PRESSED 30
#macro KEY_ITEM_RELEASED 31
#macro KEY_INVENTORY_OPEN 32
#macro KEY_INVENTORY_CLOSE 33
#macro KEY_CHAT_PRESSED 34

// Tools
#macro NO_ITEM 0
#macro TOOL_HOE 1
#macro TOOL_WATERING_CAN 2
#macro TOOL_SICKLE 3
#macro TOOL_HAMMER 4
#macro TOOL_AXE 5
#macro TOOL_FISHING_ROD 6

// Seeds
#macro SEED_CORN 20
#macro SEED_TOMATO 21
#macro SEED_CARROT 22
#macro SEED_TURNIP 23
#macro SEED_POTATO 24
#macro SEED_EGGPLANT 25
#macro SEED_ONION 26
#macro SEED_PUMPKIN 27
#macro SEED_STRAWBERRY 28
#macro SEED_RADISH 29
#macro SEED_CUCUMBER 30
#macro SEED_CAULIFLOWER 31
#macro SEED_CHILIPEPPER 32
#macro SEED_MUSHROOM 33
#macro SEED_SUGARCANE 34
#macro SEED_LEEK 35
#macro SEED_WATERMELON 36
#macro SEED_BROCCOLI 37
#macro SEED_WHEAT 38
#macro SEED_CABBAGE 39
#macro SEED_LETTUCE 40
#macro SEED_BELLPEPPER 41
#macro SEED_PEANUT 42
#macro SEED_GRAPES 43
#macro SEED_JASMINE 44
#macro SEED_RANDOM 999


// Actions
#macro GET_CROP 49
#macro GET_WOOD 50
#macro GET_ROCK 51

// Crops(Crop = Seed + 60)

#macro CROP_CORN 80
#macro CROP_TOMATO 81
#macro CROP_CARROT 82
#macro CROP_TURNIP 83
#macro CROP_POTATO 84
#macro CROP_EGGPLANT 85
#macro CROP_ONION 86
#macro CROP_PUMPKIN 87
#macro CROP_STRAWBERRY 88
#macro CROP_RADISH 89
#macro CROP_CUCUMBER 90
#macro CROP_CAULIFLOWER 91
#macro CROP_CHILIPEPPER 92
#macro CROP_MUSHROOM 93
#macro CROP_SUGARCANE 94
#macro CROP_LEEK 95
#macro CROP_WATERMELON 96
#macro CROP_BROCCOLI 97
#macro CROP_WHEAT 98
#macro CROP_CABBAGE 99
#macro CROP_LETTUCE 100
#macro CROP_BELLPEPPER 101
#macro CROP_PEANUT 102
#macro CROP_GRAPES 103
#macro CROP_JASMINE 104


// Itens 

#macro ITEM_WOOD 200
#macro ITEM_TREE 201
#macro ITEM_ROCK 202
#macro ITEM_BROKEN_ROCK 203
#macro ITEM_FISH 204

// Rooms
#macro ROOM_FARM 0
#macro ROOM_HOUSE 1