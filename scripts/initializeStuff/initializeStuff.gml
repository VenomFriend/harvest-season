enum states {
	moving,
	action,
	animation,
	inventory,
	menu,
	store,
	chat,
	fishing
}

global.itemPricesSell = ds_map_create();
ds_map_add(global.itemPricesSell, CROP_CARROT, 120);
ds_map_add(global.itemPricesSell, CROP_CORN, 100);
ds_map_add(global.itemPricesSell, CROP_EGGPLANT, 80);
ds_map_add(global.itemPricesSell, CROP_ONION, 80);
ds_map_add(global.itemPricesSell, CROP_POTATO, 80);
ds_map_add(global.itemPricesSell, CROP_PUMPKIN, 250);
ds_map_add(global.itemPricesSell, CROP_TOMATO, 60);
ds_map_add(global.itemPricesSell, CROP_TURNIP, 60);

ds_map_add(global.itemPricesSell, CROP_STRAWBERRY, 375);
ds_map_add(global.itemPricesSell, CROP_RADISH, 75); 
ds_map_add(global.itemPricesSell, CROP_CUCUMBER, 55);
ds_map_add(global.itemPricesSell, CROP_CAULIFLOWER, 60); 
ds_map_add(global.itemPricesSell, CROP_CHILIPEPPER, 350);
ds_map_add(global.itemPricesSell, CROP_MUSHROOM, 120);
ds_map_add(global.itemPricesSell, CROP_SUGARCANE, 210);
ds_map_add(global.itemPricesSell, CROP_LEEK, 60);
ds_map_add(global.itemPricesSell, CROP_WATERMELON, 1000);
ds_map_add(global.itemPricesSell, CROP_BROCCOLI, 80);
ds_map_add(global.itemPricesSell, CROP_WHEAT, 210);
ds_map_add(global.itemPricesSell, CROP_CABBAGE, 225);
ds_map_add(global.itemPricesSell, CROP_LETTUCE, 60);
ds_map_add(global.itemPricesSell, CROP_BELLPEPPER, 60);
ds_map_add(global.itemPricesSell, CROP_PEANUT, 60);
ds_map_add(global.itemPricesSell, CROP_GRAPES, 165);
ds_map_add(global.itemPricesSell, CROP_JASMINE, 60);

ds_map_add(global.itemPricesSell, ITEM_FISH, 200);
ds_map_add(global.itemPricesSell, ITEM_WOOD, 10);
ds_map_add(global.itemPricesSell, ITEM_BROKEN_ROCK, 10);

global.itemPricesBuy = ds_map_create();
ds_map_add(global.itemPricesBuy, SEED_CARROT, 30);
ds_map_add(global.itemPricesBuy, SEED_CORN, 30);
ds_map_add(global.itemPricesBuy, SEED_EGGPLANT, 12);
ds_map_add(global.itemPricesBuy, SEED_ONION, 15);
ds_map_add(global.itemPricesBuy, SEED_POTATO, 15);
ds_map_add(global.itemPricesBuy, SEED_PUMPKIN, 50);
ds_map_add(global.itemPricesBuy, SEED_TOMATO, 20);
ds_map_add(global.itemPricesBuy, SEED_TURNIP, 12);

ds_map_add(global.itemPricesBuy, SEED_STRAWBERRY, 100);
ds_map_add(global.itemPricesBuy, SEED_RADISH, 25); 
ds_map_add(global.itemPricesBuy, SEED_CUCUMBER, 20);
ds_map_add(global.itemPricesBuy, SEED_CAULIFLOWER, 20);
ds_map_add(global.itemPricesBuy, SEED_CHILIPEPPER, 100);
ds_map_add(global.itemPricesBuy, SEED_MUSHROOM, 40);
ds_map_add(global.itemPricesBuy, SEED_SUGARCANE, 80);
ds_map_add(global.itemPricesBuy, SEED_LEEK, 20);
ds_map_add(global.itemPricesBuy, SEED_WATERMELON, 300);
ds_map_add(global.itemPricesBuy, SEED_BROCCOLI, 20);
ds_map_add(global.itemPricesBuy, SEED_WHEAT, 50);
ds_map_add(global.itemPricesBuy, SEED_CABBAGE, 60);
ds_map_add(global.itemPricesBuy, SEED_LETTUCE, 15);
ds_map_add(global.itemPricesBuy, SEED_BELLPEPPER, 15);
ds_map_add(global.itemPricesBuy, SEED_PEANUT, 15);
ds_map_add(global.itemPricesBuy, SEED_GRAPES, 40);
ds_map_add(global.itemPricesBuy, SEED_JASMINE, 15);

global.itemNames = ds_map_create();
ds_map_add(global.itemNames, SEED_CARROT, "Carrot Seeds");
ds_map_add(global.itemNames, SEED_CORN, "Corn Seeds");
ds_map_add(global.itemNames, SEED_EGGPLANT, "Eggplant Seeds");
ds_map_add(global.itemNames, SEED_ONION, "Onion Seeds");
ds_map_add(global.itemNames, SEED_POTATO, "Potato Seeds");
ds_map_add(global.itemNames, SEED_PUMPKIN, "Pumpkin Seeds");
ds_map_add(global.itemNames, SEED_TOMATO, "Tomato Seeds");
ds_map_add(global.itemNames, SEED_TURNIP, "Turnip Seeds");

ds_map_add(global.itemNames, SEED_STRAWBERRY, "Strawberry Seeds");
ds_map_add(global.itemNames, SEED_RADISH, "Radish Seeds");
ds_map_add(global.itemNames, SEED_CUCUMBER, "Cucumber Seeds");
ds_map_add(global.itemNames, SEED_CAULIFLOWER, "Cauliflower Seeds");
ds_map_add(global.itemNames, SEED_CHILIPEPPER, "Chilipepper Seeds");
ds_map_add(global.itemNames, SEED_MUSHROOM, "Mushroom Seeds");
ds_map_add(global.itemNames, SEED_SUGARCANE, "Sugarcane Seeds");
ds_map_add(global.itemNames, SEED_LEEK, "Leek Seeds");
ds_map_add(global.itemNames, SEED_WATERMELON, "Watermelon Seeds");
ds_map_add(global.itemNames, SEED_BROCCOLI, "Broccoli Seeds");
ds_map_add(global.itemNames, SEED_WHEAT, "Wheat Seeds");
ds_map_add(global.itemNames, SEED_CABBAGE, "Cabbage Seeds");
ds_map_add(global.itemNames, SEED_LETTUCE, "Lettuce Seeds");
ds_map_add(global.itemNames, SEED_BELLPEPPER, "Belpepper Seeds");
ds_map_add(global.itemNames, SEED_PEANUT, "Peanut Seeds");
ds_map_add(global.itemNames, SEED_GRAPES, "Grapes Seeds");
ds_map_add(global.itemNames, SEED_JASMINE, "Jasmine Seeds");


global.itemTypes = ds_map_create();
ds_map_add(global.itemTypes, SEED_CARROT, "Seed");
ds_map_add(global.itemTypes, SEED_CORN, "Seed");
ds_map_add(global.itemTypes, SEED_EGGPLANT, "Seed");
ds_map_add(global.itemTypes, SEED_ONION, "Seed");
ds_map_add(global.itemTypes, SEED_POTATO, "Seed");
ds_map_add(global.itemTypes, SEED_PUMPKIN, "Seed");
ds_map_add(global.itemTypes, SEED_TOMATO, "Seed");
ds_map_add(global.itemTypes, SEED_TURNIP, "Seed");
ds_map_add(global.itemTypes, SEED_STRAWBERRY, "Seed");
ds_map_add(global.itemTypes, SEED_RADISH, "Seed");
ds_map_add(global.itemTypes, SEED_CUCUMBER, "Seed");
ds_map_add(global.itemTypes, SEED_CAULIFLOWER, "Seed");
ds_map_add(global.itemTypes, SEED_CHILIPEPPER, "Seed");
ds_map_add(global.itemTypes, SEED_MUSHROOM, "Seed");
ds_map_add(global.itemTypes, SEED_SUGARCANE, "Seed");
ds_map_add(global.itemTypes, SEED_LEEK, "Seed");
ds_map_add(global.itemTypes, SEED_WATERMELON, "Seed");
ds_map_add(global.itemTypes, SEED_BROCCOLI, "Seed");
ds_map_add(global.itemTypes, SEED_WHEAT, "Seed");
ds_map_add(global.itemTypes, SEED_CABBAGE, "Seed");
ds_map_add(global.itemTypes, SEED_LETTUCE, "Seed");
ds_map_add(global.itemTypes, SEED_BELLPEPPER, "Seed");
ds_map_add(global.itemTypes, SEED_PEANUT, "Seed");
ds_map_add(global.itemTypes, SEED_GRAPES, "Seed");
ds_map_add(global.itemTypes, SEED_JASMINE, "Seed");

ds_map_add(global.itemTypes, NO_ITEM, "None");

ds_map_add(global.itemTypes, TOOL_HOE, "Tool");
ds_map_add(global.itemTypes, TOOL_WATERING_CAN, "Tool");
ds_map_add(global.itemTypes, TOOL_SICKLE, "Tool");
ds_map_add(global.itemTypes, TOOL_HAMMER, "Tool");
ds_map_add(global.itemTypes, TOOL_AXE, "Tool");
ds_map_add(global.itemTypes, TOOL_FISHING_ROD, "Tool");

ds_map_add(global.itemTypes, CROP_CORN, "Crop");
ds_map_add(global.itemTypes, CROP_TOMATO, "Crop");
ds_map_add(global.itemTypes, CROP_CARROT, "Crop");
ds_map_add(global.itemTypes, CROP_TURNIP, "Crop");
ds_map_add(global.itemTypes, CROP_POTATO, "Crop");
ds_map_add(global.itemTypes, CROP_EGGPLANT,"Crop");
ds_map_add(global.itemTypes, CROP_ONION, "Crop");
ds_map_add(global.itemTypes, CROP_PUMPKIN, "Crop");
ds_map_add(global.itemTypes, CROP_STRAWBERRY, "Crop");
ds_map_add(global.itemTypes, CROP_RADISH, "Crop");
ds_map_add(global.itemTypes, CROP_CUCUMBER, "Crop");
ds_map_add(global.itemTypes, CROP_CAULIFLOWER, "Crop");
ds_map_add(global.itemTypes, CROP_CHILIPEPPER, "Crop");
ds_map_add(global.itemTypes, CROP_MUSHROOM, "Crop");
ds_map_add(global.itemTypes, CROP_SUGARCANE, "Crop");
ds_map_add(global.itemTypes, CROP_LEEK, "Crop");
ds_map_add(global.itemTypes, CROP_WATERMELON, "Crop");
ds_map_add(global.itemTypes, CROP_BROCCOLI, "Crop");
ds_map_add(global.itemTypes, CROP_WHEAT, "Crop");
ds_map_add(global.itemTypes, CROP_CABBAGE, "Crop");
ds_map_add(global.itemTypes, CROP_LETTUCE, "Crop");
ds_map_add(global.itemTypes, CROP_BELLPEPPER, "Crop");
ds_map_add(global.itemTypes, CROP_PEANUT, "Crop");
ds_map_add(global.itemTypes, CROP_GRAPES, "Crop");
ds_map_add(global.itemTypes, CROP_JASMINE, "Crop");

ds_map_add(global.itemTypes, ITEM_WOOD, "Misc");
ds_map_add(global.itemTypes, ITEM_BROKEN_ROCK, "Misc");
ds_map_add(global.itemTypes, ITEM_FISH, "Misc");

// all tool images:

// Small sprites
global.itemSprites = ds_map_create();
ds_map_add(global.itemSprites, NO_ITEM, 0);
ds_map_add(global.itemSprites, TOOL_HOE, 1);
ds_map_add(global.itemSprites, TOOL_WATERING_CAN, 32);
ds_map_add(global.itemSprites, TOOL_SICKLE, 81);
ds_map_add(global.itemSprites, TOOL_HAMMER, 49);
ds_map_add(global.itemSprites, TOOL_AXE, 33);
ds_map_add(global.itemSprites, TOOL_FISHING_ROD, 64);

ds_map_add(global.itemSprites, SEED_RANDOM, 152);

ds_map_add(global.itemSprites, SEED_CARROT, 154);
ds_map_add(global.itemSprites, SEED_CORN, 152);
ds_map_add(global.itemSprites, SEED_EGGPLANT, 157);
ds_map_add(global.itemSprites, SEED_ONION, 158);
ds_map_add(global.itemSprites, SEED_POTATO, 156);
ds_map_add(global.itemSprites, SEED_PUMPKIN, 159);
ds_map_add(global.itemSprites, SEED_TOMATO, 153);
ds_map_add(global.itemSprites, SEED_TURNIP, 155);

ds_map_add(global.itemSprites, SEED_STRAWBERRY, 168);
ds_map_add(global.itemSprites, SEED_RADISH, 170);
ds_map_add(global.itemSprites, SEED_CUCUMBER, 172);
ds_map_add(global.itemSprites, SEED_CAULIFLOWER, 173);
ds_map_add(global.itemSprites, SEED_CHILIPEPPER, 175);
ds_map_add(global.itemSprites, SEED_MUSHROOM, 184);
ds_map_add(global.itemSprites, SEED_SUGARCANE, 190);
ds_map_add(global.itemSprites, SEED_LEEK, 185);
ds_map_add(global.itemSprites, SEED_WATERMELON, 187);
ds_map_add(global.itemSprites, SEED_BROCCOLI, 189);
ds_map_add(global.itemSprites, SEED_WHEAT, 191);
ds_map_add(global.itemSprites, SEED_CABBAGE, 200);
ds_map_add(global.itemSprites, SEED_LETTUCE, 201);
ds_map_add(global.itemSprites, SEED_BELLPEPPER, 202);
ds_map_add(global.itemSprites, SEED_PEANUT, 203);
ds_map_add(global.itemSprites, SEED_GRAPES, 204);
ds_map_add(global.itemSprites, SEED_JASMINE, 207);

ds_map_add(global.itemSprites, CROP_CORN, 8);
ds_map_add(global.itemSprites, CROP_TOMATO, 9);
ds_map_add(global.itemSprites, CROP_CARROT, 10);
ds_map_add(global.itemSprites, CROP_TURNIP, 11);
ds_map_add(global.itemSprites, CROP_POTATO, 12);
ds_map_add(global.itemSprites, CROP_EGGPLANT, 24);
ds_map_add(global.itemSprites, CROP_ONION, 25);
ds_map_add(global.itemSprites, CROP_PUMPKIN, 26);

ds_map_add(global.itemSprites, CROP_STRAWBERRY, 27);
ds_map_add(global.itemSprites, CROP_RADISH, 29);
ds_map_add(global.itemSprites, CROP_CUCUMBER, 41);
ds_map_add(global.itemSprites, CROP_CAULIFLOWER, 42);
ds_map_add(global.itemSprites, CROP_CHILIPEPPER, 44);
ds_map_add(global.itemSprites, CROP_MUSHROOM, 45);
ds_map_add(global.itemSprites, CROP_SUGARCANE, 46);
ds_map_add(global.itemSprites, CROP_LEEK, 56);
ds_map_add(global.itemSprites, CROP_WATERMELON, 58);
ds_map_add(global.itemSprites, CROP_BROCCOLI, 60);
ds_map_add(global.itemSprites, CROP_WHEAT, 72);
ds_map_add(global.itemSprites, CROP_CABBAGE, 73);
ds_map_add(global.itemSprites, CROP_LETTUCE, 74);
ds_map_add(global.itemSprites, CROP_BELLPEPPER, 75);
ds_map_add(global.itemSprites, CROP_PEANUT, 76);
ds_map_add(global.itemSprites, CROP_GRAPES, 77);
ds_map_add(global.itemSprites, CROP_JASMINE, 94);

ds_map_add(global.itemSprites, ITEM_WOOD, 132);
ds_map_add(global.itemSprites, ITEM_BROKEN_ROCK, 125);
ds_map_add(global.itemSprites, ITEM_FISH, 141);



// Big sprites
global.itemEquipSprites = ds_map_create();
ds_map_add(global.itemEquipSprites, TOOL_HOE, 0);
ds_map_add(global.itemEquipSprites, TOOL_WATERING_CAN, 1);
ds_map_add(global.itemEquipSprites, TOOL_SICKLE, 11);
ds_map_add(global.itemEquipSprites, TOOL_HAMMER, 9);
ds_map_add(global.itemEquipSprites, TOOL_AXE, 8);
ds_map_add(global.itemEquipSprites, TOOL_FISHING_ROD, 14);

ds_map_add(global.itemEquipSprites, SEED_RANDOM, 2);

ds_map_add(global.itemEquipSprites, SEED_CARROT, 4);
ds_map_add(global.itemEquipSprites, SEED_CORN, 2);
ds_map_add(global.itemEquipSprites, SEED_EGGPLANT, 35);
ds_map_add(global.itemEquipSprites, SEED_ONION, 36);
ds_map_add(global.itemEquipSprites, SEED_POTATO, 6);
ds_map_add(global.itemEquipSprites, SEED_PUMPKIN, 37);
ds_map_add(global.itemEquipSprites, SEED_TOMATO, 3);
ds_map_add(global.itemEquipSprites, SEED_TURNIP, 5);

ds_map_add(global.itemEquipSprites, SEED_STRAWBERRY, 38);
ds_map_add(global.itemEquipSprites, SEED_RADISH, 40);
ds_map_add(global.itemEquipSprites, SEED_CUCUMBER, 42);
ds_map_add(global.itemEquipSprites, SEED_CAULIFLOWER, 43);
ds_map_add(global.itemEquipSprites, SEED_CHILIPEPPER, 45);
ds_map_add(global.itemEquipSprites, SEED_MUSHROOM, 46);
ds_map_add(global.itemEquipSprites, SEED_SUGARCANE, 52);
ds_map_add(global.itemEquipSprites, SEED_LEEK, 47);
ds_map_add(global.itemEquipSprites, SEED_WATERMELON, 49);
ds_map_add(global.itemEquipSprites, SEED_BROCCOLI, 51);
ds_map_add(global.itemEquipSprites, SEED_WHEAT, 53);
ds_map_add(global.itemEquipSprites, SEED_CABBAGE, 54);
ds_map_add(global.itemEquipSprites, SEED_LETTUCE, 55);
ds_map_add(global.itemEquipSprites, SEED_BELLPEPPER, 56);
ds_map_add(global.itemEquipSprites, SEED_PEANUT, 57);
ds_map_add(global.itemEquipSprites, SEED_GRAPES, 58);
ds_map_add(global.itemEquipSprites, SEED_JASMINE, 61);



ds_map_add(global.itemEquipSprites, CROP_CORN, 21);
ds_map_add(global.itemEquipSprites, CROP_TOMATO, 22);
ds_map_add(global.itemEquipSprites, CROP_CARROT, 23);
ds_map_add(global.itemEquipSprites, CROP_TURNIP, 24);
ds_map_add(global.itemEquipSprites, CROP_POTATO, 25);
ds_map_add(global.itemEquipSprites, CROP_EGGPLANT, 29);
ds_map_add(global.itemEquipSprites, CROP_ONION, 30)
ds_map_add(global.itemEquipSprites, CROP_PUMPKIN, 31);

ds_map_add(global.itemEquipSprites, CROP_STRAWBERRY, 32);
ds_map_add(global.itemEquipSprites, CROP_RADISH, 34);
ds_map_add(global.itemEquipSprites, CROP_CUCUMBER, 65);
ds_map_add(global.itemEquipSprites, CROP_CAULIFLOWER, 66);
ds_map_add(global.itemEquipSprites, CROP_CHILIPEPPER, 68);
ds_map_add(global.itemEquipSprites, CROP_MUSHROOM, 69);
ds_map_add(global.itemEquipSprites, CROP_SUGARCANE, 70);
ds_map_add(global.itemEquipSprites, CROP_LEEK, 72);
ds_map_add(global.itemEquipSprites, CROP_WATERMELON, 74);
ds_map_add(global.itemEquipSprites, CROP_BROCCOLI, 76);
ds_map_add(global.itemEquipSprites, CROP_WHEAT, 80);
ds_map_add(global.itemEquipSprites, CROP_CABBAGE, 81);
ds_map_add(global.itemEquipSprites, CROP_LETTUCE, 82);
ds_map_add(global.itemEquipSprites, CROP_BELLPEPPER, 83);
ds_map_add(global.itemEquipSprites, CROP_PEANUT, 84);
ds_map_add(global.itemEquipSprites, CROP_GRAPES, 85);
ds_map_add(global.itemEquipSprites, CROP_JASMINE, 94);

ds_map_add(global.itemEquipSprites, ITEM_WOOD, 104);
ds_map_add(global.itemEquipSprites, ITEM_BROKEN_ROCK, 117);
ds_map_add(global.itemEquipSprites, ITEM_FISH, 118);