if(openBagPressed) {
	state = states.moving;
	exit;
}

// Only my own player will move his inventory
if(myPlayer) {
	if(moveUpPressed) {
		if(bagSlotSelected - bagWidth < 0) {
			bagSlotSelected = inventorySize - (bagWidth - bagSlotSelected);
		} else {
			bagSlotSelected -= bagWidth;
		}
	} else if(moveDownPressed) {
		if(bagSlotSelected + 8 >= inventorySize) {
			bagSlotSelected = (bagSlotSelected + 8) - inventorySize;
		} else {
			bagSlotSelected += bagWidth;
		}
	} else if(moveLeftPressed) {
		if(bagSlotSelected mod bagWidth == 0) {
			bagSlotSelected += 7;
		} else {
			bagSlotSelected--;
		}
	} else if(moveRightPressed) {
		if(bagSlotSelected mod bagWidth == 7) {
			bagSlotSelected -= 7;
		} else {
			bagSlotSelected++;
		}
	} else if(actionPressed){
		if(bagItemClicked == -1) {
			bagItemClicked = bagSlotSelected;
		} else if(bagItemClicked == bagSlotSelected) {
			bagItemClicked = -1;
		} else {
			var item1ToChange = ds_grid_get(inventory, 0, bagSlotSelected);
			var item1Quantity = ds_grid_get(inventory, 1, bagSlotSelected);
			var item2ToChange = ds_grid_get(inventory, 0, bagItemClicked);
			var item2Quantity = ds_grid_get(inventory, 1, bagItemClicked);
			ds_grid_set(inventory, 0, bagSlotSelected, item2ToChange);
			ds_grid_set(inventory, 1, bagSlotSelected, item2Quantity);
			ds_grid_set(inventory, 0, bagItemClicked, item1ToChange);
			ds_grid_set(inventory, 1, bagItemClicked, item1Quantity);
			bagItemClicked = -1;	
			// send the updated inventory to the server
			if(!isHost) {
				ClientSendInventory(inventory);
			}		
		}
	}
}