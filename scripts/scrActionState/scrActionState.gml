if(useItemPressed and !usingItem) {
	switch(itemEquipped){
		case NO_ITEM:
			state = states.moving;
			exit;
			break;
		case TOOL_HOE:
			if(stamina >= 5) {
				stamina -= 5;
			} else {
				state = states.moving;
				outOfStamina = true;
				exit;
			}
			usingItem = true;
			sprite_index = sprPlayerMaleHoe;
			switch(facing){
				case "up":
					image_index = 4;
					break;
				case "down":
					image_index = 0;
					break;
				case "left":
					image_index = 8;
					break;
				case "right":
					image_index = 12;
					break;
			}
			break;
		case TOOL_WATERING_CAN:
			if(stamina >= 5) {
				stamina -= 5;
			} else {
				state = states.moving;
				outOfStamina = true;
				exit;
			}
			usingItem = true;
			sprite_index = sprPlayerMaleWateringCan;
			switch(facing){
				case "up":
					image_index = 4;
					break;
				case "down":
					image_index = 0;
					break;
				case "left":
					image_index = 8;
					break;
				case "right":
					image_index = 12;
					break;
			}
			break;
		case TOOL_SICKLE:
			if(stamina >= 5) {
				stamina -= 5;
			} else {
				state = states.moving;
				outOfStamina = true;
				exit;
			}
			usingItem = true;
			sprite_index = sprPlayerMaleSickle;
			switch(facing){
				case "up":
					image_index = 4;
					break;
				case "down":
					image_index = 0;
					break;
				case "left":
					image_index = 8;
					break;
				case "right":
					image_index = 12;
					break;
			}
			break;
		case TOOL_AXE:
			if(stamina >= 5) {
				stamina -= 5;
			} else {
				state = states.moving;
				outOfStamina = true;
				exit;
			}
			usingItem = true;
			sprite_index = sprPlayerMaleAxe;
			switch(facing){
				case "up":
					image_index = 4;
					break;
				case "down":
					image_index = 0;
					break;
				case "left":
					image_index = 8;
					break;
				case "right":
					image_index = 12;
					break;
			}
			break;
		case TOOL_HAMMER:
			if(stamina >= 5) {
				stamina -= 5;
			} else {
				state = states.moving;
				outOfStamina = true;
				exit;
			}
			usingItem = true;
			sprite_index = sprPlayerMaleHammer;
			switch(facing){
				case "up":
					image_index = 4;
					break;
				case "down":
					image_index = 0;
					break;
				case "left":
					image_index = 8;
					break;
				case "right":
					image_index = 12;
					break;
			}
			break;
		case TOOL_FISHING_ROD:
			if(stamina >= 5) {
				stamina -= 5;
			} else {
				state = states.moving;
				outOfStamina = true;
				exit;
			}
			usingItem = true;
			sprite_index = sprPlayerMaleFishing;
			switch(facing){
				case "up":
					image_index = 4;
					break;
				case "down":
					image_index = 0;
					break;
				case "left":
					image_index = 8;
					break;
				case "right":
					image_index = 12;
					break;
			}
			break;
		default:
			if(global.itemTypes[? itemEquipped] == "Seed") {
				if(stamina >= 5) {
					stamina -= 5;
				} else {
					state = states.moving;
					outOfStamina = true;
					exit;
				}
				usingItem = true;
				sprite_index = sprPlayerMaleWalking;
				switch(facing){
					case "up":
						image_index = 13;
						break;
					case "down":
						image_index = 1;
						break;
					case "left":
						image_index = 5;
						break;
					case "right":
						image_index = 9;
						break;
				}
			}
			break;
	}
	useItemPressed = 0;
}
if(useItemReleased) {
	// Play sound
	switch(itemEquipped){
		case TOOL_WATERING_CAN:
			if(waterLevel > 0) {
				audio_play_sound_on(emitterID,soundWater,false,1);
			}
			//audio_play_sound_at(soundWater, x, y, 0, 100, 300, 1, false, 1);
			break;
		case TOOL_HOE:
			audio_play_sound_on(emitterID,soundDig,false,1);
			//audio_play_sound_at(soundDig, x, y, 0, 100, 300, 1, false, 1);
			break;
		case TOOL_AXE:
			audio_play_sound_on(emitterID,soundWoodChop,false,1);
			break;
		case TOOL_SICKLE:
			audio_play_sound_on(emitterID,soundSickleCut,false,1);
			break;
		case TOOL_FISHING_ROD:
			audio_play_sound_on(emitterID,soundFishingRod,false,1);
			break;
		case TOOL_HAMMER:
			audio_play_sound_on(emitterID,soundRockBreak,false,1);
			break;
	}
	state = states.animation;
	useItemReleased = 0;
}