/// @func ServerSendCropUpdate(crop)
/// @param crop

var crop = argument[0];
var socketSize = ds_list_size(global.socketList);

// Send the crop to everyone
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CROP);
buffer_write(global.bufferServerWrite, buffer_u32, crop.id); // we will use his id to identify it
buffer_write(global.bufferServerWrite, buffer_u32, crop.x);
buffer_write(global.bufferServerWrite, buffer_u32, crop.y);
buffer_write(global.bufferServerWrite, buffer_u32, crop.sprite_index);
buffer_write(global.bufferServerWrite, buffer_u32, crop.image_index);
buffer_write(global.bufferServerWrite, buffer_u8, crop.stage);
buffer_write(global.bufferServerWrite, buffer_bool, crop.isDig);
buffer_write(global.bufferServerWrite, buffer_bool, crop.isGrass);
buffer_write(global.bufferServerWrite, buffer_u8, cropType);

for(var i = 0; i<socketSize; i++) {
	var thisSocket = ds_list_find_value(global.socketList, i);
	network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}