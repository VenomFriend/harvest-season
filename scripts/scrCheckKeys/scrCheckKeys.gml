if myPlayer {
	moveUpPressed = keyboard_check_pressed(vk_up);
	moveDownPressed = keyboard_check_pressed(vk_down);
	moveLeftPressed = keyboard_check_pressed(vk_left);
	moveRightPressed = keyboard_check_pressed(vk_right);

	moveUpReleased = keyboard_check_released(vk_up);
	moveDownReleased = keyboard_check_released(vk_down);
	moveLeftReleased = keyboard_check_released(vk_left);
	moveRightReleased = keyboard_check_released(vk_right);

	moveUp = keyboard_check(vk_up);
	moveDown = keyboard_check(vk_down);
	moveLeft = keyboard_check(vk_left);
	moveRight = keyboard_check(vk_right);
	
	useItemPressed = keyboard_check_pressed(ord("Z"));
	useItemReleased = keyboard_check_released(ord("Z"));
	
	actionPressed = keyboard_check_pressed(ord("A"));
	storeItemPressed = keyboard_check_pressed(ord("S"));
	
	runPressed = keyboard_check_pressed(vk_shift);
	runReleased = keyboard_check_released(vk_shift);
	
	changeItemPressed = keyboard_check_pressed(ord("X"));
	openBagPressed = keyboard_check_pressed(ord("I"));
	chatPressed = keyboard_check_pressed(vk_enter);
	
	var key1Pressed = keyboard_check_pressed(ord("1"));
	var key2Pressed = keyboard_check_pressed(ord("2"));
	var key3Pressed = keyboard_check_pressed(ord("3"));
	var key4Pressed = keyboard_check_pressed(ord("4"));
	var key5Pressed = keyboard_check_pressed(ord("5"));
	var key6Pressed = keyboard_check_pressed(ord("6"));
	var key7Pressed = keyboard_check_pressed(ord("7"));
	var key8Pressed = keyboard_check_pressed(ord("8"));
	
	if(key1Pressed) {
		inventoryKey = 0;
	} else if (key2Pressed) {
		inventoryKey = 1;
	} else if (key3Pressed) {
		inventoryKey = 2;
	} else if (key4Pressed) {
		inventoryKey = 3;
	} else if (key5Pressed) {
		inventoryKey = 4;
	} else if (key6Pressed) {
		inventoryKey = 5;
	} else if (key7Pressed) {
		inventoryKey = 6;
	} else if (key8Pressed) {
		inventoryKey = 7;
	} else {
		inventoryKey = -1;
	}
	
	// Only need to send the keys if I'm moving or doing an action
	if(state != states.inventory and state!= states.store) {
		PlayerSendKey(id);
	}
	
}